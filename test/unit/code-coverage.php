<?php

$reportPath = "./../build/reports/coverage/clover.xml";
$requiredPercentage = $argv[1] ? (float)$argv[1] :  85;

$xml = simplexml_load_file((string)$reportPath);

function percentage($covered, $total)
{
    return round(
        100 * ($covered / $total),
        2
    );
}

if (!$xml instanceof \SimpleXMLElement
    || $xml->getName() !== 'coverage'
    || !$xml->project instanceof \SimpleXMLElement
    || $xml->project->getName() !== 'project'
    || !$xml->project->metrics instanceof \SimpleXMLElement
    || $xml->project->metrics->getName() !== 'metrics'
) {
    echo "Invalid xml report";
    exit(24);
}

$data = [];

foreach ($xml->project->metrics->attributes() as $atribute) {
    $data[$atribute->getName()] = (string)$atribute;
}

$metrics = [];

$metrics[] = [
    'total' => $data['elements'], 'covered' => $data['coveredelements']
];

foreach ($metrics as $metric) {
    if (percentage($metric['covered'], $metric['total']) < $requiredPercentage) {
//        Failing the build
        exit(50);
    }
}
exit(0);

