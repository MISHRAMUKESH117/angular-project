<?php

use PHPUnit\Framework\TestCase;

/**
 * Class AbstractUnitTest
 * @package base
 */
abstract class AbstractUnitTest extends TestCase
{
    protected $sut;

    /**
     * PHPUnit setup method. We should add supporting functionality here instead
     * of overriding setUp in each test class
     */
    final public function setUp(): void
    {
        $this->onBeforeSetUp();
        $this->setSut();
        $this->onAfterSetUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->cleanServerGlobalVar();
    }

    /**
     * First method called on setUP()
     */
    protected function onBeforeSetUp()
    {
    }


    /**
     * Override in Test class to setup and assign SUT
     */
    abstract protected function setSut();

    /**
     * Last method called on setUp()
     */
    protected function onAfterSetUp()
    {
    }

    protected function cleanServerGlobalVar()
    {
        unset($_SERVER['HTTP_Authorization']);
        unset($_SERVER['HTTP_AUTHORIZATION']); // just incase it cares about case!
        unset($_SERVER['REQUEST_METHOD']);
        unset($_SERVER['REQUEST_SCHEME']);
        unset($_SERVER['REQUEST_URI']);
        unset($_SERVER['HTTP_X_REQUEST_AUTH']);
        unset($_SERVER['SERVER_NAME']);
        unset($_SERVER['SERVER_PORT']);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object $object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     * @throws ReflectionException
     */
    public function invokeProtectedPrivateMethod($object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * A bit hacky but allows to set the value of a mocked objects inaccessible property
     *
     * @param $object
     * @param $propertyName
     * @param $value
     *
     * @throws ReflectionException
     */
    public function mockProtectedPrivateProperty($object, $propertyName, $value)
    {
        $reflection = new \ReflectionClass($object);
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);
        $property->setValue($object, $value);
        $property->setAccessible(false);
    }

    protected function assertionsInMethodCalls()
    {
        $this->assertTrue(true);
    }
}


