<?php


use PHPUnit\Framework\TestCase;

class Swl4Test extends AbstractUnitTest
{
    public function testGetPath()
    {
        // create the observer
        $mock = $this->createMock(\process\Session::class);

        $stack = [];
        $this->assertSame(0, count($stack));

        array_push($stack, 'foo');
        $this->assertSame('foo', $stack[count($stack)-1]);
        $this->assertSame(1, count($stack));

        $this->assertSame('foo', array_pop($stack));
        $this->assertSame(0, count($stack));
    }

//    public function testReadConfigJs()
//    {
//        $this->assertTrue(1);
//    }

    /**
     * @inheritDoc
     */
    protected function setSut()
    {

    }
}
