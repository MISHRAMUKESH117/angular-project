<?php


use process\Build;

class BuildTest extends AbstractUnitTest
{
    /**
     * Override in Test class to setup and assign SUT
     */
    protected function setSut()
    {
        $this->sut = new Build();
    }

    public function testCreateWithFalseToolParameter()
    {
        $data = $this->sut->create(false);
        $this->assertIsArray($data);
        $this->assertTrue(in_array('workflow', array_column($data, 'app')));
        $this->assertTrue(in_array('peerreview', array_column($data, 'app')));
    }

    public function testCreateWithNonExistingToolParameter()
    {
        $data = $this->sut->create(true);
        $this->assertCount(0, $data);
    }

    public function testCreateWithWorkflowToolParameter()
    {
        $data = $this->sut->create('workflow');
        $this->assertIsArray($data);
        $this->assertCount(1, $data);
        $this->assertTrue(in_array('workflow', array_column($data, 'app')));
        $this->assertFalse(in_array('peerreview', array_column($data, 'app')));
    }
}
