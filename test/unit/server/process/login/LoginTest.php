<?php

namespace {
    // This allow us to configure the behavior of the "global mock"
    $mockSocketCreate = false;
}

namespace process\login {

    function shell_exec()
    {
        global $mockSocketCreate;
        if (isset($mockSocketCreate) && $mockSocketCreate === true) {
            return 'validUser';
        } else {
            return call_user_func_array('\shell_exec', func_get_args());
        }
    }

    use process\db\Core;
    use process\Session;

    class LoginTest extends \AbstractUnitTest
    {
        /**
         * @var \PHPUnit\Framework\MockObject\MockObject|LoginTest
         */
        public static $functions;
        /**
         * @var \process\login\Login
         */
        protected $sut;

        /**
         * @var \PHPUnit\Framework\MockObject\MockObject|Session
         */
        private $session;
        /**
         * @var \PHPUnit\Framework\MockObject\MockObject|Core
         */
        private $db;

        public function testRemoteAccessWithAccessDisabled()
        {
            $expected = [
                'success' => false
            ];

            $this->db->expects($this->any())->method('conf')
                ->with(
                    $this->stringContains('Common'),
                    $this->stringContains('AgfaAccessEnabled'),
                    $this->stringContains('bool')
                )->will($this->returnValue(false));

            $actual = $this->sut->remoteAccess('inValiduser', 'inValidPassword');
            $this->assertSame($expected, $actual);
        }

        public function testRemoteAccessWithAccessEnabled()
        {
            $expected = [
                'success' => true
            ];

            $this->db->expects($this->exactly(2))->method('conf')
                ->withConsecutive(
                    [
                        $this->stringContains('Common'),
                        $this->stringContains('AgfaAccessEnabled'),
                        $this->stringContains('bool')
                    ],
                    [
                        $this->stringContains('Common'),
                        $this->stringContains('AgfaAccessKey')
                    ]
                )->willReturnOnConsecutiveCalls(true, 'someDummyAccessKey==');

//            self::$functions->expects($this->once())
//                ->method('shell_exec')
//                ->will($this->returnValue('validPassword'));

            $this->session->expects($this->once())->method('isAuthenticated')
                ->will($this->returnValue(true));

            $this->session->expects($this->once())->method('getUserLogin')
                ->will($this->returnValue('validUser'));

            $this->session->expects($this->once())->method('getUserPass')
                ->will($this->returnValue('validUser'));

            // mocking shell_exec for selected test case using getFunctionsMock()
//        $mock = $this->getgetFunctionMock(__NAMESPACE__, 'shell_exec');
//        $mock->expects($this->once())->with('cmd.sh')->willReturn('output');
            $actual = $this->sut->remoteAccess('validUser', 'validUser');
            $this->assertSame($expected, $actual);
        }

        /**
         * @inheritDoc
         */
        protected function setSut()
        {
            global $mockSocketCreate;
            $mockSocketCreate = true;

            $this->db = $this->getMockBuilder(Core::class)->disableOriginalConstructor()->getMock();
            $this->session = $this->getMockBuilder(Session::class)->disableOriginalConstructor()->getMock();
            $this->sut = new \process\login\Login($this->db, $this->session);

//            self::$functions = $this->createPartialMock(
//                LoginTest::class, ['shell_exec']
//            );
        }
    }
}
