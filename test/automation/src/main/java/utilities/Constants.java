package utilities;

public class Constants {

	public static final String USER_HOME_DIR = System.getProperty("user.home");
	public static final String USER_WORK_DIR = System.getProperty("user.dir");
	public static final String downloadFilePath = System.getProperty("downloadFilepath");
	public static final String WiniumDriverPath = "\\src\\main\\resources\\drivers\\Winium.Desktop.Driver.exe";
	public static final String ChromeDriverPath = "\\src\\main\\resources\\drivers\\chromedriver.exe";
	public static final String ReportScreens = "\\src\\test\\resources\\ReportScreenElements\\";
	public static final String LoginPageORFile = "\\src\\test\\resources\\objectRepository\\LoginPage.yml";
	public static final String PeerReviewManagementPageORFile = "\\src\\test\\resources\\objectRepository\\PeerReviewManagementPage.yml";
	public static final String UserRoleManagementPageORFile = "\\src\\test\\resources\\objectRepository\\UserRoleManagementPage.yml";
	public static final String UserDetailsFile = "\\src\\test\\resources\\TestData\\UserDetails.yml";
	public static final String WorklistPageORFile = "\\src\\test\\resources\\objectRepository\\WorklistPage.yml";
	public static final String HomePageORFile = "\\src\\test\\resources\\objectRepository\\HomePage.yml";
	public static final String SchedulePageOR = "\\src\\test\\resources\\objectRepository\\SchedulePage.yml";
	public static final String ScheduleDetailFile = "\\src\\test\\resources\\TestData\\ScheduleDetails.yml";
	public static final String DashboardPageOR = "\\src\\test\\resources\\objectRepository\\DashboardPage.yml";
	public static final String UserManagementPageORFile = "\\src\\test\\resources\\objectRepository\\UserManagement.yml";
	public static final String AnalyticsPageORFile = "\\src\\test\\resources\\objectRepository\\AnalyticsPage.yml";
	public static final String AdvancedSearchORFile = "\\src\\test\\resources\\objectRepository\\AdvancedSearchPage.yml";
	public static final String ReportsPageORFile = "\\src\\test\\resources\\objectRepository\\ReportsPage.yml";
	public static final String ContactManagementPageOR = "\\src\\test\\resources\\objectRepository\\ContactManagementPage.yml";	
	public static final String UserCredentialingORFile = "\\src\\test\\resources\\objectRepository\\UserCredentialingPage.yml";
	public static final String SmartReportingPageOR = "\\src\\test\\resources\\objectRepository\\SmartReportingPage.yml";
	public static final String NotesPageOR= "\\src\\test\\resources\\objectRepository\\NotesPage.yml";
	public static final String PatientViewORFile = "\\src\\test\\resources\\objectRepository\\PatientViewPage.yml";
	public static final String ConfigurationORFile = "\\src\\test\\resources\\objectRepository\\ConfigurationPage.yml";
	public static final String AssignRuleManagementPageORFile = "\\src\\test\\resources\\objectRepository\\AssignRuleManagementPage.yml";
	public static final String ProcedureManagementPageORFile = "\\src\\test\\resources\\objectRepository\\ProcedureManagementPage.yml";
	public static final String TestDataPath = "\\src\\test\\resources\\TestData";
	public static final String SystemDownloadsFolderPath = "\\Downloads";
	public static final String GroupManagementPageORFile = "\\src\\test\\resources\\objectRepository\\GroupManagementPage.yml";
	public static final String NotificationManagementPageORFile = "\\src\\test\\resources\\objectRepository\\NotificationManagementPage.yml";
	public static final String SLAManagementPageORFile = "\\src\\test\\resources\\objectRepository\\SLAManagementPage.yml";
	public static final String LdapGroupConfigurationPageORFile = "\\src\\test\\resources\\objectRepository\\LdapGroupConfigurationPage.yml";
	public static final String SiteConfigurationPageORFile = "\\src\\test\\resources\\objectRepository\\SiteConfigurationPage.yml";
	public static final String AiNavigatorORFile = "\\src\\test\\resources\\objectRepository\\AiNavigatorManagementPage.yml";
	public static final String WindowsLauncherOR= "\\src\\test\\resources\\objectRepository\\WindowsLauncher.yml";
}
