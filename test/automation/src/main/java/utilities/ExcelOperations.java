package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExcelOperations {
	private static HSSFWorkbook workbook;

	public static boolean isFileDownloaded(String downloadPath, String fileName) {
		String fileName1 = getLastModifiedFileName(Constants.downloadFilePath, fileName);
		if (fileName1.contains(fileName)) {
			return true;
		}

		return false;
	}

	public static String getLastModifiedFileName(String downloadPath, String fileName) {
		File dir = new File(Constants.downloadFilePath);
		File[] dirContents = dir.listFiles();
		File lastModifiedFile = dirContents[0];
		for (int i = 1; i < dirContents.length; i++) {
			if (lastModifiedFile.lastModified() < dirContents[i].lastModified()) {
				lastModifiedFile = dirContents[i];
			}
		}
		if (lastModifiedFile.getName().contains(fileName))
			return lastModifiedFile.getName();
		else
			return null;
	}

	public static File getFileName(String fileName) {
		File filepath = null;
		File file = new File(Constants.downloadFilePath);
		File[] files = file.listFiles();
		for (File fileNames : files) {
			System.out.println(fileNames);
			if (fileNames.getName().contains(fileName)) {
				filepath = fileNames;
			}
		}
		return filepath;
	}

	public static String appendDateTimeToFilename(String fileName, char separator, String dateFormat) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		String formattedDate = sdf.format(date);
		formattedDate.replace(':', separator);
		String fileName1 = fileName + " " + formattedDate;
		return fileName1;
	}

	public static boolean verifyExcelContents(String label, String value, String fileName) {
		boolean bool = false;
		String splitFileName = getLastModifiedFileName(Constants.downloadFilePath, fileName);
		String fileName1 = Constants.downloadFilePath + "\\" + splitFileName.trim();
		try {
			File f = new File(fileName1);
			if (f.exists()) {
				FileInputStream file = new FileInputStream(fileName1);
				workbook = new HSSFWorkbook(file);
				HSSFSheet sheet = workbook.getSheetAt(0);
				int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
				for (int i = 0; i < rowCount + 1; i++) {
					Row row = sheet.getRow(i);
					for (int j = 0; j < row.getLastCellNum(); j++) {
						if (row.getCell(j).getStringCellValue().contains(label)) {
							{
								if (row.getCell(j + 1).getStringCellValue().equalsIgnoreCase(value)) {
									return bool = true;
								} else {
									return bool = false;
								}
							}
						}
					}
				}
				file.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bool;
	}

	public static boolean verifyEditedExcelContents(String colName, String value, String fileName) {
		boolean bool = false;
		try {
			FileInputStream file = new FileInputStream(getFileName(fileName));
			workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(1);
			int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
			for (int i = 5; i < rowCount + 1; i++) {
				Row row = sheet.getRow(i);
				for (int j = 0; j < row.getLastCellNum(); j++) {
					if (row.getCell(j).getStringCellValue().equals(colName)) {
						{
							for (int k = row.getRowNum() + 1; k < rowCount + 1; k++) {
								if (row.getCell(k).equals(value))
									return bool = true;
								else
									return bool = false;
							}
						}
					}
				}
			}
			file.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bool;
	}

	public static List<String> readExcel(String fileName, String sheetName) {
		List<String> arrName = new ArrayList<String>();
		try {
			FileInputStream inputStream = new FileInputStream(getFileName(fileName));
			workbook = new HSSFWorkbook(inputStream);
			HSSFSheet excelSheet = workbook.getSheet(sheetName);
			for (int i = 1; i <= excelSheet.getLastRowNum(); i++) {
				Row row = excelSheet.getRow(i);
				if (row != null) {
					for (int j = 0; j <= row.getLastCellNum(); j++) {
						Cell cell = row.getCell(j);
						if (cell != null) {
							switch (cell.getCellType()) {
							case NUMERIC:
								arrName.add(String.valueOf(cell.getNumericCellValue()));
								break;
							case STRING:
								arrName.add(cell.getStringCellValue());
								break;
							default:
								break;
							}
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return arrName;
	}

	public static void writeDataInExcel(String fileName, String sheetName, String columnName1, String columnName2) {
		try {
			FileInputStream inputStream = new FileInputStream(getFileName(fileName));
			workbook = new HSSFWorkbook(inputStream);
			HSSFSheet excelSheet = workbook.getSheet(sheetName);
			int col_Num = -1;
			Row row = excelSheet.getRow(0);
			Cell cell = null;
			int lastrow = excelSheet.getLastRowNum();
			Row newRow = excelSheet.createRow(++lastrow);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				col_Num = i;
				if (row.getCell(i).getStringCellValue().trim().contains(columnName1)) {
					cell = newRow.createCell(col_Num);
					cell.setCellValue("Test");
				}
				if (row.getCell(i).getStringCellValue().trim().contains(columnName2)) {
					cell = newRow.createCell(col_Num);
					cell.setCellValue("User");
				}
			}
			FileOutputStream fileOutputStream = new FileOutputStream(getFileName(fileName));
			workbook.write(fileOutputStream);
			fileOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean deleteFile(String fileName) {
		File file = getFileName(fileName);
		if (file.exists()) {
			file.delete();
			Log.printInfo(getFileName(fileName) + " deleted successfully");
			return true;
		} else {
			Log.printInfo(getFileName(fileName) + " not present");
			return false;
		}
	}

	public static String getFileFromZipFolder(String folderName) {
		Enumeration<?> enu = null;
		String fileName = null;
		try {
			File directoryPath = new File(Constants.downloadFilePath);
			String listOfFolders[] = directoryPath.list();
			for (int i = 0; i < listOfFolders.length; i++) {
				if (listOfFolders[i].contains(folderName))
					enu = new ZipFile(Constants.downloadFilePath + "//" + listOfFolders[i]).entries();
			}
			while (enu.hasMoreElements()) {
				ZipEntry zipEntry = (ZipEntry) enu.nextElement();
				fileName = zipEntry.getName();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return FilenameUtils.removeExtension(fileName);

	}

	public static File getFileNameInsideFolder(String fileName, String folderName) {
		File filepath = null;
		File file = new File(folderName);
		File[] files = file.listFiles();
		for (File fileNames : files) {
			if (fileNames.getName().contains(fileName)) {
				filepath = fileNames;
				break;
			}
		}
		return filepath;
	}

	public static void setExcelValueWithDateColumn(String fileName, String sheetName, String cellHeaderValue, String cellValue) {
		try {
			FileInputStream inputStream = new FileInputStream(getFileName(fileName));
			workbook = new HSSFWorkbook(inputStream);
			HSSFSheet excelSheet = workbook.getSheet(sheetName);
			Cell cell = null;
			int lastrow = excelSheet.getLastRowNum();
			Row row = excelSheet.getRow(0);
			Row newRow = null;
			newRow = (lastrow == 0) ? excelSheet.createRow(++lastrow) : excelSheet.getRow(lastrow);		
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			for (int i = 0; i < row.getLastCellNum(); i++) {
				cell = row.getCell(i);
				switch (cell.getCellType()) {
				case NUMERIC:
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					if (dateFormat.format(row.getCell(i).getDateCellValue()).toString().trim().equals(cellHeaderValue)) {
						cell.setCellValue(dateTimeFormatter.format(LocalDateTime.now()));
						cell = newRow.createCell(i);
						cell.setCellValue(cellValue);
					}
					break;
				case STRING:
					if (row.getCell(i).getStringCellValue().trim().equals(cellHeaderValue)) {
						cell = newRow.createCell(i);
						cell.setCellValue(cellValue);
					}
					break;
				}
			}
			FileOutputStream fileOutputStream = new FileOutputStream(getFileName(fileName));
			workbook.write(fileOutputStream);
			fileOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<String> readExcelFromTestDataFolder(String fileName, String sheetName) {
		List<String> arrName = new ArrayList<String>();
		try {
			FileInputStream inputStream = new FileInputStream(fileName);
			workbook = new HSSFWorkbook(inputStream);
			HSSFSheet excelSheet = workbook.getSheet(sheetName);
			for (int i = 1; i <= excelSheet.getLastRowNum(); i++) {
				Row row = excelSheet.getRow(i);
				if (row != null) {
					for (int j = 0; j <= row.getLastCellNum(); j++) {
						Cell cell = row.getCell(j);
						if (cell != null) {
							switch (cell.getCellType()) {
							case NUMERIC:
								arrName.add(String.valueOf(cell.getNumericCellValue()));
								break;
							case STRING:
								arrName.add(cell.getStringCellValue());
								break;
							default:
								break;
							}
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return arrName;
	}

}
