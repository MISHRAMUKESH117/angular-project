package utilities;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Common {

	public static void sleep(long number, TimeUnit units) {
        try {
            units.sleep(number);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
	
	public static void killTask(String processName) {
		try {
			Runtime.getRuntime().exec("taskkill /F /IM " + processName + ".exe /T");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String GetNvalue()
	{
		Date date=new Date();
		SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/yyyy");
		return formatter.format(date);		
	}
	
	public static String GetTestDate(int numOfDays)
	{		
		String N =GetNvalue();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		try{
		   c.setTime(sdf.parse(N));
		}catch(ParseException e){
		   e.getMessage();
		 }
		c.add(Calendar.DAY_OF_MONTH, numOfDays);  
		String testDate = sdf.format(c.getTime());  
		return testDate;
	}
	
	public static Date StringToDate(String dateforconversion)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date = null;
		try 
		{
		  date = formatter.parse(dateforconversion);
		} 
		catch (ParseException e)
		{
			e.printStackTrace();
		}		
		return date;
		
		
	}
}
