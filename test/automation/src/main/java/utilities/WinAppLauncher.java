package utilities;

import java.io.IOException;

public class WinAppLauncher {
	WinDriver winDriver;
	Property property;
	public static final String USER_WORK_DIR = System.getProperty("user.dir");

	public WinAppLauncher() {
		property = new Property();
	}

	public void launchWinApp(String appName) throws IOException {
		winDriver = new WinDriver();
		winDriver.setWinAppDriver(appName);
	}

	public WinDriver getWinDriver() {
		return winDriver;
	}
}
