package utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log {
	
	private static Logger logger = LogManager.getLogger(Logger.class.getName());
	
	public static void printInfo(String message) {
		logger.info(message);
	}
	
	public static void printError(String message) {
		logger.error(message);
	}

}
