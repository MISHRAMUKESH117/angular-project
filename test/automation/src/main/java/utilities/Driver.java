package utilities;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.winium.WiniumDriver;

public class Driver {

	public WebDriver webDriver;
	public WiniumDriver winDriver;
	Property property;
	public static final String USER_WORK_DIR = System.getProperty("user.dir");
	public static final String USER_HOME_DIR = System.getProperty("user.home");
	private String propertyFile = "Automation.properties";

	public Driver() {
		property = new Property();
	}

	public WebDriver getDriver() throws InterruptedException, IOException {
		this.initializeDriver();
		return webDriver;
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}

	public WiniumDriver getWinDriver() {
		return winDriver;
	}

	private void initializeDriver() throws InterruptedException, IOException {
		String browser = (System.getProperty("browser")==null)?property.getPropertyValue(propertyFile, "browser"):System.getProperty("browser");
		String downloadFilepath = USER_WORK_DIR + "\\src\\test\\customDownloads\\";
		System.setProperty("downloadFilepath", downloadFilepath);

		try {
			switch (browser) {

			case "chrome":
				String path = USER_WORK_DIR + property.getPropertyValue(propertyFile, "chromeDriverPath");
				System.setProperty("webdriver.chrome.driver", path);
				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				chromePrefs.put("download.default_directory", downloadFilepath);
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", chromePrefs);
				options.setExperimentalOption("useAutomationExtension", false);
				options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation")); 
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				webDriver = new ChromeDriver(options);
				webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				webDriver.manage().window().maximize();
				break;

			case "firefox":
				path = USER_WORK_DIR + property.getPropertyValue(propertyFile, "firefoxDriverPath");
				System.setProperty("webdriver.gecko.driver", path);
				FirefoxProfile profile = new FirefoxProfile();
				profile.setPreference("browser.download.folderList", 2);
				profile.setPreference("browser.download.dir", downloadFilepath);
				profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
						"application/force-download, application/octet-stream, image/png, application/vnd.ms-excel");
				profile.setPreference("pdfjs.disabled", true);
				FirefoxOptions option = new FirefoxOptions();
				option.setProfile(profile);
				webDriver = new FirefoxDriver(option);
				webDriver.manage().window().maximize();
				webDriver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
				webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				break;
				
			case "edge":
				downloadFilepath = null;
				downloadFilepath = USER_HOME_DIR + Constants.SystemDownloadsFolderPath;
				System.setProperty("downloadFilepath", downloadFilepath);
				path = USER_WORK_DIR + property.getPropertyValue(propertyFile, "edgeDriverPath");
				System.setProperty("webdriver.edge.driver", path);
				webDriver = new EdgeDriver();
				webDriver.manage().window().maximize();
				webDriver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
				webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				break;
				
			}
		} catch (WebDriverException e) {
			e.printStackTrace();
			this.killDriver();
			Thread.sleep(5000);
		}

	}

	private void killDriver() {
		String driverStr = "chromedriver.exe";
		try {
			Runtime rt = Runtime.getRuntime();
			rt.exec("taskkill /f /im " + driverStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
