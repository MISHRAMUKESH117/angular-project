package utilities;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.USER_HOME_DIR;
import static utilities.Constants.WiniumDriverPath;

import java.io.IOException;

import java.net.URL;

import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

public class WinDriver {

	private static String propertyFile = "Automation.properties";
	private static String swlPropertyFile = "SWLInstallers.properties";
	WiniumDriver winDriver;
	Property property;

	public WinDriver() {
		property = new Property();
	}

	public WiniumDriver setWinAppDriver(String appName) throws IOException {
		String appPath = null;
		String AppDriverPath = USER_WORK_DIR + WiniumDriverPath;
		String localHost = property.getPropertyValue(propertyFile, "localHost");
		DesktopOptions options = new DesktopOptions();
		//Runtime.getRuntime().exec(AppDriverPath); //+ " --port " + 8080
		try {
			switch (appName) {

			case "Control Panel":
				appPath = USER_HOME_DIR + property.getPropertyValue(propertyFile, "controlPanelPath");
				options.setApplicationPath(appPath);
				winDriver = new WiniumDriver(new URL(localHost + 9999), options);
				break;

			case "zvExtender":
				appPath = USER_WORK_DIR + property.getPropertyValue(propertyFile, "zvExtenderAppPath");
				options.setApplicationPath(appPath);
				winDriver = new WiniumDriver(new URL(localHost + 9999), options);
				break;

			case "Command Prompt":
				appPath = USER_HOME_DIR + property.getPropertyValue(propertyFile, "commandPromptPath");
				options.setApplicationPath(appPath);
				winDriver = new WiniumDriver(new URL(localHost + 9999), options);
				break;

			case "SWL Database Installer":
				appPath = (System.getProperty("dbMsiPath")==null)?System.getProperty("dbMsiPath"):property.getPropertyValue(swlPropertyFile, "dbMsiPath");
				options.setApplicationPath(appPath);
				winDriver = new WiniumDriver(new URL(localHost + 9999), options);
				break;

			case "SWL Web Installer":
				appPath = (System.getProperty("webMsiPath")==null)?System.getProperty("webMsiPath"):property.getPropertyValue(swlPropertyFile, "webMsiPath");
				options.setApplicationPath(appPath);
				winDriver = new WiniumDriver(new URL(localHost + 9999), options);
				break;

			case "SWL Services Installer":
				appPath = (System.getProperty("svcExePath")==null)?System.getProperty("svcExePath"):property.getPropertyValue(swlPropertyFile, "svcExePath");
				String svcConfig = (System.getProperty("svcConfig")==null)?property.getPropertyValue(swlPropertyFile, "svcConfig"):System.getProperty("svcConfig");
				String configFile = ExcelOperations.getFileNameInsideFolder(svcConfig, Constants.USER_WORK_DIR + Constants.TestDataPath).toString();//+"JSON";
				String cmdPath = property.getPropertyValue(propertyFile, "cmdPath");
				options.setApplicationPath(cmdPath);
				winDriver = new WiniumDriver(new URL(localHost + 9999), options);
				winDriver.findElementByName("Administrator: "+cmdPath).sendKeys(appPath+" -config "+configFile);
				winDriver.findElementByName("Administrator: "+cmdPath).submit();
				winDriver.findElementByName("Administrator: "+cmdPath).sendKeys("exit");
				winDriver.findElementByName("Administrator: "+cmdPath).submit();
				break;				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return winDriver;
	}

	public WiniumDriver getCurrentDriver() {
		return winDriver;
	}
}
