package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class YmlReader {

	public static Map<String, String> getElementLocators(String objectRepositoryFilePath) {
		Yaml yaml = new Yaml();
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(objectRepositoryFilePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, String> map = yaml.load(inputStream);
		yaml.load(inputStream);
		return map;
	}
	
	public static <T> T getobjectRepository(String objectRepositoryFilePath,Class<?> objectRepositoryClass) {
		Yaml yaml = new Yaml(new Constructor(objectRepositoryClass));
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(objectRepositoryFilePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		T objectRepo = yaml.load(inputStream);
		//yaml.load(inputStream);
		return objectRepo;
	}
}
