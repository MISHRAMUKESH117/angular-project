package utilities;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class DBConnection {
	Property property;
	private String propertyFile = "DBConnection.properties";
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String driverClass = "";
	private String dbURL = "";
	private String dbName = "";
	private String username= "";
	private String password= "";
	private String query = "";
	private boolean isEntryPresent = false;
	//properties for SWL installers
	private String swlPropertyFile = "SWLInstallers.properties";
	
	public DBConnection() {
		property = new Property();
	}
	
	//get the DB connection from mvn or properties(by default)
	public List<String>  getDatabaseProperty() {
	    List<String> strings = new ArrayList<String>();
 		try {
 				strings.add((System.getProperty("dbHost")==null)?property.getPropertyValue(swlPropertyFile, "dbHost"):System.getProperty("dbHost"));
 				strings.add((System.getProperty("dbAdmin")==null)?property.getPropertyValue(swlPropertyFile, "dbAdmin"):System.getProperty("dbAdmin"));
 				strings.add((System.getProperty("dbAdminPassword")==null)?property.getPropertyValue(swlPropertyFile, "dbAdminPassword"):System.getProperty("dbAdminPassword"));
 				strings.add((System.getProperty("dbName")==null)?property.getPropertyValue(swlPropertyFile, "dbName"):System.getProperty("dbName"));
 				strings.add((System.getProperty("sessionDbName")==null)?property.getPropertyValue(swlPropertyFile, "sessionDbName"):System.getProperty("sessionDbName"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return strings;
	 }

	
	public Hashtable<String, String> getDatabasePropertyHash() {
		
		 Hashtable<String, String> hashtable = new Hashtable<String, String>();
		 try {
			hashtable.put("dbHost",((System.getProperty("dbHost")==null)?property.getPropertyValue(swlPropertyFile, "dbHost"):System.getProperty("dbHost")));
			hashtable.put("dbAdmin",((System.getProperty("dbAdmin")==null)?property.getPropertyValue(swlPropertyFile, "dbAdmin"):System.getProperty("dbAdmin")));
			hashtable.put("dbAdminPassword",((System.getProperty("dbAdminPassword")==null)?property.getPropertyValue(swlPropertyFile, "dbAdminPassword"):System.getProperty("dbAdminPassword")));
			hashtable.put("dbName",((System.getProperty("dbName")==null)?property.getPropertyValue(swlPropertyFile, "dbName"):System.getProperty("dbName")));
			hashtable.put("sessionDbName",((System.getProperty("sessionDbName")==null)?property.getPropertyValue(swlPropertyFile, "sessionDbName"):System.getProperty("sessionDbName")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hashtable;
	}
	
	
	public Connection createDatabaseConnection() {
		 try {
			driverClass = property.getPropertyValue(propertyFile, "MSSQLJDBC_driver");
			dbURL = property.getPropertyValue(propertyFile, "url");
			dbName = property.getPropertyValue(propertyFile, "databaseName");
			username = property.getPropertyValue(propertyFile, "username");
			password = property.getPropertyValue(propertyFile, "password");
			
			Class.forName(driverClass);
			con = DriverManager.getConnection(dbURL+dbName, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return con;
	 }
	
	public boolean isEntryPresentInDB(String tableName, String columnName, String value) {
		try {
			stmt =  con.createStatement();
			query = "select * from "+tableName+" where "+columnName+"= '"+value+"'";
			rs = stmt.executeQuery(query);
			isEntryPresent = rs.next() ? true : false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isEntryPresent;
	}
	
	public void updateEntry(String tableName, String columnName, String oldValue, String newValue) {
		try {
			stmt =  con.createStatement();
			query = "update "+tableName+" set "+columnName+"= '"+newValue+"' where "+columnName+"= '"+oldValue+"'";
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//Delete a String value from DB
	public void deleteEntry(String tableName, String columnName, String value) {
		try {
			stmt =  con.createStatement();
			query = "delete from "+tableName+" where "+columnName+"= '"+value+"'";
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void closeDatabaseConnection() {
		try {
			  con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	// Delete an Integer value from DB
	public void deleteEntry(String tableName, String columnName, int value) {
		try {
			stmt = con.createStatement();
			query = "delete from " + tableName + " where " + columnName + "= " + value;
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getIntegerColumnValueForEntry(String tableName, String columnNameToSearch, String valueToSearch, int columnNumberToGetValue) {
		int value = 0;
		try {
			stmt = con.createStatement();
			query = "select * from " + tableName + " where " + columnNameToSearch + "= '" + valueToSearch + "'";
			rs = stmt.executeQuery(query);
			while (rs.next())
				value = rs.getInt(columnNumberToGetValue);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return value;
	}

	public String getStringColumnValueForEntry(String tableName, String columnNameToSearch, String valueToSearch, int columnNumberToGetValue) {
		String value = "";
		try {
			stmt = con.createStatement();
			query = "select * from " + tableName + " where " + columnNameToSearch + "= '" + valueToSearch + "'";
			rs = stmt.executeQuery(query);
			value = rs.getString(columnNumberToGetValue);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	public void setBrowserCompatability(List<String> browserType) {
		try {
			stmt = con.createStatement();
			query = "Update dbo.conf_Common set value='" + getBrowserType(browserType) + "' where UQ_name = 'BrowserSupport';";
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setBrowserCompatabilityNull() {
		try {
			stmt = con.createStatement();
			query = "Update dbo.conf_Common set value=null where UQ_name = 'BrowserSupport';";
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String getBrowserType(List<String> browserTypes) {
		StringBuilder broswerType = new StringBuilder();
	    for (int i = 0; i <= browserTypes.size()-2; i++) 
	    	broswerType.append(browserTypes.get(i)+",");	    	
	    return broswerType.append(browserTypes.get(browserTypes.size()-1)).toString();
	}
	
	
	public void deleteUserSessionQuery(){
		try {
			stmt = con.createStatement();
			String dbValue= property.getPropertyValue(propertyFile,"databaseName").replaceAll("databaseName=|;", "");
			query = "Delete from "+dbValue+"_session.sess.Zvision where loginID in ( select PK_loginID from dbo.Login where FK_userID >0);";
			stmt.executeUpdate(query);
		} catch (SQLException | IOException e ) {
			e.printStackTrace();
		}
	}
}

