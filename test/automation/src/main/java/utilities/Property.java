package utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class Property {
	
	public String getPropertyPath(String propertyFileName) throws FileNotFoundException {
		String returnValue;
		String basePath = System.getProperty("user.dir") + "\\src\\main\\java";
		File manual = new File(basePath + File.separator + "Properties" + File.separator + propertyFileName);
		// find the valid directory path for this property file
		if (manual.exists()) {
			returnValue = manual.getAbsolutePath();
		} else {
			throw new FileNotFoundException("File not found:  " + propertyFileName);
		}
		// now, take out the property file name out of returnValue -- we just want to
		// return the path
		returnValue = returnValue.replace(File.separator + propertyFileName, "");
		return returnValue;

	}

	public String getPropertyValue(String fileName, String propertyName) throws IOException {
		// declaring local variables
		Properties prop = new Properties();
		String propertyDir = getPropertyPath(fileName);
		String returnValue = "";
		// open file
		InputStream input = new FileInputStream(propertyDir + File.separator + fileName);
		// load the file
		prop.load(input);
		// find the value with the listName
		returnValue = prop.getProperty(propertyName);
		return returnValue;
	}


	public Set<Entry<Object, Object>> getPropertyValues(String fileName, String propertyName) throws IOException {
		Properties properties = new Properties();
		String propertyDir = getPropertyPath(fileName);
		InputStream input = new FileInputStream(propertyDir + File.separator + fileName);
		properties.load(input);
		input.close();
		return properties.entrySet();	
	}
	
	
	public void updatePropertiesFileValue(String fileName, String propertyName, String newValue) {
		StringBuilder fileContent = new StringBuilder();
		Set<Entry<Object, Object>> propertyValues;
		try {
			propertyValues = getPropertyValues(fileName,propertyName);	
			// loop through the set and rewrite the properties replacing properties file with new value
			for (Entry<Object, Object> set : propertyValues) {
				if(propertyName.equals(set.getKey())) {	
					fileContent.append(set.getKey() +" = "+ newValue.toLowerCase()+"\n"); 
					continue;
				}
				fileContent.append(set.getKey() +" = "+ set.getValue()+"\n");   
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(getPropertyPath(fileName)+"\\"+fileName));
			//replacing \\ with \\\\ as backslash is escaped and write to file
			writer.write(fileContent.toString().replace("\\","\\\\"));   
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
