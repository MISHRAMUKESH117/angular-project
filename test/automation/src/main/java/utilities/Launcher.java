package utilities;

import static utilities.Common.sleep;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import Actions.Sikuli;

public class Launcher {

	Driver driver;
	WebDriver driverLauncher;
	Property property;
	public static final String USER_WORK_DIR = System.getProperty("user.dir");
	private String propertyFile = "Automation.properties";

	public Launcher() {
		property = new Property();
	}

	public void initializeWebDriver() {
		driver = new Driver();
		try {
			driverLauncher = driver.getDriver();
		} catch (InterruptedException | IOException e) {
		}
	}

	public void launchApplication() throws IOException {
		String applicationURL = (System.getProperty("url") == null)
				? property.getPropertyValue(propertyFile, "applicationURL")
				: System.getProperty("url");
		driver.getWebDriver().get(applicationURL);
	}

	public byte[] getScreenshot() {
		return new Sikuli().getScreenshot();
	}

	public Driver getDriver() {
		return driver;
	}

	public void quitApplication() {
		driver.getWebDriver().manage().deleteAllCookies();
		driver.getWebDriver().quit();
		Common.killTask("chromedriver");
		sleep(5, TimeUnit.SECONDS);
	}

	public boolean webDriverIsActive() {
		try {
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec("tasklist.exe");
			BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = null;
			String pidInfo = line;
			while ((line = input.readLine()) != null) {
				pidInfo += line;
			}
			input.close();
			if (pidInfo.contains("chromedriver.exe") || pidInfo.contains("firefoxdriver.exe")
					|| pidInfo.contains("msedgedriver.exe")) {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}
}
