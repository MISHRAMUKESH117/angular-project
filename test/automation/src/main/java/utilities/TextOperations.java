package utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextOperations {
	
	private static String filePath = Constants.downloadFilePath+"\\";
	
	public static boolean createFile(String fileName) {
		try {
		    File file = new File(filePath+fileName+".txt");
		        if (file.createNewFile()) {
		            return true;
		        } else {
		            return false;
		        }
		    } catch (IOException e) {
		          e.printStackTrace();
		          return false;
		    }
	}
	
	public static void textFileWriter(String name, String fileData) {
		String file = filePath+name+".txt";
		try {
			FileWriter writer = new FileWriter(file);
			writer.write(fileData);
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean checkFileExists(String fileName) {
		boolean fileExists = false;
		File file = new File(Constants.downloadFilePath);
		File[] files = file.listFiles();
		for (File fileNames : files) {
			if (fileNames.getName().contains(fileName)) {
				fileExists = true;
			}
		}
		return fileExists;
	}
}
