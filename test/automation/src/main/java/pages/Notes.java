package pages;

import static utilities.Common.sleep;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.NotesPageOR;

import java.util.concurrent.TimeUnit;

import Actions.Web;
import objectRepository.NotesPageOR;
import utilities.Driver;
import utilities.YmlReader;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Notes extends Locator {
	Web webActions;

	public Notes(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static NotesPageOR notesPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + NotesPageOR, NotesPageOR.class);

	public void clickOnNoteType(String noteType) {
		webActions.waitForElementClickability(getByLocator(notesPageOR.noteType_Xpath, LocatorIdentifier.Xpath, noteType), 30);
		webActions.click(getByLocator(notesPageOR.noteType_Xpath, LocatorIdentifier.Xpath, noteType));
	}

	public void addNote(String text) {
		webActions.setText(getByLocator(notesPageOR.communicationNoteTextArea_Xpath, LocatorIdentifier.Xpath), text);
	}

	public void saveNote(String noteEditAction) {
		webActions.waitForElementClickability(getByLocator(notesPageOR.noteSubmitCancelClear_Xpath, LocatorIdentifier.Xpath, noteEditAction), 60);
		webActions.click(getByLocator(notesPageOR.noteSubmitCancelClear_Xpath, LocatorIdentifier.Xpath, noteEditAction));
		sleep(5, TimeUnit.SECONDS);
	}

	public boolean isNoteCreated(String note) {
		webActions.waitForElementPresence(getByLocator(notesPageOR.noteCreated_Xpath, LocatorIdentifier.Xpath, note), 30);
		return webActions.isVisible(getByLocator(notesPageOR.noteCreated_Xpath, LocatorIdentifier.Xpath, note));
	}

	public void openNote(String note) {
		webActions.waitForElementClickability(getByLocator(notesPageOR.openNote_XPath, LocatorIdentifier.Xpath, note), 60);
		webActions.click(getByLocator(notesPageOR.openNote_XPath, LocatorIdentifier.Xpath, note));
	}

	public void addCommentInCommunicationNote(String comment) {
		webActions.setText(getByLocator(notesPageOR.communicationNoteCommentTextbox_XPath, LocatorIdentifier.Xpath), comment);
	}

	public boolean isCommentAdded(String comment) {
		webActions.waitForElementPresence(getByLocator(notesPageOR.noteComment_XPath, LocatorIdentifier.Xpath, comment), 30);
		return webActions.isVisible(getByLocator(notesPageOR.noteComment_XPath, LocatorIdentifier.Xpath, comment));
	}

	public boolean isNoteStatusChanged(String noteName, String noteStatus) {
		return webActions.getText(getByLocator(notesPageOR.noteStatus_XPath, LocatorIdentifier.Xpath, noteName)).equals(noteStatus);
	}

	public void deleteNote(String noteStatus) {
		webActions.waitForElementClickability(getByLocator(notesPageOR.deleteNoteButton_XPath, LocatorIdentifier.Xpath, noteStatus), 60);
		webActions.click(getByLocator(notesPageOR.deleteNoteButton_XPath, LocatorIdentifier.Xpath, noteStatus));
		webActions.click(getByLocator(notesPageOR.deleteAllNoteConfirmation_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isNoteDeleted(String note) {
		return !webActions.isVisible(getByLocator(notesPageOR.noteCreated_Xpath, LocatorIdentifier.Xpath, note));
	}

	public boolean isCommunicationNoteWindowNotVisible() {
		return !webActions.isVisible(getByLocator(notesPageOR.communicationNoteWindow_XPath, LocatorIdentifier.Xpath));
	}

	public void createTeachingNote(String textbox, String noteValue) {
		webActions.setText(getByLocator(notesPageOR.teachingNoteTitleTextbox_XPath, LocatorIdentifier.Xpath, textbox.toLowerCase()), noteValue);
	}

	public void addMessageInNote(String comment, String noteType) {
		webActions.setText(getByLocator(notesPageOR.addNoteMessageTextbox_XPath, LocatorIdentifier.Xpath, noteType), comment);
	}

	public void clickSaveTeachingNoteComment(String button) {
		webActions.click(getByLocator(notesPageOR.teachingNoteSaveComment_XPath, LocatorIdentifier.Xpath, button));
	}

	public boolean isEditNoteWindowNotVisible(String noteType) {
		return !webActions.isVisible(getByLocator(notesPageOR.noteEditWindow_XPath, LocatorIdentifier.Xpath, noteType));
	}

	public void selectExamNoteType(String noteType) {
		webActions.click(getByLocator(notesPageOR.examNoteDropdownValue_XPath, LocatorIdentifier.Xpath, noteType));
	}

	public void clickAddToWorklist() {
		webActions.click(getByLocator(notesPageOR.addWorklistDropdown_XPath, LocatorIdentifier.Xpath));
	}

	public void selectWorklist(String worklistType) {
		webActions.click(getByLocator(notesPageOR.worklistDropdownValue_XPath, LocatorIdentifier.Xpath, worklistType));
	}

	public void setNewWorklistName(String worklistName) {
		webActions.setText(getByLocator(notesPageOR.newWorklistTextboxName_XPath, LocatorIdentifier.Xpath), worklistName);
	}

	public void clickNewWorklistButton(String button) {
		webActions.click(getByLocator(notesPageOR.saveNewWorklistButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public void mouseHoverOnExamIndicator(String exam) {
		webActions.mouseHover(getByLocator(notesPageOR.examNoteIndicatorIcon_XPath, LocatorIdentifier.Xpath, exam));
	}

	public boolean isExamNoteTooltipPresent(String message) {
		return webActions.isVisible(getByLocator(notesPageOR.examNoteTooltipValue_XPath, LocatorIdentifier.Xpath, message)) ? true : false;
	}

	public void clickExamNoteButton(String noteEditAction) {
		webActions.waitForElementClickability(getByLocator(notesPageOR.noteSubmitCancelClear_Xpath, LocatorIdentifier.Xpath, noteEditAction), 60);
		webActions.click(getByLocator(notesPageOR.noteSubmitCancelClear_Xpath, LocatorIdentifier.Xpath, noteEditAction));
		webActions.click(getByLocator(notesPageOR.deleteAllNoteConfirmation_XPath, LocatorIdentifier.Xpath));
	}

	public void mouseHoverOnTechQANoteIndicator(String exam) {
		webActions.mouseHover(getByLocator(notesPageOR.techQANoteIndicatorIcon_XPath, LocatorIdentifier.Xpath, exam));
	}

	public void setCurrentDate(String textbox) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		webActions.setText(
				getByLocator(notesPageOR.examNoteType_XPath, LocatorIdentifier.Xpath, textbox), dateTimeFormatter.format(LocalDateTime.now()));
		webActions.click(getByLocator(notesPageOR.followUpNoteDueDateIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void mouseHoverOnFollowUpNoteIndicator(String exam) {
		webActions.mouseHover(getByLocator(notesPageOR.followupNoteIndicatorIcon_XPath, LocatorIdentifier.Xpath, exam));
	}

	public void mouseHoverOnCommunicationNoteIndicator(String exam) {
		webActions.mouseHover(getByLocator(notesPageOR.communicationNoteIndicatorIcon_XPath, LocatorIdentifier.Xpath, exam));
	}

	public boolean isNoteChangedWithCurrentDate(String noteStatus) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		return webActions.getText(getByLocator(notesPageOR.noteDate_XPath, LocatorIdentifier.Xpath, noteStatus))
				.contains(dateTimeFormatter.format(LocalDateTime.now()));
	}

	public String getNoteStatus(String noteMessage) {
		return webActions.getText(getByLocator(notesPageOR.noteStatus_XPath, LocatorIdentifier.Xpath, noteMessage));
	}

	public void mouseHoveOnPeerReviewNoteIndicator(String exam) {
		webActions.mouseHover(getByLocator(notesPageOR.peerReviewNoteIndicator_XPath, LocatorIdentifier.Xpath, exam));
	}

	public void deletePeerReviewNote() {
		webActions.click(getByLocator(notesPageOR.deletePeerReviewNoteIcon_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(notesPageOR.deleteAllNoteConfirmation_XPath, LocatorIdentifier.Xpath));
	}
	
	public void mouseHoverOnEDPrelimNoteIndicator(String exam) {
		webActions.mouseHover(getByLocator(notesPageOR.edPrelimNoteIndicator_XPath, LocatorIdentifier.Xpath, exam));
	}
	
	public void selectCommunicatioNoteType(String noteType) {
		webActions.click(getByLocator(notesPageOR.communicationNoteType_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(notesPageOR.communicationNoteTypeValue_XPath, LocatorIdentifier.Xpath, noteType));
	}

	public boolean isNoteAlertMessagePresent(String noteMessage) {
		return webActions.isVisible(getByLocator(notesPageOR.noteAlertMessage_XPath, LocatorIdentifier.Xpath, noteMessage));
	}

	public void clickAcknowledgeButton() {
		webActions.click(getByLocator(notesPageOR.noteAcknowledgeButton_XPath, LocatorIdentifier.Xpath));
	}
}
