package pages;

import static utilities.Common.sleep;
import static utilities.Constants.PeerReviewManagementPageORFile;
import static utilities.Constants.USER_WORK_DIR;

import java.util.concurrent.TimeUnit;

import Actions.Sikuli;
import Actions.Web;
import objectRepository.PeerReviewManagementPageOR;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;

public class PeerReviewManagement extends Locator {

	Web webActions;
	Sikuli sikuliActions;

	public PeerReviewManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static PeerReviewManagementPageOR peerReviewManagementPageOR = YmlReader
			.getobjectRepository(USER_WORK_DIR + PeerReviewManagementPageORFile, PeerReviewManagementPageOR.class);

	public boolean isPeerReviewManagementPageDisplayed() {
		return webActions.isVisible(getByLocator(peerReviewManagementPageOR.pageHeader_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickOnPeerReviewMenuOption(String menuOption) {
		webActions.click(getByLocator(peerReviewManagementPageOR.menuOptions_Xpath, LocatorIdentifier.Xpath, menuOption));
		Log.printInfo("Clicked on " + menuOption);
	}

	public boolean isSectionHeaderVisible(String sectionName) {
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.sectionHeader_Xpath, LocatorIdentifier.Xpath, sectionName));
	}
	
	public boolean isNoteNameVisible(String sectionName) {
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.noteName_Xpath, LocatorIdentifier.Xpath, sectionName));
	}
	
	public boolean isDropdownValueVisible(String dropDownValue) {
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.dropDownValue_Xpath, LocatorIdentifier.Xpath, dropDownValue));
	}

	public void clickAddNewExamTypeDefinitionButton() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.addExamTypeDefinitionButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on new Exam Type Definition Add button");
	}

	public void setNewExamTypeDefinitionName(String newExamTypeDefinitionName) {
		webActions.setText(
				getByLocator(peerReviewManagementPageOR.inputExamTypeDefinitionName_Xpath, LocatorIdentifier.Xpath),newExamTypeDefinitionName);
		Log.printInfo("Set New Exam Type Definition name: " + newExamTypeDefinitionName);
	}

	public void clickNewExamTypeDefinitionOKButton() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.newExamTypeDefinitionOKButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on new Exam Type Definition OK button");
	}

	public void clickAssignmentPropertiesTimePeriodDropdownArrow() {
		webActions.click(getByLocator(peerReviewManagementPageOR.assignmentPropertiesTimePeriodDropdownArrow_Xpath,LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Assignment Properties, Time Period Dropdown Arrow");
	}

	public void selectAssignmentPropertiesTimePeriod(String timePeriod) {
		webActions.click(getByLocator(peerReviewManagementPageOR.assignmentPropertiesTimePeriodDropdownOptions_Xpath,LocatorIdentifier.Xpath, timePeriod));
		Log.printInfo("Selected Time Period");
	}

	public void clickExamTypeDefinitionDeleteIcon() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.examTypeDefinitionDeleteIcon_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Exam Type Definition Delete Icon");
	}

	public void clickAssignmentPropertiesDefinitionNameDropdownArrow() {
		webActions.click(getByLocator(peerReviewManagementPageOR.assignmentPropertiesDefinitionNameDropdownArrow_Xpath,LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Assignment Properties, Definition Name Dropdown Arrow");
	}

	public void selectAssignmentPropertiesDefinitionName(String definitionName) {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.assignmentPropertiesDefinitionNameDropdownOptions_Xpath,LocatorIdentifier.Xpath, definitionName));
		Log.printInfo("Selected Definition Name");
	}

	public boolean isAssignmentPropertiesDefinitionNameDeleted(String definitionName) {
		boolean definitionNameDeleted = false;
		definitionNameDeleted = webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.assignmentPropertiesDefinitionNameDropdownOptions_Xpath,LocatorIdentifier.Xpath, definitionName));
		return !definitionNameDeleted;
	}

	public String getAssignmentPropertiesTimePeriod() {
		String assignmentTimePeriod = webActions.getTextboxValue(
				getByLocator(peerReviewManagementPageOR.assignmentPropertiesTimePeriod_Xpath, LocatorIdentifier.Xpath));
		return assignmentTimePeriod;
	}

	public void clickFocusedAssignmentAddRuleButton() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentAddRule_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Focused Assignment Add Rule Button");
	}

	public void setFocusedAssignmentRuleName(String ruleName) {
		webActions.setText(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentNewRuleName_Xpath, LocatorIdentifier.Xpath),ruleName);
		Log.printInfo("Set Focused Assignment Rule name: " + ruleName);
	}

	public void setFocusedAssignmentRuleDescription(String ruleDescription) {
		webActions.setText(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentNewRuleDescription_Xpath,LocatorIdentifier.Xpath), ruleDescription);
		Log.printInfo("Set Focused Assignment Rule description: " + ruleDescription);
	}

	public void setFocusedAssignmentRulePercentageExamsToAssign(String rulePercentageExamsToAssign) {
		webActions.setText(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentNewRulePercentageExamsToAssign_Xpath,LocatorIdentifier.Xpath), rulePercentageExamsToAssign);
		Log.printInfo("Set Focused Assignment Rule Percentage Exams To Assign: " + rulePercentageExamsToAssign);
	}

	public void setFocusedAssignmentRuleUserName(String ruleUserName) {
		webActions.setText(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentNewRuleUserNameTextBox_Xpath,LocatorIdentifier.Xpath), ruleUserName);
		Log.printInfo("Set Focused Assignment Rule Percentage Exams To Assign: " + ruleUserName);
	}

	public void selectFocusedAssignmentUserNameCheckbox(String ruleUserName) {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentRuleUserNameOptions_Xpath,LocatorIdentifier.Xpath, ruleUserName));
		Log.printInfo("Select Focused Assignment UserName: " + ruleUserName);
	}

	public void clickFocusedAssignmentRuleSaveButton() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentRuleSaveButton_Xpath,LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Focused Assignment Rule Save Button");
	}

	public boolean isRuleVisible(String ruleName) {
		webActions.waitForElementVisibility(getByLocator(peerReviewManagementPageOR.gridRuleName_Xpath, LocatorIdentifier.Xpath, ruleName), 30);
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.gridRuleName_Xpath, LocatorIdentifier.Xpath, ruleName));
	}

	public void clickRuleInGrid(String ruleName) {
		webActions.click(getByLocator(peerReviewManagementPageOR.gridRuleName_Xpath, LocatorIdentifier.Xpath, ruleName));
		Log.printInfo("Rule" + ruleName + " selected from the Grid");
	}

	public void clickEditRuleleButton() {
		webActions.click(getByLocator(peerReviewManagementPageOR.ruleEdit_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked edit rule button");
	}

	public void clickDeleteRuleButton() {
		webActions.click(getByLocator(peerReviewManagementPageOR.deleteRule_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked on Delete rule button");
	}

	public boolean isRuleDeleted(String userName) {
		boolean isRulevisible = false;
		isRulevisible = (webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.gridRuleName_Xpath, LocatorIdentifier.Xpath, userName)));
		return !isRulevisible;
	}

	public void clickDeleteRulePopUpYesButton() {
		webActions.waitForElementVisibility(
				getByLocator(peerReviewManagementPageOR.deleteRulePopup_Xpath, LocatorIdentifier.Xpath), 5);
		webActions.click(
				getByLocator(peerReviewManagementPageOR.deleteRulePopupYesButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked User Role Delete Popup Yes button");
	}

	public boolean isMenuOptionsHeaderVisible(String menuOptionsHeaderName) {
		boolean isRulevisible = false;
		isRulevisible = (webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.menuOptionsHeader_Xpath,LocatorIdentifier.Xpath, menuOptionsHeaderName)));
		return isRulevisible;
	}

	public void setPromptingUserName(String userName) {
		webActions.setText(getByLocator(peerReviewManagementPageOR.promptingSetUserName_Xpath, LocatorIdentifier.Xpath), userName);
		Log.printInfo("Set Prompting User name to " + userName);
	}

	public void selectPromptingUserName(String userName) {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.promptingSelectUser_Xpath, LocatorIdentifier.Xpath, userName));
		Log.printInfo("Selected User Name " + userName);
	}

	public void clickPromptingUserSelectionSaveButton() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.promptingSaveUserSelection_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Save Prompting User Selection");
	}

	public void clickPromptingIndicatorDayDropdownArrow() {
		webActions.click(getByLocator(peerReviewManagementPageOR.promptingIndicatorDayDropdownArrow_Xpath,LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Prompting Indicator Day dropdown");
	}

	public void selectPromptingIndicatorDay(String day) {
		webActions.click(getByLocator(peerReviewManagementPageOR.promptingIndicatorDayDropdownOptions_Xpath,LocatorIdentifier.Xpath, day));
		Log.printInfo("Selected Prompting Indicator Day " + day);
	}

	public void clickPromptingIndicatorFromDropdownArrow() {
		webActions.click(getByLocator(peerReviewManagementPageOR.promptingIndicatorFromDropdownArrow_Xpath,LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Prompting Indicator From dropdown");
	}

	public void selectPromptingIndicatorFromTime(String fromTime) {
		webActions.click(getByLocator(peerReviewManagementPageOR.promptingIndicatorFromDropdownOptions_Xpath,LocatorIdentifier.Xpath, fromTime));
		Log.printInfo("Selected Prompting Indicator From Time " + fromTime);
	}

	public void clickPromptingIndicatorToDropdownArrow() {
		webActions.click(getByLocator(peerReviewManagementPageOR.promptingIndicatorToDropdownArrow_Xpath,LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Prompting Indicator To dropdown");
	}

	public void selectPromptingIndicatorToTime(String toTime) {
		webActions.click(getByLocator(peerReviewManagementPageOR.promptingIndicatorToDropdownOptions_Xpath,LocatorIdentifier.Xpath, toTime));
		Log.printInfo("Selected Prompting Indicator To Time " + toTime);
	}

	public void clickPromptingIndicatorAddButton() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.promptingIndicatorAddButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Added a Prompting Indicator");
	}

	public boolean isPromptingSummaryUserNamePresent(String userName) {
		webActions.scrollToElement(getByLocator(peerReviewManagementPageOR.promptingSummaryUserName_Xpath,LocatorIdentifier.Xpath, userName));
		return webActions.isVisible(getByLocator(peerReviewManagementPageOR.promptingSummaryUserName_Xpath,LocatorIdentifier.Xpath, userName));
	}

	public boolean isPromptingSummaryUserIndicatorPresent(String userName, String indicatorTime) {
		webActions.scrollToElement(getByLocator(peerReviewManagementPageOR.promptingSummaryUserIndicator_Xpath,LocatorIdentifier.Xpath, userName, indicatorTime));
		return webActions.isVisible(getByLocator(peerReviewManagementPageOR.promptingSummaryUserIndicator_Xpath,LocatorIdentifier.Xpath, userName, indicatorTime));
	}

	public void clickPromptingSummaryDeleteUser(String userName) {
		webActions.click(getByLocator(peerReviewManagementPageOR.promptingSummaryDeleteUser_Xpath,LocatorIdentifier.Xpath, userName));
		Log.printInfo("Clicked Delete User button in Prompting Summary");
	}

	public void clickPopUpDeleteYesButton() {
		webActions.click(getByLocator(peerReviewManagementPageOR.deletePopUpYesButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked PopUp Delete Yes button");
		sleep(2, TimeUnit.SECONDS);
	}

	public boolean isPromptingSummaryUserNameDeleted(String userName) {
		boolean userDeleted = false;
		try {
			webActions.scrollToElement(
					getByLocator(peerReviewManagementPageOR.promptingSummaryUserName_Xpath,LocatorIdentifier.Xpath, userName));
			return userDeleted;
		} catch (Exception e) {
			return !userDeleted;
		}
	}
	
	public boolean isMenuOptionTableNameVisible(String menuTableName) {
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.menuOptions_Xpath, LocatorIdentifier.Xpath, menuTableName));
	}
	
	public boolean isPeerReviewAlertParameterNameVisible(String parameterName) {
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.peerReviewAlertParameterNames_Xpath, LocatorIdentifier.Xpath, parameterName));
	}
	
	public boolean isFocusedAssignmentPropertyNameVisible(String propertyName) {
		webActions.scrollToElement(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentPropertyNames_Xpath,LocatorIdentifier.Xpath, propertyName));
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentPropertyNames_Xpath,LocatorIdentifier.Xpath, propertyName));
	}
	
	public boolean isFocusedAssignmentRulesSectionVisible() {
		return webActions.isVisible(
				getByLocator(peerReviewManagementPageOR.focusedAssignmentRulesSection_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void setPeerReviewAlertName(String alertName) {
		webActions.setText(getByLocator(peerReviewManagementPageOR.peerReviewAlertName_Xpath, LocatorIdentifier.Xpath),
				alertName);
		Log.printInfo("Set Peer Review Alert Name " + alertName);
	}


	public void setPeerReviewAlertDescription(String alertDescription) {
		webActions.setText(getByLocator(peerReviewManagementPageOR.peerReviewAlertDescription_Xpath, LocatorIdentifier.Xpath),
				alertDescription);
		Log.printInfo("Set Peer Review Alert Descriptio " + alertDescription);
	}
	
	public void clickPeerReviewAlertSaveButton() {
		webActions.click(
				getByLocator(peerReviewManagementPageOR.peerReviewAlertSaveButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked Peer Review Alert Save Button");
	}
	
	
	public String getPeerReviewAlertName() {
		return webActions.getTextboxValue(
				getByLocator(peerReviewManagementPageOR.peerReviewAlertName_Xpath, LocatorIdentifier.Xpath));
	}

	public String getPeerReviewAlertDescription() {
		return webActions.getTextboxValue(
				getByLocator(peerReviewManagementPageOR.peerReviewAlertDescription_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void setFilterValue(String input) {
		webActions.setText(getByLocator(peerReviewManagementPageOR.searchPeerReviewTextbox_Xpath, LocatorIdentifier.Xpath),input);			
	}
	
	public void clickNoteConfigurationAddButton(String configurationType) {
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationAddButton_Xpath, LocatorIdentifier.Xpath,configurationType));
	}
	
	public void createNoteConfiguration(String configurationType, String name) {
		if(!isNoteConfigurationPresent(name)) {
			webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationRecordName_Xpath, LocatorIdentifier.Xpath,configurationType));
			webActions.setTextActiveElement(getByLocator(peerReviewManagementPageOR.noteConfigurationRecordName_Xpath, LocatorIdentifier.Xpath,configurationType),name);
		}
	}
	
	public void updateNoteConfiguration(String configurationType, String name) {
			webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationRecordName_Xpath, LocatorIdentifier.Xpath,configurationType));
			webActions.setTextActiveElement(getByLocator(peerReviewManagementPageOR.noteConfigurationRecordName_Xpath, LocatorIdentifier.Xpath,configurationType),name);
	}
		
	public void clickNoteConfigurationButton(String button) {
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationButton_Xpath, LocatorIdentifier.Xpath,button));
	}
	
	public void clickPeerReviewNoteConfigurationButton(String button) {
		webActions.click(getByLocator(peerReviewManagementPageOR.peerReviewNoteConfigurationButton_Xpath, LocatorIdentifier.Xpath,button));
	}

	public void checkNoteConfigurationActiveCheckbox(String configuration) {
		if(!webActions.getClassValue(getByLocator(peerReviewManagementPageOR.noteConfigurationActiveCheckboxSelected_Xpath, LocatorIdentifier.Xpath,configuration)).contains("checked")){
			webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationActiveCheckboxSelected_Xpath, LocatorIdentifier.Xpath,configuration));
		}
	}
	
	public void uncheckNoteConfigurationActiveCheckbox(String configuration) {
		if(webActions.getClassValue(getByLocator(peerReviewManagementPageOR.noteConfigurationActiveCheckboxSelected_Xpath, LocatorIdentifier.Xpath,configuration)).contains("checked")){
			webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationActiveCheckboxSelected_Xpath, LocatorIdentifier.Xpath,configuration));
		}
	}
	
	public void clickNoteConfigurationDeleteIcon(String configuration) {
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationDeleteButton_Xpath, LocatorIdentifier.Xpath,configuration));
	}

	public boolean isNoteConfigurationPresent(String name) {
		return webActions.isVisible(getByLocator(peerReviewManagementPageOR.noteConfigurationRecordNameValue_Xpath, LocatorIdentifier.Xpath,name));
	}

	public boolean isMessageVisible(String message) {
		return webActions.isVisible(getByLocator(peerReviewManagementPageOR.displayMessage_Xpath, LocatorIdentifier.Xpath,message));
	}

	public void clickNoteConfigurationRequiredCheckbox(String configuration) {
		if(!webActions.getClassValue(getByLocator(peerReviewManagementPageOR.noteConfigurationRequiredCheckbox_Xpath, LocatorIdentifier.Xpath,configuration)).contains("checked")){
			webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationRequiredCheckbox_Xpath, LocatorIdentifier.Xpath,configuration));
		}
	}
	
	public void uncheckNoteConfigurationRequiredCheckbox(String configuration) {
		if(webActions.getClassValue(getByLocator(peerReviewManagementPageOR.noteConfigurationRequiredCheckbox_Xpath, LocatorIdentifier.Xpath,configuration)).contains("checked")){
			webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationRequiredCheckbox_Xpath, LocatorIdentifier.Xpath,configuration));
		}
	}


	public void openPeerReviewNoteConfigurationDropdownButton(String configuration) {
		webActions.click(getByLocator(peerReviewManagementPageOR.peerReviewNoteConfigurationDropdownButton_Xpath, LocatorIdentifier.Xpath, configuration));
	}
	
	public void removePeerReviewNoteConfigurationDefault(String configuration) {	
		webActions.click(getByLocator(peerReviewManagementPageOR.peerReviewNoteConfigurationDropdown_Xpath, LocatorIdentifier.Xpath, configuration));
		if(webActions.isVisible(getByLocator(peerReviewManagementPageOR.removePeerReviewNoteDefaultValue_Xpath, LocatorIdentifier.Xpath,configuration))) {
			webActions.click(getByLocator(peerReviewManagementPageOR.removePeerReviewNoteDefaultValue_Xpath, LocatorIdentifier.Xpath,configuration));	
		}		
	}

	public boolean isFieldErrorDisplayed(String configuration) {
		return webActions.isVisible(getByLocator(peerReviewManagementPageOR.peerReviewNoteConfigurationFieldError_Xpath, LocatorIdentifier.Xpath,configuration));
	}

	public void clickDefaultValueDropdown(String value, String configuration) {
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationDefaultValueField_Xpath, LocatorIdentifier.Xpath,configuration));
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationDefaultValueDropdownButton_Xpath, LocatorIdentifier.Xpath,configuration));
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationDefaultValueDropdownOptions_Xpath, LocatorIdentifier.Xpath,value));
	}

	public boolean isDefaultValueVisible(String value,String configuraiton) {
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationDefaultValueField_Xpath, LocatorIdentifier.Xpath,configuraiton));
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationDefaultValueDropdownButton_Xpath, LocatorIdentifier.Xpath,configuraiton));
		return webActions.isVisible(getByLocator(peerReviewManagementPageOR.noteConfigurationRecordNameValue_Xpath, LocatorIdentifier.Xpath,value));
	}

	public boolean isDefaultDropdownValueSelected(String configuration, String defaultValue) {
		webActions.click(getByLocator(peerReviewManagementPageOR.peerReviewNoteConfigurationDropdownButton_Xpath, LocatorIdentifier.Xpath, configuration));
		return 	webActions.getClassValue(getByLocator(peerReviewManagementPageOR.noteConfigurationDefaultValueDropdownOptions_Xpath, LocatorIdentifier.Xpath, defaultValue)).contains("selected");
	}

	public void removeNotePreviewDefaultValue(String configuration) {
		webActions.click(getByLocator(peerReviewManagementPageOR.noteConfigurationDefaultValueField_Xpath, LocatorIdentifier.Xpath,configuration));
		if(webActions.isVisible(getByLocator(peerReviewManagementPageOR.removeNotePreviewDefaultValue_Xpath, LocatorIdentifier.Xpath,configuration))) {
			webActions.click(getByLocator(peerReviewManagementPageOR.removeNotePreviewDefaultValue_Xpath, LocatorIdentifier.Xpath, configuration));
		}
	}
	
	public void clickPeerReviewNoteButton(String button) {
		webActions.click(getByLocator(peerReviewManagementPageOR.peerReviewNoteButton_Xpath, LocatorIdentifier.Xpath,button));
	}
	
	public void isNoteDetailsVisible(String noteDetails) {
		webActions.isVisible(getByLocator(peerReviewManagementPageOR.noteDetails_Xpath, LocatorIdentifier.Xpath,noteDetails));
		
	}
}

