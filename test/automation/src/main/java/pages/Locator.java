package pages;

import org.openqa.selenium.By;

public class Locator {


	public By getByLocator(String locator, LocatorIdentifier identifier) {
		
		switch (identifier) {
		case Id:
			return By.id(locator);
		case Xpath:
			return By.xpath(locator);
		case Name:
			return By.name(locator);
		case ClassName:
			return By.className(locator);
		default:
			return By.xpath(locator);
		}
	}
	
	public By getByLocator(String locator, LocatorIdentifier identifier, Object... parameters)
	{
		switch (identifier) {
		case Id:
			return By.id(String.format(locator,parameters));
		case Xpath:
			return By.xpath(String.format(locator,parameters));
		case Name:
			return By.name(String.format(locator,parameters));
		case ClassName:
			return By.className(String.format(locator,parameters));
		default:
			return By.xpath(String.format(locator,parameters));
		}
	}
	
}
