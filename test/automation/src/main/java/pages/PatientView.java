package pages;

import static utilities.Constants.PatientViewORFile;
import static utilities.Constants.USER_WORK_DIR;
import java.util.concurrent.TimeUnit;
import static utilities.Common.sleep;
import java.util.List;

import java.util.ArrayList;
import java.util.Collections;

import Actions.Web;
import objectRepository.PatientViewPageOR;
import utilities.Driver;
import utilities.YmlReader;

public class PatientView extends Locator {
	Web webActions;

	public PatientView(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static PatientViewPageOR PatientViewPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + PatientViewORFile, PatientViewPageOR.class);

	public void clickVirtualLinkInPanel(String panelName, String patientName) {
		webActions.waitForElementClickability(
				getByLocator(PatientViewPageOR.patientViewPanelVirtualLink_Xpath, LocatorIdentifier.Xpath, panelName, patientName), 10);
		webActions.click(getByLocator(PatientViewPageOR.patientViewPanelVirtualLink_Xpath, LocatorIdentifier.Xpath, panelName, patientName));
	}

	public boolean hasDialogLaunched(String dialogTitle) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.dialogName_Xpath, LocatorIdentifier.Xpath, dialogTitle), 10);
		return webActions.isVisible(getByLocator(PatientViewPageOR.dialogName_Xpath, LocatorIdentifier.Xpath, dialogTitle));
	}

	public void clickEditPatientExamHistory(String dialogName) {
		if (dialogName.equalsIgnoreCase("patient history")) {
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.patientHistoryEdit_Xpath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.patientHistoryEdit_Xpath, LocatorIdentifier.Xpath));
		} else {
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examHistoryEdit_Xpath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.examHistoryEdit_Xpath, LocatorIdentifier.Xpath));
		}
	}

	public boolean isPatientExamDetailVisible(String demographicIdentifier, String valueDemographicIdentifier) {
		webActions.waitForElementVisibility(
				getByLocator(
						PatientViewPageOR.patientViewPanelElement_Xpath, LocatorIdentifier.Xpath, demographicIdentifier, valueDemographicIdentifier),
				10);
		return webActions.isVisible(
				getByLocator(
						PatientViewPageOR.patientViewPanelElement_Xpath, LocatorIdentifier.Xpath, demographicIdentifier, valueDemographicIdentifier));
	}

	public void editPatientDetails(String labelName, String textValue) {
		webActions.scrollToElement(getByLocator(PatientViewPageOR.patientHistoryTextControl_Xpath, LocatorIdentifier.Xpath, labelName));
		webActions
				.waitForElementClickability(getByLocator(PatientViewPageOR.patientHistoryTextControl_Xpath, LocatorIdentifier.Xpath, labelName), 10);
		webActions.click(getByLocator(PatientViewPageOR.patientHistoryTextControl_Xpath, LocatorIdentifier.Xpath, labelName));
		webActions.setText(getByLocator(PatientViewPageOR.patientHistoryTextControl_Xpath, LocatorIdentifier.Xpath, labelName), textValue);

	}

	public void clickOnSavePatientExamHistory(String dialogName) {
		if (dialogName.equalsIgnoreCase("patient history")) {
			webActions.scrollToElement(getByLocator(PatientViewPageOR.patientHistorySave_Xpath, LocatorIdentifier.Xpath));
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.patientHistorySave_Xpath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.patientHistorySave_Xpath, LocatorIdentifier.Xpath));
		} else {
			webActions.scrollToElement(getByLocator(PatientViewPageOR.examHistorySave_Xpath, LocatorIdentifier.Xpath));
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examHistorySave_Xpath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.examHistorySave_Xpath, LocatorIdentifier.Xpath));
		}
	}

	public void closePatientExamHistory(String dialogName) {
		if (dialogName.equalsIgnoreCase("patient history")) {
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.patientHistoryClose_Xpath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.patientHistoryClose_Xpath, LocatorIdentifier.Xpath));
		} else {
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examHistoryClose_Xpath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.examHistoryClose_Xpath, LocatorIdentifier.Xpath));
			webActions.scrollToElement(getByLocator(PatientViewPageOR.patientHistoryEdit_Xpath, LocatorIdentifier.Xpath));
		}
	}

	public boolean isFieldAbsence(String fieldName, String fieldValue) {
		webActions.waitForElementVisibility(
				getByLocator(PatientViewPageOR.patientViewPanelElement_Xpath, LocatorIdentifier.Xpath, fieldName, fieldValue), 10);
		return !webActions.isVisible(getByLocator(PatientViewPageOR.patientViewPanelElement_Xpath, LocatorIdentifier.Xpath, fieldName, fieldValue));
	}

	public void panelCollapse_Expand(String panelName, String panelNameValue) {
		if (webActions
				.isVisible(getByLocator(PatientViewPageOR.patientViewPanelExpandBtn_Xpath, LocatorIdentifier.Xpath, panelName, panelNameValue))) {
			webActions.click(getByLocator(PatientViewPageOR.patientViewPanelExpandBtn_Xpath, LocatorIdentifier.Xpath, panelName, panelNameValue));
		}
	}

	public boolean isSiteDetailVisible(String labelName, String labelValue) {
		webActions.waitForElementVisibility(
				getByLocator(PatientViewPageOR.patientViewPanelSiteFields_Xpath, LocatorIdentifier.Xpath, labelName, labelValue), 10);
		return webActions.isVisible(getByLocator(PatientViewPageOR.patientViewPanelSiteFields_Xpath, LocatorIdentifier.Xpath, labelName, labelValue));
	}

	public boolean isPanelLoaded(String panelName) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.patientViewPanelName_Xpath, LocatorIdentifier.Xpath, panelName), 10);
		return webActions.isVisible(getByLocator(PatientViewPageOR.patientViewPanelName_Xpath, LocatorIdentifier.Xpath, panelName));
	}

	public void clickAccessionVirtualLink(String accessionNo) {
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examHistoryElement_XPath, LocatorIdentifier.Xpath, accessionNo), 10);
		webActions.click(getByLocator(PatientViewPageOR.examHistoryElement_XPath, LocatorIdentifier.Xpath, accessionNo));
	}

	public void expandInformationSec() {
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examHistoryExpandInformation_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(PatientViewPageOR.examHistoryExpandInformation_Xpath, LocatorIdentifier.Xpath));
	}

	public void contextMenuRightClick(String siteProcedure) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.patientViewExamRow_Xpath, LocatorIdentifier.Xpath, siteProcedure), 10);
		webActions.rightClick(getByLocator(PatientViewPageOR.patientViewExamRow_Xpath, LocatorIdentifier.Xpath, siteProcedure));
	}

	public boolean isContextMenuItemVisible(String contextMenuItem) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.examContextMenuItem_Xpath, LocatorIdentifier.Xpath, contextMenuItem), 5);
		return webActions.isVisible(getByLocator(PatientViewPageOR.examContextMenuItem_Xpath, LocatorIdentifier.Xpath, contextMenuItem));
	}

	public void dragDropColumn(String dragDropColumn, String targetColumn) {
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examPanelColumnDrag_Xpath, LocatorIdentifier.Xpath, dragDropColumn), 5);
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.examPanelColumnDrop_Xpath, LocatorIdentifier.Xpath, targetColumn), 5);
		webActions.dragDropElementHorizontally(
				getByLocator(PatientViewPageOR.examPanelColumnDrag_Xpath, LocatorIdentifier.Xpath, dragDropColumn),
				getByLocator(PatientViewPageOR.examPanelColumnDrop_Xpath, LocatorIdentifier.Xpath, targetColumn), 5);
	}

	public boolean checkImmediateNextColumn(String columnName, String immediateNextColumn) {
		webActions
				.waitForElementVisibility(getByLocator(PatientViewPageOR.examPanelImmediateNextColumn_Xpath, LocatorIdentifier.Xpath, columnName), 5);
		String getColumnName = webActions
				.getText(getByLocator(PatientViewPageOR.examPanelImmediateNextColumn_Xpath, LocatorIdentifier.Xpath, columnName));
		if (getColumnName.equalsIgnoreCase(immediateNextColumn)) {
			return true;
		} else {
			return false;
		}
	}

	public List<String> fetchColumnValueList(String columnIndex) {
		int colIndex = Integer.parseInt(columnIndex);
		return webActions.getColumnText(
				getByLocator(PatientViewPageOR.examPanelExamTable_Xpath, LocatorIdentifier.Xpath),
				getByLocator(PatientViewPageOR.examPanelExamTableRowIdentifier_Xpath, LocatorIdentifier.Xpath), colIndex);
	}

	public List<String> sortList(String columnIndex) {
		List<String> columnList = fetchColumnValueList(columnIndex);
		Collections.sort(columnList);
		return columnList;
	}

	public void clickToSortColumn(String columnName) {
		webActions.waitForElementsClickability(getByLocator(PatientViewPageOR.examPanelColumnDrop_Xpath, LocatorIdentifier.Xpath, columnName), 10);
		webActions.click(getByLocator(PatientViewPageOR.examPanelColumnDrop_Xpath, LocatorIdentifier.Xpath, columnName));
	}

	public boolean isOrderTabItemVisible(String orderLabel) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.orderTabLabelList_XPath, LocatorIdentifier.Xpath, orderLabel), 5);
		return webActions.isVisible(getByLocator(PatientViewPageOR.orderTabLabelList_XPath, LocatorIdentifier.Xpath, orderLabel));
	}

	public void setExamHistoryInformation(String label, String value) {
			webActions.scrollToElement(getByLocator(PatientViewPageOR.examHistorytextbox_XPath, LocatorIdentifier.Xpath, label));
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examHistorytextbox_XPath, LocatorIdentifier.Xpath, label), 30);
			webActions.setText(getByLocator(PatientViewPageOR.examHistorytextbox_XPath, LocatorIdentifier.Xpath, label), value);
	}

	public boolean isExamInformationVisible(String fieldName, String value) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.examOrderTabValue_XPath, LocatorIdentifier.Xpath, fieldName, value), 10);
		return webActions.isVisible(getByLocator(PatientViewPageOR.examOrderTabValue_XPath, LocatorIdentifier.Xpath, fieldName, value));
	}

	public List<String> getOrderTableList() {
		List<String> elementsText = new ArrayList<String>();
		List<String> listofOptions = webActions.getElementTextList(getByLocator(PatientViewPageOR.orderTabLabelList_XPath, LocatorIdentifier.Xpath));
		for (String arrlistofOption : listofOptions) {
			elementsText.add(arrlistofOption.replaceAll(":", "").trim());
		}
		return elementsText;
	}

	public void mouseHoverOnExamProcedure(String procedureName) {
		for (int i = 0; i <= 2; i++)
		webActions.mouseHover(getByLocator(PatientViewPageOR.examSiteProcedureName_XPath, LocatorIdentifier.Xpath, procedureName));
	}

	public void clickExamGridAction(String iconName) {
		webActions.click(getByLocator(PatientViewPageOR.patientExamGridAction_XPath, LocatorIdentifier.Xpath, iconName));
	}

	public boolean isTemplateLoadedInDication() {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.dictationEditorBlock_XPath, LocatorIdentifier.Xpath), 10);
		return webActions.isVisible(getByLocator(PatientViewPageOR.dictationEditorBlock_XPath, LocatorIdentifier.Xpath));
	}

	public void clickTemplateInExam(String templateName) {
		if (webActions.isVisible(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName))) {
			webActions.click(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
		} else {
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.templateExpandIcon_XPath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.templateExpandIcon_XPath, LocatorIdentifier.Xpath));
			webActions.click(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
		}
	}

	public boolean isMappedTemplateLoaded(String templateName) {
		sleep(2, TimeUnit.SECONDS);
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.templateCollapseIcon_XPath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(PatientViewPageOR.templateCollapseIcon_XPath, LocatorIdentifier.Xpath));
		return webActions.isEnabled(getByLocator(PatientViewPageOR.examTemplateSelected_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void clickReportActionIcon(String iconName) {
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.reportActionIcon_XPath, LocatorIdentifier.Xpath, iconName), 10);
		webActions.click(getByLocator(PatientViewPageOR.reportActionIcon_XPath, LocatorIdentifier.Xpath, iconName));
	}

	public void clickDiscardReportDialog(String optionValue) {
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.discardReportDialog_XPath, LocatorIdentifier.Xpath, optionValue), 10);
		webActions.click(getByLocator(PatientViewPageOR.discardReportDialog_XPath, LocatorIdentifier.Xpath, optionValue));
		sleep(1, TimeUnit.SECONDS);
	}

	public void clickTemplateFavouriteIcon(String templateName) {
		if (webActions.isVisible(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName))) {
			webActions.mouseHover(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
			webActions.click(getByLocator(PatientViewPageOR.templateUnfavouriteIcon_XPath, LocatorIdentifier.Xpath, templateName));
		} else {
			webActions.waitForElementClickability(getByLocator(PatientViewPageOR.templateExpandIcon_XPath, LocatorIdentifier.Xpath), 10);
			webActions.click(getByLocator(PatientViewPageOR.templateExpandIcon_XPath, LocatorIdentifier.Xpath));
			webActions.mouseHover(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
			webActions.click(getByLocator(PatientViewPageOR.templateUnfavouriteIcon_XPath, LocatorIdentifier.Xpath, templateName));
		}
	}

	public List<String> getDicationBlockContentList() {
		List<String> elementsText = new ArrayList<String>();
		List<String> listofOptions = webActions
				.getElementTextList(getByLocator(PatientViewPageOR.dictationBlockContentList_XPath, LocatorIdentifier.Xpath));
		for (String arrlistofOption : listofOptions) {
			elementsText.add(arrlistofOption.trim());
		}
		return elementsText;
	}

	public boolean isTemplateSelectedAsFavourite(String templateName) {
		return webActions.isVisible(getByLocator(PatientViewPageOR.templateFavouriteStarIcon_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void clickTemplateUnfavouriteIcon(String templateName) {
		if (webActions.isVisible(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName))) {
			webActions.mouseHover(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
			webActions.click(getByLocator(PatientViewPageOR.templateFavouriteStarIcon_XPath, LocatorIdentifier.Xpath, templateName));
		} else {
			webActions.click(getByLocator(PatientViewPageOR.templateExpandIcon_XPath, LocatorIdentifier.Xpath));
			webActions.mouseHover(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
			webActions.click(getByLocator(PatientViewPageOR.templateFavouriteStarIcon_XPath, LocatorIdentifier.Xpath, templateName));
		}
	}

	public boolean isTemplateSelectedAsUnfavourite(String templateName) {
		webActions.mouseHover(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
		return webActions.isVisible(getByLocator(PatientViewPageOR.templateUnfavouriteIcon_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public boolean isTemplateSelectedAsDefault(String templateName) {
		webActions.mouseHover(getByLocator(PatientViewPageOR.examTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
		return webActions.isVisible(getByLocator(PatientViewPageOR.templateDefaultIcon_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void expandPatienViewSplitter() {
		if (webActions.getElementWidth(getByLocator(PatientViewPageOR.patientViewPanelWindow_XPath, LocatorIdentifier.Xpath)) < 656)
		webActions.moveElementHorizontally(getByLocator(PatientViewPageOR.patientRightSplitter_Id, LocatorIdentifier.Id), 160);
		sleep(2, TimeUnit.SECONDS);
	}

	public void collapsePatienViewSplitter() {
		if (webActions.getElementWidth(getByLocator(PatientViewPageOR.patientViewPanelWindow_XPath, LocatorIdentifier.Xpath)) > 656)
		webActions.moveElementHorizontally(getByLocator(PatientViewPageOR.patientRightSplitter_Id, LocatorIdentifier.Id), -160);
	}

	public void expandNotePanel() {
		if (webActions.getElementHeight(getByLocator(PatientViewPageOR.notePanel_Id, LocatorIdentifier.Id)) < 175)
			webActions.moveElementVertically(getByLocator(PatientViewPageOR.noteSplitter_Id, LocatorIdentifier.Id), -160);
	}

	public void collapseNotePanel() {
		if (webActions.getElementHeight(getByLocator(PatientViewPageOR.notePanel_Id, LocatorIdentifier.Id)) > 175)
		webActions.moveElementVertically(getByLocator(PatientViewPageOR.noteSplitter_Id, LocatorIdentifier.Id), 200);
	}

	public boolean isSiteDetailVisiblieUnderSite(String value) {
		return webActions.isVisible(getByLocator(PatientViewPageOR.patientViewSiteContent_XPath, LocatorIdentifier.Xpath, value));
	}

	public boolean isContactDetailVisiblieUnderOrdering(String value) {
		return webActions.isVisible(getByLocator(PatientViewPageOR.patienViewOrderingContent_XPath, LocatorIdentifier.Xpath, value));
	}

	public void clickDictationCancelled(String buttonName) {
		sleep(15, TimeUnit.SECONDS);
		webActions.switchToWindow("Clario - Worklist", 3);
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.dictationCancelled_Xpath, LocatorIdentifier.Xpath, buttonName), 15);
		webActions.click(getByLocator(PatientViewPageOR.dictationCancelled_Xpath, LocatorIdentifier.Xpath, buttonName));
		sleep(5, TimeUnit.SECONDS);
	}

	public boolean isDictationDisabled() {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.dictationDisabled_Xpath, LocatorIdentifier.Xpath), 15);
		return webActions.isVisible(getByLocator(PatientViewPageOR.dictationDisabled_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isDictationEnabled() {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.dictationEnabled_Xpath, LocatorIdentifier.Xpath), 15);
		return webActions.isVisible(getByLocator(PatientViewPageOR.dictationEnabled_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isDictationDialogDisplayed(String dialogName) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.dictationDialogVisibile_Xpath, LocatorIdentifier.Xpath, dialogName), 15);
		return webActions.isVisible(getByLocator(PatientViewPageOR.dictationDialogVisibile_Xpath, LocatorIdentifier.Xpath, dialogName));
	}

	public void clickDictationSuspendedOption(String optionValue) {
		sleep(15, TimeUnit.SECONDS);
		webActions.switchToWindow("Clario - Worklist", 3);
		webActions.waitForElementClickability(
				getByLocator(PatientViewPageOR.dictationSuspendedOptions_Xpath, LocatorIdentifier.Xpath, optionValue), 15);
		webActions.click(getByLocator(PatientViewPageOR.dictationSuspendedOptions_Xpath, LocatorIdentifier.Xpath, optionValue));
	}

	public void selectReportAction(String option) {
		sleep(20, TimeUnit.SECONDS);
		webActions.waitForElementsClickability(getByLocator(PatientViewPageOR.reportAction_Xpath, LocatorIdentifier.Xpath, option), 30);
		webActions.click(getByLocator(PatientViewPageOR.reportAction_Xpath, LocatorIdentifier.Xpath, option));
	}

	public void clickToDismissContextMenu() {
		webActions.waitForElementsClickability(getByLocator(PatientViewPageOR.dismissContextMenu_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(PatientViewPageOR.dismissContextMenu_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void selectExamHistoryInformation(String label, String value) {
		webActions.scrollToElement(getByLocator(PatientViewPageOR.examHistorytextbox_XPath, LocatorIdentifier.Xpath, label));
		webActions.waitForElementClickability(getByLocator(PatientViewPageOR.examClickDropDownArrow, LocatorIdentifier.Xpath, label), 30);
		webActions.click(getByLocator(PatientViewPageOR.examClickDropDownArrow, LocatorIdentifier.Xpath, label));
		webActions.selectDropDownElement(getByLocator(PatientViewPageOR.examDropdownValue, LocatorIdentifier.Xpath, value), value);
	}
	
	public void clickDictationAction(String buttonName, String dialogName) {
		sleep(5, TimeUnit.SECONDS);
		webActions.click(getByLocator(PatientViewPageOR.unableToStartDictationButton_Xpath, LocatorIdentifier.Xpath, buttonName, dialogName));
	}
	
	public void clickSignReportActionButton(String buttonName) {
		webActions.waitForElementVisibility(getByLocator(PatientViewPageOR.signReportWindow_XPath, LocatorIdentifier.Xpath, buttonName), 30);
		webActions.click(getByLocator(PatientViewPageOR.signReportWindow_XPath, LocatorIdentifier.Xpath, buttonName));
	}
}
