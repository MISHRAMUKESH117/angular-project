package pages;

import static utilities.Constants.ConfigurationORFile;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Common.sleep;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;

import Actions.Web;
import objectRepository.ConfigurationPageOR;
import utilities.Constants;
import utilities.DBConnection;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.Property;
import utilities.YmlReader;

public class Configuration extends Locator {

	Web webActions;
	private Logger logger = LogManager.getLogger(Worklist.class.getName());
	Property property;
	DBConnection dbConnection;
	private String databasePropertyFile = "DBConnection.properties";

	public Configuration(Driver driver) {
		webActions = new Web(driver.getWebDriver());
		property = new Property();
		dbConnection = new DBConnection();
	}

	private static ConfigurationPageOR ConfigurationPageOR = YmlReader
			.getobjectRepository(USER_WORK_DIR + ConfigurationORFile, ConfigurationPageOR.class);

	public void selectMenuOption(String menuOption) {
			webActions.click(getByLocator(ConfigurationPageOR.expandAllXpath, LocatorIdentifier.Xpath));
			webActions.scrollToElement(getByLocator(ConfigurationPageOR.configurationTableMenu_Xpath, LocatorIdentifier.Xpath, menuOption));
			webActions.waitForElementClickability(getByLocator(ConfigurationPageOR.configurationTableMenu_Xpath, LocatorIdentifier.Xpath, menuOption), 10);
			webActions.click(getByLocator(ConfigurationPageOR.configurationTableMenu_Xpath, LocatorIdentifier.Xpath, menuOption));
	}

	public void selectEditFieldConfigurationDropdownOption(String dropdownOption) {
		webActions.waitForElementClickability(
				getByLocator(ConfigurationPageOR.configurationEditFieldMenu_DropdownArrow_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(ConfigurationPageOR.configurationEditFieldMenu_DropdownArrow_Xpath, LocatorIdentifier.Xpath));

		webActions.waitForElementClickability(
				getByLocator(ConfigurationPageOR.configurationEditFieldMenu_DropdownOptions_Xpath, LocatorIdentifier.Xpath, dropdownOption), 10);
		webActions.click(getByLocator(ConfigurationPageOR.configurationEditFieldMenu_DropdownOptions_Xpath, LocatorIdentifier.Xpath, dropdownOption));
	}

	public void deleteFieldFromEditField(String fieldName) {
		webActions.waitForElementClickability(
				getByLocator(ConfigurationPageOR.configurationEditFieldMenu_Delete_Xpath, LocatorIdentifier.Xpath, fieldName), 10);
		webActions.click(getByLocator(ConfigurationPageOR.configurationEditFieldMenu_Delete_Xpath, LocatorIdentifier.Xpath, fieldName));
		logger.info("Deleted the " + fieldName + "field from the Patient Panel Configuration.");

	}

	public void dragDropFieldToColumn_EditField(String fromFieldName, String toFieldName) {
		webActions.dragDropElementVertically(
				getByLocator(ConfigurationPageOR.configurationEditFieldMenu_EditFieldTable_Xpath, LocatorIdentifier.Xpath, fromFieldName),
				getByLocator(ConfigurationPageOR.configurationEditFieldMenu_TableElement_Xpath, LocatorIdentifier.Xpath, toFieldName), 50);
		logger.info("Added the " + fromFieldName + "field from the Patient Panel Configuration.");
	}

	public boolean isConfigurationMenuHeaderVisible(String menuName) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.configurationMenuHeader_Xpath, LocatorIdentifier.Xpath, menuName));
	}

	public List<String> getConfigurationTableList() {
		sleep(3,TimeUnit.SECONDS);
		return webActions.getElementTextList(getByLocator(ConfigurationPageOR.configurationEditFieldTableList_XPath, LocatorIdentifier.Xpath));
	}
	
	public void selectCustomReportsPageReport(String customReportName) {
		webActions.click(getByLocator(ConfigurationPageOR.customReportsPageCustomReportName_Xpath, LocatorIdentifier.Xpath, customReportName));
	}
	
	public void clickCustomReportsPagePropertiesActiveCheckbox() {
		webActions.click(getByLocator(ConfigurationPageOR.customReportsPagePropertiesActiveCheckbox_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void clickCustomReportsPageBottomPanelButton(String buttonName) {
		webActions.click(getByLocator(ConfigurationPageOR.customReportsPageBottomPanelButtons_Xpath, LocatorIdentifier.Xpath, buttonName, buttonName));
		sleep(1,TimeUnit.SECONDS);
	}
	
	public boolean isCustomReportsPageReportActive(String customReportName) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.customReportsPageCustomReportActive_Xpath, LocatorIdentifier.Xpath, customReportName));
	}
	
	public boolean isCustomReportsPagePropertiesActiveCheckboxChecked() {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.customReportsPagePropertiesActiveCheckboxChecked_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void clickNotePageEditNoteIcon(String noteName) {
		webActions.click(getByLocator(ConfigurationPageOR.notePageEditNoteIcon_Xpath, LocatorIdentifier.Xpath, noteName));
	}
	
	public boolean isNotePageEditNoteWindowVisible(String noteName) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.notePageEditNoteWindow_Xpath, LocatorIdentifier.Xpath, noteName));
	}
	
	public void selectConfigurationMenuOption(String configurationName, String configurationType) {
		if (!webActions.isVisible(getByLocator(ConfigurationPageOR.configurationName_Xpath, LocatorIdentifier.Xpath, configurationName))) {
			webActions.click(getByLocator(ConfigurationPageOR.configurationType_Xpath, LocatorIdentifier.Xpath, configurationType));
			webActions.click(getByLocator(ConfigurationPageOR.configurationName_Xpath, LocatorIdentifier.Xpath, configurationName));
		} else {
			webActions.click(getByLocator(ConfigurationPageOR.configurationName_Xpath, LocatorIdentifier.Xpath, configurationName));
		}
	}
	
	public boolean isRVUTaskPresentInDatabase(String taskName) {
		boolean valuePresent =false;
		try {
			valuePresent = dbConnection.isEntryPresentInDB(
					property.getPropertyValue(databasePropertyFile, "RVUTask_TableName"),
					property.getPropertyValue(databasePropertyFile, "RVUTask_ColumnName"), taskName);
			if(!valuePresent)
				dbConnection.closeDatabaseConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return valuePresent;
	}
	
	public void updateRVUTaskNameInDatabase(String oldTaskName, String newTaskName) {
		try {
			dbConnection.updateEntry(
					property.getPropertyValue(databasePropertyFile, "RVUTask_TableName"),
					property.getPropertyValue(databasePropertyFile, "RVUTask_ColumnName"), oldTaskName, newTaskName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteRVUTaskFromDatabase(String taskName) {
		try {
			dbConnection.deleteEntry(
					property.getPropertyValue(databasePropertyFile, "RVUTask_TableName"),
					property.getPropertyValue(databasePropertyFile, "RVUTask_ColumnName"), taskName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeDatabaseConnection() {
		dbConnection.closeDatabaseConnection();
	}
	
	public void connectToDatabase() {
		dbConnection.createDatabaseConnection();
	}
	
	public void clickWorklistGroupsPageSiteColumn(String worklistGroupName) {
		webActions.click(getByLocator(ConfigurationPageOR.worklistGroupsSiteColumn_Xpath, LocatorIdentifier.Xpath, worklistGroupName));
	}
	
	public void clickWorklistGroupsPageMultiChoiceDropdownArrow() {
		webActions.click(getByLocator(ConfigurationPageOR.multiChoiceDropdownArrow_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void selectWorklistGroupsPageMultiChoiceDropdownValue(String dropdownValue) {
		if(webActions.isVisible(getByLocator(ConfigurationPageOR.multiChoiceDropdownValue_Xpath, LocatorIdentifier.Xpath, dropdownValue)))
			webActions.click(getByLocator(ConfigurationPageOR.multiChoiceDropdownValue_Xpath, LocatorIdentifier.Xpath, dropdownValue));
	}
	
	public void clickWorklistGroupsPageBottomPanelButton(String buttonName) {
		webActions.click(getByLocator(ConfigurationPageOR.worklistGroupsBottomPanelButton_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public boolean isWorklistsGroupsPageSiteAddedForGroup(String worklistGroupName, String siteName) {
		return  webActions.getText(getByLocator(ConfigurationPageOR.worklistGroupsSiteColumn_Xpath, LocatorIdentifier.Xpath, worklistGroupName)).contains(siteName);
	}
	
	public void deSelectWorklistGroupsPageMultiChoiceDropdownValue(String dropdownValue) {
		if(webActions.isVisible(getByLocator(ConfigurationPageOR.multiChoiceSelectedDropdownValue_Xpath, LocatorIdentifier.Xpath, dropdownValue)))
			webActions.click(getByLocator(ConfigurationPageOR.multiChoiceSelectedDropdownValue_Xpath, LocatorIdentifier.Xpath, dropdownValue));
	}
	
	public void clickConfigurationPageHeader(String pageName) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPagesHeader_Xpath, LocatorIdentifier.Xpath, pageName));
	}
	
	public String getWorksheetQuery(String fileName, String sheetName) {
		List<String> cellValues = ExcelOperations.readExcelFromTestDataFolder(Constants.USER_WORK_DIR + Constants.TestDataPath+"\\"+fileName+".xls", sheetName);
		String cellValue = null;
		for (String value : cellValues) {
			if (!value.contains("null"))
				cellValue = cellValues.get(cellValues.indexOf(value));
		}
		return cellValue;
	}
	
	public void setWorksheetQuery(String query) {
		webActions.setText(getByLocator(ConfigurationPageOR.setCustomReportQuery_Xpath, LocatorIdentifier.Xpath), query);
	}
	
	public void setReportName(String name) {
		webActions.setText(getByLocator(ConfigurationPageOR.customReportsPageCreateNewReportName_Xpath, LocatorIdentifier.Xpath), name);
	}
	
	public void setWorksheetName(String worksheetName) {
		webActions.setText(getByLocator(ConfigurationPageOR.customReportsPageWorksheetName_Xpath, LocatorIdentifier.Xpath), worksheetName);
	}
	
	public void dragDropColumnNamesToAddInReport(String sectionName, String columnName) {
		webActions.waitForElementVisibility(getByLocator(ConfigurationPageOR.fromColumnName_Xpath, LocatorIdentifier.Xpath, columnName), 15);
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.fromColumnName_Xpath, LocatorIdentifier.Xpath, columnName));
		webActions.dragDropElementHorizontally(
				getByLocator(ConfigurationPageOR.fromColumnName_Xpath, LocatorIdentifier.Xpath, columnName),
				getByLocator(ConfigurationPageOR.toColumnName_Xpath, LocatorIdentifier.Xpath), 50);
		logger.info("Added the " + columnName + "field from the Search Pool.");
	}
	
	public void clickCustomReportsPageQueryActiveCheckbox() {
		webActions.click(getByLocator(ConfigurationPageOR.customReportsPageQueryActiveCheckbox_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void clickDeleteCustomReportIcon(String reportName) {
		webActions.click(getByLocator(ConfigurationPageOR.customReportDeleteIcon_Xpath, LocatorIdentifier.Xpath, reportName));
	}
	
	public void confirmDeleteCustomReport() {
		webActions.waitForElementVisibility(getByLocator(ConfigurationPageOR.confirmCustomReportDeletion_Xpath, LocatorIdentifier.Xpath), 15);
		webActions.click(getByLocator(ConfigurationPageOR.confirmCustomReportDeletion_Xpath, LocatorIdentifier.Xpath));
		sleep(5,TimeUnit.SECONDS);
		webActions.click(getByLocator(ConfigurationPageOR.confirmCustomReportDeletion_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void navigateToHomePage() {
		webActions.closeTab();
		sleep(3, TimeUnit.SECONDS);
		webActions.switchTab(0);
	}
	
	public boolean isReportDeleted(String customReportName) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.customReportDeleteIcon_Xpath, LocatorIdentifier.Xpath, customReportName));
	}
	
	public void clickRVUTaskPageButton(String buttonName) {
		webActions.click(getByLocator(ConfigurationPageOR.rvuTaskPageButton_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void setRVUTaskPageNewTaskName(String taskName) {
		webActions.click(getByLocator(ConfigurationPageOR.rvuTaskPageNewTaskName_Xpath, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.rvuTaskPageNewTaskName_Xpath, LocatorIdentifier.Xpath), taskName);
	}
	
	public void setRVUTaskPageNewTaskRVUPerMinute(String rvuPerMinute) {
		webActions.click(getByLocator(ConfigurationPageOR.rvuTaskPageNewTaskRVUPerMinute_Xpath, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.rvuTaskPageNewTaskRVUPerMinute_Xpath, LocatorIdentifier.Xpath), rvuPerMinute);
	}
	
	public void setRVUTaskPageNewTaskWorkUnitsPerMinute(String workUnitsPerMinute) {
		webActions.click(getByLocator(ConfigurationPageOR.rvuTaskPageNewTaskWorkUnitsPerMinute_Xpath, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.rvuTaskPageNewTaskWorkUnitsPerMinute_Xpath, LocatorIdentifier.Xpath), workUnitsPerMinute);
	}
	
	public boolean isRVUTaskPageTaskVisible(String taskName) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.rvuTaskPageTaskName_Xpath, LocatorIdentifier.Xpath, taskName));
	}
	
	public void setWorklistNameInWorklistTunning(String worklistName) {
		webActions.setText(getByLocator(ConfigurationPageOR.configurationWorklistTunningWorklistName_Xpath, LocatorIdentifier.Xpath), worklistName);
		sleep(1, TimeUnit.SECONDS);
	}

	public void selectWorklistCheckboxInWorklistTunning(String worklistName) {
		if(!webActions.getClassValue(getByLocator(ConfigurationPageOR.configurationWorklistTunningWorklistCheckBoxClass_Xpath, LocatorIdentifier.Xpath, worklistName)).contains("checked"))
			webActions.click(getByLocator(ConfigurationPageOR.configurationWorklistTunningWorklistCheckBox_Xpath, LocatorIdentifier.Xpath, worklistName));
			sleep(1, TimeUnit.SECONDS);
	}
	
	public void clickCommunicationTypePageButton(String buttonName) {
		webActions.click(getByLocator(ConfigurationPageOR.communicationTypePageButton_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void setCommunicationTypePageNewTypeName(String typeName) {
		webActions.click(getByLocator(ConfigurationPageOR.communicationTypePageNewTypeName_Xpath, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.communicationTypePageNewTypeName_Xpath, LocatorIdentifier.Xpath), typeName);
	}
	
	public void selectCommunicationTypePageNewTypeWaitingAttribute(String attributeCode) {
		webActions.click(getByLocator(ConfigurationPageOR.communicationTypePageNewTypeWaitingColumn_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(ConfigurationPageOR.singleDropdownArrow_Xpath, LocatorIdentifier.Xpath));
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.singleChoiceDropdownValue_Xpath, LocatorIdentifier.Xpath, attributeCode));
		webActions.click(getByLocator(ConfigurationPageOR.singleChoiceDropdownValue_Xpath, LocatorIdentifier.Xpath, attributeCode));
	}
	
	public void selectCommunicationTypePageNewTypeFinalAttribute(String attributeCode) {
		webActions.click(getByLocator(ConfigurationPageOR.communicationTypePageNewTypeFinalColumn_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(ConfigurationPageOR.singleDropdownArrow_Xpath, LocatorIdentifier.Xpath));
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.singleChoiceDropdownValue_Xpath, LocatorIdentifier.Xpath, attributeCode));
		webActions.click(getByLocator(ConfigurationPageOR.singleChoiceDropdownValue_Xpath, LocatorIdentifier.Xpath, attributeCode));
	}
	
	public boolean isCommunicationTypeVisible(String typeName) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.communicationTypePageTypeName_Xpath, LocatorIdentifier.Xpath, typeName));
	}
	
	public boolean isCommunicationTypePresentInDatabase(String typeName) {
		boolean valuePresent = false;
		try {
			valuePresent = dbConnection.isEntryPresentInDB(
					property.getPropertyValue(databasePropertyFile, "CommunicationType_TableName"),
					property.getPropertyValue(databasePropertyFile, "CommunicationType_ColumnName"), typeName);
			if(!valuePresent)
				dbConnection.closeDatabaseConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return valuePresent;
	}
		
	public void deleteCommunicationTypeFromDatabase(int communicationTypeID, String typeName) {
		try {
			dbConnection.deleteEntry(
					property.getPropertyValue(databasePropertyFile, "CommunicationTypeIcon_TableName"),
					property.getPropertyValue(databasePropertyFile, "CommunicationTypeIconCommunicationTypeID_ColumnName"), communicationTypeID);
			
			dbConnection.deleteEntry(
					property.getPropertyValue(databasePropertyFile, "CommunicationType_TableName"),
					property.getPropertyValue(databasePropertyFile, "CommunicationType_ColumnName"), typeName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getCommunicationTypeIDFromDatabase(String typeName) {
		int communicationTypeID = 0;
		try {
			communicationTypeID = dbConnection.getIntegerColumnValueForEntry(
					property.getPropertyValue(databasePropertyFile, "CommunicationType_TableName"),
					property.getPropertyValue(databasePropertyFile, "CommunicationType_ColumnName"), typeName, 1);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return communicationTypeID;
	}

	public void changeProperties(String propertyLabel, String propertyValue) {
		if (propertyLabel.contains("Refresh rate")) {
			webActions.waitForElementClickability(getByLocator(ConfigurationPageOR.propertiesLimitDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel), 10);
			webActions.setText(getByLocator(ConfigurationPageOR.propertiesLimitDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel), propertyValue);
		} else {
			webActions.waitForElementClickability(getByLocator(ConfigurationPageOR.propertiesDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel), 10);
			webActions.click(getByLocator(ConfigurationPageOR.propertiesDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel));
			webActions.selectDropDownElement(getByLocator(ConfigurationPageOR.selectOptionFromDropdown_Xpath, LocatorIdentifier.Xpath, propertyValue), propertyValue);
		}
	}

	public void clickStatusCheckbox() {
		if(!webActions.getClassValue((getByLocator(ConfigurationPageOR.statusCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked"))
			webActions.click(getByLocator(ConfigurationPageOR.statusCheckBox_Xpath, LocatorIdentifier.Xpath));
	}

	public void wrapContent() {	
		if(!webActions.getClassValue((getByLocator(ConfigurationPageOR.wrapContentCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked")) 
			webActions.click(getByLocator(ConfigurationPageOR.wrapContentCheckBox_Xpath, LocatorIdentifier.Xpath));
	}
	
	public boolean isStatusCheckboxSelected() {
		return webActions.getClassValue((getByLocator(ConfigurationPageOR.statusCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked");
		}

	public boolean isWrapContentCheckBoxSelected() {
		return	webActions.getClassValue((getByLocator(ConfigurationPageOR.wrapContentCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked");
		
	}

	public void uncheckStatusCheckbox() {
		if(webActions.getClassValue((getByLocator(ConfigurationPageOR.statusCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked"))
			webActions.click(getByLocator(ConfigurationPageOR.statusCheckBox_Xpath, LocatorIdentifier.Xpath));
	}

	public void uncheckWrapContent() {
		if(webActions.getClassValue((getByLocator(ConfigurationPageOR.wrapContentCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked"))
			webActions.click(getByLocator(ConfigurationPageOR.wrapContentCheckBox_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickMySupportQueueButton(String button) {
		webActions.click(getByLocator(ConfigurationPageOR.buttonSupportQueue_Xpath, LocatorIdentifier.Xpath,button));			
	}
	
	public void checkMySupportQueuePermissionsCheckbox(String permissionName) {
		if(!webActions.getClassValue(getByLocator(ConfigurationPageOR.supportQueueConfigCheckBoxIsChecked_Xpath, LocatorIdentifier.Xpath,permissionName)).contains("checked")) 
			webActions.click(getByLocator(ConfigurationPageOR.supportQueueConfigCheckBox_Xpath, LocatorIdentifier.Xpath,permissionName));			
	}
	
	public void uncheckMySupportQueuePermissionsCheckbox(String permissionName) {
		if(webActions.getClassValue(getByLocator(ConfigurationPageOR.supportQueueConfigCheckBoxIsChecked_Xpath, LocatorIdentifier.Xpath,permissionName)).contains("checked"))
			webActions.click(getByLocator(ConfigurationPageOR.supportQueueConfigCheckBox_Xpath, LocatorIdentifier.Xpath,permissionName));			
	}
	
	public void changeBrowserCompatibility(List<String> browserTypeList) {
		dbConnection.setBrowserCompatability(browserTypeList);
	}

	public void deselectWorklistCheckboxInWorklistTunning(String worklistName) {
		if(webActions.getClassValue(getByLocator(ConfigurationPageOR.configurationWorklistTunningWorklistCheckBoxClass_Xpath, LocatorIdentifier.Xpath, worklistName)).contains("checked"))
		webActions.click(getByLocator(ConfigurationPageOR.configurationWorklistTunningWorklistCheckBox_Xpath, LocatorIdentifier.Xpath, worklistName));
		sleep(1, TimeUnit.SECONDS);
	}
	
	public void setBodyPartsPageAddKeywordTextbox(String keywordName) {
		webActions.setText(getByLocator(ConfigurationPageOR.bodyPartsPageAddNewKeywordTextbox_Xpath, LocatorIdentifier.Xpath), keywordName);
	}
	
	public void clickBodyPartsPageAddNewKeywordAddIcon() {
		webActions.click(getByLocator(ConfigurationPageOR.bodyPartsPageNewKeywordAddButton_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void setBodyPartsPageKeywordSearch(String keywordName) {
		webActions.setText(getByLocator(ConfigurationPageOR.bodyPartsPageKeywordSearchTextbox_Xpath, LocatorIdentifier.Xpath), keywordName);
	}
	
	public boolean isBodyPartsPageKeywordPresentOnSearch(String keywordName) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.bodyPartsPageKeywordSearchResults_Xpath, LocatorIdentifier.Xpath, keywordName));
	}
	
	public void setBodyPartsPageAddBodyPart(String bodyPartName) {
		webActions.setText(getByLocator(ConfigurationPageOR.bodyPartsPageAddNewBodyPartNameTextbox_Xpath, LocatorIdentifier.Xpath), bodyPartName);
	}
	
	public void selectBodyPartsPageKeywordDropdownResult(String keywordName) {
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.bodyPartsPageKeywordSearchCheckbox_Xpath, LocatorIdentifier.Xpath, keywordName));
		webActions.click(getByLocator(ConfigurationPageOR.bodyPartsPageKeywordSearchCheckbox_Xpath, LocatorIdentifier.Xpath, keywordName));
	}
	
	public void clickBodyPartsPageNewBodyPartAddIcon() {
		webActions.click(getByLocator(ConfigurationPageOR.bodyPartsPageNewBodyPartAddButton_Xpath, LocatorIdentifier.Xpath));
	}
	
	public boolean isValuePresentInBodyPartsGrid(String gridValue) {
		return webActions.isVisible(getByLocator(ConfigurationPageOR.bodyPartsPageBodyPartsGridItem_Xpath, LocatorIdentifier.Xpath, gridValue));
	}
	
	public void clickBodyPartsPageKeywordSearchDropdownArrow() {
		webActions.click(getByLocator(ConfigurationPageOR.bodyPartsPageKeywordSearchDropdownArrow_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void clickSitePageSitesGridSiteName(String siteName) {
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.sitePageSitesGridSiteName_Xpath, LocatorIdentifier.Xpath, siteName));
		webActions.click(getByLocator(ConfigurationPageOR.sitePageSitesGridSiteName_Xpath, LocatorIdentifier.Xpath, siteName));
	}
	
	public void clickSitePageSitePropertyMultiChoiceDropdownArrow(String propertyName) {
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.sitePageSitePropertyMultiChoiceDropdownArrow_Xpath, LocatorIdentifier.Xpath, propertyName));
		webActions.click(getByLocator(ConfigurationPageOR.sitePageSitePropertyMultiChoiceDropdownArrow_Xpath, LocatorIdentifier.Xpath, propertyName));
	}
	
	public void selectSitePageSitePropertyMultiChoiceDropdownOption(String dropdownValue) {
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.sitePageSitePropertyMultiChoiceDropdownOptionsCheckbox_Xpath, LocatorIdentifier.Xpath, dropdownValue));
		if(!webActions.isVisible(getByLocator(ConfigurationPageOR.sitePageSitePropertyMultiChoiceDropdownOptionChecked_Xpath, LocatorIdentifier.Xpath, dropdownValue)))
			webActions.click(getByLocator(ConfigurationPageOR.sitePageSitePropertyMultiChoiceDropdownOptionsCheckbox_Xpath, LocatorIdentifier.Xpath, dropdownValue));
	}
	
	public void selectSitePageSitePropertySingleChoiceDropdownValue(String propertyDropdown, String dropdownValue) {
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.sitePageSitePropertySingleChoiceDropdownArrow_Xpath, LocatorIdentifier.Xpath, propertyDropdown));
		webActions.click(getByLocator(ConfigurationPageOR.sitePageSitePropertySingleChoiceDropdownArrow_Xpath, LocatorIdentifier.Xpath, propertyDropdown));
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.sitePageSitePropertySingleChoiceDropdownArrow_Xpath, LocatorIdentifier.Xpath, propertyDropdown));
		webActions.click(getByLocator(ConfigurationPageOR.sitePageSitePropertySingleChoiceDropdownOptions_Xpath, LocatorIdentifier.Xpath, dropdownValue));
	}

	public void clickSitePageSitePropertyBottomButton(String buttonName) {
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.sitePageSitePropertyBottomButtons_Xpath, LocatorIdentifier.Xpath, buttonName));
		webActions.click(getByLocator(ConfigurationPageOR.sitePageSitePropertyBottomButtons_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void clickConfigurationPageButton(String buttonName) {
		webActions.scrollToElement(getByLocator(ConfigurationPageOR.configurationPageButton_Xpath, LocatorIdentifier.Xpath, buttonName));
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageButton_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void setLdapServerName(String ldapName) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapServerName, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapServerName, LocatorIdentifier.Xpath), ldapName);
	}
	public void setLdapServerPort(String port) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapServerPort, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapServerPort, LocatorIdentifier.Xpath), port);
	}
	
	public void setLdapServerDomain(String domain) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapServerDomain, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapServerDomain, LocatorIdentifier.Xpath), domain);
	}
	
	public void setLdapServerAuthGroup(String authGroup) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapServerAuthGroup, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapServerAuthGroup, LocatorIdentifier.Xpath), authGroup);
	}
	
	
	public void setLdapServerAuthUser(String authUser) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapServerAuthUser, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapServerAuthUser, LocatorIdentifier.Xpath), authUser);
	}
	
	public void setLdapServerAuthPassword(String userDomain) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapServerAuthPassword, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapServerAuthPassword, LocatorIdentifier.Xpath), userDomain);
	}
	
	public void setLdapServerUserDomain(String userDomain) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapServerUserDomain, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapServerUserDomain, LocatorIdentifier.Xpath), userDomain);
	}
	
	public void setLdapGroupName(String groupName) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapGroupName, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapGroupName, LocatorIdentifier.Xpath), groupName);
	}
	
	public void setLdapGroupServer(String groupServer) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapGroupServer, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapGroupServer, LocatorIdentifier.Xpath), groupServer);
	}
	
	public void setLdapGroupSite(String site) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapGroupSite, LocatorIdentifier.Xpath));
		webActions.setTextActiveElement(getByLocator(ConfigurationPageOR.configurationPageLdapGroupSite, LocatorIdentifier.Xpath), site);
		//webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapGroupSiteOption, LocatorIdentifier.Xpath,site));
	}
	
	public void setLdapGroupRoles(String role) {
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapGroupRole, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(ConfigurationPageOR.configurationPageLdapGroupRolesDropDownButton, LocatorIdentifier.Xpath));
		webActions.clickCheckboxListElements(getByLocator(ConfigurationPageOR.configurationPageLdapGroupRolesDropDownOptions, LocatorIdentifier.Xpath));
	}

	public void addLicense(List<String> licenses) {
		for(String license: licenses) {
			webActions.click(getByLocator(ConfigurationPageOR.addLicense_Xpath,LocatorIdentifier.Xpath,license));
		}
	}
	
	public void clickButton(String buttonName) {
		webActions.click(getByLocator(ConfigurationPageOR.saveLicensesButton_Xpath,LocatorIdentifier.Xpath,buttonName));
	}

}
