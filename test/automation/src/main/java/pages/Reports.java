package pages;

import static utilities.Common.sleep;
import static utilities.Constants.ReportsPageORFile;
import static utilities.Constants.USER_WORK_DIR;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Actions.Web;
import objectRepository.ReportsPageOR;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.YmlReader;

public class Reports extends Locator {
	Web webActions;
	List<String> elementsText = new ArrayList<String>();
	@SuppressWarnings("unused")
	private Logger logger = LogManager.getLogger(Reports.class.getName());

	public Reports(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static ReportsPageOR ReportsPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + ReportsPageORFile, ReportsPageOR.class);

	public void clickReportsTab() {
		webActions.waitForElementClickability(
				getByLocator(ReportsPageOR.reportsButton_Xpath, LocatorIdentifier.Xpath), 30);
		webActions.click(getByLocator(ReportsPageOR.reportsButton_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isReportsPageDisplayed() {
		webActions.waitForElementVisibility(getByLocator(ReportsPageOR.createReport_Xpath, LocatorIdentifier.Xpath),30);
		 return webActions.isVisible(getByLocator(ReportsPageOR.createReport_Xpath, LocatorIdentifier.Xpath));
	}

	public void selectReportName(String reportName) {
		webActions.waitForElementVisibility(
				getByLocator(ReportsPageOR.reportName_Xpath, LocatorIdentifier.Xpath, reportName), 30);
		webActions.click(getByLocator(ReportsPageOR.reportName_Xpath, LocatorIdentifier.Xpath, reportName));
	}

	public void clickCreateReport() {
		webActions.waitForElementClickability(getByLocator(ReportsPageOR.createReport_Xpath, LocatorIdentifier.Xpath), 30);
		webActions.click(getByLocator(ReportsPageOR.createReport_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isReportDownloadAlertVisible(String reportName) {
		webActions.waitForElementVisibility(
				getByLocator(ReportsPageOR.reportDownloadAlert_Xpath, LocatorIdentifier.Xpath, reportName), 30);
		return webActions.isVisible(
				getByLocator(ReportsPageOR.reportDownloadAlert_Xpath, LocatorIdentifier.Xpath, reportName));
	}

	public void closeReportDownloadAlert() {
		webActions.waitForElementClickability(
				getByLocator(ReportsPageOR.closeReportDownloadAlert_Xpath, LocatorIdentifier.Xpath), 30);
		webActions.click(getByLocator(ReportsPageOR.closeReportDownloadAlert_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isReportDownloadSuccessful(String reportName) {
		webActions.waitForElementVisibility(
				getByLocator(ReportsPageOR.reportDownloadSuccess_Xpath, LocatorIdentifier.Xpath, reportName), 20);
		return webActions.isVisible(
				getByLocator(ReportsPageOR.reportDownloadSuccess_Xpath, LocatorIdentifier.Xpath, reportName));
	}

	public boolean verifyReportInArchive(String reportName) {
		webActions.waitForElementVisibility(
				getByLocator(ReportsPageOR.archiveReport_Xpath, LocatorIdentifier.Xpath, reportName), 30);
		return  webActions.isVisible(
				getByLocator(ReportsPageOR.archiveReport_Xpath, LocatorIdentifier.Xpath, reportName));
	}

	public boolean isDataSheetDownloaded(String fileName) {
		String modifiedFileName = ExcelOperations.appendDateTimeToFilename(fileName, '-', "yyyy-MM-dd");
		sleep(30, TimeUnit.SECONDS);
		return ExcelOperations.isFileDownloaded(System.getProperty("downloadFilepath"), modifiedFileName);
	}

	public void clickReportDownloadIcon(String fileName) {
		webActions.waitForElementClickability(
				getByLocator(ReportsPageOR.reportDownloadIcon_Xpath, LocatorIdentifier.Xpath, fileName, fileName), 15);
		webActions.click(
				getByLocator(ReportsPageOR.reportDownloadIcon_Xpath, LocatorIdentifier.Xpath, fileName, fileName));
	}

	public boolean verifyExcelContents(String label, String value, String fileName) {
		return ExcelOperations.verifyExcelContents(label, value, fileName);
	}

	public void selectEditInputParams(String label, String value) {
			webActions.waitForElementClickability(
					getByLocator(ReportsPageOR.clickdropdown_Xpath, LocatorIdentifier.Xpath, label), 10);
			webActions.click(getByLocator(ReportsPageOR.clickdropdown_Xpath, LocatorIdentifier.Xpath, label));
			webActions.click(
					getByLocator(ReportsPageOR.valueSelect_Xpath, LocatorIdentifier.Xpath, value));
			webActions.click(getByLocator(ReportsPageOR.clickOkAfterValueSelect, LocatorIdentifier.Xpath));
		
	}

	public void clickExamDate(String range) {
		webActions.waitForElementClickability(getByLocator(ReportsPageOR.date_Xpath, LocatorIdentifier.Xpath, range), 20);
		webActions.click(getByLocator(ReportsPageOR.date_Xpath, LocatorIdentifier.Xpath, range));
	}
	
	public boolean verifyEditedReport(String label, String value, String fileName) {
		return ExcelOperations.verifyEditedExcelContents(label, value, fileName);
	}
	
	public void clickReportSaveButton() {
		webActions.waitForElementClickability(getByLocator(ReportsPageOR.saveButton_Xpath, LocatorIdentifier.Xpath), 15);
		webActions.click(getByLocator(ReportsPageOR.saveButton_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void setReportFields(String name, String schedule) {
		webActions.waitForElementClickability(getByLocator(ReportsPageOR.setReportName_XPath, LocatorIdentifier.Xpath), 15);
		webActions.setText(getByLocator(ReportsPageOR.setReportName_XPath, LocatorIdentifier.Xpath), name);
		webActions.waitForElementClickability(getByLocator(ReportsPageOR.reportDropDownArrow_Xpath, LocatorIdentifier.Xpath), 15);
		webActions.click(getByLocator(ReportsPageOR.reportDropDownArrow_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(ReportsPageOR.reportDropDownValue_Xpath, LocatorIdentifier.Xpath, schedule));
	}
	
	public void clickSaveSchedule() {
		webActions.waitForElementClickability(getByLocator(ReportsPageOR.saveScheduleButton_Xpath, LocatorIdentifier.Xpath), 15);
		webActions.click(getByLocator(ReportsPageOR.saveScheduleButton_Xpath, LocatorIdentifier.Xpath));
	}
}
