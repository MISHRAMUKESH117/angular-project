package pages;

import static utilities.Common.sleep;
import static utilities.Constants.AdvancedSearchORFile;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.WorklistPageORFile;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import Actions.Robo;
import Actions.Web;
import objectRepository.AdvancedSearchOR;
import objectRepository.WorklistPageOR;
import utilities.Driver;
import utilities.YmlReader;

public class AdvancedSearch extends Locator {
    Web webActions;
    Robo robotActions;
    private Logger logger = LogManager.getLogger(AdvancedSearch.class.getName());

    public AdvancedSearch(Driver driver) {
    	robotActions = new Robo();
        webActions = new Web(driver.getWebDriver());
    }

    private static AdvancedSearchOR AdvancedSearchOR = YmlReader.getobjectRepository(USER_WORK_DIR + AdvancedSearchORFile, AdvancedSearchOR.class);

    private static WorklistPageOR WorklistPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + WorklistPageORFile, WorklistPageOR.class);

    public boolean isAdvancedSearchLaunched() {
        webActions.waitForElementPresence(getByLocator(AdvancedSearchOR.launchedSearch_Xpath, LocatorIdentifier.Xpath), 30);
        return webActions.isVisible(getByLocator(AdvancedSearchOR.launchedSearch_Xpath, LocatorIdentifier.Xpath));
    }

    public void selectAdvancedSearchType(String module) {
        webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.advanceSearchType_Xpath, LocatorIdentifier.Xpath), 30);
        webActions.click(getByLocator(AdvancedSearchOR.advanceSearchType_Xpath, LocatorIdentifier.Xpath));
        webActions.selectDropDownElement(getByLocator(AdvancedSearchOR.subModuleAdvanceSearch_Xpath, LocatorIdentifier.Xpath, module), module);
    }

    public void expandAdvancedSearchType(String module) {
        if (module.equalsIgnoreCase("Tech QA")) {
            if (module.equalsIgnoreCase("Tech QA"))
                module = "Tech QA Note"; {
                webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.expandIconTechQANote_Xpath, LocatorIdentifier.Xpath, module), 30);
                expandSearchType(
                    getByLocator(AdvancedSearchOR.expandCollapse_Xpath, LocatorIdentifier.Xpath, module),
                    getByLocator(AdvancedSearchOR.expandIconTechQANote_Xpath, LocatorIdentifier.Xpath, module));
            }
        }
    }

    public void setExamAdvancedSearch(String subModuleEnclosure, String label, String value) {
        logger.info("Label value is" + label);
        if (label.equalsIgnoreCase("Status") || label.equalsIgnoreCase("Priority") || label.equalsIgnoreCase("Modality") ||
            label.equalsIgnoreCase("Type") || label.equalsIgnoreCase("Subspeciality") || label.equalsIgnoreCase("Laterlity") ||
            label.equalsIgnoreCase("AI Status") || label.equalsIgnoreCase("External") || label.equalsIgnoreCase("Billing Status") ||
            label.equalsIgnoreCase("Assigned (U)") || label.equalsIgnoreCase("Assigned (G)") || label.equalsIgnoreCase("Category") ||
            label.equalsIgnoreCase("Reason") || label.equalsIgnoreCase("Rating")) {
        	examDropDown(subModuleEnclosure, label, value);
        } 
        else {
            webActions.setText(getByLocator(AdvancedSearchOR.advancedSearchAttribute_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
            webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
        }
    }

    public void setCommAdvancedSearch(String subModuleEnclosure, String label, String value) {
        logger.info("Label value is" + label);
        if (subModuleEnclosure.equalsIgnoreCase("Comm Note") || subModuleEnclosure.equalsIgnoreCase("Comm Assignment")) {
            if (label.equalsIgnoreCase("Status") || label.equalsIgnoreCase("Type") || label.equalsIgnoreCase("Assigned (U)") || label.equalsIgnoreCase("Assigned (G)")) {
                commTaskDropDown(subModuleEnclosure, label, value);
            } 
            else if (label.equalsIgnoreCase("Created By")) {
                webActions.setText(getByLocator(AdvancedSearchOR.communicationTaskSetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            }
            else if (subModuleEnclosure.equalsIgnoreCase("Comm Date - Start")) {
                webActions.setText(getByLocator(AdvancedSearchOR.commuincationDateStart, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Comm Date - End")) {
                webActions.setText(getByLocator(AdvancedSearchOR.commuincationDateEnd, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            }
        } 
        else if (subModuleEnclosure.equalsIgnoreCase("Exam") || subModuleEnclosure.equalsIgnoreCase("Exam Date") || subModuleEnclosure.equalsIgnoreCase("Enterprise"))
            {
               if (label.equalsIgnoreCase("Status") || label.equalsIgnoreCase("Priority") || label.equalsIgnoreCase("Modality") ||
                label.equalsIgnoreCase("Type") || label.equalsIgnoreCase("Site") ||
                label.equalsIgnoreCase("Site Group") || label.equalsIgnoreCase("Location")) {
                commExamEnterpriseDropDown(subModuleEnclosure, label, value);
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Exam") && label.equalsIgnoreCase("Accession")) {
                webActions.setText(getByLocator(AdvancedSearchOR.communicationTaskExamSetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            }
        }
    }

    public void setTaskAdvancedSearch(String subModuleEnclosure, String label, String value) {
        logger.info("Label value is" + label);
        if (subModuleEnclosure.equalsIgnoreCase("Task") || subModuleEnclosure.equalsIgnoreCase("Task Assignment") || subModuleEnclosure.contains("Task Date")) {
            if (label.equalsIgnoreCase("Status") || label.equalsIgnoreCase("Type") || label.equalsIgnoreCase("Assigned (U)") || label.equalsIgnoreCase("Assigned (G)")) {
            	commTaskDropDown(subModuleEnclosure, label, value);

            } 
            else if (label.equalsIgnoreCase("Created By")) {
                webActions.setText(getByLocator(AdvancedSearchOR.communicationTaskSetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Task Date - Start")) {
                webActions.setText(getByLocator(AdvancedSearchOR.taskDateStart, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Task Date - End")) {
                webActions.setText(getByLocator(AdvancedSearchOR.taskDateEnd, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            }
        }
    }

    public void setPeerReviewAdvancedSearch(String subModuleEnclosure, String label, String value) {
        logger.info("Label value is" + label);
        if (subModuleEnclosure.equalsIgnoreCase("Peer Review") || subModuleEnclosure.contains("Peer Review Date") || subModuleEnclosure.contains("Peer Review Assignment")) {
            if (label.equalsIgnoreCase("Status") || label.equalsIgnoreCase("Reason") || label.equalsIgnoreCase("Category") || label.equalsIgnoreCase("Rating") || label.equalsIgnoreCase("Created By") ||
                label.equalsIgnoreCase("Type") || label.equalsIgnoreCase("Modality") || label.equalsIgnoreCase("Priority") || label.equalsIgnoreCase("Subspeciality") ||
                label.equalsIgnoreCase("Laterality") || label.equalsIgnoreCase("Emergency") || label.equalsIgnoreCase("Billing Status")) {
            	PRTechQATeachingNoteDropDown(subModuleEnclosure, label, value);
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Peer Review Assignment - Start")) {
                webActions.setText(getByLocator(AdvancedSearchOR.peerReviewAssignmentDateStart, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Peer Review Assignment - End")) {
                webActions.setText(getByLocator(AdvancedSearchOR.peerReviewAssignmentDateEnd, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Peer Review Date - Start")) {
                webActions.setText(getByLocator(AdvancedSearchOR.peerReviewDateStart, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Peer Review Date - End")) {
                webActions.setText(getByLocator(AdvancedSearchOR.peerReviewDateEnd, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            }
        } 
        else if (label.equalsIgnoreCase("Accession") || label.equalsIgnoreCase("Site Accession") || label.equalsIgnoreCase("Reason")) {
            webActions.setText(getByLocator(AdvancedSearchOR.peerReviewSetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
            webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
        } 
        else {
        	PRTechQATeachingNoteDropDown(subModuleEnclosure, label, value);
        }
    }

    public void setTechQAAdvancedSearch(String subModuleEnclosure, String label, String value) {
        logger.info("Label value is" + label);
        if (subModuleEnclosure.equalsIgnoreCase("Tech QA Note") || subModuleEnclosure.contains("Tech QA Date")) {
            if (label.equalsIgnoreCase("Status") || label.equalsIgnoreCase("Rating")) {
            	techQADropdown(subModuleEnclosure, label, value);
            } 
            else if (label.equalsIgnoreCase("Created By")) {
                webActions.setText(getByLocator(AdvancedSearchOR.TechQASetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Tech QA Date - Start")) {
                webActions.setText(getByLocator(AdvancedSearchOR.TechQADateStart_Xpath, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            } 
            else if (subModuleEnclosure.equalsIgnoreCase("Tech QA Date - End")) {
                webActions.setText(getByLocator(AdvancedSearchOR.TechQADateEnd_Xpath, LocatorIdentifier.Xpath, label), value);
                webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
            }
        } 
        else if (label.equalsIgnoreCase("Accession") || label.equalsIgnoreCase("Site Accession") || label.equalsIgnoreCase("Reason") ||
            label.equalsIgnoreCase("Ordering") || label.equalsIgnoreCase("Attending") || label.equalsIgnoreCase("Tech") || label.equalsIgnoreCase("Resident")) {
            webActions.setText(getByLocator(AdvancedSearchOR.TechQASetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
            webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
        } 
        else {
        	PRTechQATeachingNoteDropDown(subModuleEnclosure, label, value);
        }
    }

    public void setTeachingNoteAdvancedSearch(String subModuleEnclosure, String label, String value) {
        logger.info("Label value is" + label);
        if (subModuleEnclosure.equalsIgnoreCase("Teaching Note")) {
            webActions.setText(getByLocator(AdvancedSearchOR.TechQASetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
            webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
        } 
        else if (subModuleEnclosure.equalsIgnoreCase("Teaching Note - Start")) {
            webActions.setText(getByLocator(AdvancedSearchOR.TeachingNoteDateStart_Xpath, LocatorIdentifier.Xpath, label), value);
            webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
        } 
        else if (subModuleEnclosure.equalsIgnoreCase("Teaching Note - End")) {
            webActions.setText(getByLocator(AdvancedSearchOR.TeachingNoteDateEnd_Xpath, LocatorIdentifier.Xpath, label), value);
            webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
        } 
        else if (label.equalsIgnoreCase("Accession") || label.equalsIgnoreCase("Site Accession") || label.equalsIgnoreCase("Reason") ||
            label.equalsIgnoreCase("Ordering") || label.equalsIgnoreCase("Attending") || label.equalsIgnoreCase("Tech") || label.equalsIgnoreCase("Resident")) {
            webActions.setText(getByLocator(AdvancedSearchOR.TechQASetText_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), value);
            webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));
        } 
        else {
        	PRTechQATeachingNoteDropDown(subModuleEnclosure, label, value);
        }
    }


    public void examDropDown(String subModuleEnclosure, String label, String value) {
        webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.displayCheckbox_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), 15);
        webActions.click(getByLocator(AdvancedSearchOR.displayCheckbox_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label));
        webActions.selectDropDownElement(getByLocator(AdvancedSearchOR.checkBox_Xpath, LocatorIdentifier.Xpath, value), value);
        webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));

    }

    public void commTaskDropDown(String subModuleEnclosure, String label, String value) {
        webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.dropdownTaskCommunication_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), 15);
        webActions.click(getByLocator(AdvancedSearchOR.dropdownTaskCommunication_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label));
        webActions.selectDropDownElement(getByLocator(AdvancedSearchOR.checkBox_Xpath, LocatorIdentifier.Xpath, value), value);
        webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));

    }

    public void PRTechQATeachingNoteDropDown(String subModuleEnclosure, String label, String value) {
        webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.dropdownPeerReview_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), 15);
        webActions.click(getByLocator(AdvancedSearchOR.dropdownPeerReview_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label));
        webActions.selectDropDownElement(getByLocator(AdvancedSearchOR.checkBox_Xpath, LocatorIdentifier.Xpath, value), value);
        webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));

    }

    public void techQADropdown(String subModuleEnclosure, String label, String value) {
        webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.TechQALabels, LocatorIdentifier.Xpath, subModuleEnclosure, label), 15);
        webActions.click(getByLocator(AdvancedSearchOR.TechQALabels, LocatorIdentifier.Xpath, subModuleEnclosure, label));
        webActions.selectDropDownElement(getByLocator(AdvancedSearchOR.TechQADropDown, LocatorIdentifier.Xpath, value), value);
        webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));

    }

    public void commExamEnterpriseDropDown(String subModuleEnclosure, String label, String value) {
        webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.dropdownTaskCommunicationExamEnterprise_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label), 30);
        webActions.click(getByLocator(AdvancedSearchOR.dropdownTaskCommunicationExamEnterprise_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label));
        webActions.selectDropDownElement(getByLocator(AdvancedSearchOR.checkBox_Xpath, LocatorIdentifier.Xpath, value), value);
        webActions.click(getByLocator(AdvancedSearchOR.randomClick_XPath, LocatorIdentifier.Xpath, label));

    }

    public void actionAdvanceSearch(String action) {
        webActions.click(getByLocator(AdvancedSearchOR.searchCountClear_Xpath, LocatorIdentifier.Xpath, action));
        sleep(5, TimeUnit.SECONDS);

    }

    public Integer countResults() {
        webActions.click(getByLocator(AdvancedSearchOR.searchCountClear_Xpath, LocatorIdentifier.Xpath, "Count"));
        sleep(3, TimeUnit.SECONDS);
        String searchResults = webActions.getText(getByLocator(AdvancedSearchOR.searchResultCount_Xpath, LocatorIdentifier.Xpath));
        String searchResult[] = searchResults.split("\\(");
        String removeBrackets[] = searchResult[1].split("\\)");
        String actualCount[] = removeBrackets[0].split("\\s");
        Integer count = Integer.valueOf(actualCount[0].trim());
        webActions.click(getByLocator(AdvancedSearchOR.searchCountClear_Xpath, LocatorIdentifier.Xpath, "Search"));
        verifyCount(count);
        sleep(3, TimeUnit.SECONDS);
        return count;
    }

    public void clearResults() {
        webActions.click(getByLocator(AdvancedSearchOR.searchCountClear_Xpath, LocatorIdentifier.Xpath, "Clear"));
        sleep(5, TimeUnit.SECONDS);
    }

    public void isclearResult(String label, String subModuleEnclosure) {
        String text = webActions.getText(getByLocator(AdvancedSearchOR.advancedSearchAttribute_Xpath, LocatorIdentifier.Xpath, subModuleEnclosure, label));
        text.contentEquals("null");
    }

    public void restore() {
        webActions.click(getByLocator(AdvancedSearchOR.advancedSearch_Xpath, LocatorIdentifier.Xpath));
        sleep(5, TimeUnit.SECONDS);
        webActions.click(getByLocator(WorklistPageOR.expandWorklist_Xpath, LocatorIdentifier.Xpath));
    }

    public void expandSearchType(By locator1, By locator2) {
        boolean collapse = webActions.isCollapse(locator1);
        if (collapse) {
            webActions.click(locator2);
        }
    }

    public boolean verifyCount(Integer count) {
        if (count < 50) {
            Integer actualCount = webActions.getElementCount(getByLocator(AdvancedSearchOR.examRow_Xpath, LocatorIdentifier.Xpath));
            if (actualCount == count)
                return true;
            else
                return false;

        } else
            return false;
    }

    public void enterValueInKeyword(String message, String noteType) {
        webActions.setText(getByLocator(AdvancedSearchOR.noteKeywordsTextbox_XPath, LocatorIdentifier.Xpath, noteType), message);
    }

    public void clickAdvanceSearchButton(String button) {
        webActions.click(getByLocator(AdvancedSearchOR.searchCountClear_Xpath, LocatorIdentifier.Xpath, button));
        sleep(5, TimeUnit.SECONDS);
    }

    public boolean isExamFromAdvanceSearchPresent() {
        webActions.waitForElementPresence(getByLocator(AdvancedSearchOR.advanceSearchResultMeesage_XPath, LocatorIdentifier.Xpath), 30);
        return webActions.isVisible(getByLocator(AdvancedSearchOR.advanceSearchResultMeesage_XPath, LocatorIdentifier.Xpath));
    }
   
    public void selectMultiChoiceDropdownValue(String sectionName, String label, String dropdownValue) {
		webActions.click(getByLocator(AdvancedSearchOR.multiChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, sectionName, label));
		webActions.click(getByLocator(AdvancedSearchOR.multiChoiceDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		webActions.click(getByLocator(AdvancedSearchOR.multiChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, sectionName, label));
	}

	public void selectSingleChoiceDropdownValue(String sectionName, String label, String dropdownValue) {
		webActions.click(getByLocator(AdvancedSearchOR.singleChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, sectionName, label));
		webActions.click(getByLocator(AdvancedSearchOR.singleChoiceDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
	}

	public void setTextValue(String sectionName, String label, String value) {
		webActions.setText(getByLocator(AdvancedSearchOR.advancedSearchAttribute_Xpath, LocatorIdentifier.Xpath, sectionName, label),value);
	}
	
	public void selectExamDateRange(String examDateRange, String subModule) {
		if(subModule.toLowerCase().equals("exam")) {
			webActions.click(getByLocator(AdvancedSearchOR.examDateRangeExamPage_XPath, LocatorIdentifier.Xpath, examDateRange));
		}
		else {
			webActions.click(getByLocator(AdvancedSearchOR.examDateRangeOtherPage_XPath, LocatorIdentifier.Xpath, examDateRange));
		}
	}
	
	public void cancelExtender() {
		webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.zvExtenderCancel_Xpath, LocatorIdentifier.Xpath), 180);
		webActions.click(getByLocator(AdvancedSearchOR.zvExtenderCancel_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(AdvancedSearchOR.shiftCancel_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickDropDownLabel(String label) {
		webActions.click(getByLocator(AdvancedSearchOR.dropDownLabel_Xpath, LocatorIdentifier.Xpath,label));	
	}

	public void enterDropDownOption(String dropdownItem) {
		webActions.setText(getByLocator(AdvancedSearchOR.searchDropDown_Xpath, LocatorIdentifier.Xpath),dropdownItem);
		webActions.click(getByLocator(AdvancedSearchOR.firstCheckBoxDropDown_Xpath, LocatorIdentifier.Xpath));
	}


	public void clickOkResetButton(String action) {
		webActions.click(getByLocator(AdvancedSearchOR.okResetButton_Xpath, LocatorIdentifier.Xpath,action));
	}


	public boolean isSelectedCheckBoxItemVisible(String searchValue) {
		return webActions.isVisible(getByLocator(AdvancedSearchOR.inputCellValue_Xpath, LocatorIdentifier.Xpath,searchValue));
	}
	

	public void clickDropdownPageButton(String pageButton) {
		webActions.click(getByLocator(AdvancedSearchOR.dropDownPaging_Xpath, LocatorIdentifier.Xpath,pageButton));
	}

	public boolean isItemInFirstRowSelectedVisible(String item) {
		return webActions.isVisible(getByLocator(AdvancedSearchOR.firstCheckBoxTextDropDown_Xpath, LocatorIdentifier.Xpath,item));
	}

	public boolean advancedSearchErrorMessage(String message) {
		return webActions.isVisible(getByLocator(AdvancedSearchOR.stringDisplayed_Xpath, LocatorIdentifier.Xpath,message));
	}

	public boolean advancedSearchErrorBox() {
		return webActions.isVisible(getByLocator(AdvancedSearchOR.errorBoxDisplayed_Xpath, LocatorIdentifier.Xpath));	
	}

	public boolean isPopulatedFromFieldCheck(String subModule) {
		if(subModule.toLowerCase().equals("exam")) {
			return webActions.validateDateFormat(getByLocator(AdvancedSearchOR.startDateFieldExamPage_Xpath,LocatorIdentifier.Xpath));
		}
		else {
			return webActions.validateDateFormat(getByLocator(AdvancedSearchOR.startDateFieldOtherPage_Xpath,LocatorIdentifier.Xpath));
		}
	}

	public boolean verifyResultString(String resultString) {
		return webActions.isVisible(getByLocator(AdvancedSearchOR.searchResultCounts_Xpath, LocatorIdentifier.Xpath,resultString));
	}

	public int getResultCount(String resultString) {
		return webActions.returnRegexInt(getByLocator(AdvancedSearchOR.searchResultCounts_Xpath, LocatorIdentifier.Xpath,resultString), "\\d+");
	}

	public void clickFromCalenderMonthBackButton(int numberOfMonths) {
		for(int i=0; i<numberOfMonths;i++) 
			webActions.click(getByLocator(AdvancedSearchOR.calenderBackButton_Xpath,LocatorIdentifier.Xpath));
	}

	public void clickFromCalendarDate(int date) {
		webActions.click(getByLocator(AdvancedSearchOR.calenderFromDate_Xpath,LocatorIdentifier.Xpath,Integer.toString(date)));
	}

	public void clickCalendarIcon(String calendarType) {
		if(!webActions.isVisible(getByLocator(AdvancedSearchOR.calendarIcon_Xpath, LocatorIdentifier.Xpath,calendarType))) {
			webActions.click(getByLocator(AdvancedSearchOR.dropDownExamDate_Xpath, LocatorIdentifier.Xpath));
		}
		webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.calendarIcon_Xpath, LocatorIdentifier.Xpath,calendarType), 25).click();
	}
	
	public void clickToCalenderMonthButton(int loop) {
		for(int i=0; i<loop;i++) {
			webActions.click(getByLocator(AdvancedSearchOR.calenderBackButton_Xpath,LocatorIdentifier.Xpath));
		}
	}

	public void clickToCalendarDate(int date) {
		webActions.click(getByLocator(AdvancedSearchOR.calenderToDate_Xpath,LocatorIdentifier.Xpath,Integer.toString(date)));
	}
	

	public void clickCalendarTodayButton(String calendarType) {
		//if(!webActions.isVisible(getByLocator(AdvancedSearchOR.calendarIcon_Xpath, LocatorIdentifier.Xpath,"To"))) {
		//	webActions.click((getByLocator(AdvancedSearchOR.dropDownExamDate_Xpath,LocatorIdentifier.Xpath)));
		//}
			webActions.waitForElementClickability(getByLocator(AdvancedSearchOR.calendarIcon_Xpath, LocatorIdentifier.Xpath,calendarType), 25).click();
			webActions.click((getByLocator(AdvancedSearchOR.calenderTodayButton_Xpath,LocatorIdentifier.Xpath)));
	}
	
	public void selectTime(String time,String calendarType) {
		webActions.click((getByLocator(AdvancedSearchOR.examDateTimeDropdown_Xpath,LocatorIdentifier.Xpath,calendarType)));
		webActions.click((getByLocator(AdvancedSearchOR.examDateTimeValue_Xpath,LocatorIdentifier.Xpath,time)));		
	}

	public void clearExamDateField(String calendarType) {
		webActions.clearText((getByLocator(AdvancedSearchOR.examDateDateField_Xpath,LocatorIdentifier.Xpath,calendarType)));
	}
	
	public String getCalendarValueAttribute(String calendarType) {
		return webActions.getValueAttribute(getByLocator(AdvancedSearchOR.examDateDateField_Xpath,LocatorIdentifier.Xpath,calendarType));
	}
	
	public boolean dateFieldValueMatchesExpectedDate(String calendarType, int day, int month) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		//DecimalFormat mFormat= new DecimalFormat("00");
		LocalDate todaysDate = LocalDate.now(); 
		LocalDate setDatTimeYear = todaysDate.minusMonths(month).withDayOfMonth(day);
		String expectedDate = dateTimeFormatter.format(setDatTimeYear);
		return webActions.getValueAttribute(getByLocator(AdvancedSearchOR.examDateDateField_Xpath,LocatorIdentifier.Xpath,calendarType)).equals(expectedDate.toString());
	}
	
	public boolean isTimeFieldPresent(String calendarType,String time) {
		return webActions.getValueAttribute(getByLocator(AdvancedSearchOR.examDateTimeField_Xpath,LocatorIdentifier.Xpath,calendarType)).equals(time);
	}
	
	public boolean isTimeFieldEmpty(String calendarType) {
		sleep(1,TimeUnit.SECONDS);
		return webActions.getValueAttribute(getByLocator(AdvancedSearchOR.examDateTimeField_Xpath,LocatorIdentifier.Xpath,calendarType)).equals("");
	}
	
	public boolean isDateFieldEmpty(String calendarType) {
		return webActions.getValueAttribute(getByLocator(AdvancedSearchOR.examDateDateField_Xpath,LocatorIdentifier.Xpath,calendarType)).equals("");
	}

	public boolean isDateFieldTodaysDate(String calendarType) {
		DecimalFormat mFormat= new DecimalFormat("00");
		LocalDate now = LocalDate.now();
		String expectedDate = mFormat.format(now.getMonth().getValue())+"/"+ mFormat.format(now.getDayOfMonth())+"/"+now.getYear();
		return webActions.getValueAttribute(getByLocator(AdvancedSearchOR.examDateDateField_Xpath,LocatorIdentifier.Xpath,calendarType)).matches(expectedDate);
	}

}