package pages;

import static utilities.Common.sleep;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.WorklistPageORFile;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;

import com.google.common.collect.Ordering;

import Actions.Sikuli;
import Actions.Web;
import objectRepository.WorklistPageOR;
import utilities.Constants;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.Log;
import utilities.YmlReader;

public class Worklist extends Locator {
	Web webActions;
	private Logger logger = LogManager.getLogger(Worklist.class.getName());
	Sikuli sikuli;

	public Worklist(Driver driver) {
		webActions = new Web(driver.getWebDriver());
		sikuli = new Sikuli();
	}

	private static WorklistPageOR WorklistPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + WorklistPageORFile, WorklistPageOR.class);

	public boolean isWorklistLoaded() {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.expandWorklist_Xpath, LocatorIdentifier.Xpath), 60);
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.expandWorklist_Xpath, LocatorIdentifier.Xpath), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.expandWorklist_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isZvExtenderDisplayed() {
		webActions.waitForAlert(getByLocator(WorklistPageOR.zvExtenderAlert_Xpath, LocatorIdentifier.Xpath), 60);
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.zvExtenderAlert_Xpath, LocatorIdentifier.Xpath), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.zvExtenderAlert_Xpath, LocatorIdentifier.Xpath));
	}

	public void confirmDeleteWorklist() {
		webActions.click(getByLocator(WorklistPageOR.confirmDeleteWorklist_Xpath, LocatorIdentifier.Xpath));
	}

	public void cancelShift() {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.shiftCancel_Xpath, LocatorIdentifier.Xpath), 30);
		webActions.click(getByLocator(WorklistPageOR.shiftCancel_Xpath, LocatorIdentifier.Xpath));
	}

	public void expandWorklist(String name) {
		webActions.click(getByLocator(WorklistPageOR.worklistExpander_Xpath, LocatorIdentifier.Xpath, name));
	}

	public void setQuickSearchAttribute(String label, String value) {
		if (label.equalsIgnoreCase("site")) {
			if (value.contains("*") || value.contains("%") || value.contains("[a-zA-Z]") || value.contains("[A-Z]"))
				quickSearchDropDownWildcard(label, value);
			else
				quickSearchDropDownSelect(label, value);
		} else {
			webActions.waitForElementClickability(getByLocator(WorklistPageOR.quickSearch_Xpath, LocatorIdentifier.Xpath, label), 30);
			webActions.setText(getByLocator(WorklistPageOR.quickSearch_Xpath, LocatorIdentifier.Xpath, label), value);
		}
	}

	public void quickSearchDropDownSelect(String label, String value) {
		webActions.click(getByLocator(WorklistPageOR.quickSearchAttributeDropdown_Xpath, LocatorIdentifier.Xpath, label));
		webActions.selectDropDownElement(getByLocator(WorklistPageOR.checkBox_Xpath, LocatorIdentifier.Xpath, value), value);
	}

	public void quickSearchDropDownWildcard(String label, String value) {
		webActions.click(getByLocator(WorklistPageOR.quickSearchAttributeDropdown_Xpath, LocatorIdentifier.Xpath, label));
		webActions.selectDropDownWildcard(getByLocator(WorklistPageOR.wildcardCheckBox_Xpath, LocatorIdentifier.Xpath), value);
		webActions.keyPress(getByLocator(WorklistPageOR.quickSearch_Xpath, LocatorIdentifier.Xpath, label), Keys.ENTER);
	}

	public void selectExamType(String type) {
		webActions.click(getByLocator(WorklistPageOR.expandReadingQueue_Xpath, LocatorIdentifier.Xpath));
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.examType_XPath, LocatorIdentifier.Xpath, type), 60);
		webActions.doubleClick(getByLocator(WorklistPageOR.examType_XPath, LocatorIdentifier.Xpath, type));
	}

	public void setAdvancedSearchAttribute(String attribute) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.advanceSearch_Xpath, LocatorIdentifier.Xpath), 30);
		webActions.setText(getByLocator(WorklistPageOR.advanceSearch_Xpath, LocatorIdentifier.Xpath), attribute);
		webActions.keyPress(getByLocator(WorklistPageOR.advanceSearch_Xpath, LocatorIdentifier.Xpath), Keys.ENTER);

	}

	public void setWorklistSearchAttribute(String attribute) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.search_Xpath, LocatorIdentifier.Xpath), 30);
		webActions.setText(getByLocator(WorklistPageOR.search_Xpath, LocatorIdentifier.Xpath), attribute);
		webActions.keyPress(getByLocator(WorklistPageOR.search_Xpath, LocatorIdentifier.Xpath), Keys.ENTER);

	}

	public void selectExam(String exam) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, exam), 60);
		webActions.click(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, exam));
	}

	public boolean isExamOpen(String patient) {
		webActions.waitForElementPresence(getByLocator(WorklistPageOR.examOpen_Xpath, LocatorIdentifier.Xpath, patient), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.examOpen_Xpath, LocatorIdentifier.Xpath, patient));
	}

	public boolean isExamLoaded(String attribute) {
		webActions.waitForElementPresence(getByLocator(WorklistPageOR.examLoaded_Xpath, LocatorIdentifier.Xpath, attribute), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.examLoaded_Xpath, LocatorIdentifier.Xpath, attribute));
	}

	public boolean isCurrentShiftPageLoaded(String dialogName) {
		webActions.waitForElementPresence(getByLocator(WorklistPageOR.selectCurrentShift_Xpath, LocatorIdentifier.Xpath, dialogName), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.selectCurrentShift_Xpath, LocatorIdentifier.Xpath, dialogName));
	}

	public void selectShift(String shiftType, String shift) {
		sleep(1, TimeUnit.SECONDS);
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.shiftTypeText_Xpath, LocatorIdentifier.Xpath), 50);
		webActions.setText(getByLocator(WorklistPageOR.shiftTypeText_Xpath, LocatorIdentifier.Xpath), shiftType);
		logger.info("Selected the Shift Type");
		sleep(1, TimeUnit.SECONDS);
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.shiftText_Xpath, LocatorIdentifier.Xpath), 50);
		webActions.click(getByLocator(WorklistPageOR.shiftText_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(WorklistPageOR.selectReadingRoom_Xpath, LocatorIdentifier.Xpath, shift));
		logger.info("Selected the Shift");

	}

	public void selectLocationReadingRoom(String location, String readingRoom) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.selectReadingLocation_Xpath, LocatorIdentifier.Xpath), 50);
		webActions.setText(getByLocator(WorklistPageOR.selectReadingLocation_Xpath, LocatorIdentifier.Xpath), location);

		logger.info("Selected the Reading Location");
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.dropDownReadingRoom_Xpath, LocatorIdentifier.Xpath), 50);
		webActions.click(getByLocator(WorklistPageOR.dropDownReadingRoom_Xpath, LocatorIdentifier.Xpath));

		webActions.click(getByLocator(WorklistPageOR.selectReadingRoom_Xpath, LocatorIdentifier.Xpath, readingRoom));

		logger.info("Selected the Reading Room");
	}

	public void saveShiftonWorklistLaunch() {
		sleep(3, TimeUnit.SECONDS);
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.shiftSaveButton2_Xpath, LocatorIdentifier.Xpath), 50);
		webActions.click(getByLocator(WorklistPageOR.shiftSaveButton2_Xpath, LocatorIdentifier.Xpath));
		logger.info("Saved the Shift");
	}

	public boolean isSelectedShiftLoaded(String worklistName) {
		webActions.waitForElementPresence(getByLocator(WorklistPageOR.shiftWorklistUnreadBody_Xpath, LocatorIdentifier.Xpath), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.shiftWorklistUnreadBody_Xpath, LocatorIdentifier.Xpath, worklistName));
	}

	public void selectShiftLoadFromWorklist() {
		sleep(1, TimeUnit.SECONDS);
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.selectShiftWorklist_Xpath, LocatorIdentifier.Xpath), 50);
		webActions.click(getByLocator(WorklistPageOR.selectShiftWorklist_Xpath, LocatorIdentifier.Xpath));
		sleep(2, TimeUnit.SECONDS);
	}

	public void selectShiftRadioBtn(String radioBtnSelect) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.radioShiftSelect_Xpath, LocatorIdentifier.Xpath, radioBtnSelect), 10);
		webActions.click(getByLocator(WorklistPageOR.radioShiftSelect_Xpath, LocatorIdentifier.Xpath, radioBtnSelect));
		sleep(1, TimeUnit.SECONDS);
	}

	public void saveShiftFromWorkList() {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.shiftSaveButton1_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(WorklistPageOR.shiftSaveButton1_Xpath, LocatorIdentifier.Xpath));
		sleep(1, TimeUnit.SECONDS);
	}

	public boolean isExamOpenInPatientView() {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.patienViewText_XPath, LocatorIdentifier.Xpath), 60);
		return webActions.isVisible(getByLocator(WorklistPageOR.patienViewText_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isWorklistVisible(String worklistName) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.worklistTree_Xpath, LocatorIdentifier.Xpath, worklistName), 10);
		return webActions.isVisible(getByLocator(WorklistPageOR.worklistTree_Xpath, LocatorIdentifier.Xpath, worklistName));
	}

	public void clickOnWorklist(String worklistName) {
		webActions.scrollToElement(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		webActions.click(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
	}

	public List<String> getWorklistGridSiteNames() {
		return webActions.getElementTextList(getByLocator(WorklistPageOR.workListGridSiteName_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickQuickSearchDropdown(String dropdownName) {
		webActions
				.waitForElementVisibility(getByLocator(WorklistPageOR.quickSearchAttributeDropdown_Xpath, LocatorIdentifier.Xpath, dropdownName), 10);
		webActions.click(getByLocator(WorklistPageOR.quickSearchAttributeDropdown_Xpath, LocatorIdentifier.Xpath, dropdownName));

	}

	public void clickAdvancedSearch() {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.advancedSearch_Xpath, LocatorIdentifier.Xpath), 30);
		if (!webActions.isVisible(getByLocator(WorklistPageOR.advancedSearchHeader, LocatorIdentifier.Xpath)))
			webActions.click(getByLocator(WorklistPageOR.advancedSearch_Xpath, LocatorIdentifier.Xpath));
	}

	public List<String> getQuickSearchSiteDropdownNames() {
		return webActions.getElementTextList(getByLocator(WorklistPageOR.quickSearchSiteDropdownNames_Xpath, LocatorIdentifier.Xpath));
	}

	public void expandAdvancedSearchSection(String sectionName) {
		if (webActions
				.isVisible(getByLocator(WorklistPageOR.advancedSearchSectionDropdownArrowCollapsed_Xpath, LocatorIdentifier.Xpath, sectionName)))
			webActions.click(getByLocator(WorklistPageOR.advancedSearchSectionDropdownArrowCollapsed_Xpath, LocatorIdentifier.Xpath, sectionName));
	}
	
	public void clickAdvancedSearchLabelDropdown(String labelName) {
		webActions.scrollToElement(getByLocator(WorklistPageOR.advancedSearchLabelNameDropdown_Xpath, LocatorIdentifier.Xpath, labelName));
		webActions.click(getByLocator(WorklistPageOR.advancedSearchLabelNameDropdown_Xpath, LocatorIdentifier.Xpath, labelName));
	}

	public List<String> getAdvancedSearchLabelDropdownOptions() {
		return webActions.getElementTextList(getByLocator(WorklistPageOR.advancedSearchLabelDropdownOptions_Xpath, LocatorIdentifier.Xpath));
	}

	public void check_AdvancedSearch_Close() {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.advancedSearch_Xpath, LocatorIdentifier.Xpath), 30);
		if (webActions.isVisible(getByLocator(WorklistPageOR.advancedSearchHeader, LocatorIdentifier.Xpath)))
			webActions.click(getByLocator(WorklistPageOR.advancedSearch_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean check_AdvancedSearch_Open() {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.advancedSearch_Xpath, LocatorIdentifier.Xpath), 30);
		if (webActions.isVisible(getByLocator(WorklistPageOR.advancedSearchHeader, LocatorIdentifier.Xpath)))
			return true;
		else
			return false;
	}

	public void expandWorklist(String worklistName, String worklist) {
		if (!webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName))) {
			webActions.click(getByLocator(WorklistPageOR.worklistTree_Xpath, LocatorIdentifier.Xpath, worklist));
			webActions.click(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		} else {
			webActions.click(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		}
	}

	public boolean isWorklistPresent(String worklistName, String worklistType) {
		boolean isWorklistCreated = false;
		if (!webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName))) {
			webActions.click(getByLocator(WorklistPageOR.worklistTree_Xpath, LocatorIdentifier.Xpath, worklistType));
			isWorklistCreated = webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		} else {
			isWorklistCreated = webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		}
		return isWorklistCreated;
	}

	public boolean isShiftWorklistAdded(String worklistName, String worklist) {
		boolean isShiftWorklistPresent = false;
		sleep(10, TimeUnit.SECONDS);
		if (!webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName))) {
			webActions.click(getByLocator(WorklistPageOR.worklistTree_Xpath, LocatorIdentifier.Xpath, worklist));
			isShiftWorklistPresent = webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		} else {
			isShiftWorklistPresent = webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		}
		return isShiftWorklistPresent;
	}

	public void clickQuickSearchButton() {
		webActions.click(getByLocator(WorklistPageOR.quickSearchButton_Id, LocatorIdentifier.Id));
	}

	public List<String> getWorklistGridPatientNames() {
		return webActions.getElementTextList(getByLocator(WorklistPageOR.workListGridPatientName_Xpath, LocatorIdentifier.Xpath));
	}

	public void rightClickOnExam(String accessionNo) {
		if (!webActions.isVisible(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, accessionNo))) {
			webActions.scrollToElement(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, accessionNo));
			webActions.waitForElementClickability(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, accessionNo), 15);
			webActions.rightClick(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, accessionNo));
		} else {
			webActions.waitForElementClickability(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, accessionNo), 15);
			webActions.rightClick(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, accessionNo));
		}
	}

	public void mouseHoveOnAddNoteOption(String menuOption) {
		webActions.mouseHover(getByLocator(WorklistPageOR.examRightClickMenu_XPath, LocatorIdentifier.Xpath, menuOption));
	}

	public void clickOnNoteSubMenu(String subMenuOption) {
		webActions.click(getByLocator(WorklistPageOR.noteSubMenuItem_XPath, LocatorIdentifier.Xpath, subMenuOption));
		sleep(2, TimeUnit.SECONDS);
	}

	public boolean isExamPresentInHistoryPanel(String accessionNo) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.historyExam_XPath, LocatorIdentifier.Xpath, accessionNo), 60);
		return webActions.isVisible(getByLocator(WorklistPageOR.historyExam_XPath, LocatorIdentifier.Xpath, accessionNo));
	}

	public void clickExamInHistoryPanel(String accessionNo) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.historyExam_XPath, LocatorIdentifier.Xpath, accessionNo), 60);
		webActions.click(getByLocator(WorklistPageOR.historyExam_XPath, LocatorIdentifier.Xpath, accessionNo));
		//webActions.scrollToElement(getByLocator(WorklistPageOR.search_Xpath, LocatorIdentifier.Xpath, accessionNo));
	}

	public void rightClickOnWorkList(String worklistName, String worklistType) {
		if (!webActions.isVisible(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName))) {
			webActions.click(getByLocator(WorklistPageOR.worklistTree_Xpath, LocatorIdentifier.Xpath, worklistType));
			webActions.click(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
			webActions.rightClick(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		} else {
			webActions.click(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
			webActions.rightClick(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName));
		}
	}

	public void clickOnWorklistMenuOption(String menuOption) {
		webActions.click(getByLocator(WorklistPageOR.worklistRightClickMenuOptions_XPath, LocatorIdentifier.Xpath, menuOption));
	}

	public void deleteWorklist(String worklistName) {
		webActions.click(getByLocator(WorklistPageOR.deleteWorklistConfirmation_XPath, LocatorIdentifier.Xpath, worklistName));
	}

	public String getWorklistTreeCount(String worklistName) {
		return webActions.getText(getByLocator(WorklistPageOR.worklistTreeCount_Xpath, LocatorIdentifier.Xpath, worklistName));
	}

	public Integer getWorklistTabCount() {
		String textFromTab = webActions.getText(getByLocator(WorklistPageOR.worklistTabCount_XPath, LocatorIdentifier.Xpath));
		String modifiedtextFromTab[] = textFromTab.split("-");
		String worklistTabCount = modifiedtextFromTab[0];
		Integer worklistTabCountInteger = Integer.parseInt(worklistTabCount);
		return worklistTabCountInteger;
	}

	public Double getWorklistLoadTime() {
		String timeFromTab = webActions.getText(getByLocator(WorklistPageOR.worklistTabLoadTime_Xpath, LocatorIdentifier.Xpath));
		String modifiedTimeFromTab[] = timeFromTab.split(":");
		String extractText[] = modifiedTimeFromTab[1].trim().split("\\(");
		String worklistLoadTime = extractText[0].trim();
		Double worklistLoadTimeInteger = Double.parseDouble(worklistLoadTime);
		return worklistLoadTimeInteger;
	}

	
	public void backToHomePage() {
		webActions.closeTab();
		sleep(3, TimeUnit.SECONDS);
		webActions.switchTab(0);
	}

	public void clickOnExportWorklist() {
		webActions.click(getByLocator(WorklistPageOR.exportWorklist_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isWorklistExported(String worklistName) {
		String fileName = worklistName + " Worklist" + "  -";
		String modifiedWorklistSheetName = ExcelOperations.appendDateTimeToFilename(fileName, '-', "yyyy-MM-dd");
		sleep(10, TimeUnit.SECONDS);
		return ExcelOperations.isFileDownloaded(System.getProperty("downloadFilepath"), modifiedWorklistSheetName);
	}

	public void saveWorklistButton(String worklistName, String button) {
		if (worklistName.contentEquals("Combine Worklist"))
			webActions.click(getByLocator(WorklistPageOR.combineWorklistButton_Xpath, LocatorIdentifier.Xpath, worklistName, button));
		else
			webActions.click(getByLocator(WorklistPageOR.saveWorklistButton_XPath, LocatorIdentifier.Xpath, worklistName, button));
	}

	public boolean verifyPropertiesDialogue() {
		return webActions.isVisible(getByLocator(WorklistPageOR.propertiesDialogue_xpath, LocatorIdentifier.Xpath));
	}

	public void changeProperties(String propertyLabel, String propertyValue) {
		if (propertyLabel.contains("Refresh rate")) {
			webActions.waitForElementClickability(getByLocator(WorklistPageOR.propertiesLimitDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel), 10);
			webActions.setText(getByLocator(WorklistPageOR.propertiesLimitDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel), propertyValue);
		} else {
			webActions.waitForElementClickability(getByLocator(WorklistPageOR.propertiesDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel), 10);
			webActions.click(getByLocator(WorklistPageOR.propertiesDropdownLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel));
			webActions.selectDropDownElement(getByLocator(WorklistPageOR.selectOptionFromDropdown_Xpath, LocatorIdentifier.Xpath, propertyValue), propertyValue);
		}
	}

	public void clickPropetiesCancelChangeClear(String option) {
		webActions.click(getByLocator(WorklistPageOR.propertiesButton_Xpath, LocatorIdentifier.Xpath, option));
	}

	// If statement added as empty values are converted to null
	public boolean verifyPropertiesAreChanged(String propertyLabel, String propertyValue) {
		String actualValue = webActions.getElementAttributeValue(getByLocator(WorklistPageOR.propertiesLabel_Xpath, LocatorIdentifier.Xpath, propertyLabel), "value");
		if(propertyValue.equals("[blank]")) {
			propertyValue=propertyValue.replace("[blank]", "");
		}
		return actualValue.trim().contentEquals(propertyValue.trim());
	}
	
	public boolean verifySortOrder(String columnName, String sortOrder) {
		sleep(3,TimeUnit.SECONDS);
		webActions.click(getByLocator(WorklistPageOR.columnName_Xpath, LocatorIdentifier.Xpath, columnName));
		sleep(2,TimeUnit.SECONDS);
		return webActions.isVisible(getByLocator(WorklistPageOR.columnSortOrder_Xpath, LocatorIdentifier.Xpath, columnName.trim(), sortOrder.trim()));
	}

	public void clickColumnName(String columnName) {
		webActions.click(getByLocator(WorklistPageOR.columnName_Xpath, LocatorIdentifier.Xpath, columnName));
		webActions.click(getByLocator(WorklistPageOR.columnNameSortArrow_Xpath, LocatorIdentifier.Xpath, columnName));
	}

	public void selectProperty(String property) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.sortOrder_Xpath, LocatorIdentifier.Xpath, property), 10);
		webActions.click(getByLocator(WorklistPageOR.sortOrder_Xpath, LocatorIdentifier.Xpath, property));
	}

	public void dragDropWorklists(String worklistName1, String worklistName2) {
		sleep(10, TimeUnit.SECONDS);
		webActions.scrollToElement(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, "Exams in Transcription"));
		webActions.dragDropElementVertically(
				getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName1),
				getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, worklistName2), 2);
	}

	public List<String> getReadingQueueList() {
		return webActions.getElementTextList(getByLocator(WorklistPageOR.readingQueueWorklistList_XPath, LocatorIdentifier.Xpath));
	}

	public void clickWorklistExamRightClickOptions(String rightClickOption) {
		webActions.click(getByLocator(WorklistPageOR.examRightClickMenu_XPath, LocatorIdentifier.Xpath, rightClickOption));
		Log.printInfo("Clicked on " + rightClickOption + " option after right clicking on an exam.");
	}

	public void selectAssignToRadiologistName(String radiologistName) {
		webActions.setText(getByLocator(WorklistPageOR.assignToRadiologistTextbox_Xpath, LocatorIdentifier.Xpath), radiologistName);
		webActions.click(getByLocator(WorklistPageOR.assignToRadiologistDropDownValue_Xpath, LocatorIdentifier.Xpath, radiologistName));
		Log.printInfo("Selected " + radiologistName + " to assign the Peer Review.");
	}

	public void setAssignPeerReviewPopupNoteMessage(String noteMessage) {
		webActions.setText(getByLocator(WorklistPageOR.assignPeerReviewPopupNoteMessage_Xpath, LocatorIdentifier.Xpath), noteMessage);
		Log.printInfo(noteMessage + " is set as Peer Review note message");
	}

	public void clickAssignPeerReviewPopupAssignButton() {
		webActions.click(getByLocator(WorklistPageOR.assignPeerReviewPopupAssignButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Assign button.");
		sleep(1, TimeUnit.SECONDS);
	}

	public boolean verifyPatientLoadedInPeerReviewWindow(String patientName) {
		sleep(3, TimeUnit.SECONDS);
		webActions.switchToWindow("Clario - Peer Review", 3);
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.peerReviewWindowPatientName_Xpath, LocatorIdentifier.Xpath, patientName), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.peerReviewWindowPatientName_Xpath, LocatorIdentifier.Xpath, patientName));
	}

	public void clickBottomButton(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.bottomButton_Xpath, LocatorIdentifier.Xpath, buttonName));
	}

	public void selectPeerReviewWindowNoteDropdownValue(String dropdownName, String dropdownValue) {
		webActions.click(getByLocator(WorklistPageOR.peerReviewWindowNoteSectionDropdownArrows, LocatorIdentifier.Xpath, dropdownName));
		webActions.click(getByLocator(WorklistPageOR.peerReviewWindowNoteSectionDropdownArrowOptions, LocatorIdentifier.Xpath, dropdownValue));
	}

	public String getPeerReviewWindowNoteSectionDropdownValue(String dropdownName) {
		return webActions.getTextboxValue(getByLocator(WorklistPageOR.peerReviewWindowNoteSectionValue_Xpath, LocatorIdentifier.Xpath, dropdownName));
	}

	public boolean isPeerReviewWindowNoteMessageCleared() {
		if (webActions.getTextboxValue(getByLocator(WorklistPageOR.assignPeerReviewPopupNoteMessage_Xpath, LocatorIdentifier.Xpath)).equals(""))
			return true;
		else
			return false;
	}

	public void setDismissPeerReviewReason(String reason) {
		webActions.setText(getByLocator(WorklistPageOR.dismissPeerReviewReasonTextbox_Xpath, LocatorIdentifier.Xpath), reason);
		Log.printInfo("Dismiss Peer Review reason is set to " + reason);
		webActions.click(getByLocator(WorklistPageOR.confirmationPopupYesButton_Xpath, LocatorIdentifier.Xpath));
		webActions.switchToWindow("Clario - Worklist", 3);

	}

	public boolean isExamNotVisibleInWorklist(String examMRNNumber) {
		return !webActions.isVisible(getByLocator(WorklistPageOR.examLoaded_Xpath, LocatorIdentifier.Xpath, examMRNNumber));
	}

	public void switchToTab(String tabName) {
		webActions.switchToWindow("Clario - " + tabName, 3);
	}

	public void deleteExamFromWorklist(String exam) {
		webActions.click(getByLocator(WorklistPageOR.deleteExamFromWorklist_Xpath, LocatorIdentifier.Xpath, exam));
		Log.printInfo("Clicked on delete icon for exam " + exam);
		webActions.click(getByLocator(WorklistPageOR.confirmationPopupYesButton_Xpath, LocatorIdentifier.Xpath));
		sleep(1, TimeUnit.SECONDS);
	}

	public void clickRejectExamFromWorklist(String exam) {
		webActions.click(getByLocator(WorklistPageOR.rejectExamFromWorklist_Xpath, LocatorIdentifier.Xpath, exam));
		Log.printInfo("Clicked on reject icon for exam " + exam);
	}

	public void setRejectPeerReviewReason(String rejectReason) {
		webActions.setText(getByLocator(WorklistPageOR.rejectPeerReviewReasonTextbox_Xpath, LocatorIdentifier.Xpath), rejectReason);
		webActions.click(getByLocator(WorklistPageOR.rejectPeerReviewPopupRejectButton_Xpath, LocatorIdentifier.Xpath));
		sleep(1, TimeUnit.SECONDS);
	}

	public boolean readAllColumnValues(String pattern, int colIndex) {
		boolean bool = false;
		List<String> colValues = webActions.getColumnText(
				getByLocator(WorklistPageOR.examTable_Xpath, LocatorIdentifier.Xpath),
				getByLocator(WorklistPageOR.examTableRowIdentifier_Xpath, LocatorIdentifier.Xpath), colIndex);
		for (String value : colValues) {
			if (value.matches(pattern))
				bool = true;
			else
				bool = false;
		}
		return bool;
	}

	public void checkPeerReviewWindowAutoNextCheckbox() {
		if (!webActions.isVisible(getByLocator(WorklistPageOR.peerReviewWindowAutoNextCheckboxChecked_Xpath, LocatorIdentifier.Xpath)))
			webActions.click(getByLocator(WorklistPageOR.peerReviewWindowAutoNextCheckbox_Xpath, LocatorIdentifier.Xpath));
	}

	public void launchAdvancedSearchFromQuickSearch() {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.quickSearchAdvancedSearchLaunch_Xpath, LocatorIdentifier.Xpath), 15);
		webActions.click(getByLocator(WorklistPageOR.quickSearchAdvancedSearchLaunch_Xpath, LocatorIdentifier.Xpath));
	}

	public void selectDeselectColumn(String colName) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.colNameAddRemove_Xpath, LocatorIdentifier.Xpath, colName), 15);
		webActions.click(getByLocator(WorklistPageOR.colNameAddRemove_Xpath, LocatorIdentifier.Xpath, colName));
	}

	public boolean verifyNewColumnAdded(String colName) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.columnName_Xpath, LocatorIdentifier.Xpath, colName), 20);
		webActions.scrollToElement(getByLocator(WorklistPageOR.columnName_Xpath, LocatorIdentifier.Xpath, colName));
		return webActions.isVisible(getByLocator(WorklistPageOR.columnName_Xpath, LocatorIdentifier.Xpath, colName));
	}

	public boolean verifyColumnRemoved(String colName) {
		return webActions.isVisible(getByLocator(WorklistPageOR.columnName_Xpath, LocatorIdentifier.Xpath, colName));
	}

	public void clickPeerReviewWindowOpenInViewerIcon() {
		webActions.click(getByLocator(WorklistPageOR.peerReviewWindowOpenInViewerButton_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isPatientDetailsDisplayedInViewer(String patientName) {
		return sikuli
				.isElementVisible(ExcelOperations.getFileNameInsideFolder(patientName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
	}

	public void clickOnViewerButton(String buttonName) {
		sikuli.click(ExcelOperations.getFileNameInsideFolder(buttonName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
		sleep(1, TimeUnit.SECONDS);
	}

	public void checkAlwaysLaunchViewerCheckbox(String exam) {
		sleep(3, TimeUnit.SECONDS);
		switchToTab("Peer Review");
		if (!webActions.isVisible(getByLocator(WorklistPageOR.alwaysLaunchViewerCheckboxChecked_Xpath, LocatorIdentifier.Xpath))) {
			webActions.click(getByLocator(WorklistPageOR.alwaysLaunchViewerCheckbox_Xpath, LocatorIdentifier.Xpath));
			clickBottomButton("Cancel");
			switchToTab("Worklist");
			selectExam(exam);
		}
	}

	public boolean isElementVisibleInInteleviewer(String elementName) {
		sleep(3, TimeUnit.SECONDS);
		return sikuli
				.isElementVisible(ExcelOperations.getFileNameInsideFolder(elementName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
	}

	public void sikuliClick() {
		sikuli.click();
	}

	public void mouseHoverStatusColumn(String accessionNo, String status) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.mouseHoverStatus_Xpath, LocatorIdentifier.Xpath, accessionNo, status), 15);
		webActions.mouseHover(getByLocator(WorklistPageOR.mouseHoverStatus_Xpath, LocatorIdentifier.Xpath, accessionNo, status));
	}

	public void clickExamStatusIcon(String accessionNo, String hoverIcon) {
		webActions.click(getByLocator(WorklistPageOR.AssignIcon_Xpath, LocatorIdentifier.Xpath, accessionNo, hoverIcon));
	}

	public void selectAssignDropDownValue(String label, String value) {
		webActions.click(getByLocator(WorklistPageOR.selectUserRadDropDown_Xpath, LocatorIdentifier.Xpath, label));
		webActions.scrollToElement(getByLocator(WorklistPageOR.userRad_Xpath, LocatorIdentifier.Xpath, value));
		webActions.selectDropDownElement(getByLocator(WorklistPageOR.userRad_Xpath, LocatorIdentifier.Xpath, value), value);
	}

	public void setReasonInAssignWindow(String reason) {
		webActions.setText(getByLocator(WorklistPageOR.reasonAssignExam_Xpath, LocatorIdentifier.Xpath), reason);
	}

	public void clickAssignWindowButton(String button) {
		webActions.click(getByLocator(WorklistPageOR.AssignOrUnassign_Xpath, LocatorIdentifier.Xpath, button));
	}

	public void uncheckAlwaysLaunchViewerCheckbox(String exam) {
		sleep(3, TimeUnit.SECONDS);
		switchToTab("Peer Review");
		if (webActions.isVisible(getByLocator(WorklistPageOR.alwaysLaunchViewerCheckboxChecked_Xpath, LocatorIdentifier.Xpath))) {
			webActions.click(getByLocator(WorklistPageOR.alwaysLaunchViewerCheckbox_Xpath, LocatorIdentifier.Xpath));
			clickBottomButton("Cancel");
			switchToTab("Worklist");
			selectExam(exam);
		}
	}
	
	public void selectAutoNextOption(String option, String buttonName) {
		sleep(10, TimeUnit.SECONDS);
		webActions.clickElementByOffSet(getByLocator(WorklistPageOR.myReadingQueueButtons_Xpath, LocatorIdentifier.Xpath, "Start"));
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.selectViewerDictate_Xpath, LocatorIdentifier.Xpath, option), 15);
		webActions.click(getByLocator(WorklistPageOR.selectViewerDictate_Xpath, LocatorIdentifier.Xpath, option));
		webActions.click(getByLocator(WorklistPageOR.advanceSearch_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickOnAutoNextTab(String buttonName) {
		sleep(10, TimeUnit.SECONDS);
		webActions.switchToWindow("Clario - Worklist", 3);
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.clickNext_Xpath, LocatorIdentifier.Xpath, buttonName), 15);
		webActions.click(getByLocator(WorklistPageOR.clickNext_Xpath, LocatorIdentifier.Xpath, buttonName));
	}

	public void stopAutoNext() {
		webActions.click(getByLocator(WorklistPageOR.stopAutonext_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isWorklistLoadedAfterAutoNext() {
		sleep(10, TimeUnit.SECONDS);
		webActions.switchToWindow("Clario - Worklist", 3);
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.expandWorklist_Xpath, LocatorIdentifier.Xpath), 60);
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.expandWorklist_Xpath, LocatorIdentifier.Xpath), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.expandWorklist_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isExamStatusChanged(String accessionNo, String status) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.mouseHoverStatus_Xpath, LocatorIdentifier.Xpath, accessionNo, status), 15);
		if (webActions.isVisible(getByLocator(WorklistPageOR.mouseHoverStatus_Xpath, LocatorIdentifier.Xpath, accessionNo, status)))
			return webActions.isVisible(getByLocator(WorklistPageOR.mouseHoverStatus_Xpath, LocatorIdentifier.Xpath, accessionNo, status));
		else
			webActions.scrollToElement(getByLocator(WorklistPageOR.mouseHoverStatus_Xpath, LocatorIdentifier.Xpath, accessionNo, status));
		return webActions.isVisible(getByLocator(WorklistPageOR.mouseHoverStatus_Xpath, LocatorIdentifier.Xpath, accessionNo, status));
	}

	public boolean isInteleViewerLaunched() {
		sleep(10, TimeUnit.SECONDS);
		return sikuli.isElementVisible(
				ExcelOperations.getFileNameInsideFolder("iVAcknowledgeButton", Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
	}

	public void changeExamStatus(String newStatus) {
		webActions.click(getByLocator(WorklistPageOR.changeStatusDropdown_Xpath, LocatorIdentifier.Xpath));
		webActions.scrollToElement(getByLocator(WorklistPageOR.changeStatusDropdownValue_Xpath, LocatorIdentifier.Xpath, newStatus));
		webActions.click(getByLocator(WorklistPageOR.changeStatusDropdownValue_Xpath, LocatorIdentifier.Xpath, newStatus));
	}

	public void saveChangedStatus(String buttonName) {
		webActions.waitForElementsClickability(getByLocator(WorklistPageOR.saveExamStatusButton_Xpath, LocatorIdentifier.Xpath, buttonName), 15);
		webActions.click(getByLocator(WorklistPageOR.saveExamStatusButton_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public boolean getPatientNameList() {
		return Ordering.natural().isOrdered(webActions.getElementTextList(getByLocator(WorklistPageOR.patienNameList_XPath, LocatorIdentifier.Xpath)));
	}

	public boolean getSiteNameList() {
		return Ordering.natural().isOrdered(webActions.getElementTextList(getByLocator(WorklistPageOR.siteNameList_XPath, LocatorIdentifier.Xpath)));
	}

	public boolean getMrnNumberList() {
		return Ordering.natural().isOrdered(webActions.getElementTextList(getByLocator(WorklistPageOR.mrnNumberList_XPath, LocatorIdentifier.Xpath)));
	}
	
	public void setThresholdValue(String label, String thresholdValue) {
		webActions.setText(getByLocator(WorklistPageOR.propertiesThresholdValue_XPath, LocatorIdentifier.Xpath, label), thresholdValue);
	}

	public boolean isWorklistActivated(String worklistName) {		
		sleep(60,TimeUnit.SECONDS);
		return  webActions.isVisible(getByLocator(WorklistPageOR.deactivatedWorklistName_XPath, LocatorIdentifier.Xpath, worklistName))?true:false;		
	}	
	
	public boolean isCounterActivated(String worklistName, String examCount) {
		return webActions.isVisible(getByLocator(WorklistPageOR.worklistExamCount_XPath, LocatorIdentifier.Xpath, worklistName,examCount))?true:false;
	}
	
	public void clickSkipOrNext(String iconName, String exam) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.clickSkipOrNext_Xpath, LocatorIdentifier.Xpath,exam, iconName), 5);
		webActions.click(getByLocator(WorklistPageOR.clickSkipOrNext_Xpath, LocatorIdentifier.Xpath,exam, iconName));
	}

	public void clearSortOrder(String sortOrder) {
		if(webActions.getTextboxValue(getByLocator(WorklistPageOR.propertiesLimitDropdownLabel_Xpath, LocatorIdentifier.Xpath, sortOrder)).length()>1) {
			webActions.click(getByLocator(WorklistPageOR.propertiesLimitDropdownLabel_Xpath, LocatorIdentifier.Xpath, sortOrder));
			webActions.click(getByLocator(WorklistPageOR.sortOrderDelete_Xpath, LocatorIdentifier.Xpath, sortOrder));
		}		
	}
	
	public void clickReadingQueueButton(String buttonName) {
		webActions.waitForElementsClickability(getByLocator(WorklistPageOR.readingQueueButton_XPath, LocatorIdentifier.Xpath, buttonName),60);
		webActions.click(getByLocator(WorklistPageOR.readingQueueButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void selectWorklistFromCreateMRQ(String subWorklistName, String worklistName) {
		if (!webActions.getClassValue(getByLocator(WorklistPageOR.createSupportCheckboxQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName))
				.contains("checked")) {
			if (webActions.isVisible(getByLocator(WorklistPageOR.createReadingQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName))) {
				webActions.click(getByLocator(WorklistPageOR.createReadingQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			} else {
				webActions.click(getByLocator(WorklistPageOR.createReadingQueueNode_XPath, LocatorIdentifier.Xpath, worklistName));
				webActions.click(getByLocator(WorklistPageOR.createReadingQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			}
		}
	}
	
	public void clickCreateMRQActionButton(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.createReadingQueueButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public boolean isWorklistAddedInMRQ(String subWorklistName) {		
		return webActions.isVisible(getByLocator(WorklistPageOR.myReadingQueueWorklist_XPath, LocatorIdentifier.Xpath, subWorklistName));
	}
	
	public void refreshPage() {
		webActions.refreshPage();
	}
	
	public void clickWorklistPropertyTab(String tabName) {
		webActions.click(getByLocator(WorklistPageOR.worklistPropertyTab_XPath, LocatorIdentifier.Xpath,tabName));
	}
	
	public void clickActiveTimeCheckbox() {		
			webActions.click(getByLocator(WorklistPageOR.activeTimeCheckbox_XPath, LocatorIdentifier.Xpath));
	}
	
	public void selectActiveTimeDay(String day) {
		webActions.click(getByLocator(WorklistPageOR.activeTimeDayLabel_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(WorklistPageOR.multiChoiceDropdownValue_XPath, LocatorIdentifier.Xpath, day));
		webActions.click(getByLocator(WorklistPageOR.activeTimeDayLabel_XPath, LocatorIdentifier.Xpath));
	}
	
	public String setActiveTimeRange(String label, Integer minutesToAdd) {
		if (webActions.getTimeAddMiutes(minutesToAdd).charAt(0) == '0') {
			webActions.setText(
					getByLocator(WorklistPageOR.activeTimeRangeLabel_XPath, LocatorIdentifier.Xpath, label),
					webActions.getTimeAddMiutes(minutesToAdd).substring(1));
			return webActions.getTimeAddMiutes(minutesToAdd).substring(1);
		} else {
			webActions.setText(
					getByLocator(WorklistPageOR.activeTimeRangeLabel_XPath, LocatorIdentifier.Xpath, label),
					webActions.getTimeAddMiutes(minutesToAdd));
			return webActions.getTimeAddMiutes(minutesToAdd);
		}
	}

	public void clickActiveNewTimeIcon() {
		webActions.click(getByLocator(WorklistPageOR.activeNewTime_XPath, LocatorIdentifier.Xpath));
	}
	
	public void deleteAddedActiveTime(String day) {
		webActions.click(getByLocator(WorklistPageOR.deleteActiveTime_XPath, LocatorIdentifier.Xpath, day));
	}
	
	public boolean verifyPatientLoadedInPeerReviewWindow(String patientName,int windowNumber) {
		sleep(3, TimeUnit.SECONDS);
		webActions.switchToWindow("Clario - Peer Review", windowNumber);
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.peerReviewWindowPatientName_Xpath, LocatorIdentifier.Xpath, patientName), 30);
		return webActions.isVisible(getByLocator(WorklistPageOR.peerReviewWindowPatientName_Xpath, LocatorIdentifier.Xpath, patientName));
	}


	public void clickCreateQueueQueueButton(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.createQueueButton_Xpath, LocatorIdentifier.Xpath, buttonName));
		webActions.click(getByLocator(WorklistPageOR.createQueue_XPath, LocatorIdentifier.Xpath, buttonName));
		
	}
	
	public void selectWorklistFromCreateQueue(String subWorklistName, String worklistName) {
		if (webActions.isVisible(getByLocator(WorklistPageOR.createReadingQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName))) {
			webActions.click(getByLocator(WorklistPageOR.createQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
		} else {
			webActions.click(getByLocator(WorklistPageOR.createReadingQueueNode_XPath, LocatorIdentifier.Xpath, worklistName));
			webActions.click(getByLocator(WorklistPageOR.createQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
		}
	}
	
	public void switchToTab(String tabName,int tabNumber) {
		webActions.switchToWindow("Clario - " + tabName, tabNumber);
	}

	public void shiftButton(String shift) {
		webActions.click(getByLocator(WorklistPageOR.shiftButton_XPath, LocatorIdentifier.Xpath, shift));	
	}
	
	public void rightClickOnQueue(String queueName) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.queueMenuOption_Xpath, LocatorIdentifier.Xpath, queueName),40);
		webActions.rightClick(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, queueName));
	}
	
	public boolean queueName(String queueName) {
		return webActions.isVisible(getByLocator(WorklistPageOR.queueName_XPath, LocatorIdentifier.Xpath, queueName));
	}
	
	public void clickOnQueue(String queueName) {
		webActions.click(getByLocator(WorklistPageOR.worklistName_Xpath, LocatorIdentifier.Xpath, queueName));
	}

	public void clickPropetiesQueueButton(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.queuePropertiesButton_Xpath, LocatorIdentifier.Xpath, buttonName));
		
	}

	public void clickQueueMenuOption(String menuOption) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.queueMenuOption_Xpath, LocatorIdentifier.Xpath, menuOption),10);
		webActions.click(getByLocator(WorklistPageOR.queueMenuOption_Xpath, LocatorIdentifier.Xpath, menuOption));
	}

	public void clickStatusCheckbox() {
		if(!webActions.getClassValue((getByLocator(WorklistPageOR.statusCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked")) 
			webActions.click(getByLocator(WorklistPageOR.statusCheckBox_Xpath, LocatorIdentifier.Xpath));
	}

	public void wrapContent() {	
		if(!webActions.getClassValue((getByLocator(WorklistPageOR.wrapContentCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked")) 
			webActions.click(getByLocator(WorklistPageOR.wrapContentCheckBox_Xpath, LocatorIdentifier.Xpath));
	}
	
	public boolean isStatusCheckboxSelected() {
		return webActions.getClassValue((getByLocator(WorklistPageOR.statusCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked");
	}

	public boolean isWrapContentCheckBoxSelected() {
		return	webActions.getClassValue((getByLocator(WorklistPageOR.wrapContentCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked");
	}

	public void uncheckStatusCheckbox() {
		if(webActions.getClassValue((getByLocator(WorklistPageOR.statusCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked"))
			webActions.click(getByLocator(WorklistPageOR.statusCheckBox_Xpath, LocatorIdentifier.Xpath));
	}

	public void uncheckWrapContent() {
		if(webActions.getClassValue((getByLocator(WorklistPageOR.wrapContentCheckBoxIsSelected_Xpath, LocatorIdentifier.Xpath))).contains("checked")) {
			webActions.click(getByLocator(WorklistPageOR.wrapContentCheckBox_Xpath, LocatorIdentifier.Xpath));
		}
	}

	public void timeShiftDeleteIcon() {		
		webActions.click(getByLocator(WorklistPageOR.timeShiftDeleteIcon_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickSubscribeButtonQueue(String button) {
		webActions.click(getByLocator(WorklistPageOR.queueSubscribeButton_Xpath, LocatorIdentifier.Xpath,button));	
	}
	
	public boolean displayedString_Xpath(String message) {
		return webActions.isVisible(getByLocator(WorklistPageOR.displayedString_Xpath, LocatorIdentifier.Xpath,message));
	}

	public void clickMyQueueRefreshIcon() {
		webActions.click(getByLocator(WorklistPageOR.supportQueueRefresh_Xpath, LocatorIdentifier.Xpath));	
	}

	public boolean checkBoxItemSelected(String checkBoxName) {
		return webActions.getClassValue(getByLocator(WorklistPageOR.supportQueueCheckBox_Xpath, LocatorIdentifier.Xpath, checkBoxName)).contains("checked");
	}

	public void selectShiftDropdown(String value) {
		webActions.click(getByLocator(WorklistPageOR.selectShiftDropdownDropdownButton_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(WorklistPageOR.selectShiftValueDropdownOptions_Xpath, LocatorIdentifier.Xpath,value));
	}
	
	
	public void clickCreateSupportQueueButton(String buttonName) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.createSupportQueueButton_XPath, LocatorIdentifier.Xpath, buttonName),30);
		webActions.mouseHover(getByLocator(WorklistPageOR.createSupportQueueButton_XPath, LocatorIdentifier.Xpath, buttonName));
		webActions.click(getByLocator(WorklistPageOR.createSupportQueueButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void checkWorklistCheckBoxFromCreateQueue(String subWorklistName, String worklistName) {
		if(!webActions.getClassValue(getByLocator(WorklistPageOR.createSupportCheckboxQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName)).contains("checked")){
			if (webActions.isVisible(getByLocator(WorklistPageOR.createSupportQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName))) {
				webActions.click(getByLocator(WorklistPageOR.createQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			} else {
				webActions.click(getByLocator(WorklistPageOR.createSupportQueueNode_XPath, LocatorIdentifier.Xpath, worklistName));
				webActions.click(getByLocator(WorklistPageOR.createQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			}
		}
	}
	
	public void uncheckWorklistCheckBoxFromCreateQueue(String subWorklistName, String worklistName) {
		if(webActions.getClassValue(getByLocator(WorklistPageOR.createSupportCheckboxQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName)).contains("checked")){
			if (webActions.isVisible(getByLocator(WorklistPageOR.createSupportQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName))) {
				webActions.click(getByLocator(WorklistPageOR.createQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			} else {
				webActions.click(getByLocator(WorklistPageOR.createSupportQueueNode_XPath, LocatorIdentifier.Xpath, worklistName));
				webActions.click(getByLocator(WorklistPageOR.createQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			}
		}
	}
	
	public void expandWorklistNode(String worklistName) {
		if (!webActions.getClassValue((getByLocator(WorklistPageOR.expandedWorklistNode_XPath, LocatorIdentifier.Xpath))).contains("expanded"))
		webActions.click(getByLocator(WorklistPageOR.expandWorklistNode_XPath, LocatorIdentifier.Xpath, worklistName));
	}
	
	public void expandHistoryPanel() {
		if (webActions.getElementHeight(getByLocator(WorklistPageOR.historyPanel_Id, LocatorIdentifier.Id)) >= 424)
			webActions.moveElementVertically(getByLocator(WorklistPageOR.historySplitter_Id, LocatorIdentifier.Id), -120);
	}
	
	public void collpaseHistoryPanel() {
		if (webActions.getElementHeight(getByLocator(WorklistPageOR.historyPanel_Id, LocatorIdentifier.Id)) < 424)
			webActions.moveElementVertically(getByLocator(WorklistPageOR.historySplitter_Id, LocatorIdentifier.Id), 100);
	}
	
	public boolean isMRQWorklistActivated(String worklistName) {
		sleep(60, TimeUnit.SECONDS);
		return webActions.isVisible(getByLocator(WorklistPageOR.mrqDeactivedWorklistName_XPath, LocatorIdentifier.Xpath, worklistName)) ? true
				: false;
	}
	
	public void deselectWorklistFromCreateMRQ(String subWorklistName, String worklistName) {
		if (webActions.getClassValue(getByLocator(WorklistPageOR.createSupportCheckboxQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName))
				.contains("checked")) {
			if (webActions.isVisible(getByLocator(WorklistPageOR.createReadingQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName))) {
				webActions.click(getByLocator(WorklistPageOR.createReadingQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			} else {
				webActions.click(getByLocator(WorklistPageOR.createReadingQueueNode_XPath, LocatorIdentifier.Xpath, worklistName));
				webActions.click(getByLocator(WorklistPageOR.createReadingQueueSubNode_XPath, LocatorIdentifier.Xpath, subWorklistName));
			}
		}
	}
	
	public boolean isAssignedIconDisplayed(String accessionNo) {
		return webActions.isVisible(getByLocator(WorklistPageOR.assignedIcon_Xpath, LocatorIdentifier.Xpath, accessionNo)) ? true : false;
	}
	
	public void mouseHoverAssignedIcon(String accessionNo) {
		webActions.mouseHover(getByLocator(WorklistPageOR.assignedIcon_Xpath, LocatorIdentifier.Xpath, accessionNo));
	}
	
	public boolean isAssignedTooltipPresent(String tooltipLabel, String tooltipMessage) {
		return webActions.isVisible(getByLocator(WorklistPageOR.assignedTooltipValue_Xpath, LocatorIdentifier.Xpath, tooltipLabel, tooltipMessage)) ? true : false;
	}
	
	public void doubleClickExamInHistoryPanel(String accessionNo) {
		webActions.waitForElementVisibility(getByLocator(WorklistPageOR.historyExam_XPath, LocatorIdentifier.Xpath, accessionNo), 60);
		webActions.doubleClick(getByLocator(WorklistPageOR.historyExam_XPath, LocatorIdentifier.Xpath, accessionNo));
	}
	
	public void clickWorklistGridExamIcon(String iconName) {
		webActions.click(getByLocator(WorklistPageOR.worklistGridExamIcon_Xpath, LocatorIdentifier.Xpath, iconName));
	}
	
	public void doubleClickOnExamFromWorklist(String exam) {
		webActions.waitForElementClickability(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, exam), 60);
		webActions.doubleClick(getByLocator(WorklistPageOR.examToOpen_Xpath, LocatorIdentifier.Xpath, exam));
	}
	
	public void clickWorklistPageTab(String tabName) {
		webActions.click(getByLocator(WorklistPageOR.worklistPageTab_XPath, LocatorIdentifier.Xpath, tabName));
	}
	
	public void clickFolderToolbarOption(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.folderToolbarActionButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void setFolderName(String folderName) {
		webActions.setText(getByLocator(WorklistPageOR.createFolderNameTextbox_XPath, LocatorIdentifier.Xpath), folderName);
	}	
	
	public void clickCreateFolderActionButton(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.createFolderActionButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}	
	
	public boolean isFolderVisible(String folderName) {
		return webActions.isVisible(getByLocator(WorklistPageOR.folderName_XPath, LocatorIdentifier.Xpath, folderName));
	}
	
	public void setAssignExamToFolderTitle(String title) {
		webActions.setText(getByLocator(WorklistPageOR.assignExamToFolderTitle_XPath, LocatorIdentifier.Xpath), title);
	}
	
	public void setAssignExamToFolderMessage(String message) {
		webActions.setText(getByLocator(WorklistPageOR.assignExamToFolderMessage_XPath, LocatorIdentifier.Xpath), message);
	}
	
	public void clickAssignExamToFolderCheckbox(String folderName) {
		if(!webActions.getClassValue(getByLocator(WorklistPageOR.assignExamToFolderCheckbox_XPath, LocatorIdentifier.Xpath, folderName)).contains("checked"))
           webActions.click(getByLocator(WorklistPageOR.assignExamToFolderCheckbox_XPath, LocatorIdentifier.Xpath, folderName));
	}
	
	public void clickAssignExamToFolderButton(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.assignExamToFolderButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void clickOnFolderName(String folderName) {
		webActions.click(getByLocator(WorklistPageOR.folderName_XPath, LocatorIdentifier.Xpath, folderName));
	}
	
	public void openFolderContextMenu(String folderName) {
		webActions.rightClick(getByLocator(WorklistPageOR.folderName_XPath, LocatorIdentifier.Xpath, folderName));
	}
	
	public void selectContextMenuOption(String option) {
		webActions.click(getByLocator(WorklistPageOR.folderRightClickMenu_XPath, LocatorIdentifier.Xpath, option));
	}
	
	public void setSubFolderName(String subFolderName) {
		webActions.setText(getByLocator(WorklistPageOR.createSubFolderNameTextbox_XPath, LocatorIdentifier.Xpath), subFolderName);
	}	
	
	public void clickCreateSubFolderActionButton(String buttonName) {
		webActions.click(getByLocator(WorklistPageOR.createSubFolderActionButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void clickDeleteFoldersConfirmationOption(String option) {
		webActions.click(getByLocator(WorklistPageOR.deleteFoldersConfirmationOption_XPath, LocatorIdentifier.Xpath, option));
	}
}