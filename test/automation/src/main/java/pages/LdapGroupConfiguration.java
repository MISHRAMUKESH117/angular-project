package pages;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.LdapGroupConfigurationPageORFile;

import Actions.Web;
import objectRepository.LdapGroupConfigurationPageOR;
import utilities.Driver;
import utilities.YmlReader;

public class LdapGroupConfiguration extends Locator {

	Web webActions;

	
	public LdapGroupConfiguration(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static LdapGroupConfigurationPageOR ldapGroupPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + LdapGroupConfigurationPageORFile, LdapGroupConfigurationPageOR.class);


	public void setFilterValue(String ldap) {
		webActions.setText(getByLocator(ldapGroupPageOR.searchLDAPGroupsTextbox_Xpath, LocatorIdentifier.Xpath),ldap);	
	}
	
	public boolean isLdapGroupFilterValueVisible(String ldapName) {
		return webActions.isVisible(
				getByLocator(ldapGroupPageOR.gridLdapName_Xpath, LocatorIdentifier.Xpath, ldapName));
	}


}
