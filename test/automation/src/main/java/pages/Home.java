package pages;

import static utilities.Common.sleep;
import static utilities.Constants.HomePageORFile;
import static utilities.Constants.USER_WORK_DIR;
import java.util.concurrent.TimeUnit;
import Actions.Web;
import objectRepository.HomePageOR;
import utilities.Driver;
import utilities.YmlReader;

public class Home extends Locator {

	Web webActions;

	public Home(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static HomePageOR homePageOR = YmlReader.getobjectRepository(USER_WORK_DIR + HomePageORFile, HomePageOR.class);

	public boolean isHomePageTabSelected(String button) {
		return webActions.isSelected(getByLocator(homePageOR.homePageTab_XPath, LocatorIdentifier.Xpath, button));
	}

	public void openhomePageApplicationModulesButton(String moduleName) {
		webActions.click(getByLocator(homePageOR.homePageApplicationModules_XPath, LocatorIdentifier.Xpath, moduleName));
		sleep(10, TimeUnit.SECONDS);
	}
	
	public void openhomePageApplicationHeaderButton(String moduleName) {
		webActions.click(getByLocator(homePageOR.headerModule_XPath, LocatorIdentifier.Xpath, moduleName));
		webActions.switchTab(2);
		sleep(10, TimeUnit.SECONDS);
	}

	public String getApplicationTitle() {
		String[] applicationTitle = webActions.getwindowTitle().split("-");
		return applicationTitle[1].replace("\"", "").trim();
	}

	public void openhomePageApplication(String applicationName) {
		webActions.click(getByLocator(homePageOR.homePageApplicationButton_XPath, LocatorIdentifier.Xpath, applicationName));
		webActions.switchTab(1);
		sleep(10, TimeUnit.SECONDS);
	}

	public void clickUserDropdown() {
		webActions.click(getByLocator(homePageOR.loginTextIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void clickUserDropdownMenuOption(String dropdownValue) {
		webActions.click(getByLocator(homePageOR.userProfileDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
	}

	public void setUserSettingsScheduleId(String scheduleId) {
		webActions.setText(getByLocator(homePageOR.userSettingsScheduleIdTextbox_XPath, LocatorIdentifier.Xpath), scheduleId);
	}

	public void clickUserSettingsActionButton(String buttonName) {
		webActions.click(getByLocator(homePageOR.userSettingsActionButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void clickProfileManagementActionButton(String buttonName) {
		webActions.click(getByLocator(homePageOR.profileManagementActionButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}

	public void clickCheckboxProfileManagement(String checkBoxName) {
		if(!webActions.getClassValue(getByLocator(homePageOR.isProfileManagementCheckboxChecked_XPath, LocatorIdentifier.Xpath, checkBoxName)).contains("checked")) {
			webActions.click(getByLocator(homePageOR.profileManagementCheckbox_XPath, LocatorIdentifier.Xpath, checkBoxName));	
		}
	}
	
	public void uncheckCheckboxProfileManagement(String checkBoxName) {
		if(webActions.getClassValue(getByLocator(homePageOR.isProfileManagementCheckboxChecked_XPath, LocatorIdentifier.Xpath, checkBoxName)).contains("checked")) {
			webActions.click(getByLocator(homePageOR.profileManagementCheckbox_XPath, LocatorIdentifier.Xpath, checkBoxName));	
		}
	}

	public void uncheckCheckboxProfileManagement(int tabNumber) {
		webActions.switchTab(2);
		sleep(2, TimeUnit.SECONDS);
	}
	
	public void clickModuleMenuItemDropdown() {
		webActions.click(getByLocator(homePageOR.moduleMenuItemDropdownButton_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void clickModuleMenuItemDropdownOption(String dropdownValue) {
		webActions.click(getByLocator(homePageOR.moduleMenuItemDropdownList_Xpath, LocatorIdentifier.Xpath, dropdownValue));
	}
	
	public void selectProfileManagementMultiChoiceDropdownValue(String label, String dropdownValue) {
		webActions.click(getByLocator(homePageOR.profileManagementMultiChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
		if (!webActions.getClassValue(getByLocator(homePageOR.profileManagementMultiChoiceDropdownClass_XPath, LocatorIdentifier.Xpath, dropdownValue))
					.contains("selected"))
		         webActions.click(getByLocator(homePageOR.profileManagementMultiChoiceDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		webActions.click(getByLocator(homePageOR.profileManagementMultiChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
	}
}
