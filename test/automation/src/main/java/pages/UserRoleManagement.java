package pages;

import static utilities.Common.sleep;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.UserRoleManagementPageORFile;

import java.util.concurrent.TimeUnit;

import Actions.Web;
import objectRepository.UserRoleManagementPageOR;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;

public class UserRoleManagement extends Locator {

	Web webActions;

	public UserRoleManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static UserRoleManagementPageOR userRoleManagementPageOR = YmlReader.getobjectRepository(
			USER_WORK_DIR + UserRoleManagementPageORFile, UserRoleManagementPageOR.class);

	public boolean isUserRoleManagementPageDisplayed() {
		return webActions.isVisible(getByLocator(userRoleManagementPageOR.pageHeader_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickAddUserRoleButton() {
		webActions.waitForElementVisibility(getByLocator(userRoleManagementPageOR.addUserRole_Id, LocatorIdentifier.Id),10);
		webActions.click(getByLocator(userRoleManagementPageOR.addUserRole_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked Add button on User Role Management Page.");
	}

	public void setUserRoleName(String roleName) {
		webActions.setText(getByLocator(userRoleManagementPageOR.userRoleName_Id, LocatorIdentifier.Id), roleName);
		Log.printInfo("Set User role name: " + roleName);
	}

	public void setUserRoleDescription(String roleDescription) {
		webActions.setText(getByLocator(userRoleManagementPageOR.userRoleDescription_Xpath, LocatorIdentifier.Xpath),roleDescription);
		Log.printInfo("Set User role description: " + roleDescription);
	}

	public void clickPermissionsCheckbox(String permissionType, String permissionName) {
			webActions.scrollToElement(
					getByLocator(userRoleManagementPageOR.permissionCheckbox_Xpath, LocatorIdentifier.Xpath, permissionType, permissionName));
			webActions.waitForElementClickability(
					getByLocator(userRoleManagementPageOR.permissionCheckbox_Xpath, LocatorIdentifier.Xpath, permissionType, permissionName), 10);
			webActions.click(
					getByLocator(userRoleManagementPageOR.permissionCheckbox_Xpath, LocatorIdentifier.Xpath, permissionType, permissionName));
			sleep(5, TimeUnit.SECONDS);
			Log.printInfo("Checked " + permissionName + " under permissions type " + permissionType);
	}

	public void clickSaveNewRoleButton() {
		webActions.click(getByLocator(userRoleManagementPageOR.saveNewRole_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked on Save User role button");
	}

	public boolean isUserRoleVisible(String userRoleName) {
		return webActions.isVisible(
				getByLocator(userRoleManagementPageOR.gridUserRoleName_Xpath, LocatorIdentifier.Xpath, userRoleName));
	}

	public void clickUserRoleInGrid(String userRoleName) {
		webActions.click(
				getByLocator(userRoleManagementPageOR.gridUserRoleName_Xpath, LocatorIdentifier.Xpath, userRoleName));
		Log.printInfo("User Role" + userRoleName + " selected from the Grid");
	}

	public void clickEditUserRoleButton() {
		webActions.click(getByLocator(userRoleManagementPageOR.userRoleEdit_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked edit User role button");
	}

	public boolean userRoleDoesNotExists() {
		boolean errorMessage = false;
		errorMessage = (webActions.isVisible(
				getByLocator(userRoleManagementPageOR.userRoleAlreadyExistsError_Xpath, LocatorIdentifier.Xpath)));
		if (errorMessage == true)
			Log.printInfo("User Role already exists");
		return !errorMessage;
	}

	public void numberOfUsersHyperlink(String userRoleName) {
		webActions.click(
				getByLocator(userRoleManagementPageOR.numberOfUsersHyperlink_Xpath, LocatorIdentifier.Xpath,userRoleName));
		Log.printInfo("Clicked on" + userRoleName + " number of users hyperlink");
	}

	public void setUserNameToSearch(String userName) {
		webActions.setText(
				getByLocator(userRoleManagementPageOR.userNameSearchBox_Xpath, LocatorIdentifier.Xpath),userName);
		Log.printInfo("Set User name to search: " + userName);
	}

	public void clickSearchButtonOnUsersDialougeBox() {
		webActions.click(getByLocator(userRoleManagementPageOR.searchUsersButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Search button to search users on Users dialog box");
	}

	public void selectUsersFromUsersDialougeBox(String userName) {
		webActions.click(getByLocator(userRoleManagementPageOR.selectUser_Xpath, LocatorIdentifier.Xpath, userName));
		Log.printInfo("Selected" + userName);
	}

	public void clickSaveButtonOnUsersDialougeBox() {
		webActions.click(getByLocator(userRoleManagementPageOR.saveUsersButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Save Users button");
	}

	public boolean isUserNameSelected(String userName) {
		boolean isUserSelected = false;
		isUserSelected = (webActions.isVisible(
				getByLocator(userRoleManagementPageOR.deSelectUser_Xpath, LocatorIdentifier.Xpath, userName)));
		return isUserSelected;
	}

	public void deSelectUsersFromUsersDialougeBox(String userName) {
		webActions.click(getByLocator(userRoleManagementPageOR.deSelectUser_Xpath, LocatorIdentifier.Xpath, userName));
		Log.printInfo("Selected" + userName);
	}

	public boolean isUserNameNotSelected(String userName) {
		boolean isUserNotSelected = false;
		isUserNotSelected = (webActions.isVisible(
				getByLocator(userRoleManagementPageOR.selectUser_Xpath, LocatorIdentifier.Xpath, userName)));
		return isUserNotSelected;
	}

	public void clickDeleteUserRoleButton() {
		webActions.click(getByLocator(userRoleManagementPageOR.deleteUserRole_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked on Delete User role button");
	}

	public boolean isUserRoleDeleted(String userRole) {
		boolean isUserRolevisible = false;
		isUserRolevisible = (webActions.isVisible(
				getByLocator(userRoleManagementPageOR.gridUserRoleName_Xpath, LocatorIdentifier.Xpath, userRole)));
		return !isUserRolevisible;
	}

	public void clickDeleteUserRolePopUpYesButton() {
		webActions.waitForElementVisibility(
				getByLocator(userRoleManagementPageOR.deleteUserRolePopup_Xpath, LocatorIdentifier.Xpath), 5);
		webActions.click(
				getByLocator(userRoleManagementPageOR.deleteUserRolePopupYesButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked User Role Delete Popup Yes button");
	}
	
	public void setFilterValue(String input) {
		webActions.setText(getByLocator(userRoleManagementPageOR.userRoleSearchBox_Xpath, LocatorIdentifier.Xpath),input);	
	}

	public boolean userRoleContains(String userRoleName) {
		return webActions.isVisible(
				getByLocator(userRoleManagementPageOR.gridUserRoleNameContains_Xpath, LocatorIdentifier.Xpath, userRoleName));
	}
}
