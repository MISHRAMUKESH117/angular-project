package pages;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.WindowsLauncherOR;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Location;
import org.sikuli.script.Screen;
import org.sikuli.script.App;

import Actions.Sikuli;
import Actions.Windows;
import objectRepository.WindowsLauncherOR;
import utilities.Constants;
import utilities.ExcelOperations;
import utilities.Property;
import utilities.WinDriver;
import utilities.YmlReader;

import static utilities.Common.sleep;

public class WindowsLaunch extends Locator {
	
	Windows windowsActions;
	Property property;
	Sikuli sikuli;

	
	public WindowsLaunch(WinDriver driver) {
		
		windowsActions = new Windows(driver.getCurrentDriver());
		property = new Property();
		//sikuli = new Sikuli();

	}

	private static WindowsLauncherOR windowsLauncherOR = YmlReader.getobjectRepository(USER_WORK_DIR + WindowsLauncherOR, WindowsLauncherOR.class);

	public void clickUninstallIcon() {
		WebElement window = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.applicationWindow_Xpath, LocatorIdentifier.Xpath));
		if(windowsActions.isVisible(window, getByLocator(windowsLauncherOR.maximize_Xpath, LocatorIdentifier.Xpath)))
		{windowsActions.click(getByLocator(windowsLauncherOR.maximize_Xpath, LocatorIdentifier.Xpath));}
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.programsUninstalllink_Xpath, LocatorIdentifier.Xpath), 40);
		windowsActions.click(window,getByLocator(windowsLauncherOR.programsUninstalllink_Xpath, LocatorIdentifier.Xpath));
		WebElement programsFeaturesWindow = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.programsFeaturesWindow_Xpath, LocatorIdentifier.Xpath));
		windowsActions.doubleClick(programsFeaturesWindow,getByLocator(windowsLauncherOR.zvExtenderIcon_Xpath, LocatorIdentifier.Xpath));
		if(windowsActions.isVisible(getByLocator(windowsLauncherOR.continueButton_Xpath, LocatorIdentifier.Xpath)))
		{
			windowsActions.click(getByLocator(windowsLauncherOR.continueButton_Xpath, LocatorIdentifier.Xpath));
		}
	}
	
	public boolean iszvUninstalled() {
		WebElement programsFeaturesWindow = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.programsFeaturesWindow_Xpath, LocatorIdentifier.Xpath));
		return windowsActions.isVisible(programsFeaturesWindow,getByLocator(windowsLauncherOR.zvExtenderIcon_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void closeControlPanel() {
		WebElement programsFeaturesWindow = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.programsFeaturesWindow_Xpath, LocatorIdentifier.Xpath));
		windowsActions.click(programsFeaturesWindow,getByLocator(windowsLauncherOR.closeButton_Xpath, LocatorIdentifier.Xpath));
		sleep(10,TimeUnit.SECONDS);
	}
	
	public void closeWindow() {
		windowsActions.click(getByLocator(windowsLauncherOR.maximize_Xpath, LocatorIdentifier.Xpath));
	}
	
	public boolean iszvInstalled() {
		return windowsActions.isVisible(getByLocator(windowsLauncherOR.zvIcon_Xpath, LocatorIdentifier.Xpath));
	}
	
	
	public void clickOnIconButton(String iconName, String winName) {
		System.out.println("<click>");
		//WebElement window = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, winName));
		//System.out.println("window - "+window.getAttribute("Name"));
		//window.submit();
		//windowsActions.click(window, getByLocator(windowsLauncherOR.namePane_Xpath, LocatorIdentifier.Xpath, "Destination Root:"));
		//windowsActions.click(window, getByLocator(windowsLauncherOR.nameButton_Xpath, LocatorIdentifier.Xpath, "Maximize"));
		//sleep(15, TimeUnit.SECONDS);
		//System.out.println("<<click>>");
		//sikuli = new Sikuli();
		//System.out.println("java.version - "+java.lang.System.getProperty("java.version").toString());
		//String JavaVersion = java.lang.System.getProperty("java.version").substring(2, 3).toString();
		//System.out.println("Java version - "+JavaVersion);
		//System.out.println(ExcelOperations.getFileNameInsideFolder(iconName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
		//System.out.println("1st click");
		
		//Location l = sikuli.getLocation(ExcelOperations.getFileNameInsideFolder(iconName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
		//System.out.println("before:"+l.getX()+" and "+l.getY());
	//sikuli.click(ExcelOperations.getFileNameInsideFolder(iconName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
		//l = sikuli.getLocation(ExcelOperations.getFileNameInsideFolder(iconName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
		//System.out.println("after:"+l.getX()+" and "+l.getY());
		//WebElement window = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, winName));
		//windowsActions.click(window, getByLocator(windowsLauncherOR.namePane_Xpath, LocatorIdentifier.Xpath, "Destination Root:"));
		//windowsActions.moveByOffset(window, l.getX(),l.getY());
	}

	
	public void clickWindow(String winName) {
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, winName), 60);
		windowsActions.click(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, winName));
	}

	public void clickPane(String paneName) {
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.namePane_Xpath, LocatorIdentifier.Xpath, paneName), 60);
		windowsActions.click(getByLocator(windowsLauncherOR.namePane_Xpath, LocatorIdentifier.Xpath, paneName));
	}

	
	public void clickButton(String btnName) {
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.nameButton_Xpath, LocatorIdentifier.Xpath, btnName), 600);
		windowsActions.click(getByLocator(windowsLauncherOR.nameButton_Xpath, LocatorIdentifier.Xpath, btnName));
	}

	public void clickCheckBox(String chkbxName) {
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.nameCheckBox_Xpath, LocatorIdentifier.Xpath, chkbxName), 60);
		windowsActions.click(getByLocator(windowsLauncherOR.nameCheckBox_Xpath, LocatorIdentifier.Xpath, chkbxName)); 
	}
	
	public void clickRadioButton(String btnName) {
		windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.nameRadioButton_Xpath, LocatorIdentifier.Xpath, btnName)).click();
	}

	public void setTextEdit(String text) {
		windowsActions.findWindowElementsBy(getByLocator(windowsLauncherOR.edit_Xpath, LocatorIdentifier.Xpath)).get(0).clear();
		windowsActions.findWindowElementsBy(getByLocator(windowsLauncherOR.edit_Xpath, LocatorIdentifier.Xpath)).get(0).sendKeys(text);
	}

	
	public void uninstallApp(String appName) { 
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, "Control Panel"), 60);
		WebElement window = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, "Control Panel"));
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.programsUninstalllink_Xpath, LocatorIdentifier.Xpath), 60);
		windowsActions.click(window,getByLocator(windowsLauncherOR.programsUninstalllink_Xpath, LocatorIdentifier.Xpath));
		
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath,"Programs and Features"), 60);
		WebElement programsFeaturesWindow = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath,"Programs and Features"));
		
		if(windowsActions.isVisible(programsFeaturesWindow,getByLocator(windowsLauncherOR.nameDataItem_Xpath, LocatorIdentifier.Xpath, appName)))
		{
			windowsActions.doubleClick(programsFeaturesWindow,getByLocator(windowsLauncherOR.nameDataItem_Xpath, LocatorIdentifier.Xpath, appName));

			if(windowsActions.isVisible(programsFeaturesWindow,getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, "Warning")))
				{
					windowsActions.click(programsFeaturesWindow,getByLocator(windowsLauncherOR.nameButton_Xpath, LocatorIdentifier.Xpath, "Continue"));
				}
		}
		windowsActions.click(window, getByLocator(windowsLauncherOR.nameButton_Xpath, LocatorIdentifier.Xpath, "Close"));
	}

	
	public void getDbProperties(String setupWindow, List<String> listStrings) {
		windowsActions.waitForWindowElementToBeVisible(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, setupWindow), 60);
		WebElement window = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, setupWindow));

		List<WebElement> edits = windowsActions.findWindowElementsBy(window, getByLocator(windowsLauncherOR.edit_Xpath, LocatorIdentifier.Xpath));
			switch (setupWindow) {

			case "Clario Database Setup":
				windowsActions.setText(edits.get(0), listStrings.get(0));
				windowsActions.setText(edits.get(1), listStrings.get(1));
				windowsActions.setText(edits.get(2), listStrings.get(2));
				windowsActions.setText(edits.get(3), listStrings.get(3));
				windowsActions.setText(edits.get(4), listStrings.get(4));				
				break;

			case "Clario SmartWorklist Setup":
				windowsActions.setText(edits.get(0), listStrings.get(0));
				windowsActions.setText(edits.get(1), listStrings.get(3));
				windowsActions.setText(edits.get(2), listStrings.get(4));
				windowsActions.setText(edits.get(3), listStrings.get(1));
				windowsActions.setText(edits.get(4), listStrings.get(2));				
				break;
				
			case "SWL Services Dashboard":
				System.out.println("---getting DB properties");
				break;

			}
	}
	
	public void getServiceConfig(String config) {
		String configJson = ExcelOperations.getFileNameInsideFolder(config, Constants.USER_WORK_DIR + Constants.TestDataPath).toString();//+"JSON";
		System.out.println(configJson);
	}

	public void elementList(String controlName, String winName) {	
		System.out.println("...in progress...");
		//By control = By
		//		.xpath("//*[contains(@ControlType,'ControlType.Pane')]");
		
		WebElement window = windowsActions.findWindowElementBy(getByLocator(windowsLauncherOR.nameWindow_Xpath, LocatorIdentifier.Xpath, winName));
		System.out.println(window.getAttribute("Name"));
		List<WebElement> elements = windowsActions.findWindowElementsBy(window, getByLocator(windowsLauncherOR.namePane_Xpath, LocatorIdentifier.Xpath, "Browse"));
		System.out.println("Number of elements: " + elements.size());
		elements = windowsActions.findWindowElementsBy(window, getByLocator(windowsLauncherOR.nameConrtolType_Xpath, LocatorIdentifier.Xpath, controlName));
		
		elements = windowsActions.findWindowElementsBy(elements.get(1), getByLocator(windowsLauncherOR.nameConrtolType_Xpath, LocatorIdentifier.Xpath, controlName));
		System.out.println("Number of elements: " + elements.size());
		elements.forEach((item) -> {
			//if (item.getAttribute("Name").matches("☰")) {
			System.out.println(item.getAttribute("AutomationId")+">"+item.getAttribute("Name"));
			//}
		});
		windowsActions.findWindowElementBy(window, getByLocator(windowsLauncherOR.namePane_Xpath, LocatorIdentifier.Xpath, "Add, Configure, or Remove individual SWL services")).click();

				
	}
	
	public String getValueFromDatabase(String tableName, String columnNameToSearch, String valueToSearch, int columnNumberToGetValue) {

		String value = "";
		//dbConnection.createDatabaseConnection();
		//value = dbConnection.getStringColumnValueForEntry(tableName,columnNameToSearch,valueToSearch,columnNumberToGetValue);
		return value;
	}
}
