package pages;

public enum LocatorIdentifier {
	Id,
	Xpath,
	Name,
	ClassName
}
