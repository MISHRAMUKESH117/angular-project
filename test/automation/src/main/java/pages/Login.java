package pages;

import static utilities.Common.sleep;
import static utilities.Constants.LoginPageORFile;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.UserDetailsFile;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TimeoutException;

import Actions.Web;
import Actions.Windows;
import objectRepository.LoginPageOR;
import objectRepository.UserDetails;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;
import utilities.TextOperations;

public class Login extends Locator {

	Web webActions;
	Windows winActions;
	private Logger logger = LogManager.getLogger(Login.class.getName());

	public Login(Driver driver) {
		webActions = new Web(driver.getWebDriver());
		winActions = new Windows(driver.getWinDriver());
	}

	private static LoginPageOR loginPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + LoginPageORFile,LoginPageOR.class);
	public static UserDetails userDetails = YmlReader.getobjectRepository(USER_WORK_DIR + UserDetailsFile,UserDetails.class);

	public void login(String username, String password) throws TimeoutException {
		setUserName(username);
		setPassword(password);
		clickLoginButton();
		sleep(10, TimeUnit.SECONDS);
	}

	public void loginAsUser(String userType) {
		switch (userType.toUpperCase()) {
		case "RADIOLOGIST":
			login(userDetails.radiologistUsername, userDetails.radiologistPassword);
			break;
		case "ADMIN":
			login(userDetails.adminUsername, userDetails.adminPassword);
			break;
		case "OTHER":
			login(userDetails.otherUsername, userDetails.otherPassword);
			break;
		case "DEFAULT":
			login(userDetails.defaultUsername, userDetails.defaultPassword);
			break;
		case "LICENSE":
			login(userDetails.licenseUsername, userDetails.licensePassword);
			break;
		}	
	}

	public void setUserName(String username) {
		webActions.setText(getByLocator(loginPageOR.userName_Id, LocatorIdentifier.Id), username);
	}

	public void setPassword(String password) {
		webActions.setText(getByLocator(loginPageOR.password_Id, LocatorIdentifier.Id), password);
	}
	
	public void setNewPassword(String password, String newPassword) {
		webActions.setText(getByLocator(loginPageOR.password_Id, LocatorIdentifier.Id), password);
	}

	public void clickLoginButton() {
		webActions.click(getByLocator(loginPageOR.loginButton_Xpath, LocatorIdentifier.Xpath));
		logger.info("Clicked on login button");
	}

	public boolean isUserLoggedIn(String userType) {
		webActions.waitForElementVisibility(getByLocator(loginPageOR.loginText_Xpath, LocatorIdentifier.Xpath), 10);
		String actualLoginText = webActions.getText(getByLocator(loginPageOR.loginText_Xpath, LocatorIdentifier.Xpath));
		String username = null;
		switch (userType.toUpperCase()) {
		case "RADIOLOGIST":
			username = userDetails.radiologistName;
			break;
		case "ADMIN":
			username = userDetails.adminName;
			break;
		case "OTHER":
			username = userDetails.otherName;
			break;
		}
		return actualLoginText.contains(username);
	}

	public void logout() throws TimeoutException {
		try {
			webActions.switchToWindow("Clario - Home", 3);
			webActions.click(getByLocator(loginPageOR.logoutButton_Xpath, LocatorIdentifier.Xpath));
			if(webActions.isVisible(getByLocator(loginPageOR.examAssignmentAlert_Xpath, LocatorIdentifier.Xpath))) 
				webActions.click(getByLocator(loginPageOR.examAssignmentAlertLogoutButton_Xpath, LocatorIdentifier.Xpath));
			Log.printInfo("User logged out");
		} catch (Exception e) {
			Log.printInfo("User did not log out");
		}
	}
	
	public boolean isUserOnLoginPage() {
		webActions.waitForElementVisibility(getByLocator(loginPageOR.userName_Id, LocatorIdentifier.Id), 10);
		return webActions.isVisible(getByLocator(loginPageOR.userName_Id, LocatorIdentifier.Id));
	}
	
	public void getBuildVersion(String fileName) throws IOException {
		webActions.waitForElementClickability(getByLocator(loginPageOR.clarioLogo_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(loginPageOR.clarioLogo_Xpath, LocatorIdentifier.Xpath));
		webActions.waitForElementClickability(getByLocator(loginPageOR.buildVersion_Xpath, LocatorIdentifier.Xpath), 10);
		String version = (webActions.getText(getByLocator(loginPageOR.buildVersion_Xpath, LocatorIdentifier.Xpath)));
		if(TextOperations.createFile(fileName)) {
			TextOperations.textFileWriter(fileName, version);
		}
	}
	
	public boolean isFileExisting(String fileName) {
		return TextOperations.checkFileExists(fileName);
	}	
	
	public boolean isBrowserDialogueBoxDisplayed() {
		return webActions.isVisible(getByLocator(loginPageOR.browserDialogueBox_Xpath, LocatorIdentifier.Xpath));
	}
	
	public boolean isBrowserCheckMessageDisplayed(String message) {
		return webActions.isVisible(getByLocator(loginPageOR.browserDialogueBoxMessage_Xpath,LocatorIdentifier.Xpath,message));
	}

	public void updateUserPassword(String currentPassword, String newPassword) {
		webActions.setText(getByLocator(loginPageOR.currentUserPassword_Xpath, LocatorIdentifier.Xpath), currentPassword);
		webActions.setText(getByLocator(loginPageOR.currentUserNewPassword_Xpath, LocatorIdentifier.Xpath), newPassword);
		webActions.setText(getByLocator(loginPageOR.currentUserVerifyPassword_Xpath, LocatorIdentifier.Xpath), newPassword);
		webActions.click(getByLocator(loginPageOR.changePasswordButton_Xpath, LocatorIdentifier.Xpath));
	}


}
