package pages;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.AssignRuleManagementPageORFile;
import Actions.Web;
import objectRepository.AssignRuleManagementPageOR;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;

public class AssignRuleManagement extends Locator {

	Web webActions;

	public AssignRuleManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static AssignRuleManagementPageOR assignRuleManagementPageOR = YmlReader.getobjectRepository(
			USER_WORK_DIR + AssignRuleManagementPageORFile, AssignRuleManagementPageOR.class);

	public void clickAddAssignRuleButton() {
		webActions.waitForElementVisibility(
				getByLocator(assignRuleManagementPageOR.addAssignRule_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(assignRuleManagementPageOR.addAssignRule_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked Add button on Assign Rule Management Page.");
	}

	public void setAssignRuleName(String ruleName) {
		webActions.waitForElementVisibility(
				getByLocator(assignRuleManagementPageOR.assignRuleName_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.setText(getByLocator(assignRuleManagementPageOR.assignRuleName_Xpath, LocatorIdentifier.Xpath),
				ruleName);
		Log.printInfo("Set Assign Rule name: " + ruleName);
	}

	public void selectSingleChoiceDropdownValue(String propertyName, String dropdownOptions) {
		webActions.scrollToElement(
				getByLocator(assignRuleManagementPageOR.assignRulePropertiesDropdownArrow_Xpath,LocatorIdentifier.Xpath, propertyName));
		webActions.click(
				getByLocator(assignRuleManagementPageOR.assignRulePropertiesDropdownArrow_Xpath,LocatorIdentifier.Xpath, propertyName));
		webActions.click(
				getByLocator(assignRuleManagementPageOR.assignRulePropertiesSingleDropdownArrowOptions_Xpath,LocatorIdentifier.Xpath, dropdownOptions));
	}

	public void clickSaveAssignmentRuleButton() {
		webActions.click(getByLocator(assignRuleManagementPageOR.saveAssignmentRule_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked on Save Assignment Rule button");
	}

	public boolean ruleDoesNotExists() {
		boolean errorMessage = webActions.isVisible(
				getByLocator(assignRuleManagementPageOR.ruleAlreadyExistsError_Xpath, LocatorIdentifier.Xpath));

		if (errorMessage == true)
			Log.printInfo("Assignment Role already exists");

		return !errorMessage;
	}

	public boolean isRuleVisible(String ruleName) {
		return webActions.isVisible(
				getByLocator(assignRuleManagementPageOR.gridRuleName_Xpath, LocatorIdentifier.Xpath, ruleName));
	}

	public void clickRuleInGrid(String ruleName) {
		webActions.click(
				getByLocator(assignRuleManagementPageOR.gridRuleName_Xpath, LocatorIdentifier.Xpath, ruleName));
		Log.printInfo("Rule" + ruleName + " is selected from the Grid");
	}

	public void clickEditRuleButton() {
		webActions.click(getByLocator(assignRuleManagementPageOR.editRuleIcon_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked edit Rule button");
	}

	public void clickDeleteAssignmentRuleButton() {
		webActions.click(getByLocator(assignRuleManagementPageOR.deleteRuleIcon_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked on delete assign rule button");
		webActions.waitForElementVisibility(
				getByLocator(assignRuleManagementPageOR.deleteRulePopup_Xpath, LocatorIdentifier.Xpath), 5);
		webActions.click(
				getByLocator(assignRuleManagementPageOR.deleteRulePopupYesButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked Assignment rule Delete Popup Yes button");
	}

	public boolean isAssignmentRuleDeleted(String ruleName) {
		return !webActions.isVisible(
				getByLocator(assignRuleManagementPageOR.gridRuleName_Xpath, LocatorIdentifier.Xpath, ruleName));

	}
	
	public void setFilterValue(String input) {
		webActions.setText(getByLocator(assignRuleManagementPageOR.assignRuleSearchBox_Xpath, LocatorIdentifier.Xpath),input);	
	}

	public void clickPageButton(String button) {
		webActions.click(getByLocator(assignRuleManagementPageOR.pagingButton_Xpath, LocatorIdentifier.Xpath,button));
	}

	public void clickAddAssignmentPropertiesButton() {
		webActions.click(getByLocator(assignRuleManagementPageOR.addAssignmentProperties_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on add Assignment Rule button");
	}
	
	public void clickRulesGridHeaderButton(String buttonName) {
		webActions.click(getByLocator(assignRuleManagementPageOR.rulesGridHeaderButtons_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public Integer getRuleRankInGrid(String ruleName) {
		webActions.scrollToElement(getByLocator(assignRuleManagementPageOR.ruleRankInGrid_Xpath, LocatorIdentifier.Xpath, ruleName));
		return Integer.valueOf(webActions.getText(getByLocator(assignRuleManagementPageOR.ruleRankInGrid_Xpath, LocatorIdentifier.Xpath, ruleName)));
	}
	
	public void rearrangeRule(Integer originalRank, Integer destinationRank) {
		destinationRank = (originalRank > destinationRank)? destinationRank-1 : destinationRank;
		webActions.dragDropElementVertically(
				getByLocator(assignRuleManagementPageOR.rankNumberInGrid_Xpath, LocatorIdentifier.Xpath, originalRank),
				getByLocator(assignRuleManagementPageOR.rankNumberInGrid_Xpath, LocatorIdentifier.Xpath, destinationRank), 2);
	}
	
	public void clickAssignmentPropertyAddIcon() {
		webActions.click(getByLocator(assignRuleManagementPageOR.assignmentPropertyAddIcon_Xpath, LocatorIdentifier.Xpath));
	}

}
