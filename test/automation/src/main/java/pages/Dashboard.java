package pages;

import static utilities.Constants.DashboardPageOR;
import static utilities.Constants.USER_WORK_DIR;

import Actions.Robo;
import Actions.Web;
import objectRepository.DashboardPageOR;
import utilities.Driver;
import utilities.YmlReader;

public class Dashboard extends Locator {

	Web webActions;
	Robo robotActions;

	public Dashboard(Driver driver) {
		webActions = new Web(driver.getWebDriver());
		robotActions = new Robo();
	}

	private static DashboardPageOR dashboardOR = YmlReader.getobjectRepository(USER_WORK_DIR + DashboardPageOR, DashboardPageOR.class);

	public void clickDashboardMenu(String menuOption) {
		webActions.waitForElementClickability(
				getByLocator(dashboardOR.menuButton_XPath, LocatorIdentifier.Xpath, menuOption), 60);
		webActions.click(getByLocator(dashboardOR.menuButton_XPath, LocatorIdentifier.Xpath, menuOption));
	}

	public boolean isServiceMenuLoaded() {
		webActions.waitForElementVisibility(getByLocator(dashboardOR.serviceText_XPath, LocatorIdentifier.Xpath), 30);
		 return webActions.isVisible(getByLocator(dashboardOR.serviceText_XPath, LocatorIdentifier.Xpath));
	}
	
	public boolean isUserMonitoringLoaded() {
		webActions.waitForElementVisibility(getByLocator(dashboardOR.userMonitoring_XPath, LocatorIdentifier.Xpath), 30);
		 return webActions.isVisible(getByLocator(dashboardOR.userMonitoring_XPath, LocatorIdentifier.Xpath));
	}
	
	public boolean isDicomRoutingLoaded() {
		webActions.waitForElementVisibility(getByLocator(dashboardOR.dicomRouting_XPath, LocatorIdentifier.Xpath), 30);
		 return webActions.isVisible(getByLocator(dashboardOR.dicomRouting_XPath, LocatorIdentifier.Xpath));
	}

}
