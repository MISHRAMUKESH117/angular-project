package pages;

import static utilities.Constants.NotificationManagementPageORFile;
import static utilities.Constants.USER_WORK_DIR;

import Actions.Web;
import objectRepository.NotificationManagementOR;
import utilities.Driver;
import utilities.YmlReader;

public class NotificationManagement extends Locator {
	Web webActions;

	public NotificationManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static NotificationManagementOR notificationManagementOR = YmlReader
			.getobjectRepository(USER_WORK_DIR + NotificationManagementPageORFile, NotificationManagementOR.class);

	public void clickAddNewNotificationIcon() {
		webActions.click(getByLocator(notificationManagementOR.addNewNotificationIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void setPropertyName(String name) {
		webActions.setText(getByLocator(notificationManagementOR.propertyName_XPath, LocatorIdentifier.Xpath), name);
	}

	public void setPropertyDescription(String description) {
		webActions.setText(getByLocator(notificationManagementOR.propertyDescription_XPath, LocatorIdentifier.Xpath), description);
	}

	public void clickPropertySaveIcon() {
		webActions.click(getByLocator(notificationManagementOR.savePropertyIcon_Id, LocatorIdentifier.Id));
	}

	public void selectMultiChoiceDropdownValue(String label, String dropdownValue) {
		webActions.click(getByLocator(notificationManagementOR.multiChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
		webActions.click(getByLocator(notificationManagementOR.multiChoiceDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		webActions.click(getByLocator(notificationManagementOR.multiChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
	}

	public void selectSingleChoiceDropdownValue(String label, String dropdownValue) {
		webActions.scrollToElement(getByLocator(notificationManagementOR.singleChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
		webActions.click(getByLocator(notificationManagementOR.singleChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
		webActions.click(getByLocator(notificationManagementOR.singleChoiceDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
	}

	public boolean isNotificationRulePresent(String alertName) {
		return webActions.isVisible(getByLocator(notificationManagementOR.notificationRuleTableValue_XPath, LocatorIdentifier.Xpath, alertName));
	}

	public void clickOnNotificationRuleName(String alertName) {
		webActions.click(getByLocator(notificationManagementOR.notificationRuleTableValue_XPath, LocatorIdentifier.Xpath, alertName));
	}
	
	public void clickPropertyEditIcon() {
		webActions.click(getByLocator(notificationManagementOR.editPropertyIcon_Id, LocatorIdentifier.Id));
	}
	
	public void clickPropertyDeleteIcon() {
		webActions.click(getByLocator(notificationManagementOR.deletePropertyIcon_Id, LocatorIdentifier.Id));
		webActions.click(getByLocator(notificationManagementOR.deleteConfirmationWindow_XPath, LocatorIdentifier.Xpath));	
	}
	
	public String setPropertyTimeOfDayStartTime() {
		if (webActions.getTimeInTwelveHrFormat().charAt(0) == '0') {
			webActions.setText(getByLocator(notificationManagementOR.propertyTimeOfDayStartTime_Xpath, LocatorIdentifier.Xpath),
					webActions.getTimeInTwelveHrFormat().substring(1));
			return webActions.getTimeInTwelveHrFormat().substring(1);
		}
		else {
			webActions.setText(getByLocator(notificationManagementOR.propertyTimeOfDayStartTime_Xpath, LocatorIdentifier.Xpath),
					webActions.getTimeInTwelveHrFormat());
			return webActions.getTimeInTwelveHrFormat(); 
		}
	}
	
	public String setPropertyTimeOfDayFinishTime(int minutesToAdd) {
		if (webActions.getTimeAddMiutes(minutesToAdd).charAt(0) == '0') {
			webActions.setText(getByLocator(notificationManagementOR.propertyTimeOfDayFinishTime_Xpath, LocatorIdentifier.Xpath),
					webActions.getTimeAddMiutes(minutesToAdd).substring(1));
			return webActions.getTimeAddMiutes(minutesToAdd).substring(1);
		}
		else {
			webActions.setText(getByLocator(notificationManagementOR.propertyTimeOfDayFinishTime_Xpath, LocatorIdentifier.Xpath),
					webActions.getTimeAddMiutes(minutesToAdd));
			return webActions.getTimeAddMiutes(minutesToAdd);
		}
	}
	
	public String getPropertyTimeOfDayStartTime() {
		return webActions.getTextboxValue(getByLocator(notificationManagementOR.propertyTimeOfDayStartTime_Xpath, LocatorIdentifier.Xpath)); 
	}
	
	public String getPropertyTimeOfDayFinishTime() {
		return webActions.getTextboxValue(getByLocator(notificationManagementOR.propertyTimeOfDayFinishTime_Xpath, LocatorIdentifier.Xpath)); 
	}
	
	public void setFilterValue(String input) {
		webActions.setText(getByLocator(notificationManagementOR.searchNotificationTextbox_Xpath, LocatorIdentifier.Xpath),input);	
	}
	

}
