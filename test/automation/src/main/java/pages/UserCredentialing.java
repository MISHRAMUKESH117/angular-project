package pages;

import static utilities.Common.sleep;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.UserCredentialingORFile;

import java.util.concurrent.TimeUnit;

import Actions.Web;
import objectRepository.UserCredentialingPageOR;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;

public class UserCredentialing extends Locator {

	Web webActions;

	public UserCredentialing(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static UserCredentialingPageOR userCredentialingPageOR = YmlReader.getobjectRepository(
			USER_WORK_DIR + UserCredentialingORFile, UserCredentialingPageOR.class);

	public void clickUserCredentialingPageLeftPanelTabs(String tabName) {
		webActions.click(getByLocator(userCredentialingPageOR.leftPanelTabNames_Xpath, LocatorIdentifier.Xpath, tabName));
		Log.printInfo("Clicked on " + tabName + " tab in User Credentialing Page.");
	}

	public void setUserNameToSearch(String userName) {
		webActions.setText(getByLocator(userCredentialingPageOR.userNameToSearch_Xpath, LocatorIdentifier.Xpath),userName);
		Log.printInfo("Set user name to search: " + userName);
	}

	public void clickUserFromUsersTable(String userName) {
		webActions.click(getByLocator(userCredentialingPageOR.userTabUserTableUserName_Xpath, LocatorIdentifier.Xpath,userName));
		Log.printInfo("Clicked on " + userName + " in the users table.");
	}

	public void doubleClickCommonMemberCheckbox() {
		webActions.doubleClick(getByLocator(userCredentialingPageOR.commonMemberCheckbox_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isRemoveGroupMembershipMessageDisplayed() {
		return webActions.isVisible(
				getByLocator(userCredentialingPageOR.removeGroupMembershipMessage_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickUserCredentialingSaveButton() {
		webActions.click(
				getByLocator(userCredentialingPageOR.userCredentialingSaveButton_Xpath, LocatorIdentifier.Xpath));
		sleep(2, TimeUnit.SECONDS);
		Log.printInfo("Clicked user credentialing Save button");
	}

	public void doubleClickCommonFinalRadioButton() {
		webActions.doubleClick(
				getByLocator(userCredentialingPageOR.commonFinalRadioButton_Xpath, LocatorIdentifier.Xpath));
	}
	
	public boolean isRemoveAllCredentialingMessageDisplayed() {
		return webActions.isVisible(
				getByLocator(userCredentialingPageOR.removeAllCredentialingMessage_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void clickSpecificMemberCheckbox(String valueName) {
		webActions.click(
				getByLocator(userCredentialingPageOR.userCredentialingMemberCheckbox_Xpath, LocatorIdentifier.Xpath, valueName));
		Log.printInfo("Clicked " +valueName+ " Member checkbox");
	}
	
	public void clickSpecificFinalRadioButton(String valueName) {
		webActions.click(
				getByLocator(userCredentialingPageOR.userCredentialingFinalRadioButton_Xpath, LocatorIdentifier.Xpath, valueName));
		Log.printInfo("Clicked " +valueName+ " Final radio button");
	}
	
	public void clickUserTabRightPanelTabs(String tabName) {
		webActions.click(getByLocator(userCredentialingPageOR.userTabRightPanelTabNames_Xpath, LocatorIdentifier.Xpath, tabName));
		Log.printInfo("Clicked on " + tabName + " tab under User Tab in User Credentialing Page.");
	}
	
	public void clickCommonMemberCheckbox() {
		webActions.click(getByLocator(userCredentialingPageOR.commonMemberCheckbox_Xpath, LocatorIdentifier.Xpath));
	}
}
