package pages;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.AiNavigatorORFile;
import static utilities.Constants.PatientViewORFile;
import Actions.Web;
import objectRepository.AiNavigatorPageOR;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;

public class AiNavigatorManagement extends Locator {

	Web webActions;

	
	public AiNavigatorManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static AiNavigatorPageOR AiNavigatorPageOR = YmlReader.getobjectRepository(
			USER_WORK_DIR + AiNavigatorORFile, AiNavigatorPageOR.class);


	public void setFilterValue(String input) {
		webActions.setText(getByLocator(AiNavigatorPageOR.assignRuleSearchBox_Xpath, LocatorIdentifier.Xpath),input);	
	}


	public boolean isAiNavigatorFilterValueVisible(String aiNavigatorName) {
		return webActions.isVisible(
				getByLocator(AiNavigatorPageOR.gridAiNavigatorName_Xpath, LocatorIdentifier.Xpath, aiNavigatorName));
	}
	
	
	
	
}
