package pages;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.UserManagementPageORFile;

import java.util.List;

import Actions.Web;
import objectRepository.UserManagementPageOR;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;

public class UserManagement extends Locator {

	Web webActions;

	public UserManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static UserManagementPageOR userManagementPageOR = YmlReader.getobjectRepository(
			USER_WORK_DIR + UserManagementPageORFile, UserManagementPageOR.class);

	public void clickAddUserButton() {
		webActions.waitForElementVisibility(
				getByLocator(userManagementPageOR.addUserButton_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(userManagementPageOR.addUserButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked Add button on User Management Page.");
	}

	public void setUserFirstName(String firstName) {
		webActions.setText(getByLocator(userManagementPageOR.userFirstName_Xpath, LocatorIdentifier.Xpath), firstName);
		Log.printInfo("Set User First name: " + firstName);
	}

	public void setUserLastName(String lastName) {
		webActions.setText(getByLocator(userManagementPageOR.userLastName_Xpath, LocatorIdentifier.Xpath), lastName);
		Log.printInfo("Set User Last name: " + lastName);
	}

	public void setUserLoginName(String loginName) {
		webActions.setText(getByLocator(userManagementPageOR.userLoginName_Xpath, LocatorIdentifier.Xpath), loginName);
		Log.printInfo("Set User Login name: " + loginName);
	}

	public void setPassword(String password) {
		webActions.setText(getByLocator(userManagementPageOR.userPassword_Xpath, LocatorIdentifier.Xpath), password);
		Log.printInfo("Set passowrd: " + password);
	}

	public void setVerifyPassword(String verifyPassword) {
		webActions.setText(getByLocator(userManagementPageOR.userPasswordVerify_Xpath, LocatorIdentifier.Xpath),verifyPassword);
		Log.printInfo("Set verify password: " + verifyPassword);
	}

	public void selectLocalUserRoles(String localUserRole) {
		webActions.click(getByLocator(userManagementPageOR.localUserRolesDropdownArrow_Xpath, LocatorIdentifier.Xpath));
		webActions.scrollToElement(getByLocator(userManagementPageOR.localUserRolesOptionsCheckbox_Xpath,LocatorIdentifier.Xpath, localUserRole));
		webActions.click(getByLocator(userManagementPageOR.localUserRolesOptionsCheckbox_Xpath, LocatorIdentifier.Xpath,localUserRole));
		webActions.click(getByLocator(userManagementPageOR.localUserRolesDropdownArrow_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Checked Local User Role " + localUserRole);
	}
	
	public void clickSaveUserButton() {
		webActions.isVisible(
				getByLocator(userManagementPageOR.saveUserButton_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(userManagementPageOR.saveUserButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Clicked on Save User button");
	}
	
	public void searchUser(String userName) {
		webActions.setText(getByLocator(userManagementPageOR.searchUserTextbox_Xpath, LocatorIdentifier.Xpath),userName);
		webActions.click(getByLocator(userManagementPageOR.searchUserButton_Xpath, LocatorIdentifier.Xpath));
		Log.printInfo("Search User: " + userName);
	}
	
	public boolean isUserVisible(String userName) {
		return webActions.isVisible(
				getByLocator(userManagementPageOR.gridUserName_Xpath, LocatorIdentifier.Xpath, userName));
	}
	
	public void clickUserInGrid(String userName) {
		webActions.click(
				getByLocator(userManagementPageOR.gridUserName_Xpath, LocatorIdentifier.Xpath, userName));
		Log.printInfo("User " + userName + " selected from the Grid");
	}
	
	public void clickEditUserButton() {
		webActions.click(getByLocator(userManagementPageOR.userEdit_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked edit User button");
	}

	public void generateUserLicense(List<String> licenses) {
		for(String license: licenses) {
			webActions.click(getByLocator(userManagementPageOR.addLicense_Xpath,LocatorIdentifier.Xpath,license));
		}	
	}
}
