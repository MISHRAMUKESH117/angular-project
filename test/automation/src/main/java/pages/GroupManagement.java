package pages;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.GroupManagementPageORFile;


import Actions.Web;
import objectRepository.GroupManagementOR;
import utilities.Driver;
import utilities.Log;
import utilities.YmlReader;

public class GroupManagement extends Locator {

	Web webActions;

	public GroupManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static GroupManagementOR groupManagementOR = YmlReader
			.getobjectRepository(USER_WORK_DIR + GroupManagementPageORFile, GroupManagementOR.class);

	public void clickGroupOrSubspecialtyAddButton() {
		webActions.waitForElementClickability(getByLocator(groupManagementOR.addButton_Id, LocatorIdentifier.Id), 10);
		webActions.click(getByLocator(groupManagementOR.addButton_Id, LocatorIdentifier.Id));
	}

	public void setGroupOrSubspecialtyName(String groupOrSubspecialtyName) {
		webActions.setText(getByLocator(groupManagementOR.groupName_Id, LocatorIdentifier.Id), groupOrSubspecialtyName);
		Log.printInfo("Set Group name: " + groupOrSubspecialtyName);
	}

	public void setGroupOrSubspecialtyDescription(String groupOrSubspecialtyDescription) {
		webActions.setText(getByLocator(groupManagementOR.groupDescription_Id, LocatorIdentifier.Id), groupOrSubspecialtyDescription);
		Log.printInfo("Set Group description: " + groupOrSubspecialtyDescription);
	}

	public void clickSaveGroupButton() {
		webActions.click(getByLocator(groupManagementOR.saveGroup_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked on Save Group button");
	}

	public boolean isGroupOrSubspecialtyVisible(String groupOrSubspecialtyName) {
		//webActions.scrollToElement(getByLocator(groupManagementOR.gridGroupName_Xpath, LocatorIdentifier.Xpath, groupOrSubspecialtyName));
		return webActions.isVisible(getByLocator(groupManagementOR.gridGroupName_Xpath, LocatorIdentifier.Xpath, groupOrSubspecialtyName));
	}

	public void clickGroupOrSubspecialtyNameInGrid(String groupOrSubspecialtyName) {
		webActions.scrollToElement(getByLocator(groupManagementOR.gridGroupName_Xpath, LocatorIdentifier.Xpath, groupOrSubspecialtyName));
		webActions.click(getByLocator(groupManagementOR.gridGroupName_Xpath, LocatorIdentifier.Xpath, groupOrSubspecialtyName));
		Log.printInfo(groupOrSubspecialtyName + " selected from the Grid");
	}

	public void clickEditGroupIcon() {
		webActions.click(getByLocator(groupManagementOR.editGropIcon_Id, LocatorIdentifier.Id));
		Log.printInfo("Clicked on Edit Group icon");
	}

	public void deleteGroup() {
		webActions.click(getByLocator(groupManagementOR.deleteGroupIcon_Id, LocatorIdentifier.Id));
		webActions.click(getByLocator(groupManagementOR.confirmationPopupYesButton_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickExamTypeDefinitionAddButton() {
		webActions.click(getByLocator(groupManagementOR.examTypeDefinitionAddButton_Xpath, LocatorIdentifier.Xpath));
	}

	public void setNewExamTypeDefinitionName(String newExamTypeDefinitionName) {
		webActions.setText(getByLocator(groupManagementOR.newExamTypeDefinitionName_Xpath, LocatorIdentifier.Xpath), newExamTypeDefinitionName);
	}

	public void clickOKButton() {
		webActions.click(getByLocator(groupManagementOR.btnOK_Xpath, LocatorIdentifier.Xpath));
	}

	public void clickExamTypeDefinitionNameDropdown() {
		webActions.click(getByLocator(groupManagementOR.examTypeDefinitionNameDropdownArrow_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean isExamTypeDefinitionNameVisible(String examTypeDefinitionName) {
		return webActions.isVisible(getByLocator(groupManagementOR.examTypeDefinitionDropdownNames_Xpath, LocatorIdentifier.Xpath, examTypeDefinitionName));
	}

	public void clickGroupOrSubspecialtyGridButton(String gridButtonName) {
		webActions.click(getByLocator(groupManagementOR.gridActionIcon_Xpath, LocatorIdentifier.Xpath, gridButtonName));
	}

	public Integer getGroupOrSubspecialtyRankInGrid(String groupOrSubspecialtyName) {
		webActions.scrollToElement(getByLocator(groupManagementOR.subSpecialtyOrGroupRankInGrid_Xpath, LocatorIdentifier.Xpath, groupOrSubspecialtyName));
		return Integer.valueOf(webActions.getText(getByLocator(groupManagementOR.subSpecialtyOrGroupRankInGrid_Xpath, LocatorIdentifier.Xpath, groupOrSubspecialtyName)));
	}

	public void rearrangeSubSpecialtyOrGroup(Integer originalRank, Integer destinationRank) {
		destinationRank = (originalRank > destinationRank)? destinationRank-1 : destinationRank;
		webActions.dragDropElementVertically(
				getByLocator(groupManagementOR.rankNumberInGrid_Xpath, LocatorIdentifier.Xpath, originalRank),
				getByLocator(groupManagementOR.rankNumberInGrid_Xpath, LocatorIdentifier.Xpath, destinationRank), 2);
	}

	public void clickToggleButton(String toggle) {
		webActions.click(getByLocator(groupManagementOR.groupSpecialityToggleButton_Xpath,LocatorIdentifier.Xpath,toggle));
	}

	public void setFilterValue(String input) {
		webActions.setText(getByLocator(groupManagementOR.searchGroupTextbox_Xpath, LocatorIdentifier.Xpath),input);	
	}


}
