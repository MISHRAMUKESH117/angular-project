package pages;

import static utilities.Constants.USER_WORK_DIR;
import static utilities.Constants.SiteConfigurationPageORFile;

import Actions.Web;
import objectRepository.SiteConfigurationPageOR;
import utilities.Driver;
import utilities.YmlReader;

public class SiteConfiguration extends Locator {

	Web webActions;

	
	public SiteConfiguration(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static SiteConfigurationPageOR siteConfigurationPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + SiteConfigurationPageORFile, SiteConfigurationPageOR.class);

	public void setFilterValue(String site) {
		webActions.setText(getByLocator(siteConfigurationPageOR.searchSiteTextbox_Xpath, LocatorIdentifier.Xpath),site);	
	}
	
	public boolean isSiteVisible(String site) {
		return webActions.isVisible(
				getByLocator(siteConfigurationPageOR.gridSiteName_Xpath, LocatorIdentifier.Xpath, site));
	}

}
