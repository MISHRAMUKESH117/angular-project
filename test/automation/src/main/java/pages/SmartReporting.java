package pages;

import static utilities.Common.sleep;
import static utilities.Constants.SmartReportingPageOR;
import static utilities.Constants.USER_WORK_DIR;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import Actions.Web;
import objectRepository.SmartReportingPageOR;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.YmlReader;

public class SmartReporting extends Locator {
	Web webActions;

	public SmartReporting(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static SmartReportingPageOR smartReportingOR = YmlReader
			.getobjectRepository(USER_WORK_DIR + SmartReportingPageOR, SmartReportingPageOR.class);

	public void clickCreateNewTemplate() {
		webActions.waitForElementClickability(getByLocator(smartReportingOR.createNewTemplateIcon_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(smartReportingOR.createNewTemplateIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void enterValuesInTexbox(String templateName) {
		webActions.waitForElementClickability(getByLocator(smartReportingOR.createTemplateTexbox_XPath, LocatorIdentifier.Xpath), 60);
		webActions.setText(getByLocator(smartReportingOR.createTemplateTexbox_XPath, LocatorIdentifier.Xpath), templateName);
	}

	public void selectFilterValues(String textbox, String dropdownValue) {
		webActions.click(getByLocator(smartReportingOR.filterDropdownIcon_XPath, LocatorIdentifier.Xpath, textbox));
		webActions.waitForElementClickability(getByLocator(smartReportingOR.filterDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue), 60);
		webActions.click(getByLocator(smartReportingOR.filterDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		sleep(1, TimeUnit.SECONDS);
		webActions.click(getByLocator(smartReportingOR.filterDropdownIcon_XPath, LocatorIdentifier.Xpath, textbox));
	}

	public void addFilterDropdownValues() {
		webActions.waitForElementClickability(getByLocator(smartReportingOR.addFilterDropdownValues_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(smartReportingOR.addFilterDropdownValues_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isTemplateCreated(String templateName) {
		webActions.setText(getByLocator(smartReportingOR.searchTemplateTextbox_XPath, LocatorIdentifier.Xpath), templateName);
		webActions.waitForElementVisibility(getByLocator(smartReportingOR.searchTemplateTextbox_XPath, LocatorIdentifier.Xpath, templateName), 60);
		return webActions.isVisible(getByLocator(smartReportingOR.searchTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void searchTemplate(String templateName) {
		webActions.setText(getByLocator(smartReportingOR.searchTemplateTextbox_XPath, LocatorIdentifier.Xpath), templateName);
		webActions.waitForElementClickability(getByLocator(smartReportingOR.selectTemplateValue_XPath, LocatorIdentifier.Xpath, templateName), 60);
		webActions.clickElementByOffSet(getByLocator(smartReportingOR.selectTemplateValue_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void clickDeleteIcon() {
		webActions.click(getByLocator(smartReportingOR.deleteTemplateIcon_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(smartReportingOR.deleteConfirmationAlert_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isTemplateDeleted(String templateName) {
		sleep(10, TimeUnit.SECONDS);
		return !webActions.isVisible(getByLocator(smartReportingOR.searchTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public boolean isTemplatePropertiesVisible(String propertyName) {
		return webActions.isVisible(getByLocator(smartReportingOR.templateProperties_XPath, LocatorIdentifier.Xpath, propertyName));
	}

	public boolean isProcedureValueVisible(String procedureValue) {
		webActions.waitForElementVisibility(getByLocator(smartReportingOR.procedureValue_XPath, LocatorIdentifier.Xpath, procedureValue), 60);
		return webActions.isVisible(getByLocator(smartReportingOR.procedureValue_XPath, LocatorIdentifier.Xpath, procedureValue));
	}

	public boolean isTemplateTableHeadersPresent(String headerName) {
		webActions.waitForElementVisibility(getByLocator(smartReportingOR.templateTableHeader_XPath, LocatorIdentifier.Xpath, headerName), 60);
		return webActions.isVisible(getByLocator(smartReportingOR.templateTableHeader_XPath, LocatorIdentifier.Xpath, headerName));
	}

	public String getTemplateName() {
		webActions.waitForElementVisibility(getByLocator(smartReportingOR.buildTemplateName_XPath, LocatorIdentifier.Xpath), 60);
		return webActions.getTextboxValue(getByLocator(smartReportingOR.buildTemplateName_XPath, LocatorIdentifier.Xpath));
	}

	public void clickOnTemplateMenu(String menuOption) {
		webActions.click(getByLocator(smartReportingOR.templateMenu_XPath, LocatorIdentifier.Xpath, menuOption));
	}

	public void searchBuildTemplate(String templateName) {
		webActions.waitForElementClickability(getByLocator(smartReportingOR.buildTemplateName_XPath, LocatorIdentifier.Xpath), 60);
		webActions.setText(getByLocator(smartReportingOR.buildTemplateName_XPath, LocatorIdentifier.Xpath), templateName);
		webActions.click(getByLocator(smartReportingOR.templateNameDropdown_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void addBuildSectionToTemplate(String sectionName) {
		webActions.mouseHover(getByLocator(smartReportingOR.buildSectionName_XPath, LocatorIdentifier.Xpath, sectionName));
		webActions.click(getByLocator(smartReportingOR.buildSectionNameIcon_XPath, LocatorIdentifier.Xpath, sectionName));
	}

	public boolean isSectionAddedInTemplate(String sectionName) {
		return webActions.isVisible(getByLocator(smartReportingOR.templateSectionName_XPath, LocatorIdentifier.Xpath, sectionName));
	}

	public void clickNewBlockIcon() {
		webActions.click(getByLocator(smartReportingOR.createNewBlockIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void setBlockName(String blockName) {
		webActions.setText(getByLocator(smartReportingOR.blockNameTextbox_XPath, LocatorIdentifier.Xpath), blockName);
	}

	public void selectQualityMeasure(String qualityMeasureValue) {
		webActions.click(getByLocator(smartReportingOR.qualityMeasureDropdownIcon_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(smartReportingOR.qualityMeasureDropdownValue_XPath, LocatorIdentifier.Xpath, qualityMeasureValue));
		webActions.click(getByLocator(smartReportingOR.qualityMeasureDropdownIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void setBlockContent(String blockContentText) {
		webActions.setText(getByLocator(smartReportingOR.blockContentTextbox_XPath, LocatorIdentifier.Xpath), blockContentText);
	}

	public void clickBlockContentAction(String action) {
		webActions.click(getByLocator(smartReportingOR.blockContentActionIcon_XPath, LocatorIdentifier.Xpath, action));
	}

	public void searchBlock(String blockValue) {
		webActions.setText(getByLocator(smartReportingOR.searchBlockTextbox_XPath, LocatorIdentifier.Xpath), blockValue);
	}

	public boolean isBlockPresent(String blockName) {
		return webActions.isVisible(getByLocator(smartReportingOR.blockSearchValue_XPath, LocatorIdentifier.Xpath, blockName));
	}

	public void selectTemplateLabelDropdown(String dropdownValue, String dropdownLabel) {
		webActions.click(getByLocator(smartReportingOR.templateDropdownLabelIcon_XPath, LocatorIdentifier.Xpath, dropdownLabel));
		webActions.click(getByLocator(smartReportingOR.filterDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		sleep(1, TimeUnit.SECONDS);
		webActions.click(getByLocator(smartReportingOR.templateDropdownLabelIcon_XPath, LocatorIdentifier.Xpath, dropdownLabel));
	}

	public boolean isTemplatePrsesent(String templateName) {
		webActions.waitForElementVisibility(getByLocator(smartReportingOR.searchTemplateTextbox_XPath, LocatorIdentifier.Xpath, templateName), 60);
		return webActions.isVisible(getByLocator(smartReportingOR.searchTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void clearTemplateNameValue() {
		webActions.clearText(getByLocator(smartReportingOR.searchTemplateTextbox_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isAllTemplateListLoaded(String templateName) {
		return webActions.getElementTextList(getByLocator(smartReportingOR.searchTemplateName_XPath, LocatorIdentifier.Xpath, templateName))
				.equals(webActions.getElementTextList(getByLocator(smartReportingOR.templateList_XPath, LocatorIdentifier.Xpath)));
	}

	public void clickPublishCheckbox() {
		webActions.click(getByLocator(smartReportingOR.publishTemplateCheckbox_XPath, LocatorIdentifier.Xpath));
	}

	public void setDateInTemplate(String date) {
		webActions.setText(getByLocator(smartReportingOR.templateUpdateOnTextbox_XPath, LocatorIdentifier.Xpath), date);
		webActions.click(getByLocator(smartReportingOR.templateUpdatedOnDateIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void clearBlockName() {
		webActions.clearText(getByLocator(smartReportingOR.searchBlockTextbox_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isAllBlocksLoaded(String blockName) {
		webActions.waitForElementVisibility(getByLocator(smartReportingOR.blockList_XPath, LocatorIdentifier.Xpath), 60);
		return webActions.getElementTextList(getByLocator(smartReportingOR.blockSearchValue_XPath, LocatorIdentifier.Xpath, blockName))
				.equals(webActions.getElementTextList(getByLocator(smartReportingOR.blockList_XPath, LocatorIdentifier.Xpath)));
	}

	public void selectBlockLabelDropdown(String dropdownValue, String dropdownLabel) {
		webActions.click(getByLocator(smartReportingOR.blockLabelDropdown_XPath, LocatorIdentifier.Xpath, dropdownLabel));
		webActions.click(getByLocator(smartReportingOR.filterDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		sleep(1, TimeUnit.SECONDS);
		webActions.click(getByLocator(smartReportingOR.blockLabelDropdown_XPath, LocatorIdentifier.Xpath, dropdownLabel));
	}

	public void setUpdatedOnDateInBlock() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		webActions.setText(
				getByLocator(smartReportingOR.blockUpdatedOnTextbox_XPath, LocatorIdentifier.Xpath), dateTimeFormatter.format(LocalDateTime.now()));
		webActions.click(getByLocator(smartReportingOR.blockUpdatedOnDateIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void clearDateOfBlock() {
		webActions.clearText(getByLocator(smartReportingOR.blockUpdatedOnTextbox_XPath, LocatorIdentifier.Xpath));
	}

	public void clearDateOfTemplate() {
		webActions.clearText(getByLocator(smartReportingOR.templateUpdateOnTextbox_XPath, LocatorIdentifier.Xpath));
	}

	public void setTextByBlockContent(String blockContent) {
		webActions.setText(getByLocator(smartReportingOR.searchBlockByContent_XPath, LocatorIdentifier.Xpath), blockContent);
	}

	public void addBlockSections(String blockContent, String section) {
		if (webActions.isVisible(getByLocator(smartReportingOR.buildOrphanName_XPath, LocatorIdentifier.Xpath, blockContent))) {
			webActions.mouseHover(getByLocator(smartReportingOR.buildOrphanName_XPath, LocatorIdentifier.Xpath, blockContent));
			webActions.click(getByLocator(smartReportingOR.buildOrphanMoveIcon_XPath, LocatorIdentifier.Xpath, blockContent));
		} else {
			webActions.click(getByLocator(smartReportingOR.expandBlockByContent_XPath, LocatorIdentifier.Xpath, section));
			webActions.mouseHover(getByLocator(smartReportingOR.buildOrphanName_XPath, LocatorIdentifier.Xpath, blockContent));
			webActions.click(getByLocator(smartReportingOR.buildOrphanMoveIcon_XPath, LocatorIdentifier.Xpath, blockContent));
		}
	}

	public void clickCreateNewMacro() {
		webActions.waitForElementClickability(getByLocator(smartReportingOR.createNewMacroIcon_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(smartReportingOR.createNewMacroIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void setMacroName(String macroName) {
		webActions.setText(getByLocator(smartReportingOR.newMacroNameTextbox_XPath, LocatorIdentifier.Xpath), macroName);
	}

	public void setMacroContent(String macroContent) {
		webActions.setText(getByLocator(smartReportingOR.macroContentTextbox_XPath, LocatorIdentifier.Xpath), macroContent);
	}

	public void clickMacroPublishCheckbox() {
		webActions.click(getByLocator(smartReportingOR.publishMacroCheckbox_XPath, LocatorIdentifier.Xpath));
	}

	public void searchMacroByName(String macroName) {
		webActions.setText(getByLocator(smartReportingOR.searchMacroTextbox_XPath, LocatorIdentifier.Xpath), macroName);
	}

	public boolean isMacroPresent(String macroName) {
		return webActions.isVisible(getByLocator(smartReportingOR.newMacroSearchValue_XPath, LocatorIdentifier.Xpath, macroName));
	}

	public void selectMacroFilterValues(String textbox, String dropdownValue) {
		webActions.click(getByLocator(smartReportingOR.macroFilterDropdownLabel_XPath, LocatorIdentifier.Xpath, textbox));
		webActions.waitForElementClickability(getByLocator(smartReportingOR.filterDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue), 60);
		webActions.click(getByLocator(smartReportingOR.filterDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		sleep(1, TimeUnit.SECONDS);
		webActions.click(getByLocator(smartReportingOR.macroFilterDropdownLabel_XPath, LocatorIdentifier.Xpath, textbox));
	}

	public void clickMacroFilterAddIcon() {
		webActions.click(getByLocator(smartReportingOR.macroAddFilterIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void selectMacroLabelDropdown(String dropdownValue, String dropdownLabel) {
		webActions.click(getByLocator(smartReportingOR.macroDropdownLabel_XPath, LocatorIdentifier.Xpath, dropdownLabel));
		webActions.click(getByLocator(smartReportingOR.filterDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		sleep(1, TimeUnit.SECONDS);
		webActions.click(getByLocator(smartReportingOR.macroDropdownLabel_XPath, LocatorIdentifier.Xpath, dropdownLabel));
	}

	public void clearMacroName() {
		webActions.clearText(getByLocator(smartReportingOR.searchMacroTextbox_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isAllMacroLoaded(String macroName) {
		return webActions.getElementTextList(getByLocator(smartReportingOR.newMacroSearchValue_XPath, LocatorIdentifier.Xpath, macroName))
				.equals(webActions.getElementTextList(getByLocator(smartReportingOR.macroList_XPath, LocatorIdentifier.Xpath)));
	}

	public void openBlock(String blockName) {
		webActions.clickElementByOffSet(getByLocator(smartReportingOR.openBlock_XPath, LocatorIdentifier.Xpath, blockName));
	}

	public void deleteBlock() {
		webActions.click(getByLocator(smartReportingOR.deleteBlockAction_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(smartReportingOR.deleteBlockConfirmationYes_XPath, LocatorIdentifier.Xpath));
		sleep(5, TimeUnit.SECONDS);
	}

	public void setUpdatedOnDateInMacro() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		webActions.setText(
				getByLocator(smartReportingOR.macroUpdatedOnTextbox_XPath, LocatorIdentifier.Xpath), dateTimeFormatter.format(LocalDateTime.now()));
		webActions.click(getByLocator(smartReportingOR.macroUpdatedOnDateIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void openMacro(String macroName) {
		webActions.clickElementByOffSet(getByLocator(smartReportingOR.openMacro_XPath, LocatorIdentifier.Xpath, macroName));
	}

	public void deleteMacro() {
		webActions.click(getByLocator(smartReportingOR.deleteMacroIcon_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(smartReportingOR.deleteMacroConfirmationYes_XPath, LocatorIdentifier.Xpath));
		sleep(5, TimeUnit.SECONDS);
	}

	public void clickTemplatePropertyIcon(String action) {
		webActions.click(getByLocator(smartReportingOR.templatePropertyAction_XPath, LocatorIdentifier.Xpath, action));
	}

	public boolean isTemplateDownloaded(String fileName) {
		sleep(10, TimeUnit.SECONDS);
		return ExcelOperations.isFileDownloaded(System.getProperty("downloadFilepath"), fileName);			
	}
}
