package pages;

import static utilities.Common.sleep;
import static utilities.Constants.SchedulePageOR;
import static utilities.Constants.USER_WORK_DIR;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Actions.Robo;
import Actions.Sikuli;
import Actions.Web;
import objectRepository.SchedulePageOR;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.YmlReader;

public class Schedule extends Locator {
	Web webActions;
	Robo robotActions;
	Sikuli sikuliActions;

	public Schedule(Driver driver) {
		webActions = new Web(driver.getWebDriver());
		robotActions = new Robo();
		sikuliActions = new Sikuli();
	}

	private static SchedulePageOR schedulePageOR = YmlReader.getobjectRepository(USER_WORK_DIR + SchedulePageOR, SchedulePageOR.class);

	public void clickShiftManagementButton(String button) {
		webActions.waitForElementClickability(getByLocator(schedulePageOR.shiftManagementButton_XPath, LocatorIdentifier.Xpath, button), 60);
		webActions.click(getByLocator(schedulePageOR.shiftManagementButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public void setDescriptionValue(String value, String textbox) {
		webActions.waitForElementVisibility(getByLocator(schedulePageOR.editShiftWindow_XPath, LocatorIdentifier.Xpath), 60);
		webActions.mouseHover(getByLocator(schedulePageOR.editShiftWindow_XPath, LocatorIdentifier.Xpath));
		webActions.waitForElementClickability(getByLocator(schedulePageOR.shiftTextbox_XPath, LocatorIdentifier.Xpath, textbox), 60);
		webActions.setText(getByLocator(schedulePageOR.shiftTextbox_XPath, LocatorIdentifier.Xpath, textbox), value);
	}

	public void clickSaveShiftButton(String button) {
		webActions.waitForElementClickability(getByLocator(schedulePageOR.saveShiftButton_XPath, LocatorIdentifier.Xpath, button), 60);
		webActions.click(getByLocator(schedulePageOR.saveShiftButton_XPath, LocatorIdentifier.Xpath, button));
		sleep(5, TimeUnit.SECONDS);
	}

	public void clickShiftButton(String button, String shiftName) {
		switch (button) {
		case "Create":
			webActions.waitForElementClickability(getByLocator(schedulePageOR.shiftButton_XPath, LocatorIdentifier.Xpath, button), 60);
			webActions.click(getByLocator(schedulePageOR.shiftButton_XPath, LocatorIdentifier.Xpath, button));
			break;

		case "Delete":
			clickOnShift(shiftName);
			webActions.waitForElementClickability(getByLocator(schedulePageOR.shiftButton_XPath, LocatorIdentifier.Xpath, button), 60);
			webActions.click(getByLocator(schedulePageOR.shiftButton_XPath, LocatorIdentifier.Xpath, button));
			webActions.waitForElementClickability(getByLocator(schedulePageOR.deleteShiftButton_XPath, LocatorIdentifier.Xpath, button), 60);
			webActions.click(getByLocator(schedulePageOR.deleteShiftButton_XPath, LocatorIdentifier.Xpath));
			break;
		}
	}

	public void clickOnShift(String shiftName) {
		webActions.waitForElementClickability(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath, shiftName), 60);
		webActions.click(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath, shiftName));
	}

	public void createShift(String textBox, String value) {
		webActions.setText(getByLocator(schedulePageOR.shiftTextbox_XPath, LocatorIdentifier.Xpath, textBox), value);
	}

	public void createNewEvent(String textBox, String value) {
		webActions.setText(getByLocator(schedulePageOR.shiftTextbox_XPath, LocatorIdentifier.Xpath, textBox), value);
		robotActions.enter();
	}

	public boolean isShiftEdited(String shiftName, String textbox, String value) {
		webActions.click(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath, shiftName));
		webActions.waitForElementVisibility(getByLocator(schedulePageOR.shiftTextbox_XPath, LocatorIdentifier.Xpath, textbox), 60);
		return webActions.getTextboxValue(getByLocator(schedulePageOR.shiftTextbox_XPath, LocatorIdentifier.Xpath, textbox)).equals(value);
	}

	public boolean isNewShiftRowCreated(String shiftName) {
		webActions.waitForElementVisibility(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath, shiftName), 60);
		return webActions.isVisible(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath, shiftName));
	}

	public boolean isShiftDeleted() {
		return !webActions.isVisible(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath));
	}

	public void clickOnDate() {
		webActions.click(getByLocator(schedulePageOR.calendarBlock_XPath, LocatorIdentifier.Xpath));
	}

	public void selectEventTime(String checkbox) {
		webActions.waitForElementClickability(getByLocator(schedulePageOR.eventCheckbox_XPath, LocatorIdentifier.Xpath, checkbox), 60);
		webActions.click(getByLocator(schedulePageOR.eventCheckbox_XPath, LocatorIdentifier.Xpath, checkbox));
	}

	public void clickEventButton(String button) {
		webActions.scrollToElement(getByLocator(schedulePageOR.eventButton_XPath, LocatorIdentifier.Xpath, button));
		webActions.waitForElementClickability(getByLocator(schedulePageOR.eventButton_XPath, LocatorIdentifier.Xpath, button), 60);
		webActions.click(getByLocator(schedulePageOR.eventButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public String isEventCreated() {
		webActions.waitForElementVisibility(getByLocator(schedulePageOR.eventText_XPath, LocatorIdentifier.Xpath), 60);
		return webActions.getText(getByLocator(schedulePageOR.eventText_XPath, LocatorIdentifier.Xpath));
	}

	public void selectRadiologist(String textbox) {
		webActions.click(getByLocator(schedulePageOR.enterRadiologist, LocatorIdentifier.Xpath, textbox));
	}

	public void selectWorklist(String worklistName, String worklistType) {
		webActions.mouseHover(getByLocator(schedulePageOR.worklistWindow_XPath, LocatorIdentifier.Xpath));
		if (webActions.isVisible(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName))) {
			webActions.click(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName));
		} else {
			webActions.click(getByLocator(schedulePageOR.expandWorklist_XPath, LocatorIdentifier.Xpath, worklistType));
			webActions.waitForElementClickability(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName), 60);
			webActions.click(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName));
		}
	}

	public void saveWorklist() {
		webActions.click(getByLocator(schedulePageOR.saveWorklist_XPath, LocatorIdentifier.Xpath));
	}

	public void selectSortWorklist() {
		webActions.click(getByLocator(schedulePageOR.sortWorklistCheckbox_XPath, LocatorIdentifier.Xpath));
	}

	public void clickRowAssignUserToShift(String shiftName) {
		webActions.mouseHover(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath, shiftName));
		webActions.moveCursor();
		webActions.waitForElementClickability(getByLocator(schedulePageOR.rowToAssignUser_XPath, LocatorIdentifier.Xpath, shiftName), 60);
		webActions.click(getByLocator(schedulePageOR.rowToAssignUser_XPath, LocatorIdentifier.Xpath, shiftName));
	}

	public void setAssignUserValue(String assignUserValue) {
		webActions.click(getByLocator(schedulePageOR.assignUserDropdownIcon_XPath, LocatorIdentifier.Xpath));
		webActions
				.waitForElementClickability(getByLocator(schedulePageOR.assignUserDropdownValue_XPath, LocatorIdentifier.Xpath, assignUserValue), 60);
		webActions.click(getByLocator(schedulePageOR.assignUserDropdownValue_XPath, LocatorIdentifier.Xpath, assignUserValue));
	}

	public void saveAssignUser() {
		webActions.waitForElementClickability(getByLocator(schedulePageOR.doneButton_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(schedulePageOR.doneButton_XPath, LocatorIdentifier.Xpath));
	}

	public boolean getAssignUserValue(String userName, String shiftName) {
		webActions.mouseHover(getByLocator(schedulePageOR.newShiftRow_XPath, LocatorIdentifier.Xpath, shiftName));
		webActions.moveCursor();
		return webActions.isVisible(getByLocator(schedulePageOR.assignUserValueToShift_XPath, LocatorIdentifier.Xpath, shiftName, userName));
	}

	public void deleteAssignUser(String userName,String shiftName) {		
		webActions.click(getByLocator(schedulePageOR.assignUserValueToShift_XPath, LocatorIdentifier.Xpath, shiftName, userName));
		webActions.waitForElementClickability(getByLocator(schedulePageOR.deleteUser_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(schedulePageOR.deleteUser_XPath, LocatorIdentifier.Xpath));
	}

	public void deleteEvent() {
		webActions.click(getByLocator(schedulePageOR.eventText_XPath, LocatorIdentifier.Xpath));
		webActions.waitForElementClickability(getByLocator(schedulePageOR.deleteEvent_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(schedulePageOR.deleteEvent_XPath, LocatorIdentifier.Xpath));
		webActions.waitForElementClickability(getByLocator(schedulePageOR.deleteShiftButton_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(schedulePageOR.deleteShiftButton_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isEventDeleted() {
		return !webActions.isVisible(getByLocator(schedulePageOR.eventText_XPath, LocatorIdentifier.Xpath));
	}

	public void clickApplication(String applicationName) {
		webActions.waitForElementClickability(getByLocator(schedulePageOR.smartWorklistButton_XPath, LocatorIdentifier.Xpath, applicationName), 60);
		webActions.click(getByLocator(schedulePageOR.smartWorklistButton_XPath, LocatorIdentifier.Xpath, applicationName));
		webActions.switchTab(2);
		sleep(3, TimeUnit.SECONDS);
	}

	public void switchToModule() {
		webActions.switchTab(1);
	}

	public boolean isUserDeleted(String userName, String shiftName) {
		return !webActions.isVisible(getByLocator(schedulePageOR.assignUserValue_XPath, LocatorIdentifier.Xpath, shiftName, userName));
	}

	public List<String> getShiftWorklistList() {
		return webActions.getElementTextList(getByLocator(schedulePageOR.shiftWorklistList_XPath, LocatorIdentifier.Xpath));
	}

	public void clickImportTemplateFileHyperlink() {
		webActions.click(getByLocator(schedulePageOR.downloadDefaultTemplate_XPath, LocatorIdentifier.Xpath));
	}

	public void setValueInExcelSheet(String fileName, String sheetName, String cellHeaderValue, String cellValue) {
		ExcelOperations.setExcelValueWithDateColumn(fileName, sheetName, cellHeaderValue, cellValue);
	}

	public void importExcelFile(String fileName) {
		webActions.clickElementByOffSet(getByLocator(schedulePageOR.browseFileButton_XPath, LocatorIdentifier.Xpath));
		sleep(2, TimeUnit.SECONDS);
		sikuliActions.type(ExcelOperations.getFileName(fileName).toString());
		robotActions.enter();
	}

	public void clickImportDialogButton(String buttonName) {
		webActions.click(getByLocator(schedulePageOR.importFileActionButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}

	public boolean getShiftDate() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String[] shiftDateAndDay = webActions.getText(getByLocator(schedulePageOR.shiftDateColumn_XPath, LocatorIdentifier.Xpath)).split(" ");
		String shiftDate = shiftDateAndDay[1].trim();
		return shiftDate.equals(dateTimeFormatter.format(LocalDateTime.now()));
	}
	
	public void addShiftToWorklist(String label) {
		webActions.click(getByLocator(schedulePageOR.shiftWorklist_XPath, LocatorIdentifier.Xpath, label));
	}
	
	public void selectValueFromDropdownForEvent(String label, String value) {
		webActions.click(getByLocator(schedulePageOR.singleChoiceDropdownLabel_XPath, LocatorIdentifier.Xpath,label));
		webActions.click(getByLocator(schedulePageOR.singleChoiceDropdownValue_XPath, LocatorIdentifier.Xpath,value));
	}

	public void checkWorklistCheckBox(String worklistName, String worklistType) {
		if(!webActions.getClassValue(getByLocator(schedulePageOR.worklistCheckbox_XPath, LocatorIdentifier.Xpath, worklistName)).contains("checked")){
			if (webActions.isVisible(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName))) {
				webActions.click(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName));
			} else {
				webActions.click(getByLocator(schedulePageOR.expandWorklist_XPath, LocatorIdentifier.Xpath, worklistType));
				webActions.waitForElementClickability(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName), 60);
				webActions.click(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName));
			}
		}
	}

	public void uncheckWorklistCheckBox(String worklistName, String worklistType) {
		if(webActions.getClassValue(getByLocator(schedulePageOR.worklistCheckbox_XPath, LocatorIdentifier.Xpath, worklistName)).contains("checked")){
			if (webActions.isVisible(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName))) {
				webActions.click(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName));
			} else {
				webActions.click(getByLocator(schedulePageOR.expandWorklist_XPath, LocatorIdentifier.Xpath, worklistType));
				webActions.waitForElementClickability(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName), 60);
				webActions.click(getByLocator(schedulePageOR.worklistValue_XPath, LocatorIdentifier.Xpath, worklistName));
			}
		}
	}
}
