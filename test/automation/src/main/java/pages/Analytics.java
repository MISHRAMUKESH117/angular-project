package pages;

import static utilities.Common.sleep;
import static utilities.Constants.AnalyticsPageORFile;
import static utilities.Constants.USER_WORK_DIR;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Actions.Web;
import objectRepository.AnalyticsPageOR;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.YmlReader;

public class Analytics extends Locator {
	Web webActions;
	@SuppressWarnings("unused")
	private Logger logger = LogManager.getLogger(Analytics.class.getName());

	public Analytics(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static AnalyticsPageOR AnalyticsPageOR = YmlReader.getobjectRepository(USER_WORK_DIR + AnalyticsPageORFile, AnalyticsPageOR.class);

	public boolean isAnalyticsLaunched() {
		webActions.waitForElementPresence(getByLocator(AnalyticsPageOR.analyticsLaunch_Xpath, LocatorIdentifier.Xpath), 30);
		return webActions.isVisible(getByLocator(AnalyticsPageOR.analyticsLaunch_Xpath, LocatorIdentifier.Xpath));
	}

	public void selectWidget(String widgetName) {
		sleep(3, TimeUnit.SECONDS);
		webActions.waitForElementClickability(getByLocator(AnalyticsPageOR.widgetName_Xpath, LocatorIdentifier.Xpath, widgetName), 30);
		webActions.click(getByLocator(AnalyticsPageOR.widgetName_Xpath, LocatorIdentifier.Xpath, widgetName));
	}

	public boolean isWidgetDashboardDisplayed(String widgetName) {
		if (webActions.isVisible(getByLocator(AnalyticsPageOR.widgetSourceLocator_Xpath, LocatorIdentifier.Xpath, widgetName)))
			return true; 
			
		else
			return false;
	}

	public Integer hoverCountUnreadExam() {
		webActions.waitForElementVisibility(getByLocator(AnalyticsPageOR.hoverText1_Xpath, LocatorIdentifier.Xpath), 30);
		Integer sum = 0;
		List<String> hoverText1 = webActions.getHiddenHoverElementTextList(
				getByLocator(AnalyticsPageOR.hoverText1_Xpath, LocatorIdentifier.Xpath),
				getByLocator(AnalyticsPageOR.hoverText2_Xpath, LocatorIdentifier.Xpath));
		for (String text : hoverText1) {
			String textFromGraph = text.substring(9);
			Integer value = Integer.valueOf(textFromGraph.trim());
			sum = sum + value;
		}
		return sum;
	}

	public void editWidgetDialogue(String widgetName) {
		webActions.scrollToElement(getByLocator(AnalyticsPageOR.editWidget_Xpath, LocatorIdentifier.Xpath, widgetName));
		webActions.waitForElementClickability(getByLocator(AnalyticsPageOR.editWidget_Xpath, LocatorIdentifier.Xpath, widgetName), 45);
		webActions.click(getByLocator(AnalyticsPageOR.editWidget_Xpath, LocatorIdentifier.Xpath, widgetName));
	}

	public void editInputParameterTime(String examTime) {
			webActions.click(getByLocator(AnalyticsPageOR.examRange_Xpath, LocatorIdentifier.Xpath, examTime));
			webActions.click(getByLocator(AnalyticsPageOR.randomClick_Xpath, LocatorIdentifier.Xpath));
	}

	public void selectSingleChoiceDropdownValue(String param, String value) {
		webActions.waitForElementClickability(getByLocator(AnalyticsPageOR.singleChoiceDropdownLabel_Xpath, LocatorIdentifier.Xpath, param), 15);
		webActions.click(getByLocator(AnalyticsPageOR.singleChoiceDropdownLabel_Xpath, LocatorIdentifier.Xpath, param));
		webActions.click(getByLocator(AnalyticsPageOR.singleChoiceDropDownValue_Xpath, LocatorIdentifier.Xpath, value));
	}
	
	public void selectMultiChoiceDropdownValue(String param, String value) {
		webActions.waitForElementClickability(getByLocator(AnalyticsPageOR.multiChoiceDropdownLabel_Xpath, LocatorIdentifier.Xpath, param), 15);
		webActions.click(getByLocator(AnalyticsPageOR.multiChoiceDropdownLabel_Xpath, LocatorIdentifier.Xpath, param));
		webActions.click(getByLocator(AnalyticsPageOR.multiChoiceDropdownValue_Xpath, LocatorIdentifier.Xpath, value));
		webActions.click(getByLocator(AnalyticsPageOR.multiChoiceDropdownLabel_Xpath, LocatorIdentifier.Xpath, param));
	}
	
	public List<String> getAllModality() {
		List<String> modalities = new ArrayList<String>();
		List<String> modality = new ArrayList<String>();
		modalities = webActions.getElementTextList(getByLocator(AnalyticsPageOR.allModality_Xpath, LocatorIdentifier.Xpath));
		for (String modal : modalities) {
			if (modal.isEmpty())
				break;
			modality.add(modal.trim());
		}
		return modality;
	}

	public void actionWidget(String widgetName, String action) {
		webActions.waitForElementPresence((getByLocator(AnalyticsPageOR.actionWidget_Xpath, LocatorIdentifier.Xpath, widgetName, action)), 30);
		webActions.scrollToElement(getByLocator(AnalyticsPageOR.actionWidget_Xpath, LocatorIdentifier.Xpath, widgetName, action));
		webActions.click(getByLocator(AnalyticsPageOR.actionWidget_Xpath, LocatorIdentifier.Xpath, widgetName, action));
		if (action.equals("Delete"))
			deleteWidget();
	}

	public void deleteWidget() {
		sleep(3, TimeUnit.SECONDS);
		webActions.waitForElementPresence(getByLocator(AnalyticsPageOR.deleteWidget_Xpath, LocatorIdentifier.Xpath), 30);
		webActions.click(getByLocator(AnalyticsPageOR.deleteWidget_Xpath, LocatorIdentifier.Xpath));
		sleep(1, TimeUnit.SECONDS);
	}

	public void clickOnExport(String widgetName) {
		webActions.click(getByLocator(AnalyticsPageOR.exportWidget_Xpath, LocatorIdentifier.Xpath, widgetName));
	}

	public void isExportAlertPresent() {
		webActions.waitForElementPresence(getByLocator(AnalyticsPageOR.exportAlert_Xpath, LocatorIdentifier.Xpath), 30);
	}

	public void clickToExportSheet() {
		webActions.click(getByLocator(AnalyticsPageOR.exportSheet_Xpath, LocatorIdentifier.Xpath));
		sleep(20,TimeUnit.SECONDS);
	}

	public boolean isFileDownloaded(String fileName) {
		String fileName1 = ExcelOperations.appendDateTimeToFilename(fileName, '_', "MM_dd_yyyy");
		sleep(30, TimeUnit.SECONDS);
		boolean bool = ExcelOperations.isFileDownloaded(System.getProperty("downloadFilepath"), fileName1);
		return bool;
	}

	public boolean isWidgetDialogueDisplayed(String widgetName) {
		sleep(5, TimeUnit.SECONDS);
		if (webActions.isVisible(getByLocator(AnalyticsPageOR.verifyEditDialogue_Xpath, LocatorIdentifier.Xpath, widgetName)))
			return true;
		else
			return false;
	}

	public boolean isChangedParamReflected(String param) {
		boolean visible = webActions.isVisible(getByLocator(AnalyticsPageOR.changedInputParam_Xpath, LocatorIdentifier.Xpath, param));
		return visible;
	}

	public void backToHomePage() {
		webActions.closeTab();
		sleep(3, TimeUnit.SECONDS);
		webActions.switchTab(0);
	}

	public void clickWidgetLabelDropdown(String labelName) {
		webActions.scrollToElement(getByLocator(AnalyticsPageOR.widgetLabelNameDropdown_Xpath, LocatorIdentifier.Xpath, labelName));
		webActions.click(getByLocator(AnalyticsPageOR.widgetLabelNameDropdown_Xpath, LocatorIdentifier.Xpath, labelName));
	}

	public List<String> getWidgetLabelDropdownOptions() {
		return webActions.getElementTextList(getByLocator(AnalyticsPageOR.widgetLabelNameDropdownOptions_Xpath, LocatorIdentifier.Xpath));
	}

	public void setEditWidgetWindowStartDate(String startDate) {
		webActions.setText(getByLocator(AnalyticsPageOR.editWidgetStartDate_Xpath, LocatorIdentifier.Xpath), startDate);
	}

	public void exportwidget(String widgetName, String exportType) {
		webActions.click(getByLocator(AnalyticsPageOR.widgetExportButton_Xpath, LocatorIdentifier.Xpath, widgetName));
		webActions.click(getByLocator(AnalyticsPageOR.widgetExportType_Xpath, LocatorIdentifier.Xpath, exportType));
	}

	public boolean verifySiteNameNotPresentInExcelValue(String fileName, String sheetName, String siteName) {
		List<String> excelCellValues = ExcelOperations.readExcel(fileName, sheetName);
		return !excelCellValues.contains(siteName);
	}

	public int widgetExamCount(String widgetName) {
		webActions.waitForElementVisibility(getByLocator(AnalyticsPageOR.widgetBarGraph_Xpath, LocatorIdentifier.Xpath, widgetName), 30);
		List<String> examCountToolTip = webActions.getHiddenHoverElementTextList(
				getByLocator(AnalyticsPageOR.widgetBarGraph_Xpath, LocatorIdentifier.Xpath, widgetName),
				getByLocator(AnalyticsPageOR.numberOfExamsToolTip_Xpath, LocatorIdentifier.Xpath));

		int totalNumberOfExams = 0;
		for (String examCount : examCountToolTip) {
			Integer value = Integer.valueOf(examCount.substring(0, examCount.indexOf(' ')).trim());
			totalNumberOfExams = totalNumberOfExams + value;
		}
		return totalNumberOfExams;

	}

	public int getNumberOfExamsPresentInExcel(String fileName, String sheetName, String siteName) {
		List<String> excelCellValues = ExcelOperations.readExcel(fileName, sheetName);
		Map<String, Integer> cellValues = new HashMap<String, Integer>();
		for (String excelCellValue : excelCellValues) {
			Integer value = cellValues.get(excelCellValue);
			cellValues.put(excelCellValue, (value == null) ? 1 : value + 1);
		}
		return cellValues.get(siteName);
	}

	public String getCellValueFromExcel(String fileName, String sheetName, String filterCellValue) {
		List<String> cellValues = ExcelOperations.readExcel(fileName, sheetName);
		String cellValue = null;
		for (String value : cellValues) {
			if (value.equalsIgnoreCase(filterCellValue))
				cellValue = cellValues.get(cellValues.indexOf(value) + 1);
		}
		return cellValue;
	}

	public void dragDropWidgetsHorizontal(String source, String destination) {
		webActions.dragDropElementHorizontally(
				getByLocator(AnalyticsPageOR.widgetSourceLocator_Xpath, LocatorIdentifier.Xpath, source),
				getByLocator(AnalyticsPageOR.widgetDestinationLocator_Xpath, LocatorIdentifier.Xpath, destination), 100);
	}

	public void dragDropWidgetsVertical(String source, String destination) {
		webActions.dragDropElementVertically(
				getByLocator(AnalyticsPageOR.widgetSourceLocator_Xpath, LocatorIdentifier.Xpath, source),
				getByLocator(AnalyticsPageOR.widgetDestinationLocator_Xpath, LocatorIdentifier.Xpath, destination), 10);
	}

	public void exportWidget(String widgetName, String exportType) {
		webActions.click(getByLocator(AnalyticsPageOR.widgetExportButton_Xpath, LocatorIdentifier.Xpath, widgetName));
		webActions.click(getByLocator(AnalyticsPageOR.widgetExportType_Xpath, LocatorIdentifier.Xpath, exportType));
	}
}
