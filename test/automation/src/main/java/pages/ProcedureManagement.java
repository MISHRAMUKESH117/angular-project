package pages;

import static utilities.Constants.USER_WORK_DIR;

import java.util.concurrent.TimeUnit;
import static utilities.Common.sleep;
import static utilities.Constants.ProcedureManagementPageORFile;

import Actions.Robo;
import Actions.Sikuli;
import Actions.Web;
import objectRepository.ProcedureManagementPageOR;
import utilities.Constants;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.Log;
import utilities.YmlReader;

public class ProcedureManagement extends Locator {

	Web webActions;
	Robo robotActions;
	Sikuli sikuliActions;

	public ProcedureManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
		sikuliActions = new Sikuli();
		robotActions = new Robo();
	}

	private static ProcedureManagementPageOR procedureManagementPageOR = YmlReader
			.getobjectRepository(USER_WORK_DIR + ProcedureManagementPageORFile, ProcedureManagementPageOR.class);

	public void clickLeftMenuTabs(String tabName) {
		webActions.waitForElementClickability(
				getByLocator(procedureManagementPageOR.procedureManagementLeftMenuTabs_Xpath, LocatorIdentifier.Xpath, tabName), 10);
		webActions.click(getByLocator(procedureManagementPageOR.procedureManagementLeftMenuTabs_Xpath, LocatorIdentifier.Xpath, tabName));
	}

	public boolean isProcedureManagementMenuHeaderVisible(String tabName) {
		return webActions.isVisible(getByLocator(procedureManagementPageOR.procedureManagementTabHeaderName_Xpath, LocatorIdentifier.Xpath, tabName));
	}

	public void procedureTabUploadProcedureListExcel(String fileName) {
		webActions.click(getByLocator(procedureManagementPageOR.configurationOfProcedurePageImportButton_Xpath, LocatorIdentifier.Xpath));
		webActions.clickElementByOffSet(getByLocator(procedureManagementPageOR.configurationOfProcedurePageImportProcedurePopupBrowseButton_Xpath, LocatorIdentifier.Xpath));
		sleep(2, TimeUnit.SECONDS);
		sikuliActions.type(ExcelOperations.getFileNameInsideFolder(fileName, Constants.USER_WORK_DIR + Constants.TestDataPath).toString());
		robotActions.enter();
		webActions.clickElementByOffSet(getByLocator(procedureManagementPageOR.configurationOfProcedurePageImportProcedurePopupImportButton_Xpath, LocatorIdentifier.Xpath));
	}


	public void setConfigurationOfProcedurePageProcedureToSearch(String procedureName) {
		webActions.setText(
				getByLocator(procedureManagementPageOR.configurationOfProcedurePageSearchProcedureNameTextbox_Xpath, LocatorIdentifier.Xpath),
				procedureName);
		Log.printInfo("Set Configuration Of Procedure page Procedure to search as: " + procedureName);
	}

	public void clickConfigurationOfProcedurePageSearchProcedureButton() {
		webActions.waitForElementClickability(
				getByLocator(procedureManagementPageOR.configurationOfProcedurePageSearchProcedureButton_Xpath, LocatorIdentifier.Xpath), 10);
		webActions.click(getByLocator(procedureManagementPageOR.configurationOfProcedurePageSearchProcedureButton_Xpath, LocatorIdentifier.Xpath));
	}

	public boolean checkConfigurationOfProcedurePageProcedureVisibilityInGrid(String procedureName, String procedureVisibility) {
		if (procedureVisibility.equals("visible"))
			return webActions.isVisible(
					getByLocator(procedureManagementPageOR.configurationOfProcedurePageGridProcedureName_Xpath, LocatorIdentifier.Xpath, procedureName));
		else
			return !webActions.isVisible(
					getByLocator(procedureManagementPageOR.configurationOfProcedurePageGridProcedureName_Xpath, LocatorIdentifier.Xpath, procedureName));
	}

	public void clickConfigurationOfProcedurePageGridDeleteProcedureButton(String procedureName) {
		webActions.scrollToElement(
				getByLocator(procedureManagementPageOR.configurationOfProcedurePageDeleteGridProcedure_Xpath, LocatorIdentifier.Xpath, procedureName));
		webActions.waitForElementClickability(
				getByLocator(procedureManagementPageOR.configurationOfProcedurePageDeleteGridProcedure_Xpath, LocatorIdentifier.Xpath, procedureName),
				10);
		webActions.click(
				getByLocator(procedureManagementPageOR.configurationOfProcedurePageDeleteGridProcedure_Xpath, LocatorIdentifier.Xpath, procedureName));
	}

	public void clickConfigurationOfProcedurePageProcedureGridSaveButton() {
		webActions.click(getByLocator(procedureManagementPageOR.configurationOfProcedurePageProcedureGridSaveButton_Xpath, LocatorIdentifier.Xpath));
	}

	public void setTemplateNameInTemplatePanel(String templateName) {
		webActions.setText(
				getByLocator(procedureManagementPageOR.configurationOfTemplatePageTemplateName_XPath, LocatorIdentifier.Xpath), templateName);
	}

	public void clickTemplatePanelActionIcon(String iconName) {
		webActions.click(getByLocator(procedureManagementPageOR.configurationOfTemplatePageActionIcon_XPath, LocatorIdentifier.Xpath, iconName));
	}
	
	public boolean isTemplateLoaded(String templateName) {
		return webActions.isVisible(
				getByLocator(procedureManagementPageOR.configurationOfTemplatePageSearchTemplateName_XPath, LocatorIdentifier.Xpath, templateName));
	}	

	public void selectTemplateProcedureName(String procedureName) {
		webActions.setText(
				getByLocator(procedureManagementPageOR.configurationOfTemplatePageProcedureName_XPath, LocatorIdentifier.Xpath), procedureName);
		webActions.click(
				getByLocator(procedureManagementPageOR.configurationOfTemplatePageProcedureDropdownValue_XPath, LocatorIdentifier.Xpath, procedureName));
	}

	public void selectDefaultTemplate(String templateName) {
		webActions.click(
				getByLocator(procedureManagementPageOR.configurationOfTemplatePageTemplateRadioButtonIcon_XPath, LocatorIdentifier.Xpath, templateName));
	}

	public void clickTemplatePageButton(String buttonName) {
		webActions.click(getByLocator(procedureManagementPageOR.configurationOfTemplatePageSaveButton_XPath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void setProcedurePageNewProcedureName(String procedureName) {
		webActions.setText(getByLocator(procedureManagementPageOR.procedurePageAddNewProcedureNameTextbox_Xpath, LocatorIdentifier.Xpath), procedureName);
	}
		
	public void setProcedurePageNewProcedureBodyPart(String bodyPart) {
		webActions.setText(getByLocator(procedureManagementPageOR.procedurePageAddNewProcedureSearchBodyPart_Xpath, LocatorIdentifier.Xpath), bodyPart);
	}
	
	public void clickProcedurePageNewProcedureBodyPartDropdownOption(String bodyPart) {
		webActions.scrollToElement(getByLocator(procedureManagementPageOR.procedurePageAddNewProcedureSelectBodyPart_Xpath, LocatorIdentifier.Xpath, bodyPart));
		webActions.click(getByLocator(procedureManagementPageOR.procedurePageAddNewProcedureSelectBodyPart_Xpath, LocatorIdentifier.Xpath, bodyPart));
	}
	
	public void clickNewProcedureAddIcon() {
		webActions.click(getByLocator(procedureManagementPageOR.procedurePageNewProcedureAddButton_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void setProcedurePageNewProcedureCode(String procedureCode) {
		webActions.setText(getByLocator(procedureManagementPageOR.procedurePageNewProcedureCodeTextbox_Xpath, LocatorIdentifier.Xpath), procedureCode);
	}
	
	public void clickProcedurePageMultiChoiceDropdownArrow(String dropdownName) {
		webActions.click(getByLocator(procedureManagementPageOR.procedurePageMultiChoiceDropdownArrow_Xpath, LocatorIdentifier.Xpath, dropdownName));
	}
}
