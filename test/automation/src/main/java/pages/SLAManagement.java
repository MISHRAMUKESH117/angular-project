package pages;

import static utilities.Constants.SLAManagementPageORFile;
import static utilities.Constants.USER_WORK_DIR;

import java.util.List;

import Actions.Web;
import objectRepository.SLAManagementOR;
import utilities.Driver;
import utilities.YmlReader;

public class SLAManagement extends Locator {

	Web webActions;

	public SLAManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
	}

	private static SLAManagementOR slaManagementOR = YmlReader.getobjectRepository(USER_WORK_DIR + SLAManagementPageORFile, SLAManagementOR.class);

	public void clickAddNewRuleButton() {
		webActions.click(getByLocator(slaManagementOR.addNewRuleButton_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void setRuleName(String ruleName) {
		webActions.setText(getByLocator(slaManagementOR.ruleName_Xpath, LocatorIdentifier.Xpath), ruleName);
	}
	
	public void setEscalationMinutes(Integer escalationMinutes) {
		webActions.setText(getByLocator(slaManagementOR.escalationMinutes_Xpath, LocatorIdentifier.Xpath), escalationMinutes.toString());
	}

	public void selectEscalationPriority(String escalationPriority) {
		webActions.click(getByLocator(slaManagementOR.escalationPriorityDropdownArrow_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(slaManagementOR.escalationPriorityDropdownOptions_Xpath, LocatorIdentifier.Xpath, escalationPriority));
	}
	
	public void clickPropertiesPanelButton(String buttonName) {
		webActions.click(getByLocator(slaManagementOR.propertiesButton_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public boolean isSLARuleVisible(String ruleName) {
		return webActions.isVisible(getByLocator(slaManagementOR.rulesGridValue_Xpath, LocatorIdentifier.Xpath, ruleName));
	}
	
	public void clickRuleInGrid(String ruleName) {
		webActions.click(getByLocator(slaManagementOR.rulesGridValue_Xpath, LocatorIdentifier.Xpath, ruleName));
	}
	
	public void deleteSLARule() {
		webActions.click(getByLocator(slaManagementOR.deleteSLARuleIcon_Xpath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(slaManagementOR.confirmationPopupYesButton_Xpath, LocatorIdentifier.Xpath));
	}

	public void setFilterValue(String input) {
		webActions.isVisible(getByLocator(slaManagementOR.searchSLATextbox_Xpath, LocatorIdentifier.Xpath));
		webActions.setText(getByLocator(slaManagementOR.searchSLATextbox_Xpath, LocatorIdentifier.Xpath),input);	
	}
	
	public List<String> getAllRulesNameInGrid() {
		return webActions.getElementTextList(getByLocator(slaManagementOR.rulesNameInGrid_Xpath, LocatorIdentifier.Xpath));
	}
	
	public void clickRulesGridHeaderButton(String buttonName) {
		webActions.click(getByLocator(slaManagementOR.rulesGridHeaderButtons_Xpath, LocatorIdentifier.Xpath, buttonName));
	}
	
	public void clickRulesGridColumnHeaderDropdownArrow(String columnHeaderName) {
		webActions.mouseHover(getByLocator(slaManagementOR.rulesGridColumnHeader_Xpath, LocatorIdentifier.Xpath, columnHeaderName));
		webActions.click(getByLocator(slaManagementOR.rulesGridColumnHeaderDropdownArrow_Xpath, LocatorIdentifier.Xpath, columnHeaderName));
	}
	
	public void clickRulesGridColumnHeaderDropdownOption(String dropdownOption) {
		webActions.click(getByLocator(slaManagementOR.rulesGridColumnHeaderDropdownArrowOptions_Xpath, LocatorIdentifier.Xpath, dropdownOption));
	}
	
	public List<String> sortRulesInAscendingOrder(List<String> rulesList) {
		return webActions.sortListInAscendingOrder(rulesList);
	}
	
	public List<String> sortRulesInDescendingOrder(List<String> rulesList) {
		return webActions.sortListInDescendingOrder(rulesList);
	}
	
	public Integer getRuleRankInGrid(String ruleName) {
		webActions.scrollToElement(getByLocator(slaManagementOR.ruleRankInGrid_Xpath, LocatorIdentifier.Xpath, ruleName));
		return Integer.valueOf(webActions.getText(getByLocator(slaManagementOR.ruleRankInGrid_Xpath, LocatorIdentifier.Xpath, ruleName)));
	}
	
	public void rearrangeRule(Integer originalRank, Integer destinationRank) {
		webActions.dragDropElementVertically(
				getByLocator(slaManagementOR.rankNumberInGrid_Xpath, LocatorIdentifier.Xpath, originalRank),
				getByLocator(slaManagementOR.rankNumberInGrid_Xpath, LocatorIdentifier.Xpath, destinationRank), 2);
	}
}
