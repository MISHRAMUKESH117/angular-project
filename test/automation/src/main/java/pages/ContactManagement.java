package pages;

import static utilities.Common.sleep;
import static utilities.Constants.ContactManagementPageOR;
import static utilities.Constants.USER_WORK_DIR;

import java.util.List;
import java.util.concurrent.TimeUnit;

import Actions.Robo;
import Actions.Sikuli;
import Actions.Web;
import objectRepository.ContactManagementPageOR;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.YmlReader;

public class ContactManagement extends Locator {

	Web webActions;
	Robo robotActions;
	Sikuli sikuliActions;

	public ContactManagement(Driver driver) {
		webActions = new Web(driver.getWebDriver());
		sikuliActions = new Sikuli();
		robotActions = new Robo();
	}

	private static ContactManagementPageOR contactManagementOR = YmlReader
			.getobjectRepository(USER_WORK_DIR + ContactManagementPageOR, ContactManagementPageOR.class);

	public void enterContactDetails(String contactName, String textbox) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.nameTextBox_XPath, LocatorIdentifier.Xpath, textbox), 60);
		webActions.setText(getByLocator(contactManagementOR.nameTextBox_XPath, LocatorIdentifier.Xpath, textbox), contactName);
	}

	public void clickContactButton(String button) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.contactButton_XPath, LocatorIdentifier.Xpath, button), 60);
		webActions.click(getByLocator(contactManagementOR.contactButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public boolean isSearchSuccessfull(String contactName) {		
		return webActions.isVisible(getByLocator(contactManagementOR.searchData_XPath, LocatorIdentifier.Xpath,contactName));
	}

	public void clickCreateNewContact() {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.createContactIcon_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(contactManagementOR.createContactIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void createNewContact(String contactDetail, String textbox) {
		webActions.scrollToElement(getByLocator(contactManagementOR.createNewContactTextbox_XPath, LocatorIdentifier.Xpath, textbox));
		webActions.waitForElementClickability(getByLocator(contactManagementOR.createNewContactTextbox_XPath, LocatorIdentifier.Xpath, textbox), 60);
		webActions.setText(getByLocator(contactManagementOR.createNewContactTextbox_XPath, LocatorIdentifier.Xpath, textbox), contactDetail);
	}

	public void clickSaveContact(String button) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.createButton_XPath, LocatorIdentifier.Xpath, button), 60);
		webActions.click(getByLocator(contactManagementOR.createButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public boolean isContactCreated(String contactName) {
		webActions.waitForElementVisibility(getByLocator(contactManagementOR.newContactText_XPath, LocatorIdentifier.Xpath, contactName), 60);
		return webActions.isVisible(getByLocator(contactManagementOR.newContactText_XPath, LocatorIdentifier.Xpath, contactName));
	}

	public void openContactToEdit(String contactName) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.contactOpen_XPath, LocatorIdentifier.Xpath, contactName), 60);
		webActions.click(getByLocator(contactManagementOR.contactOpen_XPath, LocatorIdentifier.Xpath, contactName));
	}

	public void clickEditContactButton(String button) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.editContactButton_XPath, LocatorIdentifier.Xpath, button), 60);
		webActions.click(getByLocator(contactManagementOR.editContactButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public void editDetailsFromDropdown(String contactDetail, String textbox) {
		webActions.click(getByLocator(contactManagementOR.editContactDropdownIcon_XPath, LocatorIdentifier.Xpath, textbox));
		webActions.waitForElementClickability(
				getByLocator(contactManagementOR.editContactDropdownValue_XPath, LocatorIdentifier.Xpath, contactDetail), 60);
		webActions.click(getByLocator(contactManagementOR.editContactDropdownValue_XPath, LocatorIdentifier.Xpath, contactDetail));
		webActions.click(getByLocator(contactManagementOR.closeContactDropdown_XPath, LocatorIdentifier.Xpath, textbox));
		sleep(10, TimeUnit.SECONDS);
	}

	public String isContactEdit() {
		webActions.waitForElementVisibility(getByLocator(contactManagementOR.editContact_XPath, LocatorIdentifier.Xpath), 60);
		String communicationPreferenceText[] = webActions.getText(getByLocator(contactManagementOR.editContact_XPath, LocatorIdentifier.Xpath))
				.split(":");
		return communicationPreferenceText[1].trim();
	}

	public void clickContactToMerge(String firstContact, String secondContact) {
		webActions.clickCheckbox(firstContact, secondContact);
	}

	public boolean isContactMerged(String contactName,String emailId) {
		webActions.click(getByLocator(contactManagementOR.newContactText_XPath, LocatorIdentifier.Xpath, contactName));
		webActions.waitForElementVisibility(getByLocator(contactManagementOR.editContact_XPath, LocatorIdentifier.Xpath), 60);
		return webActions.isVisible(getByLocator(contactManagementOR.contantInfo_XPath, LocatorIdentifier.Xpath, emailId));
	}

	public void selectContactToDelete(String contactName) {
		webActions.scrollToElement(getByLocator(contactManagementOR.selectContact_XPath, LocatorIdentifier.Xpath, contactName));
		webActions.waitForElementClickability(getByLocator(contactManagementOR.selectContact_XPath, LocatorIdentifier.Xpath, contactName), 60);
		webActions.click(getByLocator(contactManagementOR.selectContact_XPath, LocatorIdentifier.Xpath, contactName));
	}

	public void clickDeleteConfirmation(String button) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.deleteConfirmation_XPath, LocatorIdentifier.Xpath, button), 60);
		webActions.click(getByLocator(contactManagementOR.deleteConfirmation_XPath, LocatorIdentifier.Xpath, button));
	}

	public boolean isContactDeleted(String contactName) {
		return !webActions.isVisible(getByLocator(contactManagementOR.newContactText_XPath, LocatorIdentifier.Xpath, contactName));
	}

	public void clickExportImportButton(String button) {
		webActions.click(getByLocator(contactManagementOR.exportImportButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public void selectExportImportDropdownOption(String exportImportDropdownValue) {
		webActions.waitForElementClickability(
				getByLocator(contactManagementOR.exportImportDropdownValue_XPath, LocatorIdentifier.Xpath, exportImportDropdownValue), 60);
		webActions.click(getByLocator(contactManagementOR.exportImportDropdownValue_XPath, LocatorIdentifier.Xpath, exportImportDropdownValue));
		sleep(20, TimeUnit.SECONDS);
	}

	public void enterSite(String siteName) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.siteDropDownIcon_XPath, LocatorIdentifier.Xpath), 60);
		webActions.click(getByLocator(contactManagementOR.siteDropDownIcon_XPath, LocatorIdentifier.Xpath));
		webActions.click(getByLocator(contactManagementOR.siteDropDownValue_XPath, LocatorIdentifier.Xpath, siteName));
	}

	public boolean isExcelSheetDownlaoded(String fileName, String sheetName) {
		List<String> excelCellValues = ExcelOperations.readExcel(fileName, sheetName);
		return excelCellValues.contains("Referring");
	}

	public void writeDataInExcel(String fileName) {
		ExcelOperations.writeDataInExcel(fileName,"Contacts", "First Name", "Last Name");
	}

	public void uploadFile(String fileName) {
		webActions.clickElementByOffSet(getByLocator(contactManagementOR.browseFile_XPath, LocatorIdentifier.Xpath));
		sleep(2, TimeUnit.SECONDS);
		sikuliActions.type(ExcelOperations.getFileName(fileName).toString());
		robotActions.enter();
		webActions.click(getByLocator(contactManagementOR.importFileButton_XPath, LocatorIdentifier.Xpath));
	}

	public boolean isExcelSheetUploaded(String contactName) {
		webActions.waitForElementVisibility(getByLocator(contactManagementOR.addedUserValue_XPath, LocatorIdentifier.Xpath, contactName), 60);
		return webActions.isVisible(getByLocator(contactManagementOR.addedUserValue_XPath, LocatorIdentifier.Xpath, contactName));
	}

	public void clickEditSiteIcon() {
		webActions.click(getByLocator(contactManagementOR.editSiteIcon_Id, LocatorIdentifier.Id));
	}

	public void setExamNoteText(String text) {
		webActions.scrollToElement(getByLocator(contactManagementOR.editSiteExamNoteText_XPath, LocatorIdentifier.Xpath));
		webActions.setText(getByLocator(contactManagementOR.editSiteExamNoteText_XPath, LocatorIdentifier.Xpath), text);
	}

	public void selectExamNoteDropdownValue(String label, String dropdownValue) {
		webActions.scrollToElement(getByLocator(contactManagementOR.examNoteMultiChiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
		webActions.click(getByLocator(contactManagementOR.examNoteMultiChiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
		webActions.click(getByLocator(contactManagementOR.multiChoiceDropdownValue_XPath, LocatorIdentifier.Xpath, dropdownValue));
		webActions.click(getByLocator(contactManagementOR.examNoteMultiChiceDropdownLabel_XPath, LocatorIdentifier.Xpath, label));
	}

	public void clickAddExamNoteIcon() {
		webActions.click(getByLocator(contactManagementOR.contactSiteWindowAddExamNoteIcon_XPath, LocatorIdentifier.Xpath));
	}

	public void clickContactSiteWindowButton(String button) {
		webActions.click(getByLocator(contactManagementOR.editContactSiteWindowButton_XPath, LocatorIdentifier.Xpath, button));
	}

	public boolean isValuePresentUnderSiteInformationPanel(String value) {
		return webActions.isVisible(getByLocator(contactManagementOR.siteInformationPanelValue_XPath, LocatorIdentifier.Xpath, value));
	}

	public void clickNoteDeleteIcon(String text) {
		webActions.scrollToElement(getByLocator(contactManagementOR.deleteExamNote_XPath, LocatorIdentifier.Xpath, text));
		webActions.click(getByLocator(contactManagementOR.deleteExamNote_XPath, LocatorIdentifier.Xpath, text));
	}

	public void openContactBySiteName(String siteName) {
		webActions.waitForElementClickability(getByLocator(contactManagementOR.openContactWithSiteName_XPath, LocatorIdentifier.Xpath, siteName), 60);
		webActions.click(getByLocator(contactManagementOR.openContactWithSiteName_XPath, LocatorIdentifier.Xpath, siteName));
	}

	public boolean isValuePresentUnderContactInformationPanel(String value) {
		return webActions.isVisible(getByLocator(contactManagementOR.contactInformationPanelValue_XPath, LocatorIdentifier.Xpath, value));
	}
}
