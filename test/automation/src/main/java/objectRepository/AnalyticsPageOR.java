package objectRepository;

public class AnalyticsPageOR {	
	public String analyticsLaunch_Xpath;
	public String widgetName_Xpath;
	public String hoverText1_Xpath;
	public String hoverText2_Xpath;
	public String dashboard_Xpath;
	public String editWidget_Xpath;
	public String actionWidget_Xpath;
	public String verifyEditDialogue_Xpath;
	public String deleteWidget_Xpath;
	public String exportWidget_Xpath;
	public String exportAlert_Xpath;
	public String exportSheet_Xpath;
	public String singleChoiceDropDownValue_Xpath;
	public String allModality_Xpath;
	public String singleChoiceDropdownLabel_Xpath;
	public String changedInputParam_Xpath;
	public String checkBox_Xpath;
	public String randomClick_Xpath;
	public String widgetLabelNameDropdown_Xpath;
	public String widgetLabelNameDropdownOptions_Xpath;
	public String editWidgetStartDate_Xpath;
	public String widgetExportButton_Xpath;
	public String widgetExportType_Xpath;
	public String widgetBarGraph_Xpath;
	public String numberOfExamsToolTip_Xpath;
	public String widgetSourceLocator_Xpath;
	public String widgetDestinationLocator_Xpath;
	public String verticalShift_Xpath;
	public String examRange_Xpath;
	public String editFilter_Xpath;
	public String multiChoiceDropdownLabel_Xpath;
	public String multiChoiceDropdownValue_Xpath;
}
