package objectRepository;

public class NotificationManagementOR {
	public String propertyName_XPath;
	public String propertyDescription_XPath;
	public String singleChoiceDropdownLabel_XPath;
	public String singleChoiceDropdownValue_XPath;
	public String multiChoiceDropdownLabel_XPath;
	public String multiChoiceDropdownValue_XPath;
	public String savePropertyIcon_Id;
	public String deletePropertyIcon_Id;
	public String deleteConfirmationWindow_XPath;
	public String cancelPropertyIcon_Id;
	public String addNewNotificationIcon_XPath;
	public String notificationRuleTableValue_XPath;
	public String editPropertyIcon_Id;
	public String propertyTimeOfDayStartTime_Xpath;
	public String propertyTimeOfDayFinishTime_Xpath;
	public String searchNotificationTextbox_Xpath;
}
