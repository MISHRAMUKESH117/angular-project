package objectRepository;

public class UserManagementPageOR {
	public String addUserButton_Xpath;
	public String userFirstName_Xpath;
	public String userLastName_Xpath;
	public String userLoginName_Xpath;
	public String userPassword_Xpath;
	public String userPasswordVerify_Xpath;
	public String localUserRolesDropdownArrow_Xpath;
	public String localUserRolesOptionsCheckbox_Xpath;
	public String saveUserButton_Xpath;
	public String searchUserTextbox_Xpath;
	public String searchUserButton_Xpath;
	public String gridUserName_Xpath;
	public String userEdit_Id;
	public String addLicense_Xpath;
	public String saveLicensesButton_Xpath;

}
