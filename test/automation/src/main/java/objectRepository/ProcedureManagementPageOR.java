package objectRepository;

public class ProcedureManagementPageOR {
	public String procedureManagementLeftMenuTabs_Xpath;
	public String procedureManagementTabHeaderName_Xpath;
	public String configurationOfProcedurePageImportButton_Xpath;
	public String configurationOfProcedurePageImportProcedurePopupBrowseButton_Xpath;
	public String configurationOfProcedurePageImportProcedurePopupImportButton_Xpath;
	public String configurationOfProcedurePageSearchProcedureNameTextbox_Xpath;
	public String configurationOfProcedurePageSearchProcedureButton_Xpath;
	public String configurationOfProcedurePageGridProcedureName_Xpath;
	public String configurationOfProcedurePageDeleteGridProcedure_Xpath;
	public String configurationOfProcedurePageProcedureGridSaveButton_Xpath;
	public String configurationOfTemplatePageTemplateName_XPath;
	public String configurationOfTemplatePageActionIcon_XPath;	
	public String configurationOfTemplatePageSearchTemplateName_XPath;
	public String configurationOfTemplatePageProcedureName_XPath;
	public String configurationOfTemplatePageProcedureDropdownValue_XPath;
	public String configurationOfTemplatePageTemplateRadioButtonIcon_XPath;
	public String configurationOfTemplatePageSaveButton_XPath;
	public String procedurePageAddNewProcedureNameTextbox_Xpath;
	public String procedurePageAddNewProcedureSearchBodyPart_Xpath;
	public String procedurePageAddNewProcedureSelectBodyPart_Xpath;
	public String procedurePageNewProcedureAddButton_Xpath;
	public String procedurePageNewProcedureCodeTextbox_Xpath;
	public String procedurePageMultiChoiceDropdownArrow_Xpath;
}
