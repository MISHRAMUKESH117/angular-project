package objectRepository;

public class UserDetails {

	public String radiologistUsername;
	public String radiologistPassword;
	public String radiologistName;
	public String adminUsername;
	public String adminPassword;
	public String adminName;
	public String otherUsername;
	public String otherPassword;
	public String otherName;
	public String defaultUsername;
	public String defaultPassword;
	public String licenseUsername;
	public String licensePassword;
	
}
