package objectRepository;

public class ContactManagementPageOR {
	public String nameTextBox_XPath;
	public String contactButton_XPath;
	public String searchData_XPath;
	public String createContactIcon_XPath;
	public String createButton_XPath;
	public String createNewContactTextbox_XPath;
	public String newContactText_XPath;
	public String contactOpen_XPath;
	public String editContactDropdownIcon_XPath;
	public String editContactDropdownValue_XPath;
	public String closeContactDropdown_XPath;
	public String editContact_XPath;
	public String selectContact_XPath;
	public String editContactButton_XPath;
	public String deleteConfirmation_XPath;
	public String exportImportButton_XPath;
	public String exportImportDropdownValue_XPath;
	public String siteDropDownIcon_XPath;
	public String siteDropDownValue_XPath;
	public String browseFile_XPath;
	public String browserFileTextbox_XPath;	
	public String importFileButton_XPath;
	public String addedUserValue_XPath;
	public String editSiteIcon_Id;
	public String editSiteExamNoteText_XPath;
	public String examNoteMultiChiceDropdownLabel_XPath;
	public String multiChoiceDropdownValue_XPath;
	public String contactSiteWindowAddExamNoteIcon_XPath;
	public String editContactSiteWindowButton_XPath;
	public String siteInformationPanelValue_XPath;
	public String deleteExamNote_XPath;
	public String openContactWithSiteName_XPath;
	public String contactInformationPanelValue_XPath;
	public String contantInfo_XPath;
}
