package objectRepository;

public class HomePageOR {
	public String homePageTab_XPath;
	public String homePageApplicationButton_XPath;	
	public String homePageApplicationModules_XPath;	
	public String loginTextIcon_XPath;
	public String userProfileDropdownValue_XPath;
	public String userSettingsScheduleIdTextbox_XPath;
	public String userSettingsActionButton_XPath;
	public String headerModule_XPath;
	public String profileManagementCheckbox_XPath;
	public String isProfileManagementCheckboxChecked_XPath;
	public String moduleMenuItemDropdownButton_Xpath;
	public String moduleMenuItemDropdownList_Xpath;
	public String profileManagementActionButton_XPath;
	public String profileManagementMultiChoiceDropdownLabel_XPath;
	public String profileManagementMultiChoiceDropdownValue_XPath;
	public String profileManagementMultiChoiceDropdownClass_XPath;
}
