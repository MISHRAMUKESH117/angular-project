package objectRepository;

public class SLAManagementOR {
	public String addNewRuleButton_Xpath;
	public String ruleName_Xpath;
	public String escalationMinutes_Xpath;
	public String escalationPriorityDropdownArrow_Xpath;
	public String escalationPriorityDropdownOptions_Xpath;
	public String propertiesButton_Xpath;
	public String rulesGridValue_Xpath;
	public String deleteSLARuleIcon_Xpath;
	public String confirmationPopupYesButton_Xpath;
	public String searchSLATextbox_Xpath;
	public String rulesGridRow_Xpath;
	public String rulesNameInGrid_Xpath;
	public String rulesGridHeaderButtons_Xpath;
	public String rulesGridColumnHeader_Xpath;
	public String rulesGridColumnHeaderDropdownArrow_Xpath;
	public String rulesGridColumnHeaderDropdownArrowOptions_Xpath;
	public String ruleRankInGrid_Xpath;
	public String rankNumberInGrid_Xpath;
}