package objectRepository;

public class UserCredentialingPageOR {
	public String leftPanelTabNames_Xpath;
	public String userNameToSearch_Xpath;
	public String userTabUserTableUserName_Xpath;
	public String commonMemberCheckbox_Xpath;
	public String commonFinalRadioButton_Xpath;
	public String removeGroupMembershipMessage_Xpath;
	public String userCredentialingSaveButton_Xpath;
	public String removeAllCredentialingMessage_Xpath;
	public String userCredentialingMemberCheckbox_Xpath;
	public String userCredentialingFinalRadioButton_Xpath;
	public String userTabRightPanelTabNames_Xpath;
	public String commonMemberCheckboxChecked_Xpath;
	

}
