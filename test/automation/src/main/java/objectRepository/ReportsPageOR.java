package objectRepository;

public class ReportsPageOR {
	public String reportsButton_Xpath;
	public String reportName_Xpath;
	public String date_Xpath;
	public String toDate_Xpath;
	public String createReport_Xpath;
	public String closeReportDownloadAlert_Xpath;
	public String reportDownloadAlert_Xpath;
	public String reportDownloadSuccess_Xpath;
	public String archiveReport_Xpath;
	public String reportDownloadIcon_Xpath;
	public String clickdropdown_Xpath;
	public String valueSelect_Xpath;
	public String saveButton_Xpath;
	public String setReportName_XPath;
	public String reportDropDownArrow_Xpath;
	public String reportDropDownValue_Xpath;
	public String saveScheduleButton_Xpath;
	public String clickOkAfterValueSelect;
}
