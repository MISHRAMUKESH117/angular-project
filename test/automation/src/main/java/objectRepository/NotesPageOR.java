package objectRepository;

public class NotesPageOR {

	public String noteType_Xpath;
	public String communicationNoteTextArea_Xpath;
	public String noteSubmitCancelClear_Xpath;
	public String noteCreated_Xpath;
	public String openNote_XPath;
	public String noteStatus_XPath;
	public String deleteNoteButton_XPath;
	public String deleteAllNoteConfirmation_XPath;
	public String communicationNoteCommentTextbox_XPath;
	public String noteComment_XPath;
	public String communicationNoteWindow_XPath;
	public String teachingNoteTitleTextbox_XPath;
	public String teachingNoteSaveComment_XPath;
	public String noteEditWindow_XPath;
	public String examNoteType_XPath;
	public String examNoteDropdownValue_XPath;
	public String addWorklistDropdown_XPath;
	public String worklistDropdownValue_XPath;
	public String saveNewWorklistButton_XPath;
	public String examNoteIndicatorIcon_XPath;
	public String examNoteTooltipValue_XPath;
	public String addNoteMessageTextbox_XPath;
	public String newWorklistTextboxName_XPath;
	public String techQANoteIndicatorIcon_XPath;
	public String followUpNoteDueDateIcon_XPath;
	public String followupNoteIndicatorIcon_XPath;
	public String communicationNoteIndicatorIcon_XPath;
	public String noteDate_XPath;
	public String peerReviewNoteIndicator_XPath;
	public String deletePeerReviewNoteIcon_XPath;
	public String edPrelimNoteIndicator_XPath;
	public String communicationNoteType_XPath;
	public String communicationNoteTypeValue_XPath;
	public String noteAlertMessage_XPath;
	public String noteAcknowledgeButton_XPath;
}
