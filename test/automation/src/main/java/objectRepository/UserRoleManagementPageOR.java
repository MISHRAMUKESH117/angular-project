package objectRepository;

public class UserRoleManagementPageOR {
	public String pageHeader_Xpath;
	public String addUserRole_Id;
	public String userRoleName_Id;
	public String userRoleDescription_Xpath;
	public String permissionCheckbox_Xpath;
	public String saveNewRole_Id;
	public String gridUserRoleName_Xpath;
	public String userRoleAlreadyExistsError_Xpath;
	public String userRoleEdit_Id;
	public String numberOfUsersHyperlink_Xpath;
	public String userNameSearchBox_Xpath;
	public String selectUser_Xpath;
	public String saveUsersButton_Xpath;
	public String deSelectUser_Xpath;
	public String searchUsersButton_Xpath;
	public String deleteUserRole_Id;
	public String deleteUserRolePopup_Xpath;
	public String deleteUserRolePopupYesButton_Xpath;
	public String userRoleSearchBox_Xpath;
	public String gridUserRoleNameContains_Xpath;
}
