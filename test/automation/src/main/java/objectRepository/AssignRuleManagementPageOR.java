package objectRepository;

public class AssignRuleManagementPageOR {
	public String addAssignRule_Xpath;
	public String assignRuleName_Xpath;
	public String assignRulePropertiesDropdownArrow_Xpath;
	public String assignRulePropertiesSingleDropdownArrowOptions_Xpath;
	public String saveAssignmentRule_Id;
	public String ruleAlreadyExistsError_Xpath;
	public String gridRuleName_Xpath;
	public String editRuleIcon_Id;
	public String deleteRuleIcon_Id;
	public String deleteRulePopup_Xpath;
	public String deleteRulePopupYesButton_Xpath;
	public String assignRuleSearchBox_Xpath;
	public String pagingButton_Xpath;
	public String addAssignmentProperties_Xpath;
	public String rulesGridHeaderButtons_Xpath;
	public String ruleRankInGrid_Xpath;
	public String rankNumberInGrid_Xpath;
	public String assignmentPropertyAddIcon_Xpath;
}
