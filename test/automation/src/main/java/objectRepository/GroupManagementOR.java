package objectRepository;

public class GroupManagementOR {
	public String addButton_Id;
	public String addDropdownOption_Xpath;
	public String groupName_Id;
	public String groupDescription_Id;
	public String saveGroup_Id;
	public String gridGroupName_Xpath;
	public String editGropIcon_Id;
	public String deleteGroupIcon_Id;
	public String confirmationPopupYesButton_Xpath;
	public String examTypeDefinitionAddButton_Xpath;
	public String newExamTypeDefinitionName_Xpath;
	public String btnOK_Xpath;
	public String examTypeDefinitionNameDropdownArrow_Xpath;
	public String examTypeDefinitionDropdownNames_Xpath;
	public String gridActionIcon_Xpath;
	public String subSpecialtyOrGroupRankInGrid_Xpath;
	public String rankNumberInGrid_Xpath;
	public String groupSpecialityToggleButton_Xpath;
	public String userRoleSearchBox_Xpath;
	public String searchGroupTextbox_Xpath;
}