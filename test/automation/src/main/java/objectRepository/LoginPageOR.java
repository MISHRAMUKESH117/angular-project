package objectRepository;

import org.openqa.selenium.By;

public class LoginPageOR {	
	public String userName_Id;
	public String password_Id;
	public String loginButton_Xpath;
	public String logoutButton_Xpath;
	public String loginText_Xpath;
	public String examAssignmentAlert_Xpath;
	public String examAssignmentAlertLogoutButton_Xpath;
	public String clarioLogo_Xpath;
	public String buildVersion_Xpath;
	public String browserDialogueBox_Xpath;
	public String browserDialogueBoxMessage_Xpath;
	public String currentUserPassword_Xpath;
	public String currentUserNewPassword_Xpath;
	public String currentUserVerifyPassword_Xpath;
	public String changePasswordButton_Xpath;
}
