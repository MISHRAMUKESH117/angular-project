package Actions;

import static utilities.Constants.ConfigurationORFile;
import static utilities.Constants.USER_WORK_DIR;
import static utilities.Common.sleep;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import objectRepository.ConfigurationPageOR;
import pages.Locator;
import utilities.Constants;
import utilities.DBConnection;
import utilities.Driver;
import utilities.ExcelOperations;
import utilities.Property;
import utilities.YmlReader;

public class ConfigurationNoDriver {


	DBConnection dbConnection;
	Property property;
	private String databasePropertyFile = "Automation.properties";

	public ConfigurationNoDriver() {
		dbConnection = new DBConnection();
		property = new Property();
	}
	
	public void changeBrowserCompatibilityConfiguration(List<String> browserTypeList) {
		dbConnection.createDatabaseConnection();
		dbConnection.setBrowserCompatability(browserTypeList);
	}

	public void changeAutomationPropertyValue(String propertyType, String propertyValue) {
		property.updatePropertiesFileValue(databasePropertyFile,propertyType,propertyValue);
	}
	
	public void changeBrowserCompatibilityConfigurationNull() {
		dbConnection.createDatabaseConnection();
		dbConnection.setBrowserCompatabilityNull();
	}
	
	public void deleteUserSession() {
		dbConnection.createDatabaseConnection();
		dbConnection.deleteUserSessionQuery();
	}
}
