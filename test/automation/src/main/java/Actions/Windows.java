package Actions;

import java.awt.Rectangle;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.winium.WiniumDriver;

public class Windows {

	Actions actions;
	WiniumDriver winDriver;

	public Windows(WiniumDriver winDriver) {
		this.winDriver = winDriver;
	}

	public List<WebElement> findWindowElementsBy(By locator) {
		List<WebElement> elements = null;
		elements = winDriver.findElements(locator);
		return elements;
	}

	public List<WebElement> findWindowElementsBy(WebElement window, By locator) {
		List<WebElement> elements = null;
		elements = window.findElements(locator);
		return elements;
	}

	public WebElement findWindowElementBy(By locator) {
		WebElement element = null;
		element = winDriver.findElement(locator);
		return element;
	}

	public WebElement findWindowElementBy(WebElement window, By locator) {
		WebElement element = null;
		element = window.findElement(locator);
		return element;
	}

	public void waitForWindowElementToBeVisible(By locator, long timeoutInSeconds) {
		WebDriverWait wait = new WebDriverWait(winDriver, timeoutInSeconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public void waitForWindowElementToBeInvisible(By locator, long timeoutInSeconds) {
		WebDriverWait wait = new WebDriverWait(winDriver, timeoutInSeconds);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}

	public void click(By locator) {
		findWindowElementBy(locator).click();
	}

	public void click(WebElement window, By locator) {
		findWindowElementBy(window, locator).click();
	}
	
	public void doubleClick(WebElement window, By locator)  {
		Actions action = new Actions(winDriver);
		WebElement element = findWindowElementBy(window, locator);
		action.doubleClick(element).build().perform();
	}
	
	public void rightClick(WebElement window, By locator)  {
		Actions action = new Actions(winDriver);
		WebElement element = findWindowElementBy(window, locator);
		action.doubleClick(element).build().perform();
	}
	
	public void setText(WebElement window, By locator, String value) {
		WebElement element = findWindowElementBy(window, locator);
		element.clear();
		element.sendKeys(value);
	}

	public void setText(By locator, String value) {
		WebElement element = findWindowElementBy(locator);
		element.clear();
		element.sendKeys(value);
	}
	
	public void setText(WebElement element, String value) {
		element.clear();
		element.sendKeys(value);
	}

	public void hoverOnElement(By locator) {
		actions = new Actions(winDriver);
		WebElement element = findWindowElementBy(locator);
		actions.moveToElement(element).build().perform();
	}

	public void hoverOnElement(WebElement window, By locator) {
		actions = new Actions(winDriver);
		WebElement element = findWindowElementBy(window, locator);
		actions.moveToElement(element).build().perform();
	}

	public void movescrollbarToBottom(WebElement window, By scrollbarLocator) {
		actions = new Actions(winDriver);
		WebElement element = findWindowElementBy(window, scrollbarLocator);
		int scrollHeight = element.getSize().height;
		actions.moveToElement(element, 10, 25);
		actions.clickAndHold().moveByOffset(0, scrollHeight).release();
		actions.build().perform();
	}

	public boolean isVisible(By locator) {
		try {
			waitForWindowElementToBeVisible(locator, 10);
			return winDriver.findElement(locator).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isVisible(WebElement window, By locator) {
		try {
			waitForWindowElementToBeVisible(locator, 10);
			WebElement element = findWindowElementBy(window, locator);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	public void clearText(By locator) {
		findWindowElementBy(locator).click();
		findWindowElementBy(locator).clear();
	}

	public boolean isElementEnabled(By locator) {
		return findWindowElementBy(locator).isEnabled();
	}

	public int getElementsCount(By locator) {
		return findWindowElementsBy(locator).size();
	}

	public String getText(WebElement window, By locator) {

		WebElement element = findWindowElementBy(window, locator);
		return element.getAttribute("Name").toString();
	}
	
	public Rectangle getElementBounds(WebElement window, By locator) {
		Rectangle rectangle = new Rectangle();
		WebElement element = findWindowElementBy(window, locator);
		String[] rectangleValue = element.getAttribute("BoundingRectangle").split(",");
		rectangle.x = Integer.parseInt(rectangleValue[0]);
		rectangle.y = Integer.parseInt(rectangleValue[1]);
		rectangle.width = Integer.parseInt(rectangleValue[2]);
		rectangle.height = Integer.parseInt(rectangleValue[3]);
		return rectangle;
	}
	
	public void moveByOffset(WebElement winName, int x, int y) {
		//actions.moveToElement(winName, x, y);
		actions.moveByOffset(x, y);
		actions.click();
		actions.build();
		actions.perform();
	}
}
