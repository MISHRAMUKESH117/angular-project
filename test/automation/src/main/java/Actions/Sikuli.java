package Actions;

import static utilities.Constants.ReportScreens;
import static utilities.Constants.USER_WORK_DIR;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.sikuli.script.Button;
import org.sikuli.script.Env;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;


import utilities.Common;
import utilities.Log;

@SuppressWarnings("deprecation")
public class Sikuli {

	Screen screen;
	Pattern pattern;
	public static final String CommonElementDir = USER_WORK_DIR + ReportScreens + "Common\\";

	public Sikuli() {
		screen = new Screen();
		pattern = new Pattern();
	}

	public Location getLocation(String patternFile) {
		Match m = null;
		pattern.setFilename(patternFile);
		try {
			m = screen.find(pattern);
		} catch (FindFailed e) {
			e.printStackTrace();
		}
		return m.getTarget();
	}
	
	
	
	public void click(String patternFile) {
		pattern.setFilename(patternFile);
		try {
			screen.find(pattern).click();
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}

	public void doubleClick(String patternFile) {
		pattern.setFilename(patternFile);
		try {
			screen.find(pattern).doubleClick();
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void wait(double secondsToWait) {
		screen.wait(secondsToWait);
	}

	public void clearTextFromField() {
		screen.type("a", KeyModifier.CTRL);
		screen.type(Key.BACKSPACE);
	}

	public void typeTextInField(String patternFile, String value) {
		try {
			pattern.setFilename(patternFile);
			screen.find(pattern).click();
			screen.wait(1.0);
			clearTextFromField();
			screen.type(pattern, value);
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}

	public String getTextFromField(String patternFile) throws FindFailed {
		pattern.setFilename(patternFile);
		String FieldValue = "";
		screen.find(pattern).click();
		screen.type("a", KeyModifier.CTRL);
		screen.type("c", KeyModifier.CTRL);
		FieldValue = Env.getClipboard();
		return FieldValue;

	}

	public void selectValueFromDropdown(String patternFile, String value) throws FindFailed {
		pattern.setFilename(patternFile);
		screen.find(pattern);
		screen.click();
		screen.write(value);
		screen.type(Key.TAB);
	}

	public boolean selectRadio(String patternFile) throws FindFailed {
		boolean selected = false;
		pattern.setFilename(patternFile);
		Pattern deselectedPattern = new Pattern(CommonElementDir + "DeselectedRadio.PNG");

		if (screen.exists(pattern) != null) {
			if (screen.find(pattern).exists(deselectedPattern) != null) {
				screen.click();
				selected = true;
			}
		}
		return selected;
	}

	public void selectRadioFromRegion(String radioPatternFile, String regionPatternFile) throws FindFailed {
		String Deselected = CommonElementDir + "DeselectedRadio.PNG";
		pattern.setFilename(radioPatternFile);
		Pattern workArea = new Pattern(regionPatternFile);
		if (screen.exists(workArea) != null) {
			Region region = screen.find(workArea);
			if (region.find(pattern).exists(new Pattern(Deselected)) != null) {
				region.click();
				screen.wait(1.0);
			}
		}
	}

	public boolean isRadioButtonSelected(String radioPatternFile, String regionPatternFile) {
		String selected = CommonElementDir + "SelectedRadio.PNG";
		pattern.setFilename(radioPatternFile);
		Pattern workArea = new Pattern(regionPatternFile);
		if (screen.exists(workArea) != null) {
			try {
				Region region = screen.find(workArea);
				return region.find(pattern).exists(new Pattern(selected)) != null;
			} catch (FindFailed e) {
				return false;
			}
		}
		return false;
	}

	public void selectCheckBox(String patternFile) {
		try {
			String Unchecked = CommonElementDir + "UncheckedCheckBox.PNG";
			scrollToField(patternFile);

			if (screen.exists(pattern) != null) {
				if (screen.find(pattern).exists(new Pattern(Unchecked)) != null) {
					screen.click();
				}
			}
		} catch (FindFailed e) {

			e.printStackTrace();
		}

	}

	public boolean isCheckBoxSelected(String patternFile) throws FindFailed {
		String checked = CommonElementDir + "CheckedCheckBox.PNG";
		pattern.setFilename(patternFile);

		if (screen.exists(pattern) != null) {
			return (screen.find(pattern).exists(new Pattern(checked)) != null);
		}
		return false;

	}

	public byte[] getScreenshot() {
		BufferedImage image = screen.capture().getImage();
		byte[] imageData = null;
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, "png", byteArrayOutputStream);
			imageData = byteArrayOutputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imageData;
	}

	public void scrollToField(String patternFile) {
		pattern.setFilename(patternFile);
		while (screen.exists(pattern) == null) {
			screen.type(Key.PAGE_DOWN);
			screen.wait(2.0);
		}
	}

	public void waitForElementToExist(String patternFile) {
		pattern.setFilename(patternFile);
		while (screen.exists(pattern) == null) {
			screen.wait(1.0);
		}
	}

	public boolean isElementVisible(String patternFile) {
		try {
			pattern.setFilename(patternFile).exact();
			return screen.exists(pattern) != null;
		} finally {
			pattern.similar(0.7f);
		}
	}

	public void dragElementToOffset(String patternFile, int xOffset, int yOffset) {
		pattern.setFilename(patternFile);
		try {
			screen.mouseMove(pattern);
			screen.mouseDown(Button.LEFT);
			screen.mouseMove(xOffset, yOffset);
			screen.mouseUp(Button.LEFT);
		} catch (FindFailed e) {
			e.printStackTrace();
			Log.printInfo("Error while dragging the element");
		}
	}

	public boolean isImageChanged(Rectangle elementBounds, long delayInMilliseconds) {
		try {
			BufferedImage originalImage = screen.capture(elementBounds).getImage();
			Common.sleep(delayInMilliseconds, TimeUnit.MILLISECONDS);
			pattern.setBImage(originalImage).exact();
			return screen.exists(pattern) == null;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public BufferedImage getImageWithinBounds(Rectangle elementBounds) {
		return screen.capture(elementBounds).getImage();
	}
	
	public void click() {
		screen.click();
	}
	
	public void type(String text) {
		screen.type(text);
	}
}
