package Actions;

import static utilities.Common.sleep;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Web {

	private WebDriver driver;

	public Web(WebDriver driver) {
		this.driver = driver;
	}

	public void setText(By locator, String value) throws TimeoutException {
		WebElement webElement = waitForElementVisibility(locator, 30);
		waitForElementClickability(locator, 30);
		webElement.click();
		webElement.clear();
		webElement.sendKeys(value);
	}

	// Use click method in page definition before calling this method
	public void setTextActiveElement(By locator, String value) throws TimeoutException {
		waitForElementVisibility(locator, 30);
		driver.switchTo().activeElement().sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
		driver.switchTo().activeElement().sendKeys(value);
		driver.switchTo().activeElement().sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.ENTER));
	}

	public void clearText(By locator) throws TimeoutException {
		WebElement webElement = waitForElementVisibility(locator, 30);
		webElement.clear();
	}

	public String getText(By locator) throws TimeoutException {
		WebElement webElement = waitForElementVisibility(locator, 30);
		return webElement.getText();
	}

	public String getTextboxValue(By locator) {
		WebElement webElement = waitForElementVisibility(locator, 30);
		return webElement.getAttribute("value");
	}

	public void click(By locator) throws TimeoutException {
		waitForElementClickability(locator, 60).click();
	}

	public void clickUsingActionsClass(By locator) throws TimeoutException {
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(locator);
		action.clickAndHold(element).pause(1000).release().build().perform();
	}

	public void doubleClick(By locator) throws TimeoutException {
		Actions action = new Actions(driver);
		WebElement element = waitForElementClickability(locator, 30);
		action.doubleClick(element).perform();
	}

	public void rightClick(By locator) throws TimeoutException {
		Actions action = new Actions(driver);
		WebElement webElement = driver.findElement(locator);
		action.contextClick(webElement).perform();
	}

	public void mouseHover(By locator) throws TimeoutException {
		Actions action = new Actions(driver);
		action.moveToElement(waitForElementVisibility(locator, 30)).perform();
	}

	public void selectFromDropDown(By locator, String value) throws TimeoutException {
		Select select = new Select(waitForElementClickability(locator, 30));
		select.selectByVisibleText(value);
	}

	public void keyPress(By locator, Keys key) {
		driver.findElement(locator).sendKeys(key);
	}

	public WebElement waitForElementPresence(By locator, long timeoutInSeconds) throws TimeoutException {
		sleep(1, TimeUnit.SECONDS);
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		return element;
	}

	public void waitForElementToDisappear(By locator, long timeoutInSeconds) {
		sleep(1, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}

	public WebElement waitForElementClickability(By locator, long timeoutInSeconds) throws TimeoutException {
		sleep(1, TimeUnit.SECONDS);
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		element = driver.findElement(locator);
		return element;
	}

	public List<WebElement> waitForElementsClickability(By locator, long timeoutInSeconds) throws TimeoutException {
		sleep(1, TimeUnit.SECONDS);
		List<WebElement> elements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		elements = driver.findElements(locator);
		return elements;
	}

	public WebElement waitForElementVisibility(By locator, long timeoutInSeconds) throws TimeoutException {
		sleep(1, TimeUnit.SECONDS);
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			element = driver.findElement(locator);
			return element;
		} catch (Exception e) {
			return element;
		}
	}

	public List<String> getElementTextList(By locator) {
		List<String> elementsText = new ArrayList<String>();
		sleep(1, TimeUnit.SECONDS);
		List<WebElement> elements = null;
		elements = driver.findElements(locator);
		for (WebElement element : elements) {
			elementsText.add(element.getText());
		}
		return elementsText;
	}

	public ArrayList<String> getTabNames(By locator) throws TimeoutException {
		ArrayList<String> tabNames = new ArrayList<String>();
		WebElement tableElement = waitForElementVisibility(locator, 30);
		List<WebElement> rowElements = tableElement.findElements(By.xpath("div"));
		for (WebElement row : rowElements) {
			if (row.getText().equals(" ")) {
				break;
			}
			tabNames.add(row.getText());
		}
		return tabNames;
	}

	public void switchToFrame(String frameName) {
		driver.switchTo().frame(frameName);
	}

	private List<WebElement> getRowElements(By tablelocator, By rowLocator) throws TimeoutException {
		WebElement tableElement = waitForElementVisibility(tablelocator, 10);
		List<WebElement> rowElements = tableElement.findElements(rowLocator);
		return rowElements;
	}

	private WebElement getRowElement(By tablelocator, By rowLocator, int rowIndex) throws TimeoutException {
		List<WebElement> rowElements = getRowElements(tablelocator, rowLocator);
		if ((rowIndex < 1) || (rowIndex > rowElements.size())) {
			throw new NoSuchElementException(
					"No such row index, '" + rowIndex + "', within the row elements - '" + rowElements.size() + "'");
		}
		return rowElements.get(rowIndex - 1);
	}

	public List<String> getRowText(By tablelocator, By rowLocator, int rowIndex) {
		List<String> rowTextValues = new ArrayList<String>();
		try {
			WebElement row = getRowElement(tablelocator, rowLocator, rowIndex);
			List<WebElement> columnElements = row.findElements(By.xpath("td"));
			for (WebElement column : columnElements) {
				if (column.getText().equals("")) {
					rowTextValues.add(getIconValue(column));
				} else {
					rowTextValues.add(column.getText());
				}
			}
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		return rowTextValues;
	}

	public List<String> getColumnText(By tablelocator, By rowLocator, int columnIndex) {
		List<String> columnTextValues = new ArrayList<String>();
		if (columnIndex < 1) {
			throw new NoSuchElementException("Table::getColumnText -- Invalid column index, " + columnIndex);
		}
		try {
			List<WebElement> rowElements = getRowElements(tablelocator, rowLocator);
			for (WebElement row : rowElements) {
				columnTextValues.add(row.findElement(By.xpath("//td[" + columnIndex + "]/div")).getText());
			}
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		return columnTextValues;
	}

	public String getCellData(By tablelocator, By rowLocator, int rowIndex, int columnIndex) {
		String cellValue = "";
		List<String> rowContents = this.getRowText(tablelocator, rowLocator, rowIndex);
		if (rowContents.size() > 0) {
			if (rowContents.size() < columnIndex) {
				throw new NoSuchElementException("Table::getCellData -- column index, '" + columnIndex
						+ "' is greater than the number of columns, '" + rowContents.size() + "'.");
			} else {
				cellValue = rowContents.get(columnIndex - 1);
			}
		} else {
			throw new NoSuchElementException("Table::getCellData -- table is empty.");
		}
		return cellValue;
	}

	public void switchToWindow(String windowName, int retryCount) {
		int attemptNumber = 0;
		String currentWindowHandle = driver.getWindowHandle();
		Set<String> windowHandles = driver.getWindowHandles();
		Iterator<String> iterator = windowHandles.iterator();
		while (iterator.hasNext() && (attemptNumber < retryCount) && (!driver.getTitle().equals(windowName))) {
			String newWindow = iterator.next();
			if (!(newWindow.equals(currentWindowHandle))) {

				driver.switchTo().window(newWindow);
			}
			sleep(1, TimeUnit.SECONDS);
			attemptNumber++;
		}
	}

	public String getCurrentWindowUrl() {
		return driver.getCurrentUrl();
	}

	private String getIconValue(WebElement cellElement) {
		return "icon";
	}

	public int getElementCount(By locator) {
		List<WebElement> elements = driver.findElements(locator);
		return elements.size();
	}

	public void scrollToElement(By locator) {
		WebElement element = driver.findElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void scrollToElement(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void clickOnElementAtIndex(By locator, int index) {
		List<WebElement> elements = driver.findElements(locator);
		WebElement element = elements.get(index);
		scrollToElement(element);
		element.click();
	}

	public String getElementAttributeValue(By locator, String attribute) {
		WebElement element = driver.findElement(locator);
		return element.getAttribute(attribute);
	}

	public Point getElementLocation(By locator) {
		WebElement webElement = waitForElementVisibility(locator, 30);
		return webElement.getLocation();
	}

	public void switchTab(int tabIndex) {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(tabIndex));
	}

	public WebElement waitForAlert(By locator, long timeoutInSeconds) throws TimeoutException {
		sleep(1, TimeUnit.SECONDS);
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		element = driver.findElement(locator);
		return element;
	}

	public void acceptAlert() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public boolean isSelected(By locator) throws TimeoutException {
		return (waitForElementVisibility(locator, 30).isSelected());
	}

	public String getwindowTitle() {
		return driver.getTitle();
	}

	public boolean isVisible(By locator) throws TimeoutException {
		try {
			return (waitForElementVisibility(locator, 15).isDisplayed());
		} catch (Exception e) {
			return false;
		}
	}

	public void refreshPage() {
		driver.navigate().refresh();
	}

	public void moveCursor() throws TimeoutException {
		Actions action = new Actions(driver);
		action.moveByOffset(20, 20).build().perform();
	}

	public void selectDropDownElement(By locator, String element) {
		List<WebElement> optionsInnerText = driver.findElements(locator);
		for (WebElement ele : optionsInnerText) {
			String textContent = ele.getText();
			if (textContent.toLowerCase().equalsIgnoreCase(element.toLowerCase()))
				scrollToElement(ele);
			waitForElementClickability(locator, 30);
			ele.click();
		}
	}

	public void selectDropDownWildcard(By locator, String element) {
		List<WebElement> optionsInnerText = driver.findElements(locator);
		for (WebElement ele : optionsInnerText) {
			String textContent = ele.getText();
			if (textContent.toLowerCase().matches(element.toLowerCase()))
				scrollToElement(ele);
			waitForElementClickability(locator, 30);
			ele.click();
		}
	}

	public boolean isCollapse(By locator) {
		WebElement findCollapsed = driver.findElement(locator);
		String classField = findCollapsed.getAttribute("class");
		if (classField.toLowerCase().contains("x-fieldset-collapsed")) {
			return true;
		} else
			return false;

	}

	public List<String> getHiddenHoverElementTextList(By locator1, By locator2) {
		List<String> elementsText = new ArrayList<String>();
		Actions actions = new Actions(driver);
		waitForElementPresence(locator1, 30);
		List<WebElement> elements = driver.findElements(locator1);
		for (WebElement element : elements) {
			actions.moveToElement(element).build().perform();
			waitForElementPresence(locator2, 30);
			WebElement elementObject = driver.findElement(locator2);
			sleep(50, TimeUnit.MILLISECONDS);
			String text = elementObject.getText();
			actions.moveByOffset(100, 100).perform();
			waitForElementToDisappear(locator2, 10);
			elementsText.add(text.trim());
		}
		return elementsText;
	}

	public void closeTab() {
		driver.close();
	}

	public void switchToDefaultTab() {
		driver.switchTo().defaultContent();
	}

	public void clickCheckbox(String firstContact, String secondContact) {
		WebElement element1 = driver.findElement(By.xpath("//div[text()='" + firstContact + "']//ancestor::tr//img"));
		WebElement element2 = driver.findElement(By.xpath("//div[text()='" + secondContact + "']//ancestor::tr//img"));
		WebElement[] elements = { element1, element2 };
		for (WebElement currentElement : elements) {
			scrollToElement(currentElement);
			currentElement.click();
		}
	}

	public void clickElementByOffSet(By locator) {
		WebElement webElement = driver.findElement(locator);
		int width = webElement.getSize().getWidth();
		Actions act = new Actions(driver);
		act.moveToElement(webElement).moveByOffset((width / 4) + 10, 0).click().build().perform();
	}

	public void dragDropElementVertically(By locator1, By locator2, int offsetValue) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement dragElement = wait.until(ExpectedConditions.elementToBeClickable(locator1));
		WebElement dropElement = wait.until(ExpectedConditions.elementToBeClickable(locator2));
		Actions action = new Actions(driver);
		action.moveToElement(dragElement).clickAndHold().moveToElement(dropElement).pause(2000)
				.moveByOffset(0, offsetValue).pause(4000).release().build().perform();
	}

	public void dragDropElementHorizontally(By locator1, By locator2, int offsetValue) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement dragElement = wait.until(ExpectedConditions.elementToBeClickable(locator1));
		WebElement dropElement = wait.until(ExpectedConditions.elementToBeClickable(locator2));
		Actions action = new Actions(driver);
		action.moveToElement(dragElement).clickAndHold().moveToElement(dropElement).pause(2000)
				.moveByOffset(offsetValue, 0).pause(4000).release().build().perform();
	}

	public boolean isEnabled(By locator) throws TimeoutException {
		return (waitForElementVisibility(locator, 30).isEnabled());
	}

	public void moveElementHorizontally(By locator, int offsetValue) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement dragElement = wait.until(ExpectedConditions.elementToBeClickable(locator));
		Actions action = new Actions(driver);
		action.moveToElement(dragElement).clickAndHold().moveByOffset(offsetValue, 0).pause(4000).release().build()
				.perform();
	}

	public void moveElementVertically(By locator, int offsetValue) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement dragElement = wait.until(ExpectedConditions.elementToBeClickable(locator));
		Actions action = new Actions(driver);
		action.moveToElement(dragElement).clickAndHold().moveByOffset(0, offsetValue).pause(4000).release().build()
				.perform();
	}

	public boolean validateDateFormat(By locator) {
		WebElement element = driver.findElement(locator);
		element.getAttribute("value");
		String dateStr = element.getAttribute("value");
		DateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
		sdf.setLenient(false);
		try {
			sdf.parse(dateStr);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public int returnRegexInt(By locator, String regex) {
		WebElement element = driver.findElement(locator);
		String input = element.getText();
		int result = 0;
		Pattern pattern = Pattern.compile("[0-9]+");
		Matcher match = pattern.matcher(input);
		if (match.find()) {
			result = Integer.parseInt(match.group(0));
		}
		return result;
	}

	public String getTimeInTwelveHrFormat() {
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		return dateFormat.format(currentDate);
	}

	// Current time with added minutes is returned in twelve hour format
	public String getTimeAddMiutes(int minutesToAdd) {
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(dateFormat.parse(dateFormat.format(currentDate)));
			cal.add(Calendar.MINUTE, minutesToAdd);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateFormat.format(cal.getTime());
	}

	public List<String> sortListInAscendingOrder(List<String> list) {
		Collections.sort(list);
		return list;
	}

	public List<String> sortListInDescendingOrder(List<String> list) {
		Collections.sort(list, Collections.reverseOrder());
		return list;
	}

	public int getElementHeight(By locator) {
		WebElement webElement = driver.findElement(locator);
		return webElement.getSize().getHeight();
	}

	public String getClassValue(By locator) {
		WebElement webElement = waitForElementVisibility(locator, 30);
		return webElement.getAttribute("class");
	}

	public int getElementWidth(By locator) {
		WebElement webElement = driver.findElement(locator);
		return webElement.getSize().getWidth();
	}
			
	public String getValueAttribute(By locator) {
		WebElement element = driver.findElement(locator);
		return element.getAttribute("value");
	}
	
	public List<WebElement> getElementListItems(By locator) {
		List<String> elementsText = new ArrayList<String>();
		sleep(1, TimeUnit.SECONDS);
		List<WebElement> elements = null;
		WebElement elementUl = driver.findElement(locator);
		elements = elementUl.findElements(By.tagName("li"));

		return elements;
	}
	
	public void clickCheckboxListElements(By locator) {
		List<WebElement> elements  = getElementListItems(locator);
		for(WebElement el : elements) {
			el.click();
		}	
	}
}

