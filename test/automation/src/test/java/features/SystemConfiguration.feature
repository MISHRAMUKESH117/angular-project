@SystemConfiguration
Feature: System Configuration

@automated
@Regression
@4.1
@TC_C66498493
Scenario: Verify error message is displayed in Chrome browser for Firefox config
Given Connect to database and set browser compatibility as
|Firefox	|
When I update automation property "browser" to "Chrome"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C66498494
Scenario: Verify error message is displayed in Chrome browser for Edge config
Given Connect to database and set browser compatibility as
|Edge	|
When I update automation property "browser" to "Chrome"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C66498496
Scenario: Verify no error message is displayed in Chrome browser for Chrome config
Given Connect to database and set browser compatibility as
|Chrome	|
When I update automation property "browser" to "Chrome"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C66498497
Scenario: Verify no error message is displayed in Firefox browser for Firefox config
Given Connect to database and set browser compatibility as
|Firefox	|
When I update automation property "browser" to "Firefox"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C66498498
Scenario: Verify error message is displayed in Firefox browser for Edge config
Given Connect to database and set browser compatibility as
|Edge	|
When I update automation property "browser" to "Firefox"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C66498499
Scenario: Verify error message is displayed in Firefox browser for Chrome config
Given Connect to database and set browser compatibility as
|Chrome	|
When I update automation property "browser" to "Firefox"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"


@automated
@Regression
@4.1
@TC_C66498500
Scenario: Verify error message is displayed in Edge browser for Firefox config
Given Connect to database and set browser compatibility as
|Firefox	|
When I update automation property "browser" to "Edge"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C66498501
Scenario: Verify no error message is displayed in Edge browser for Edge config
Given Connect to database and set browser compatibility as
|Edge	|
When I update automation property "browser" to "Edge"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C66498502
Scenario: Verify error message is displayed in Edge browser for Chrome config
Given Connect to database and set browser compatibility as
|Chrome	|
When I update automation property "browser" to "Edge"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C66498503
Scenario: Verify no error message is displayed in Firefox browser for multiple browser configuration
Given Connect to database and set browser compatibility as
|Chrome		|
|Firefox	|
|Edge			|
|null			|
When I update automation property "browser" to "Firefox"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C68964225
Scenario: Verify no error message is displayed in Chrome browser for multiple browser configuration
Given Connect to database and set browser compatibility as
|Chrome		|
|Firefox	|
|Edge			|
|null			|
When I update automation property "browser" to "Chrome"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C68964226
Scenario: Verify no error message is displayed in Edge browser for multiple browser configuration
Given Connect to database and set browser compatibility as
|Chrome		|
|Firefox	|
|Edge			|
|null			|
When I update automation property "browser" to "Edge"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C68964228
Scenario: Verify no error message is displayed in Firefox browser for null value configuration
Given Connect to database and set browser compatibility as null
When I update automation property "browser" to "Firefox"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C68964229
Scenario: Verify no error message is displayed in Chrome browser for null value configuration
Given Connect to database and set browser compatibility as null
When I update automation property "browser" to "Chrome"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C68964230
Scenario: Verify no error message is displayed in Edge browser for null value configuration 
Given Connect to database and set browser compatibility as null
When I update automation property "browser" to "Edge"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C66498503
Scenario: Verify error message is displayed in Firefox browser for null value and combination browser 
Given Connect to database and set browser compatibility as
|Chrome		|
|Edge			|
|null			|
When I update automation property "browser" to "Firefox"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C68964225
Scenario: Verify error message is displayed in Firefox browser for null value and combination browser 
Given Connect to database and set browser compatibility as
|Firefox	|
|Edge			|
|null			|
When I update automation property "browser" to "Chrome"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C68964226
Scenario: Verify error message is displayed in Firefox browser for null value and combination browser 
Given Connect to database and set browser compatibility as
|Chrome		|
|Firefox	|
|null			|
When I update automation property "browser" to "Edge"
And Clario application is launched
Then Unsupported broswer message is displayed containing "You are using an unsupported browser. Please use one of the following supported browsers"

@automated
@Regression
@4.1
@TC_C69213094
Scenario: Verify no error message is displayed in Edge browser with no browser type set
Given Connect to database and set browser compatibility as
|	|
When I update automation property "browser" to "Edge"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches


@automated
@Regression
@4.1
@TC_C69213095
Scenario: Verify no error message is displayed in Firefox browser with no browser type set
Given Connect to database and set browser compatibility as
|	|
When I update automation property "browser" to "Edge"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C69213096
Scenario: Verify no error message is displayed in Chrome browser with no browser type set
Given Connect to database and set browser compatibility as
|	|
When I update automation property "browser" to "Chrome"
And Clario application is launched
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches

@automated
@Regression
@4.1
@TC_C61184720
Scenario: Logout user when session is deleted
Given Clario application is launched
And log into the application as "admin" user
When I delete user session from database
And I open "Smart Worklist" application
Then "admin" user is logged out


@SystemConfig_ldapServer
Scenario: Create LDAP configuration
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "LDAP Server" from "Active Directory" Configuration Page
And I click "Add" button on configuration page
And I create Ldap Server configuration
|Server							 |Port|Domain				|AuthGroup							|AuthUser|Password				|UserDomain	  |
|it-ad1.intelerad.com|389 |intelerad.com|Clario-test-integration|bindldap|UP5aiPheij3iJuf6|intelerad.com|
And I click "Save" button on configuration page

@SystemConfig_ldapGroup
Scenario: Create LDAP configuration
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "LDAP Groups" from "Active Directory" Configuration Page
And I click "Add" button on configuration page
And I create Ldap Group configuration
|Group Name							 |Group Server					|Site			 |
|Clario-test-integration |it-ad1.intelerad.com 	|Intelerad |
And I click "Save" button on configuration page