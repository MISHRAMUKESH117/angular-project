@PeerReviewManagement
Feature: Peer Review Management


@automated
@BVT
@Smoke
@TC_PR-001
Scenario: Peer Review Management menu option check
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
Then following tables are displayed under the Menu
|Note Configuration | 
|Ongoing Assignment |
|Focused Assignment | 
|Prompting 		    | 
|Peer Review Alert 	|
|eRad Peer          |



@automated
@Smoke
@Regression
@TC_PR-002
Scenario: All the parameters for Peer Review Alert menu are displayed
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Peer Review Alert" menu option in Peer Review Management page
Then following parameters are displayed under Peer Review Alert
|Name            		 		| 
|Description 			 		| 
|Type				 			| 
|Notification Interval 			| 
|Day of Week 					|
|Time of Day 					|
|User 				 			|
|Group 				 			|
|Unread Threshold Type 		 	|
|Threshold [above type / rad]	|



@automated
@Smoke
@Regression
@TC_PR-006
Scenario: Verify Focused Assignment List of rules on the left and propertieson the right visible
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
Then Focused Assignment rules section is displayed
And following property names are displayed under Focused Assignment
|Name:            		 		| 
|Description: 			 		| 
|Active:				 		| 
|Time period: 					| 
|% exams to assign: 			|
|Period(s) to assign (d/w/m): 	|
|User: 				 			|
|Group: 						|
|Priority: 		 	 			|
|Modality:						|
|Subspecialty:					|
|Site Procedure: 				|
|Site Procedure Code: 			|
|Site:			 				|
|Location:	 					|
|Gender:	 					|
|Age >=:	 					|
|Age <=:	 					|
|Radiologist:	 				|
|Resident Radiologist:		 	|
|Attending Radiologist: 		|


@Regression
@automated
@TC_PR-010
Scenario: Verify Notes Configuration loads properly with the following sections Ratings, Reasons, Assignment Reasons, Categories, Classifications.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
Then following sections are displayed under Note Configuration
|Peer Review Rating | 
|Reasons 			| 
|Assignment Reasons | 
|Categories 		| 
|Classifications 	|



@TC_PR-011
Scenario: Verify that new exam type defination created and existing one edited and saved.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Ongoing Assignment" menu option in Peer Review Management page
And add a new Exam Type Definition "Test Exam"
And I select Time Period as "Monthly"
And I click on Ongoing Assignment Save button
And I click on "Note Configuration" menu option in Peer Review Management page
And I click on "Ongoing Assignment" menu option in Peer Review Management page
And I select assignment properties Definition name  as "Test Exam"
Then Time Period is set to "Monthly"
When I select Time Period as "Weekly"
And I click on Ongoing Assignment Save button
And I click on "Note Configuration" menu option in Peer Review Management page
And I click on "Ongoing Assignment" menu option in Peer Review Management page
And I select assignment properties Definition name  as "Test Exam"
Then Time Period is set to "Weekly"
When I click on delete Assignment Property
Then assignment properties Definition name "Test Exam" is deleted


@Regression
@automated
@TC_PR-012
Scenario: Verify that new rule created and Existing rule edited ,saved.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And create New Rule with following details
|Rule name |Rule Description      |Percentage Exams to Assign |
|Test Rule |Test Rule Description |30                         |
And I select Time Period as "Monthly"
And I select user "Wood, Chris"
And I click on focused assignment rule save button
Then new rule "Test Rule" is added
When I select "Test Rule" rule
And I click on Edit rule icon
And I set new rule name "New Test Rule"
And I click on focused assignment rule save button
Then new rule "New Test Rule" is added
When I select "New Test Rule" rule
And I click on Edit rule icon
And I click on Delete rule icon
Then rule "New Test Rule" is deleted


@Regression
@automated
@TC_PR-013
Scenario: Add an user and indicator to Prompting.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Prompting" menu option in Peer Review Management page
And I select "Tech, Citius" user for Prompting
And I click on save button
And I add an Indicator with following details
|Day     |From Time   |To Time  |
|Tuesday |2:00 AM     |4:00 AM  |
Then "Tech, Citius" is added to Prompting Summary
And an indicator is added for user "Tech, Citius" as "2:00AM-4:00AM"
When I delete an user "Tech, Citius" from Prompting Summary
Then user "Tech, Citius" is deleted from Prompting Summary

@Regression
@automated
@TC_PR-014
Scenario: Verify peer review alert created, edited and saved properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Peer Review Alert" menu option in Peer Review Management page
And I set Peer Review Alert name as "Test Peer Review Alert Name"
And I set Peer Review Alert description as "Test Peer Review Alert Description"
And I click on Peer Review Alert save button
And I click on "Focused Assignment" menu option in Peer Review Management page
And I click on "Peer Review Alert" menu option in Peer Review Management page
Then Peer Review Alert name is set to "Test Peer Review Alert Name"
And Peer Review Alert description is set to "Test Peer Review Alert Description"
When I set Peer Review Alert name as "New Test Peer Review Alert Name"
And I click on Peer Review Alert save button
And I click on "Focused Assignment" menu option in Peer Review Management page
And I click on "Peer Review Alert" menu option in Peer Review Management page
Then Peer Review Alert name is set to "New Test Peer Review Alert Name"


@Regression
@automated
@TC_PR-015
Scenario: Verify that page loaded properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "eRad Peer" menu option in Peer Review Management page
Then "Configuration of eRad Peer" page loads properly

@automated
@Regression
@4.1
@TC_C60053000
Scenario: Verify that filtering/search Peer Review Management by name only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And create New Rule with following details
|Rule name |Rule Description      |Percentage Exams to Assign |
|New Test Rule |Test Rule Description |30                         |
And I select Time Period as "Monthly"
And I select user "Wood, Chris"
And I click on focused assignment rule save button
Then new rule "New Test Rule" is present
When I select "New Test Rule" rule
And I click on Edit rule icon
And I set new rule name "New Test Rule"
And I click on focused assignment rule save button
And I search for "New Test Rule" on Peer Review Management Page
Then new rule "New Test Rule" is present
When I select "New Test Rule" rule
And I click on Edit rule icon
And I click on Delete rule icon
And I search for "New Test Rule" on Peer Review Management Page
Then rule "New Test Rule" is deleted


@automated
@Regression
@4.1
@TC_C60053001
Scenario: Verify that filtering/search Peer Review Management by description only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And create New Rule with following details
|Rule name |Rule Description      |Percentage Exams to Assign |
|New Test Rule |Test Rule Description |30                         |
And I select Time Period as "Monthly"
And I select user "Wood, Chris"
And I click on focused assignment rule save button
Then new rule "New Test Rule" is present
When I select "New Test Rule" rule
And I click on Edit rule icon
And I set new rule name "New Test Rule"
And I click on focused assignment rule save button
And I search for "Test Rule Description" on Peer Review Management Page
Then new rule "Test Rule Description" is present
When I select "New Test Rule" rule
And I click on Edit rule icon
And I click on Delete rule icon
And I search for "New Test Rule" on Peer Review Management Page
Then rule "New Test Rule" is deleted

@automated
@Regression
@4.1
@TC_C60053003
Scenario: Verify that filtering/search Peer Review Management by special character
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And create New Rule with following details
|Rule name |Rule Description      	|Percentage Exams to Assign |
|Test Rule _-|Test Rule Description |30                         |
And I select Time Period as "Monthly"
And I select user "Wood, Chris"
And I click on focused assignment rule save button
Then new rule "Test Rule _-" is present
When I select "Test Rule _-" rule
And I click on Edit rule icon
And I set new rule name "New Test Rule _-"
And I click on focused assignment rule save button
And I search for "New Test Rule _-" on Peer Review Management Page
Then new rule "New Test Rule _-" is present
When I select "New Test Rule _-" rule
And I click on Edit rule icon
And I click on Delete rule icon
And I search for "Test Rule _-" on Peer Review Management Page
Then rule "New Test Rule _-" is deleted

@automated
@Regression
@4.1
@TC_C60053005
Scenario: Verify that filtering/search Peer Review Management by empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And I search for " " on Peer Review Management Page
Then new rule " " is present

@automated
@Regression
@4.1
@TC_C60053006
Scenario: Verify that pages button work
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page

@automated
@Regression
@4.1
@TC_C60053007
Scenario: Verify that filtering/search Peer Review Management works on 2nd page
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And create New Rule with following details
|Rule name |Rule Description      		|Percentage Exams to Assign |
|New Test Rule |Test Rule Description |30                         |
And I select Time Period as "Monthly"
And I select user "Wood, Chris"
And I click on focused assignment rule save button
Then new rule "New Test Rule" is present
When I select "New Test Rule" rule
And I click on Edit rule icon
And I set new rule name "New Test Rule"
And I click on focused assignment rule save button
And I click "Next Page" button on Managment Page
And I search for "Test Rule Description" on Peer Review Management Page
Then new rule "Test Rule Description" is present
When I select "New Test Rule" rule
And I click on Edit rule icon
And I click on Delete rule icon
And I search for "New Test Rule" on Peer Review Management Page
Then rule "New Test Rule" is deleted

@automated
@Regression
@4.1
@TC_C65212762
Scenario: Peer Learning create
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click add icon for "Peer Learning" on Note Configuration Page
And I create "Peer Learning" with name "Peer Learning Test" on Note Configuration Page
And I click Note Configuration "Peer Learning Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
Then "Peer Learning" with name "Peer Learning Test" is created on Note Configuration Page

@automated
@Regression
@4.1
@TC_C65212771
Scenario: Peer Learning Note Settings Active checkbox enabled
#Pre-requisite: Use Peer Learning created in @TC_C65212762
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
Then following sections are displayed under Note Configuration Note Preview
|Peer Learning 			|

@automated
@Regression
@4.1
@TC_C65212772
Scenario: Peer Learning Note Settings Active checkbox disabled
#Pre-requisite: Use Peer Learning created in @TC_C65212771
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I uncheck Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
Then following sections are not displayed under Note Configuration Note Preview
|Peer Learning 			|

@automated
@Regression
@4.1
@TC_C66195310
Scenario: Peer Learning verify default value is correct
#Pre-requisite: Use Peer Learning created in @TC_C65212762 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Active checkbox
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
Then the following default value is selected under Peer Review Note
|Label				 | Value							|
|Peer Learning |Peer Learning Test 	|

@automated
@Regression
@4.1
@TC_C66195311
Scenario: Peer Learning verify no default value is visible
#Pre-requisite: Use Peer Learning created in @TC_C65212762 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I click clear button for Default Value from "Peer Learning" in Note Settings
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
Then the following default value is not selected under Peer Review Note
|Label				 | Value							|
|Peer Learning |Peer Learning Test 	|

@automated
@Regression
@4.1
@TC_C65212773
Scenario: Peer Learning required checkbox enabled
#Pre-requisite: Use Peer Learning created in @TC_C65212762 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Active checkbox
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
And I click Peer Review Note Configuration "Peer Learning" clear button
Then "Peer Learning" field error message is displayed

@automated
@Regression
@4.1
@TC_C65212775
Scenario: Peer Learning required checkbox disabled
#Pre-requisite: Use Peer Learning created in @TC_C65212762 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Active checkbox
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
And I click Peer Review Note Configuration "Peer Learning" clear button
Then "Peer Learning" field error message is not displayed

@automated
@Regression
@4.1
@TC_C66180340
Scenario: Peer Learning Active checkbox enabled - Note Preview Verification
#Pre-requisite: Use Peer Learning created in @TC_C65212762
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
Then the following dropdown values and labels are visible under Peer Review Note
|Label				 | Value							|
|Peer Learning |Peer Learning Test 	|

@automated
@Regression
@4.1
@TC_C66180341
Scenario: Peer Learning Active checkbox disabled - Note Preview Verification
#Pre-requisite: Use Peer Learning created in @TC_C65212762
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Preview" button
Then the following dropdown values and labels are visible under Peer Review Note
|Label				 | Value							|
|Peer Learning |Peer Learning Test 	|


@automated
@Regression
@4.1
@TC_C65212769
Scenario: Peer Learning update - Note Preview Verification
#Pre-requisite: Use Peer Learning created in @TC_C65212762
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I update "Peer Learning" name to "Peer Learning Updated Test" on Note Configuration Page
And I click Note Configuration "Peer Learning Updated Test" Active checkbox
And I click Note Configuration "Save" button
Then "Peer Learning" with name "Peer Learning Updated Test" is created on Note Configuration Page

@automated
@Regression
@4.1
@TC_C65212770
Scenario: Peer Learning delete - Note Preview Verification
#Pre-requisite: Use Peer Learning created in @TC_C65212769
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Updated Test" delete icon
And I click Note Configuration "Save" button
Then Message is displayed "Action done"
And "Peer Learning" with name "Peer Learning Updated Test" is removed from Note Configuration Page

@automated
@Regression
@4.1
@TC_C66180344
Scenario:	Peer Learning create - Worklist Verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I uncheck "Open Peer Review in Window" checkbox on Profile Management Window
And I click on "Save" button on Profile Management window
And I click add icon for "Peer Learning" on Note Configuration Page
And I create "Peer Learning" with name "Peer Learning Worklist Test" on Note Configuration Page
And I click Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
Then "Peer Learning" with name "Peer Learning Worklist Test" is created on Note Configuration Page
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then the following dropdown values and labels are visible under Peer Review Note
|Label				 | Value												|
|Peer Learning |Peer Learning Worklist Test	 	|

@automated
@Regression
@4.1
@TC_C66191718
Scenario: Note Settings Active checkbox enabled - Worklist Verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then following sections are displayed under Note Configuration Note Preview
|Peer Learning 			|

@automated
@Regression
@4.1
@TC_C66191719
Scenario: Note Settings Active checkbox disabled - Worklist Verification
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I uncheck Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then following sections are not displayed under Note Configuration Note Preview
|Peer Learning 			|

@automated
@Regression
@4.1
@TC_C66195312
Scenario: Peer Learning verify default value is correct - Worklist verification
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Worklist Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Active checkbox
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then the following default value is selected under Peer Review Note
|Label				 | Value												|
|Peer Learning |Peer Learning Worklist Test 	|

@automated
@Regression
@4.1
@TC_C66195313
Scenario: Peer Learning verify no default value is visible Worklist verification
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I click clear button for Default Value from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Active checkbox
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then the following default value is not selected under Peer Review Note
|Label				 | Value												|
|Peer Learning |Peer Learning Worklist Test 	|

@automated
@Regression
@4.1
@TC_C66191721
Scenario: Peer Learning Active checkbox enabled - Worklist Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
And I click Peer Review Note Configuration "Peer Learning" clear button
Then the following dropdown values and labels are visible under Peer Review Note
|Label				 	| Value													|
|Peer Learning 	|Peer Learning Worklist Test	 	|

@automated
@Regression
@4.1
@TC_C66191722
Scenario: Peer Learning Active checkbox disabled - Worklist Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I uncheck Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
And I click Peer Review Note Configuration "Peer Learning" clear button
Then the following dropdown values are not visible under Peer Review Note
|Label				 	| Value													|
|Peer Learning 	|Peer Learning Worklist Test	 	|

@automated
@Regression
@4.1
@TC_C66191723
Scenario: Note Settings required checkbox enabled - WorkList Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Worklist Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
And I click Peer Review Note Configuration "Peer Learning" clear button
Then "Peer Learning" field error message is displayed

@automated
@Regression
@4.1
@TC_C66191724
Scenario: Note Settings required checkbox disabled - WorkList Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Worklist Test" from "Peer Learning" in Note Settings
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
And I click Peer Review Note Configuration "Peer Learning" clear button
Then "Peer Learning" field error message is not displayed

@automated
@Regression
@4.1
@TC_C66353449
Scenario: Note Settings required checkbox enabled click submit and verify record - WorkList Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Required checkbox
And I click Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
And I click Peer Review Note Configuration "Peer Learning" clear button
And I click Peer Review Note "Submit" button
Then "Peer Learning" field error message is displayed

@automated
@Regression
@4.1
@TC_C66353450
Scenario: Note Settings required checkbox disabled click submit - WorkList Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Worklist Test" from "Peer Learning" in Note Settings
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
And I click Peer Review Note "Submit" button
Then Note Detail "Peer Learning Worklist Test" is displayed

@automated
@Regression
@4.1
@TC_C66180345
Scenario: Update Peer Learning - Worklist Verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I create "Peer Learning" with name "Peer Learning Worklist Created Test" on Note Configuration Page
And I click Note Configuration "Save" button
And I update "Peer Learning" name to "Peer Learning Worklist Updated" on Note Configuration Page
And I click Note Configuration "Peer Learning Worklist Updated" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I select Default Value "Peer Learning Worklist Updated" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And "Peer Learning" with name "Peer Learning Worklist Updated" is created on Note Configuration Page
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then the following dropdown values and labels are visible under Peer Review Note
|Label				 | Value													|
|Peer Learning |Peer Learning Worklist Updated	|

@automated
@Regression
@4.1
@TC_C66180346
Scenario: Peer Learning delete - WorkList Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66180345 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist Updated" delete icon
And I click Note Configuration "Save" button
Then Message is displayed "Action done"
And "Peer Learning" with name "Peer Learning Worklist Updated" is removed from Note Configuration Page
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
And the following dropdown values are not visible under Peer Review Note
|Label				 | Value													|
|Peer Learning |Peer Learning Worklist Updated	|

@automated
@Regression
@4.1
@TC_C66195314
Scenario: Peer Learning create - Worklist Popout Window Verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I click "Open Peer Review in Window" checkbox on Profile Management Window
And I click on "Save" button on Profile Management window
And I click add icon for "Peer Learning" on Note Configuration Page
And I create "Peer Learning" with name "Peer Learning Worklist popout Test" on Note Configuration Page
And I click Note Configuration "Peer Learning Worklist popout Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I click Note Configuration "Peer Learning" Required checkbox
And I select Default Value "Peer Learning Worklist popout Test" from "Peer Learning" in Note Settings
And I click Peer Review Note Configuration "Save" button
Then "Peer Learning" with name "Peer Learning Worklist popout Test" is created on Note Configuration Page
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And the following dropdown values and labels are visible under Peer Review Note
|Label				 | Value																|
|Peer Learning |Peer Learning Worklist popout Test	 	|

@automated
@Regression
@4.1
@TC_C66195317
Scenario: Note Settings Active checkbox enabled - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And following sections are displayed under Note Configuration Note Preview
|Peer Learning 			|

@automated
@Regression
@4.1
@TC_C66195318
Scenario: Note Settings Active checkbox disabled - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I uncheck Note Configuration "Peer Learning" Active checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And following sections are not displayed under Note Configuration Note Preview
|Peer Learning 			|

@automated
@Regression
@4.1
@TC_C66195319
Scenario: Peer Learning verify default value is correct - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Worklist popout Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Active checkbox
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And the following default value is selected under Peer Review Note
|Label				 | Value															|
|Peer Learning |Peer Learning Worklist popout Test 	|

@automated
@Regression
@4.1
@TC_C66195320
Scenario: Peer Learning verify no default value is visible - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I click clear button for Default Value from "Peer Learning" in Note Settings
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And the following default value is not selected under Peer Review Note
|Label				 | Value															|
|Peer Learning |Peer Learning Worklist popout Test 	|

@automated
@Regression
@4.1
@TC_C66195322
Scenario: Peer Learning Active checkbox enabled - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist popout Test" Active checkbox
And I click Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And the following dropdown values and labels are visible under Peer Review Note
|Label				 	| Value																|
|Peer Learning 	|Peer Learning Worklist popout Test	 	|

@automated
@Regression
@4.1
@TC_C66195323
Scenario: Peer Learning Active checkbox disabled - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I uncheck Note Configuration "Peer Learning Worklist popout Test" Active checkbox
And I click Note Configuration "Save" button
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And the following dropdown values are not visible under Peer Review Note
|Label				 	| Value																|
|Peer Learning 	|Peer Learning Worklist popout Test	 	|

@automated
@Regression
@4.1
@TC_C66195324
Scenario: Note Settings required checkbox enabled - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist popout Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I select Default Value "Peer Learning Worklist popout Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Active checkbox
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
When I set the following values under Note section in Peer Review window
|DropdownName   			 											|DropdownValue  													 |
|Peer Learning     													|Peer Learning Worklist popout Test        |
And I click Peer Review Note Configuration "Peer Learning" clear button
Then "Peer Learning" field error message is displayed

@automated
@Regression
@4.1
@TC_C66195325
Scenario: Note Settings required checkbox disabled - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I click clear button for Default Value from "Peer Learning" in Note Settings
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I select Default Value "Peer Learning Worklist popout Test" from "Peer Learning" in Note Settings
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
When I click Peer Review Note Configuration "Peer Learning" clear button
Then "Peer Learning" field error message is not displayed

@automated
@Regression
@4.1
@TC_C66353451
Scenario: Note Settings required checkbox enabled click submit  - Worklist Popout Window Verification  
#Pre-requisite: Use Peer Learning created in @TC_C66195314 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist popout Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
When I click Peer Review Note "Submit Peer Review" button
Then "Peer Learning" field error message is displayed

@automated
@Regression
@4.1
@TC_C66353452
Scenario: Note Settings required checkbox disabled click submit - Worklist Popout Window Verification  
#Pre-requisite: Use Peer Learning created in @TC_C66180344 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist popout Test" Active checkbox
And I click Note Configuration "Save" button
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Note Settings" button
And I uncheck Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
When I set the following values under Note section in Peer Review window
|DropdownName   			 											|DropdownValue  													 |
|Peer Learning     													|Peer Learning Worklist popout Test        |
And I click Peer Review Note "Submit Peer Review" button
Then I switch to Window number 3
And Note Detail "Peer Learning Worklist popout Test" is displayed

@automated
@Regression
@4.1
@TC_C66195315
Scenario: Update Peer Learning - Worklist Popout Window Verification 
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I create "Peer Learning" with name "Peer Learning new Worklist popout Test" on Note Configuration Page
And I click Note Configuration "Peer Learning new Worklist popout Test" Active checkbox
And I click Note Configuration "Save" button
And I update "Peer Learning" name to "Peer Learning new Worklist popout Test" on Note Configuration Page
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
And I click Note Configuration "Peer Learning" Active checkbox
And I select Default Value "Peer Learning new Worklist popout Test" from "Peer Learning" in Note Settings
And I click Note Configuration "Peer Learning" Required checkbox
And I click Peer Review Note Configuration "Save" button
And "Peer Learning" with name "Peer Learning new Worklist popout Test" is created on Note Configuration Page
And I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And the following dropdown values and labels are visible under Peer Review Note
|Label				 | Value																			|
|Peer Learning |Peer Learning new Worklist popout Test	|

@automated
@Regression
@4.1
@TC_C66195316
Scenario: Peer Learning delete - Worklist Popout Window Verification 
#Pre-requisite: Use Peer Learning created in @TC_C66195315
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning new Worklist popout Test" delete icon
And I click Note Configuration "Save" button
Then Message is displayed "Action done"
When I click on "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "Peer Review - to Evaluate" from "Worklist Count Testing"
And I perform worklist search "Christopher, Longoria"
And I click on exam "MR29309"
And I expand note section
And I click on "Peer Review Note" note icon
Then "Longoria, Christopher" exam details are displayed in Peer Review window number 4
And the following dropdown values are not visible under Peer Review Note
|Label				 | Value																			|
|Peer Learning |Peer Learning new Worklist popout Test	|


@automated
@Regression
@4.1
@TC_C66456727
Scenario: Peer Learning delete - Verify selected record is made inactive and is not delete when created in Peer Review Note
#Pre-requisite: Use Peer Learning created in @TC_C66180344
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I click Note Configuration "Peer Learning Worklist Test" delete icon
And I click Note Configuration "Save" button
Then Message is displayed "Action done"
And "Peer Learning" with name "Peer Learning Worklist Test" is displayed on Note Configuration Page

@automated
@Regression
@4.1
@TC_C66456728
Scenario: Peer Learning verify deactive value cannot be se as default value 
#Pre-requisite: Use Peer Learning created in @TC_C66180344
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
And I uncheck Note Configuration "Peer Learning Worklist Test" Active checkbox
And I click Note Configuration "Save" button
And I click Note Configuration "Note Settings" button
Then Default Value "Peer Learning Test" from "Peer Learning" is not Visable in Peer Review