@Analytics
Feature: Analytics widgets

@Version
@automated
@BeforeAll
Scenario: Extract build version
Given Clario application is launched
When I create a "Version" file for writing build version
Then "Version" file is created

@zvExtender
@automated
@BeforeAll
Scenario: Uninstall and Install zVExtender application
Given "Control Panel" is launched 
When I uninstall zvExtender
Then zvExtender is uninstalled
When I close Control Panel window
Then "zvExtender" is launched
When "Control Panel" is launched
Then zvExtender is installed

@CreateUser
@automated
@BeforeAll
Scenario:  Test case to set the test user and test user permission
Given Clario application is launched
When I update the "admin" user's password from "pass" to "clario123"
When I open "Management" application
And I open "User Management" module
And create New User with following details
|First Name|Last Name|Login Name|Password       |Verify Password   |
|test123   |Test123  |atest  		|pass 					|	pass    			 	 |
And I select Local User Roles
|Active  						|
|Radiologist  			|
|Radiologist Admin  |
And I click on save user button
And I search user "atest"
Then new user "Test123, test123" is added
And log out of the application
When I update the "atest" user's password from "pass" to "test123"
Then log out of the application

@CreateLicenses
@automated
@BeforeAll
Scenario:  Test case to generate the licesnes
Given Clario application is launched
And log into the application as "license" user
When I update the "licenseadmin" user's password from "pass" to "clario123"
When I open "Management" application
And I open "Configuration" module
And I click "License" from "Configuration" Configuration Page
And I click the add button for configuring the following License/s
|Concierge   							|
|Peer Review   						|
|Reporting   							|
|Referral / Tech portal   |
|CRM   										|
|Linq   									|
And I click "Save" button on License Page
Then Message is displayed "All license changes were saved"
Then log out of the application
And log into the application as "license" user
When I open "Management" application
When I open "User Management" module
When I select "Test123, test123" user 
And I click on Edit user icon
And I generate the following user License/s
|No Speech Engine License   |
And I click on save user button
Then log out of the application

@CreateUserPermissions
@automated
@BeforeAll
Scenario:  Test case to generate the licesnes
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I select "Radiologist admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      						 			|
|Analytics        |Add Exams sheet to widget export			|
|Analytics        |View Custom Reports 						 			|
|Analytics Reports|Peer Review Analysis				 		 			|
|Analytics Reports|Peer Review Export for eRadPeer 			|
|Analytics Widgets|Peer Review Error Rate by Time of Day|
|Analytics Widgets|Peer Review Scorecard								|
|CRM|Access to CRM				 		 													|
|CRM|Create and Edit Physician Groups				 		 				|
|CRM|Edit Contacts				 		 													|
|CRM|Edit Portal Forms Management				 		 						|
|CRM|Edit Site and Contact				 		 									|
|CRM|Edit User Roles				 		 												|
|CRM|Manage Portal User				 													|
|CRM|Portal forms field editor edit				 		 					|
|CRM|View Contacts			 		 														|
|CRM|View Portal Forms Management				 		 						|
|CRM|View User Roles				 		 												|
|Dashboard|Access to Dashboard													|
|Dashboard|All actions on all pages											|
|Dashboard|View DICOM Routing														|
|Dashboard|View Services																|
|Dashboard|View User Monitor														|
|Management|Access Peer Review Management								|
|Management|Access Procedure Management									|
|Management|Edit Group Management page									|
|Management|View Group Management												|
|Message	 |Access to Messaging													|
|Note|Administrator Communication notes									|
|Note|Administrator ED Prelim notes											|
|Note|Administrator Exam notes													|
|Note|Administrator Follow-up notes											|
|Note|Administrator Patient notes												|
|Note|Administrator Peer Review notes										|
|Note|Create Communication notes												|
|Note|Create Follow-up notes														|
|Note|Create Patient notes															|
|Note|Create Peer Review notes													|
|Note|View Communication notes													|
|Note|View ED Prelim notes															|
|Note|View Exam notes																		|
|Note|View Follow-up notes															|
|Note|View Patient notes				 												|
|Note|View Peer Review notes			 											|
|Note|Access to Note					 													|
|Note|ED Prelim notes Acknowledge		 										|
|Note|ED Prelim notes Agree				 											|
|Note|ED Prelim notes Comment Discrepancy								|
|Note|ED Prelim notes Comment Pending	 									|
|Note|ED Prelim notes Major Discrepancy	 								|
|Note|ED Prelim notes Minor Discrepancy	 								|
And click on save button



@automated
@BVT
@Smoke
@TC_C48774335
Scenario: widgets are created
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets 
|Unread Exam                           |
|Practice Productivity                 |
|Exam Backlog		                       |
|Auto-next Productivity                |
Then following widgets are displayed
|Unread Exam                           |
|Practice Productivity                 |
|Exam Backlog		                       |
|Auto-next Productivity                |
When I "Delete" the following widgets
|Unread Exam                           |
|Practice Productivity                 |
|Exam Backlog		                       |
|Auto-next Productivity                |
And I create the following widgets
|Stacked Turnaround Time               |
|Current Practice                      |
|Resource Planning                     |
|Load Balancing Queue Count            |
Then following widgets are displayed
|Stacked Turnaround Time               |
|Current Practice                      |
|Resource Planning                     |
|Load Balancing Queue Count            |
When I "Delete" the following widgets
|Stacked Turnaround Time               |
|Current Practice                      |
|Resource Planning                     |
|Load Balancing Queue Count            |
And I create the following widgets
|Exam Turnaround Time                  |
|Exam Count                            |
|Stacked Exam Count                    |
Then following widgets are displayed
|Exam Turnaround Time                  |
|Exam Count                            |
|Stacked Exam Count                    |
When I "Delete" the following widgets
|Exam Turnaround Time                  |
|Exam Count                            |
|Stacked Exam Count                    |
Then widgets are deleted
|Stacked Turnaround Time               |
|Current Practice                      |
|Resource Planning                     |
|Load Balancing Queue Count            |
|Unread Exam                           |
|Practice Productivity                 |
|Exam Backlog		                       |
|Auto-next Productivity                |
|Exam Turnaround Time                  |
|Exam Count                            |
|Stacked Exam Count                    |

@automated
@Regression
@Smoke
@TC_C62656067
Scenario: widgets are deleted 
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Subspecialty Reading Rate             |
|SLA Compliance                        |
|Radiologist Productivity              |
Then following widgets are displayed
|Subspecialty Reading Rate             |
|Radiologist Productivity              |
|SLA Compliance                        |
When I "Delete" the following widgets
|Subspecialty Reading Rate             |
|Radiologist Productivity              |
|SLA Compliance                        |
And I create the following widgets
|Communications Turnaround             |
|Peer Review Scorecard                 |
|Peer Review Error Rate by Time of Day |
|Prelim Discrepancy                    |
Then following widgets are displayed
|Communications Turnaround             |
|Peer Review Scorecard                 |
|Peer Review Error Rate by Time of Day |
|Prelim Discrepancy                    |
When I "Delete" the following widgets
|Communications Turnaround             |
|Peer Review Scorecard                 |
|Peer Review Error Rate by Time of Day |
|Prelim Discrepancy                    |
Then widgets are deleted
|Radiologist Productivity              |
|SLA Compliance                        |
|Subspecialty Reading Rate             |
|Communications Turnaround             |
|Peer Review Scorecard                 |
|Peer Review Error Rate by Time of Day |
|Prelim Discrepancy                    |

@automated
@Regression
@Smoke
@TC_AT005
Scenario: Verify unread Exam data
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Unread Exam |
Then following widgets are displayed
|Unread Exam |
When count "Unread Exam" from graph
And get all modalities
Then I "Delete" the following widgets
|Unread Exam |
When I click on "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click "All Intelerad" from "My Worklist"
And I click Advanced Search
Then Advanced Search is launched
When I click on "Clear" action
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label    |  Value                                   | 
| Status   |  Unread                                  | 
| Modality |  CR - Computed Radiography               |
| Modality |  CT - Computed Tomography                | 
| Modality |  US - Ultrasound                         | 
| Modality |  XR - X-Ray                              | 
| Modality |  EC - Echocardiography                   | 
| Modality |  ES - Endoscopy                          | 
| Modality |  IR - Interventional Radiology           | 
| Modality |  MG - Mammography                        | 
| Modality |  MR - Magnetic Resonance                 | 
| Modality |  NM - Nuclear Medicine                   | 
| Modality |  OT - Other                              | 
| Modality |  XA - X-Ray Angiography                  | 
| Modality |  UN - Unknown                            | 
| Modality |  RF - Radio Fluoroscopy                  | 
| Modality |  PT - Positron emission tomography (PET) | 
| Modality |  DD - Duplex Doppler                     | 
| Modality |  DG - Diaphanography                     | 
| Modality |  DM - Digital microscopy                 | 
| Modality |  DX - Digital Radiography                | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
When I click on "Search" action
Then I check the result

@automated
@Regression
@Smoke
@TC_C62677049
Scenario: Edit widget input parameters
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Unread Exam |
When I edit "Unread Exam" widget
And I set the values for the below single choice parameters
| Label                  | Value                     |
| Plot By                | Site                      | 
And I click on "Save" button when editing of "Unread Exam" widget is complete
Then changed parameter "Site" is displayed for "Unread Exam"
Then I "Delete" the following widgets
|Unread Exam |


@automated
@Regression
@Smoke
@TC_C63895951
Scenario: widget export report
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Exam Backlog |
Then following widgets are displayed
|Exam Backlog |
When I export the report for "Exam Backlog" widget
Then I "Delete" the following widgets
|Exam Backlog	|
Then report "Exam Backlog" download is successfull

@Regression
@RadPartner
@TC_C63465867
@automated
Scenario: Rearrange widgets
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets 
|Unread Exam            |
|Exam Backlog		        |
And I Drag drop "Exam Backlog" on "Unread Exam" horizontally
Then following widgets are displayed
|Exam Backlog		        |
|Unread Exam            |
When I create the following widgets
|Exam Count              |
|Stacked Turnaround Time |
And I Drag drop "Unread Exam" on "Exam Backlog" vertically
Then following widgets are displayed 
|Unread Exam             |
|Exam Backlog	           |
|Exam Count              |
|Stacked Turnaround Time |
And I "Delete" the following widgets
|Unread Exam             |
|Exam Backlog	           |
|Exam Count              |
|Stacked Turnaround Time |

@Regression
@automated
@RadPartner
@TC_C62666558
Scenario: Verify Exam Count widget data
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Exam Count |
And I edit "Exam Count" widget
And I set the values for the below single choice parameters
| Label                  | Value                     |
| Plot By                | Modality                  | 
And I select the Exam Time as "1y"
And I click on "Save" button when editing of "Exam Count" widget is complete
Then following widgets are displayed
|Exam Count    |
When I export the report for "Exam Count" widget
# Performing widget deletion till report download is in progress
Then report "Exam Count" download is successfull
When I "Delete" the following widgets
|Exam Count	|
And I get the number of exams from the "Summary" sheet of "Exam Count" file for the Modality "CR"
When I click on "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I click Advanced Search
Then Advanced Search is launched
When I click on "Clear" action
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value                     | 
| Modality       | CR - Computed Radiography |
| Status         | Final                     |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" action
And result count is displayed
Then Advanced Search count matches with the widget excel count


@Regression
@automated
@RadPartner
@TC_C63832759
Scenario: Verify widget data for Communications Turnaround
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
| Communications Turnaround |
And I edit "Communications Turnaround" widget
And I set the values for the below multi choice parameters
| Label                  | Value                     |
| Communication Type     | Issue with Exam           |
| Communication Type     | Critical Issue with Exam  |
And I select the Exam Time as "1yz"
And I click on "Save" button when editing of "Communications Turnaround" widget is complete
Then following widgets are displayed
| Communications Turnaround  |
When I export the report for "Communications Turnaround" widget
And I "Delete" the following widgets
| Communications Turnaround	|
Then report "Communications Turnaround" download is successfull
And I get the Note Count from the "Summary" sheet of "Communications Turnaround" file for the Communication Type "Issue with Exam"
When I click on "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I click Advanced Search
Then Advanced Search is launched
When I click on "Clear" action
And I expand the "Communication Note" section in Advanced Search
And I perform "Communication Note" Advanced Search with following details
| Label          | Value                     |
| Status         | Completed                 | 
| Type           | Issue with Exam           |
| Assigned (G)   | Radiology Support Team    | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And result count is displayed
Then Advanced Search Note Count matches with the widget excel Note Count


