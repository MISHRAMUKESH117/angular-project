@UserCredentialing
Feature: User Credentialing

@automated
@BVT
@Smoke
@TC_CD-001
Scenario:  Verify that bulk action credentialing takes effect in the worklist and analytics
Given Clario application is launched
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Management" application
And I open "User Credentialing" module
And I click on "User" tab
And I search username "Test0410, Test0410"
And I click on user "Test0410, Test0410"
And I click on "Subspecialty" tab under User Tab in right panel
And I add membership for all users
Then log out of the application
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Smart Worklist" application
Then worklist is launched
When I click Advanced Search
And I expand the "Exam" section in Advanced Search
And I click on "Subspecialty:" label dropdown arrow
Then Advanced Search label dropdown contains below options
|Body CT     	  |
|Body General  	|
|Body MR  		  |
|Body US    	  |
|Breast  		    |
|Cardiac		    |
|General 		    |
|Interventional	|
|Mammo 		      |
|MSK 		 	      |
|Neuro CT 		  |
|Neuro General 	|
|Neuro MR 		  |
|Nuclear 		    |
|Oncology 		  |
|Pain 			    |
|Pediatrics		  |
|US General  	  |
And I click on "Analytics" application
Then Analytics is launched
When I create the following widgets 
|Unread Exam  |
And I click on edit "Unread Exam" widget
And I click on "Subspecialty" label dropdown arrow in widget
Then widget label dropdown contains below options
|Body CT     	 |
|Body General  	 |
|Body MR  		 |
|Body US    	 |
|Breast  		 |
|Cardiac		 |
|General 		 |
|Interventional	 |
|Mammo 		     |
|MSK 		 	 |
|Neuro CT 		 |
|Neuro General 	 |
|Neuro MR 		 |
|Nuclear 		 |
|Oncology 		 |
|Pain 			 |
|Pediatrics		 |
|US General  	 |


@automated
@BVT
@Smoke
@TC_CD-002
Scenario:  Verify that only exams specific to a particular site are displayed
Given Clario application is launched
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Management" application
And I open "User Credentialing" module
And I click on "User" tab
And I search username "Test0410, Test0410"
And I click on user "Test0410, Test0410"
And I click on "Site" tab under User Tab in right panel
And I remove membership for all users
And I remove Final credentials for all users
And I check Member checkbox
|Hospital A |
And I save user credentialing
Then log out of the application
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Smart Worklist" application
Then worklist is launched
When I click "All Intelerad" from "My Worklist"
Then worklist contains records only from site "Hospital A"


@automated
@Smoke
@Regression
@TC_CD-003
Scenario:  Verify that only the credentialed sites are displayed after downloading the widget
Given Clario application is launched
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Management" application
And I open "User Credentialing" module
And I click on "User" tab
And I search username "Test0410, Test0410"
And I click on user "Test0410, Test0410"
And I click on "Site" tab under User Tab in right panel
And I remove membership for all users
And I remove Final credentials for all users
And I select Final radio button
|Clinic B |
And I save user credentialing
And I click on "Subspecialty" tab under User Tab in right panel
And I add membership for all users
And I save user credentialing
And I click on "Location" tab under User Tab in right panel
And I add membership for all users
And I save user credentialing
And I click on "Modality" tab under User Tab in right panel
And I add membership for all users
And I save user credentialing
Then log out of the application
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Analytics" application
Then Analytics is launched
When I create the following widgets 
|Practice Productivity  |
And I click on edit "Practice Productivity" widget
And I set the start date to "01/01/2020"
And I select "Month Of Year" from the "Binning" dropdown in widget
And I click on "Save" button when editing of "Practice Productivity" widget is complete
And I click on "Export with Data sheet" the widget "Practice Productivity"
Then report "Practice Productivity" download is successfull
And "Practice Productivity" file doesnot contains values from below sites in the "Exams" tab
|Clinic A 		  |
|Clinic C 		  |
|Hospital A 	  |
|Hospital A 	  |
|Hospital A 	  |
|Imaging Center 1 |
|Imaging Center 2 |
|Imaging Center 3 |
|Imaging Center 4 |
And I delete the file "Practice Productivity"
When I "Delete" the following widgets
|Practice Productivity  |
Then widgets are deleted
|Practice Productivity  |



@automated
@BVT
@Smoke
@TC_CD-005
Scenario:  Ensure that the users are only credentialed for following sites
Given Clario application is launched
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Management" application
And I open "User Credentialing" module
And I click on "User" tab
And I search username "Test0410, Test0410"
And I click on user "Test0410, Test0410"
And I click on "Site" tab under User Tab in right panel
And I remove membership for all users
And I remove Final credentials for all users
And I check Member checkbox
|Hospital A |
|Hospital B |
And I save user credentialing
Then log out of the application
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Smart Worklist" application
Then worklist is launched
When I click on Quick Search "site" dropdown
Then following sites are displayed in Quick Search dropdown
|Hospital A |
|Hospital B |

@automated
@Smoke
@Regression
@TC_CD-006
Scenario:  Ensure that the users are only credentialed for following subspeciality
Given Clario application is launched
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Management" application
And I open "User Credentialing" module
And I click on "User" tab
And I search username "Test0410, Test0410"
And I click on user "Test0410, Test0410"
And I click on "Subspecialty" tab under User Tab in right panel
And I remove membership for all users
And I check Member checkbox
|Body CT  |
|Neuro CT |
And I save user credentialing
Then log out of the application
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Smart Worklist" application
Then worklist is launched
When I click Advanced Search
And I expand the "Exam" section in Advanced Search
And I click on "Subspecialty:" label dropdown arrow
Then following options are displayed under Advanced Search label dropdown
|Body CT  |
|Neuro CT |

@automated
@Smoke
@Regression
@TC_CD-007
Scenario:  Ensure that the users are only credentialed for following modality
Given Clario application is launched
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Management" application
And I open "User Credentialing" module
And I click on "User" tab
And I search username "Test0410, Test0410"
And I click on user "Test0410, Test0410"
And I click on "Modality" tab under User Tab in right panel
And I remove membership for all users
And I check Member checkbox
|AS - Angioscopy |
|ES - Endoscopy  |
And I save user credentialing
Then log out of the application
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Smart Worklist" application
Then worklist is launched
When I click Advanced Search
And I expand the "Exam" section in Advanced Search
And I click on "Modality:" label dropdown arrow
Then following options are displayed under Advanced Search label dropdown
|AS - Angioscopy  |
|ES - Endoscopy   |


@Regression
@automated
@Management
@TC_CD-008
Scenario:  Verify widget exam count in Analytics page against the exported data 
Given Clario application is launched
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Management" application
And I open "User Credentialing" module
And I click on "User" tab
And I search username "Test0410, Test0410"
And I click on user "Test0410, Test0410"
And I click on "Site" tab under User Tab in right panel
And I remove membership for all users
And I remove Final credentials for all users
And I select Final radio button
|Clinic B |
And I save user credentialing
And I click on "Subspecialty" tab under User Tab in right panel
And I add membership for all users
And I save user credentialing
And I click on "Location" tab under User Tab in right panel
And I add membership for all users
And I save user credentialing
And I click on "Modality" tab under User Tab in right panel
And I add membership for all users
And I save user credentialing
Then log out of the application
When log into the application with username "Test0410" and password "NewTest0410"
And I open "Analytics" application
Then Analytics is launched
When I create the following widgets 
|Practice Productivity  |
And I click on edit "Practice Productivity" widget
And I set the start date to "01/01/2020"
And I select "Month Of Year" from the "Binning" dropdown in widget
And I click on "Save" button when editing of "Practice Productivity" widget is complete
And I get number of exams from "PracticeProductivity" widget
And I click on "Export with Data sheet" the widget "Practice Productivity"
Then report "Practice Productivity" download is successfull
And "Practice Productivity" file doesnot contains values from below sites in the "Exams" tab
|Clinic A 		  |
|Clinic C 		  |
|Hospital A 	  |
|Hospital A 	  |
|Hospital A 	  |
|Imaging Center 1 |
|Imaging Center 2 |
|Imaging Center 3 |
|Imaging Center 4 |
And the number of exams in the "Exams" sheet inside "Practice Productivity" file for the site "Clinic B" matches the number shown in widget
And I delete the file "Practice Productivity"
When I "Delete" the following widgets
|Practice Productivity  |
Then widgets are deleted
|Practice Productivity  |

	