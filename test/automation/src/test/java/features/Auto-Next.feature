@Auto-Next
Feature: Auto-Next Worklist


@Regression
@RadPartner
@automated
@TC_C64502253
Scenario: Auto-Next functionality for Dictation
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value                     |
| Status         | Unread                    |
| Modality       | CR - Computed Radiography |
And I perform "Communication Note" Advanced Search with following details
| Label          | Value                     |
| Status         | Completed                 |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Demo Test Worklist 1" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue 
And I close the Advanced Search if it is loaded
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
And I deselect "Viewer" for Auto-Next
And I click on "Start" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
And Dictation is enabled
When I expand patient view panel
And  I click on "Discard Report" icon of report
And I confirm "Discard Report" option of discard report dialog
And I click on "Next Exam" button on Dictation Cancelled window
Then exam is open "COOLEY, MANUEL H"
And Dictation is enabled
And I click on "Discard Report" icon of report
And I confirm "Discard Report" option of discard report dialog
And I click on "Next Exam" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends

@Regression
@RadPartner
@automated
@TC_C64502254
Scenario: Auto-Next functionality when Viewer and Dictation is disabled
#Pre-requisite : Custom Worklist already created in TC_C64502253
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I deselect "Viewer" for Auto-Next
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
And I deselect "Dictate" for Auto-Next
And I click on "Start" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
And Dictation is disabled
And InteleViewer is not launched
When I click "Next" on Auto-Next panel
Then exam is open "COOLEY, MANUEL H"
And Dictation is disabled
And InteleViewer is not launched
When I click "Next" on Auto-Next panel
Then Worklist is loaded after Auto-Next ends

@Regression
@RadPartner
@automated
@TC_C64758039
Scenario: Auto-Next functionality for Cancel Report
#Pre-requisite : Custom Worklist already created in TC_C64502253
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
Then Exam "10307" with Status "Unread" is displayed
And Exam "10098" with Status "Unread" is displayed
When I deselect "Viewer" for Auto-Next
And I click on "Start" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
And Dictation is enabled
When I click on "Cancel Report" icon of report
Then "Dictation Cancelled" dialog is visible
When I click on "Next Exam" button on Dictation Cancelled window
Then exam is open "COOLEY, MANUEL H"
And Dictation is enabled
When I click on "Cancel Report" icon of report
Then "Dictation Cancelled" dialog is visible
When I click on "Next Exam" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends
Then Exam "10307" with Status "Unread" is displayed
And Exam "10098" with Status "Unread" is displayed

@Regression
@RadPartner
@automated
@TC_C64502255
Scenario: Auto-Next functionality for Save Report
#Pre-requisite : Custom Worklist already created in TC_C64502253
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
Then Exam "10307" with Status "Unread" is displayed
And Exam "10098" with Status "Unread" is displayed
When I deselect "Viewer" for Auto-Next
And I click on "Start" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
And Dictation is enabled
When I click on "Save Report" icon of report
Then "Dictation Suspended" dialog is visible
When I click on "Next Exam" button on Dictation Suspended window
Then exam is open "COOLEY, MANUEL H"
And Dictation is enabled
When I click on "Save Report" icon of report
Then "Dictation Suspended" dialog is visible
When I click on "Next Exam" button on Dictation Suspended window
Then Worklist is loaded after Auto-Next ends
When I click "All Intelerad" from "My Worklist"
Then Exam "10307" with Status "Draft" is displayed
And Exam "10098" with Status "Draft" is displayed
When log out of the application
And log into the application as "radiologist" user
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I click "All Intelerad" from "My Worklist"
And I right click on exam "10307"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
And I right click on exam "10098"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
Then Exam "10307" with Status "Unread" is displayed
And Exam "10098" with Status "Unread" is displayed

@Regression
@RadPartner
@automated
@TC_C64502256
Scenario: Pause and Resume Auto-Next
#Pre-requisite : Custom Worklist already created in TC_C64502253
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
And I deselect "Viewer" for Auto-Next
And I click on "Start" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
And Dictation is enabled
When I click on "Discard Report" icon of report
And I confirm "Discard Report" option of discard report dialog
And I click on "Pause" button on Dictation Cancelled window
And I click "All Intelerad" from "My Worklist"
And I click Advanced Search
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label  | Value   |
| Status | Unread  |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I close the Advanced Search if it is loaded
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
And I click on "Resume" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
When I click on "Next Exam" button on Dictation Suspended window
Then exam is open "COOLEY, MANUEL H"
And Dictation is enabled
When I click on "Discard Report" icon of report
And I confirm "Discard Report" option of discard report dialog
And I click on "Next Exam" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends
When I click "All Intelerad" from "My Worklist"
Then Exam "10307" with Status "Draft" is displayed
When log out of the application
And log into the application as "radiologist" user
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I click "All Intelerad" from "My Worklist"
And I right click on exam "10307"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
Then Exam "10307" with Status "Unread" is displayed


@Sikuli
@Regression
@RadPartner
@automated
@TC_C64287457
Scenario: Auto-Next functionality for Viewer and Dictation
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value                     |
| Status         | Unread                    |
| Modality       | CR - Computed Radiography |
And I perform "Communication Note" Advanced Search with following details
| Label          | Value                     |
| Status         | Completed                 |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Demo Sikuli Worklist" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue 
And I close the Advanced Search if it is loaded
And I click "Demo Sikuli Worklist" from "InteleViewer Worklists"
And I click "Demo Sikuli Worklist" from "InteleViewer Worklists"
And I click on "Start" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN JOANN"
And Dictation is enabled
And I close Document View window in InteleViewer if visible
And "MERCER, ROSEMARY JOANN" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click on "Next Exam" button on Dictation Cancelled window
Then exam is open "COOLEY, MANUEL H"
And Dictation is enabled
And I close Document View window in InteleViewer if visible
And "COOLEY, MANUEL H" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click on "Next Exam" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends

@Sikuli
@Regression
@RadPartner
@automated
@TC_C64502252
Scenario: Auto-Next functionality for Viewer
#Pre-requisite : Custom Worklist already created in TC_C64287457
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Demo Sikuli Worklist" from "InteleViewer Worklists"
And I deselect "Dictate" for Auto-Next
And I click on "Start" button on Worklist tab
And I close Document View window in InteleViewer if visible
Then "MERCER, ROSEMARY JOANN" details are displayed in InteleViewer
And Dictation is disabled
When I click "Next" on Auto-Next panel
Then Dictation is disabled
When I close Document View window in InteleViewer if visible
Then "COOLEY, MANUEL H" details are displayed in InteleViewer
When I exit from InteleViewer
Then Worklist is loaded after Auto-Next ends

@Sikuli
@Regression
@RadPartner
@automated
@TC_C65480561
Scenario: Skip Exam using Auto-Next
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I click on "Clear" action
And I click "All Intelerad" from "My Worklist"
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value                     |
| Status         | Dictating                 |
| Status         | Draft                     |
| Modality       | US - Ultrasound           |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Test" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue 
And I close the Advanced Search if it is loaded
And I click "Test" from "InteleViewer Worklists"
And I click on "Start" button on Worklist tab
Then exam is open "HAN, JANE"
When I click "skip" icon for exam "CHANDLER, YVONE CLARE CLARE"
And I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog 
And I click on "Next Exam" button on Dictation Cancelled window
Then exam is open "CHAN, GORDON LEE"
And I close Document View window in InteleViewer if visible
And "CHAN, GORDON LEE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click on "Exit" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends


@Sikuli
@Regression
@RadPartner
@automated
@TC_C65223644
Scenario: Pause and Resume Auto-Next using My Reading Queue Panel
#Pre-requisite : Custom Worklist already created in TC_C65480561
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
And I click on "Start" button on Worklist tab
Then Dictation is enabled
When I click on "Pause" button on Worklist tab
And I close Document View window in InteleViewer if visible
Then "MERCER, ROSEMARY JOANN" details are displayed in InteleViewer
When I exit from InteleViewer
And I click "All Intelerad" from "My Worklist"
And I click Advanced Search
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label  | Value   |
| Status | Unread  |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I close the Advanced Search if it is loaded
And I click "Demo Test Worklist 1" from "InteleViewer Worklists"
And I click on "Resume" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
And I close Document View window in InteleViewer if visible
And "MERCER, ROSEMARY JOANN" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Next Exam" button on Dictation Suspended window
Then exam is open "COOLEY, MANUEL H"
And I close Document View window in InteleViewer if visible
And "COOLEY, MANUEL H" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog 
And I click on "Next Exam" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends
When I click "All Intelerad" from "My Worklist"
Then Exam "10307" with Status "Draft" is displayed
When log out of the application
And log into the application as "radiologist" user
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I right click on exam "10307"
And I select "Change Status" from the right click option 
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
Then Exam "10307" with Status "Unread" is displayed



@Sikuli
@Regression
@RadPartner
@automated
@TC_C65535969
Scenario: Next Exam using Auto-Next
#Pre-requisite : use Worklist created in TC_C65480561
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I click "Test" from "InteleViewer Worklists"
And I click on "Start" button on Worklist tab
And I click "next" icon for exam "CALDERON, ROBIN"
Then exam is open "HAN, JANE"
And I close Document View window in InteleViewer if visible
And "HAN, JANE" details are displayed in InteleViewer
And I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog 
And I click on "Next Exam" button on Dictation Cancelled window
Then exam is open "CALDERON, ROBIN"
And I close Document View window in InteleViewer if visible
And "CALDERON, ROBIN" details are displayed in InteleViewer
And I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click on "Exit" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends


@Sikuli
@Regression
@RadPartner
@automated
@TC_C66097636
Scenario: Auto-Next for My Reading Queue Worklist
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I click "My Reading Queue" from "My Worklists"
And I click on "Start" button on Worklist tab
Then exam is open "MERCER, ROSEMARY JOANN"
And Dictation is enabled
And I close Document View window in InteleViewer if visible
And "MERCER, ROSEMARY JOANN" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog 
And I click on "Next Exam" button on Dictation Cancelled window
Then exam is open "FLAHERTY, DENISE"
And Dictation is enabled
And I close Document View window in InteleViewer if visible
And "FLAHERTY, DENISE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click on "Exit" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends


@Sikuli
@Regression
@RadPartner
@automated
@TC_C64502257
Scenario: Auto-Next functionality for Sign Report
#Pre-requisite : use Worklist created in TC_C65480561
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Test" from "InteleViewer Worklists"
Then Exam "10729" with Status "Unread" is displayed
And Exam "25889" with Status "Unread" is displayed
When I click on "Start" button on Worklist tab
Then exam is open "HAN, JANE"
When I close Document View window in InteleViewer if visible
And "HAN, JANE" details are displayed in InteleViewer
And I exit from InteleViewer
And I click on "Sign Report" option of Select Report Action dialog
Then exam is open "CHANDLER, YVONE CLARE CLARE"
And I close Document View window in InteleViewer if visible
And "CHANDLER, YVONE CLARE" details are displayed in InteleViewer
And I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog 
And I click on "Exit" button on Dictation Cancelled window
Then Worklist is loaded after Auto-Next ends
When log out of the application
And log into the application as "radiologist" user
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I click Advanced Search
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label    | Value           |
| Status   | Signed          |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
Then Exam "25889" with Status "Signed" is displayed 
When I right click on exam "25889"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
And I click "Test" from "InteleViewer Worklists"
Then Exam "25889" with Status "Unread" is displayed
