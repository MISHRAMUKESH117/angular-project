@Calendar
Feature: Calendar Page

@automated
@BVT
@ShiftManagement
@Smoke
@TC_C49169371
Scenario: Add an event in calendar
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Calendar" module
And I click on date to create event
And create New event with following details
|Title      |Event Type |Event Time |Location     |Notes   |Chat Status|
|Test Event |Meeting    |All day    |Meeting Room | Meeting|Busy       |
And I select "radiologist" from list
And I select "groupID" from list
And I select "entityID" from list
And I select "Pause peer review assignment during meeting"
And I click on "Save Event"
Then event is created
When I click on delete to delete a event
Then event is deleted