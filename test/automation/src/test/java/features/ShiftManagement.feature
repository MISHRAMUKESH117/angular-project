@ShiftManagement
Feature: ShiftManagement Page

@automated
@BVT
@Smoke
@RadPartner
@Regression
@TC_C63832760
Scenario: Create a new shift and add user to shift
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
And I click on "Create New Shift" button
And create New Shift with following details
|Name           |Start Time |End Time   |Start Date|
|Test New Shift |10:30 AM   |7:30 PM    |5/5/2020  | 
And I click on "Create" button to create "Test New Shift" shift
Then "Test New Shift" new shift is created
When I click on "Test New Shift" shift
And I set "Test" as "description" 
And I click on "save" button to save shift
Then "Test New Shift" shift is edited 
When I click on "Test New Shift" row to assign user
And I assign "Tech, Citius" as user
And I click on done button to save
Then "Tech, Citius" user is assigned to "Test New Shift" shift
When I select "Tech, Citius" user to delete from "Test New Shift" shift
And I click on done button to save
Then "Tech, Citius" user is deleted from "Test New Shift" shift


@automated
@Smoke
@Regression
@RadPartner
@TC_C63832761
Scenario: Add worklist to an existing shift
#Pre-requisite : Use shift created in TC_C63832760
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
And I click on "Test New Shift" shift
And I click on "My Reading Queue" from worklist 
And I select "Exams in Transcription" from "InteleViewer Worklists" 
And I click on sort worklist checkox
And I click to save worklist
And I click on "save" button to save shift
And I click on "Smart Worklist" application
Then worklist is launched
And "Exams in Transcription" worklist is added under "InteleViewer Worklists" worklist  

@automated
@BVT
@Smoke
@RadPartner
@TC_C63832762
Scenario: Delete an existing shift
#Pre-requisite : Use shift created in TC_C63832760
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
And I click on "Delete" button to delete "Test New Shift" shift
Then shift is deleted

@automated
@Regression
@RadPartner
@TC_C63832763
Scenario: Import ShiftSchedule file
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
And I click on "Import" button
And I click on Template file for import hyperlink
Then "Shift Schedule Import" default template file is downloaded
And I delete the file "Shift Schedule Import"


@Sikuli
@automated
@Regression
@RadPartner
@TC_C64649812
Scenario: Add user to an existing shift via import
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
And I click on user dropdown in the page header
And I select "User Settings" option from user dropdown menu
And I set "atest" as External Schedule ID
And I click on "Save" button on User Settings window
And I click on "Import" button
And I click on Template file for import hyperlink
And I click on "Close" button on Import dialog
Then "Shift Schedule Import" default template file is downloaded
When I set below values in "Shift Schedule Import" file in "Sheet1"
|Header     |Value  |
|Shift Name |IV Body|
|05/27/2015 |atest  |
And I click on "Import" button
And I import "Shift Schedule Import" file
And I click on "Import" button on Import dialog
Then "Test123, test123" user is added under the current date against "IV Body"
When I select "Test123, test123" user to delete from "IV Body" shift
And I click on done button to save
Then "Test123, test123" user is deleted from "IV Body" shift
And I delete the file "Shift Schedule Import"
