@Reports
Feature: Reports

@automated
@BVT
@Smoke
@TC_AT006
Scenario: Create Reports
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
Then Reports are displayed
When I select "1y" as exam date range for report generation
And I create the following report
| Peer Review Analysis |
Then reports are created successfully
| Peer Review Analysis |

@automated
@Smoke
@Regression
@TC_AT007
Scenario: Verify Reports data
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
Then Reports are displayed
When I select "1y" as exam date range for report generation
And I create the following report
| Peer Review Analysis  |
Then reports are created successfully
| Peer Review Analysis  |
When I download the report
| Peer Review Analysis  | 
Then datasheet shows appropriate information
| Peer Review Analysis  |


@automated
@Smoke
@Regression
@TC_AT008
Scenario: Edit input parameters and verify reports data
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
Then Reports are displayed
When I set the following fields
| Parameter Name | Value      |
| Location       | ONC        |
| Site           | Hospital B |
And I select "1y" as exam date range for report generation
And I create the following report
| Peer Review Analysis |
Then reports are created successfully
| Peer Review Analysis |
When I download the report
| Peer Review Analysis | 
Then excel contains exported information
| Column Name | Value      | FileName             |
| Site        | Hospital B | Peer Review Analysis |

@Regression
@automated
@RadPartner
@TC_C63465879
Scenario: Create Report specific to Site
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
Then Reports are displayed
When I set the following fields
| Parameter Name | Value           |
| Site           | Imaging Center 1|
And I select "1y" as exam date range for report generation
And I create the following report
| Peer Review Analysis |
Then reports are created successfully
| Peer Review Analysis |
When I download the report
| Peer Review Analysis | 
Then excel contains exported information
| Column Name | Value            | FileName             |
| Site        | Imaging Center 1 | Peer Review Analysis |


@Regression
@automated
@RadPartner
@TC_C66097634
Scenario: Save and schedule the report
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
And I select "1y" as exam date range for report generation
And I click on Save Report button
And I set the Report Name as "Test" and schedule as "Daily"
And I click on Save schedule button
And I create the following report
| Peer Review Analysis  |
Then reports are created successfully
| Peer Review Analysis  |
When I download the report
| Peer Review Analysis  | 
Then datasheet shows appropriate information
| Peer Review Analysis  |



@Regression
@automated
@RadPartner
@TC_C64502260
Scenario:  Create a Custom Report
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click on "Custom Reports" menu option
And I set the Custom Report name as "Test Custom Report"
And I activate the Custom Report
And I set the Worksheet name as "Test Custom Report Sheet"
And I get the Query from "Export_ProcedureNames" excel "Query" Worksheet for Custom Report creation
And I set the Query for Custom Report creation
And I drag-drop below columns to add in the Worksheet
| Section           | Column Name  |
| Communication     | Assigned (G) |
| Communication     | Status       |
And I click on Active checkbox to activate query
And I click on "Save" button in the bottom panel of Custom Reports
When log out of the application
And log into the application as "admin" user
And I open "Analytics" application
And I click the Reports tab 
And I select the following report
| Test Custom Report |
Then Reports are displayed
When I create the following report
| Test Custom Report |
Then reports are created successfully
| Test Custom Report |
When I download the report
| Test Custom Report |
Then Custom Report shows appropriate information
| Test Custom Report  |
When I click on "Management" application
And I open "Configuration" module
And I click on "Custom Reports" menu option
And I click the delete icon of "Test Custom Report"
And I confirm the deletion of Custom Report
Then Custom Report "Test Custom Report" is deleted


