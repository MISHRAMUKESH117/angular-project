@SmartWorklist
Feature: Worklist

@automated
@BVT
@Smoke
@TC_WT-001
Scenario: worklist launched
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded

@automated
@BVT
@Smoke
@TC_WT-021
Scenario: Verify - Quick Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
Then worklist is loaded
When I perform Quick Search 
| Label     | Value     |
| mrn       | 246930740 |
| firstName | LISA      |
| lastName  | FISCHER   |
| accession | 246930741 |
| site      | Zebra     |
And I click on Search button in Quick Search
Then exam is displayed "246930740"
When I click on exam "246930740"
Then exam is open "Fischer, Lisa"


@automated
@BVT
@Smoke
@TC_WT-005
Scenario: Verify - Advanced Search (Single Field Search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
Then worklist is loaded
When I click Advanced Search
Then Advanced Search is launched
When I expand the "Exam" section in Advanced Search
And I set "Exam" Advanced Search with below details
| Label          | Value          | 
| Accession      | 246930741      | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then result count is displayed
When I clear results
Then No records are displayed


@automated
@Smoke
@Regression
@TC_WT-006
Scenario: Verify - Advanced Search for search type Exam (Combination Search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I click Advanced Search
Then Advanced Search is launched
When I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value          | 
| Status         | Unread         | 
| Billing Status | Incomplete     | 
| Billing Status | Submitted      | 
| Billing Status | Rejected       | 
And I perform "Exam" Advanced Search with below details
| Label          | Value          | 
| Emergency      | Include Active | 
And I set "Exam" Advanced Search with below details
| Label          | Value          | 
| Accession      | 10307          | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then result count is displayed
When I clear results
Then No records are displayed



@automated
@BVT
@Smoke
@TC_WT-013
Scenario: Verify Worklist - Patient view sections load
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "DEMERS, LARRY"
Then exam is displayed "12748"
When I click on exam "12748"
And I expand the "Patient" panel for "DEMERS, LARRY"
And I expand the "Ordering" panel for "Referring Physician"
Then I check the following details
|Demographic Identifier |Value           |
|DOB         			|11/16/1980                    |
|Gender      			|Male                          |
|MRN         			|12748                         |
|Accession   			|14415                         |
|Location    			|NEURO                         |
|Modality    			|MR - Magnetic Resonance       |
|Procedure   			|MR HEAD BLACKFORD ICOMETRIX-DM|
|Priority    			|Stat                          |
|Exam Reason 			|Reason for study              |
|History     			|Relevant clinical information |
|Ordering    			|Referring Physician           |
And I check the "Note" in site panel is "Preference Notes"
And I check the following panel is loaded
|Notes|
|Exams|


@automated
@BVT
@Smoke
@TC_WT-004
Scenario: Verify - worklist count and load time
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
Then I verify "All Intelerad" worklist tree and tab count
And I verify worklist load time is less than "2" seconds


@automated
@Regression
@TC_WT-008
Scenario: Verify - count should enable after one refresh cycle of the tree
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I click on "Clear" action
And I set "Exam" Advanced Search with below details
| Label          | Value          | 
| Accession      | 246930741      |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" action
Then exam is displayed "246930741"
When I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Count Enable/Disable" as worklist name
And I click on "InteleViewer Worklists" "Save" button to add new worklist
Then "Count Enable/Disable" worklist is added under "InteleViewer Worklists"
And I verify the worklist "Count Enable/Disable" tree count is disabled
When I click on "Management" application
And I open "Configuration" module
And I click "Worklist Tuning" from "Worklist" Configuration Page
And I search for "Count Enable/Disable" worklist in Worklist panel
And I select the "Count Enable/Disable" worklist checkbox to enable count
And I switch to "Worklist" tab
And I close the Advanced Search if it is loaded
And I click "Count Enable/Disable" from "InteleViewer Worklists"
Then I verify the worklist "Count Enable/Disable" tree count is enabled with exam count 2
When I right click on "Count Enable/Disable" from "InteleViewer Worklists"
And I select "Delete" option from worklist menu
And I confirm Delete worklist
Then "Count Enable/Disable" worklist is not visible under "InteleViewer Worklists"

@automated
@Regression
@TC_WT-007
Scenario: create worklists - single,combine, combine worklist with exclusions
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I click Advanced Search
Then Advanced Search is launched
When I expand the "Exam" section in Advanced Search
And I click on "Clear" action
And I perform "Exam" Advanced Search with following details
| Label          | Value                    |  
| Priority       | Stat                     | 
| Modality       | CT - Computed Tomography |
And I set "Exam" Advanced Search with below details
| Label          | Value          | 
| Accession      | 354697231      | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then exam is displayed "354697231"
When I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Demo Test Worklist" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue 
Then "Demo Test Worklist" worklist is added under "InteleViewer Worklists" 
When I click on "Clear" action
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value                    |  
| Status         | Unread                   |
And I set "Exam" Advanced Search with below details
| Label          | Value          | 
| Accession      | 14710          | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then exam is displayed "14710"
When I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Demo Test Worklist 2" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue 
Then "Demo Test Worklist 2" worklist is added under "InteleViewer Worklists"
When I click "Demo Test Worklist 2" from "InteleViewer Worklists"
Then exam is displayed "14710" 
When I drag drop "Demo Test Worklist" on "Demo Test Worklist 2"
And I set "Combined Demo Worklists" as worklist name
And I click on "Combine" button of "Combine Worklist" dialogue
Then "Combined Demo Worklists" worklist is added under "InteleViewer Worklists"
When I click "Combined Demo Worklists" from "InteleViewer Worklists"
Then exam is displayed "354697231"
And exam is displayed "14710"
When I expand "Combined Demo Worklists"
And I click "Demo Test Worklist 2" from "Combined Demo Worklists"
And I right click on "Demo Test Worklist 2" from "Combined Demo Worklists"
And I select "Exclude" option from worklist menu
And I click "Combined Demo Worklists" from "InteleViewer Worklists"
Then exam is displayed "354697231"
And verify count of rows displayed is "2"
When I right click on "Combined Demo Worklists" from "InteleViewer Worklists"
And I select "Delete" option from worklist menu
And I confirm Delete worklist
Then "Combined Demo Worklists" worklist is not visible under "InteleViewer Worklists"

@automated
@Regression
@TC_WT-009
Scenario: Sort all Columns
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I click following columns to sort them 
| Column Name  | Property        |
| MRN          | Sort Ascending  |
| Priority     | Sort Descending |
Then I verify columns sort order
| Column Name  | Property   |
| MRN          | ASC        |


@automated
@Regression
@TC_WT-010
Scenario: Verify - properties of worklist are changed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
When I right click on "Unread Routine" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
Then I verify properties dialogue is open
When I change properties 
| Label                    | Value        |
| Refresh rate in seconds  | 10           |
| First sort order         | AI Priority  |
| Row coloring schema      | No style     |
And I click on "Change" button from Properties dialogue
When I right click on "Unread Routine" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
Then I verify properties are changed
| Label                    | Value        |
| Refresh rate in seconds  | 10           |
| First sort order         | AI Priority  |
| Row coloring schema      | No style     |



@automated
@Regression
@TC_WT-012
Scenario: Verify - worklist export
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
When I click on export worklist
Then "All Intelerad" worklist is appropriately exported

@Regression
@automated
@TC_WT-022
Scenario: Verify Exam in history panel after Change Status, Add Note, Change priority
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button 
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I expand History panel
And I perform worklist search "TREMBLAY, SALLY"
Then exam is displayed "12272"
When I click on exam "12272" 
Then exam is open "TREMBLAY, SALLY" 
When I click on "Communication Note" note icon
And I add "Communication Note Test" in note box
And I click on "Submit" button to add note
Then note is created
And "14231" exam is present in history panel 
When I click "My Reading Queue" from "My Worklist" 
And I perform worklist search "SINGH, ROBERT"
Then exam is displayed "12016"
When I click on exam "12016" 
Then exam is open "SINGH, ROBERT"
When in the Exam panel I click on accession "14802"
Then "Exam History" window is opened
When I expand the Information section
And I click on the "Exam History" edit button
And I select the following details in exam history panel
|Label Name |Value              |
|Status     |Final              |
And I save the "Exam History" changes
And I close the "Exam History" window
Then "14802" exam is present in history panel
When I click "My Reading Queue" from "My Worklist"
And I click on exam from history panel "14802"
Then exam is open "SINGH, ROBERT"
When in the Exam panel I click on accession "14802"
And I click on the "Exam History" edit button
And I select the following details in exam history panel
|Label Name |Value              |
|Status     |Unread             |
And I save the "Exam History" changes
And I close the "Exam History" window
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "DEMERS, LARRY"
Then exam is displayed "12748"
When I click on exam "12748"
Then exam is open "DEMERS, LARRY" 
When in the Exam panel I click on accession "14415"
And I click on the "Exam History" edit button
And I select the following details in exam history panel
|Label Name |Value              |
|Priority   |Stat 1             |
And I save the "Exam History" changes
And I close the "Exam History" window
Then "14415" exam is present in history panel
When I click "My Reading Queue" from "My Worklist"
And I click on exam from history panel "14415"
Then exam is open "DEMERS, LARRY"
When in the Exam panel I click on accession "14415"
And I click on the "Exam History" edit button
And I select the following details in exam history panel
|Label Name |Value              |
|Priority   |Stat               |
And I save the "Exam History" changes
And I close the "Exam History" window
And I collapse History panel
And log out of the application
Then "admin" user is logged out

@Regression
@automated
@TC_WT-023
Scenario: Verify Worklist - Patient View sections load when exam clicked from history panel
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click on worklist "My Worklist"
And I click on exam from history panel "14231"
And I expand the "Patient" panel for "TREMBLAY, SALLY"
And I expand the "Ordering" panel for "Referring Physician"
Then I check the following details
|Demographic Identifier         |Value     |
|DOB         			              |11/25/1947|
|Gender      			              |Female    |
|MRN         			              |12272     |
|Age        			              |72y       |
When in the Exam panel I click on accession "14231"
Then "Exam History" window is opened
When I expand the Information section
And I click on the "Exam History" edit button
And I edit the following details in exam
|Label Name |Value              |
|Reason     |Reason For Study 2 |
And I save the "Exam History" changes
And I close the "Exam History" window
And I click on worklist "My Worklist"
And I click on exam from history panel "14231"
Then I check the following details
|Demographic Identifier |Value              |
|Reason                 |Reason For Study 2 | 
When in the Exam panel I click on accession "14231"
And I click on the "Exam History" edit button
And I edit the following details in exam
|Label Name |Value              |
|Reason     |Reason For Study   |
And I save the "Exam History" changes
And I close the "Exam History" window
And I click on worklist "My Worklist"
And log out of the application
Then "admin" user is logged out


@Regression
@automated
@TC_WT-034
Scenario: Verify the Peer Review Assignment works properly
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "ASH, ELIZABETH"
Then exam is displayed "12461"
When I right click on "12461" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
Then log out of the application
When log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
Then exam is displayed "12461"
And note message "Test Peer Review Assignment" is displayed



@Regression
@automated
@TC_WT-035
Scenario: Verify the exam details loads properly Peer Review window
#Pre-requisite : TC_WT-034 should be executed.
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
Then exam is displayed "12461"
When I click on exam "12461"
And I uncheck Always Launch Viewer checkbox in Peer Review window for exam "12461"
Then "ASH, ELIZABETH" exam details are displayed in Peer Review window


@Regression
@automated
@TC_WT-036
Scenario: Verify the Clear button functionality in Peer Review Window
#Pre-requisite : TC_WT-034 should be executed.
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
Then exam is displayed "12461"
When I click on exam "12461"
Then "ASH, ELIZABETH" exam details are displayed in Peer Review window
When I set the note message in Peer Review window as "Test Note Message"
And I set the following values under Note section in Peer Review window
|DropdownName   |DropdownValue  |
|Reason         |Focused        |
|Rating         |3a             |
|Category       |Chest          |
|Classification |EQUIP 3        |
And I click on "Clear" button in the bottom panel
Then the dropdown values under Note section in Peer Review window is set to default as
|DropdownName   |DropdownValue   |
|Reason         |User Selected   |
|Rating         |1 - Agreement   |
|Category       |(No Category)   |
|Classification |EQUIP 1         |
And message field under the Note section in Peer Review window is cleared

@Regression
@automated
@TC_WT-037
Scenario: Verify the Dismiss Peer Review button functionality in Peer Review Window
#Pre-requisite : TC_WT-034 should be executed.
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
Then exam is displayed "12461"
When I click on exam "12461"
Then "ASH, ELIZABETH" exam details are displayed in Peer Review window
When I click on "Dismiss Peer Review" button in the bottom panel
And I set the Dismiss Peer Review reason as "Test Dismiss functionality"
And I click "All Intelerad" from "My Worklist"
And I click "Peer Reviews Assigned to Me" from "Peer Review"
Then exam "12461" is not displayed in the worklist


@Regression
@automated
@TC_WT-038
Scenario: Verify the Submit Peer Review button functionality in Peer Review Window
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "CLARK, PATRICK"
Then exam is displayed "129745863"
When I right click on "129745863" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
Then log out of the application
When log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
And I click on exam "129745863"
And I uncheck Always Launch Viewer checkbox in Peer Review window for exam "129745863"
Then "CLARK, PATRICK" exam details are displayed in Peer Review window
When I set the note message in Peer Review window as "Test Peer Review Assignment Submit functionality"
And I click on "Submit Peer Review" button in the bottom panel
And I switch to "Worklist" tab
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "CLARK, PATRICK"
Then exam is displayed "129745863"
When I click on exam "129745863"
And I expand note section
Then "Test Peer Review Assignment Submit functionality" is seen in note
And I collapse the note section


@Regression
@automated
@TC_WT-039
Scenario: Verify the Delete Peer Review functionality
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "TREMBLAY, SALLY"
Then exam is displayed "14231"
When I right click on "14231" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
Then log out of the application
When log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
And I delete exam "14231" from the worklist
Then exam "14231" is not displayed in the worklist


@Regression
@automated
@TC_WT-040
Scenario: Verify the Reject Peer Review functionality
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "TREMBLAY, SALLY"
Then exam is displayed "14231"
When I right click on "14231" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment for Reject" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
Then log out of the application
When log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
And I reject exam "14231" from the worklist
And  set reject reason as "Test Peer Review reject functionality" 
Then exam "14231" is not displayed in the worklist
When I click "All Intelerad" from "My Worklist"
And I perform worklist search "TREMBLAY, SALLY"
Then exam is displayed "14231"
When I click on exam "14231"
And I expand note section
Then "Test Peer Review reject functionality" is seen in note
And note with message "Test Peer Review Assignment for Reject" is in "Reject" state
And I collapse the note section


@Regression
@automated
@TC_WT-041
Scenario: Verify the Auto-next checkbox functionality in Peer Review Window
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "JACOBS, JENKIN"
Then exam is displayed "21103"
When I right click on "21103" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment Patient 1" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
And I perform worklist search "CLEMENTS, BILLY"
Then exam is displayed "10018"
When I right click on "10018" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment Patient 2" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box	
And I perform worklist search "ESTRADA, DIANA CAROL"
Then exam is displayed "10019"
When I right click on "10019" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment Patient 3" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
Then log out of the application
When log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
And I click on exam "21103"
And I uncheck Always Launch Viewer checkbox in Peer Review window for exam "21103"
Then "JACOBS, JENKIN" exam details are displayed in Peer Review window
When I check Auto-next checkbox in Peer Review window
And I click on "Submit Peer Review" button in the bottom panel
Then "CLEMENTS, BILLY" exam details are displayed in Peer Review window
When I click on "Submit Peer Review" button in the bottom panel
Then "ESTRADA, DIANA" exam details are displayed in Peer Review window
When I click on "Submit Peer Review" button in the bottom panel
And I switch to "Worklist" tab
Then exam "21103" is not displayed in the worklist
And exam "10018" is not displayed in the worklist
And exam "10019" is not displayed in the worklist

@Sikuli
@Regression
@automated
@TC_WT-042
Scenario: Peer Review - Check exam details in Viewer.
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "COOK, ISABELLE"
Then exam is displayed "10118"
When I right click on "10118" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
Then log out of the application
When log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
And I click on exam "10118"
And I uncheck Always Launch Viewer checkbox in Peer Review window for exam "10118"
Then "COOK, ISABELLE" exam details are displayed in Peer Review window
When I click on Open in Viewer icon in Peer Review window
And I close Document View window in InteleViewer if visible
Then "COOK, ISABELLE" details are displayed in InteleViewer
When I exit from InteleViewer
Then "COOK, ISABELLE" exam details are displayed in Peer Review window

@Sikuli
@Regression
@automated
@TC_WT-043
Scenario: Verify the Always Launch Viewer functionality in Peer Review 
#Pre-requisite : TC_WT-042 should be executed.
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
Then exam is displayed "10118"
When I click on exam "10118"
And I check Always Launch Viewer checkbox in Peer Review window for exam "10118"
And I close Document View window in InteleViewer if visible
Then "COOK, ISABELLE" details are displayed in InteleViewer
When I exit from InteleViewer
Then "COOK, ISABELLE" exam details are displayed in Peer Review window


@Sikuli
@Regression
@automated
@TC_WT-044
Scenario: Verify the Auto-next functionality with Always Launch Viewer functionality in Peer Review 
#Pre-requisite : TC_WT-042 should be executed.
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "KAREN, SMITH"
Then exam is displayed "969470208"
When I right click on "969470208" exam
And I select "Assign to Peer Review" from the right click option
And I select the radiologist name as "Test, Radiologist" to assign the Peer review
And I set "Test Peer Review Assignment" as note message in Assign Peer Review dialog box
And I click on assign button on Assign Peer Review dialog box
Then log out of the application
When log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "Peer Reviews Assigned to Me" from "Peer Review"
And I click on exam "10118"
And I check Always Launch Viewer checkbox in Peer Review window for exam "10118"
And I close Document View window in InteleViewer if visible
Then "COOK, ISABELLE" details are displayed in InteleViewer
And "COOK, ISABELLE" exam details are displayed in Peer Review window
When I click on "Submit Peer Review" button in the bottom panel
And I close Document View window in InteleViewer if visible
Then "KAREN, SMITH" details are displayed in InteleViewer
When I exit from InteleViewer
Then "KAREN, SMITH" exam details are displayed in Peer Review window


@Regression
@automated
@TC_WT-072
Scenario: Verify - Advanced Search for search type Communication (Combination search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I check if Advanced Search is open
And I expand the "Communication Note" section in Advanced Search
And I perform "Communication Note" Advanced Search with following details
| Label          | Value                  | 
| Status         | Completed              | 
| Type           | Issue with Exam        | 
| Assigned (G)   | Radiology Support Team | 
And I expand the "Exam" section in Advanced Search
And I set "Exam" Advanced Search with below details
| Label          | Value                  | 
| Accession      | 14231                  | 
And I expand the "Enterprise" section in Advanced Search
And I perform "Enterprise" Advanced Search with following details
| Label          | Value                  |
| Site Group     | Demo                   | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action 
Then result count is displayed
When I clear results
Then No records are displayed

@Regression
@automated
@TC_WT-073
Scenario: Verify - Advanced Search for search type Task (Combination search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I check if Advanced Search is open
And I select Advanced Search module "Task"
And I enter the details for Task Advanced Search
| Label          | Value                  | Enclosure        |
| Status         | Waiting                | Task             |
| Type           | Emergency              | Task             |
| Created By     | Larsen, Sara           | Task             |
| Created On     | 03/02/2020             | Task Date - Start|
And I click on "Search" action
Then result count is displayed
When I clear results
Then No records are displayed

@Regression
@automated
@TC_WT-074
Scenario: Verify - Advanced Search for search type Peer Review (Combination search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I check if Advanced Search is open
And I expand the "Peer Review" section in Advanced Search
And I perform "Peer Review" Advanced Search with following details
| Label          | Value                | 
| Status         | Final                | 
| Reason         | Ongoing              | 
| Category       | Neuro                | 
| Rating         | 1 - Agreement        | 
| Created By     | Chan, Kathy          |
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value                | 
| Subspecialty   | Mammo                | 
And I expand the "Enterprise" section in Advanced Search
And I perform "Enterprise" Advanced Search with following details
| Label          | Value                  |
| Site Group     | Demo                   | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then result count is displayed
When I clear results
Then No records are displayed

@Regression
@automated
@TC_WT-075
Scenario: Verify - Advanced Search for search type Tech QA (Combination search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I check if Advanced Search is open
And I expand the "Tech QA Note" section in Advanced Search
And I perform "Tech QA Note" Advanced Search with below details
| Label          | Value                       | 
| Status         | Pending                     | 
| Rating         | Documentation error         | 
And I set "Tech QA Note" Advanced Search with below details
| Label          | Value                       | 
| Created By     | Larsen, Sara                |  
And I expand the "Enterprise" section in Advanced Search
And I perform "Enterprise" Advanced Search with following details
| Label          | Value                  |
| Site Group     | Demo                   | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then result count is displayed
When I clear results
Then No records are displayed

@Regression
@automated
@TC_WT-077
Scenario: Verify - Advanced Search for search type Teaching Note (Combination search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I check if Advanced Search is open
And I select Advanced Search module "Teaching Note"
And I enter the details for Teaching Note Advanced Search
| Label          | Value                       | Enclosure              |
| Created By     | Larsen, Sara                | Teaching Note          |
| Commented By   | Larsen, Sara                | Teaching Note          |
| Created On     | 04/16/2020                  | Teaching Note - Start  |
| Commented On   | 04/16/2020                  | Teaching Note - Start  |
| Ordering       | Mei Li                      | Personnel              |
And I select "1y" as date range in Exam Date on the "Teaching Note" Advanced Search page
And I click on "Search" action
Then result count is displayed
When I clear results
Then No records are displayed

@Regression
@automated
@TC_WT-076
Scenario: Verify - Quick Search for wildcard characters in single field,combination of fields and all fields
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I perform Quick Search 
| Label     | Value     | 
| mrn       | 2*[0-3]0  | 
And I click on Search button in Quick Search
Then I verify all the displayed exam values
| Label     | Index     | Value     |
| mrn       | 9         | 2*[0-3]0  |
When I click on worklist "My Worklist"
And I perform Quick Search 
| Label     | Value     | 
| mrn       | 2*[0-3]0  | 
| lastName  | F[A-Z]%R* |
And I click on Search button in Quick Search
Then I verify all the displayed exam values
| Label     | Index     | Value     |
| mrn       | 9         | 2*[0-3]0  |
| lastName  | 8         | F[A-Z]%R* |
When I click on worklist "My Worklist"
And I perform Quick Search 
| Label     | Value         | 
| mrn       | 2*[0-3]0      | 
| lastName  | F[A-Z]%R*     |
| firstName | M%L           |
| accession | %1            |
| site      | I[a-z]ging    |
And I click on Search button in Quick Search
Then I verify all the displayed exam values
| Label     | Index     | Value            |
| mrn       | 9         | 2*[0-3]0         |
| lastName  | 8         | F[A-Z]%R*        |
| firstName | 8         | M%L              |
| accession | 12        | %1               |
| site      | 18        | I[a-z]ging       |

@Regression
@automated
@TC_WT-031
Scenario: Launch Advanced Search from Quick Search panel
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I launch the Advanced Search from Quick Search panel
Then I check if Advanced Search is open
When I expand the "Exam" section in Advanced Search
And I set "Exam" Advanced Search with below details
| Label          | Value          | 
| Accession      | 10307          | 
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then result count is displayed
When I clear results
Then No records are displayed

 
@Regression
@automated
@TC_WT-032
Scenario: Add a column to the Worklist
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I click on following column name dropdown arrow to add new column
| Column Name  | Property   | Value         |
| MRN          | Columns    | Date of Birth |
And I click "All Intelerad" from "My Worklist"
Then I verify new column is added
| Date of Birth  |
When I click "All Intelerad" from "My Worklist"
And I click on following column name dropdown arrow to remove a column
| Column Name  | Property   | Value         |
| MRN          | Columns    | Date of Birth |
And I click "All Intelerad" from "My Worklist"
Then I verify column is removed
| Date of Birth |

@Regression
@automated
@TC_WT-033
Scenario: Remove a column from the Worklist
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I click on following column name dropdown arrow to add new column
| Column Name  | Property   | Value         |
| MRN          | Columns    | Site MRN      |
And I click "All Intelerad" from "My Worklist"
Then I verify new column is added
| Site MRN |
When I click "All Intelerad" from "My Worklist"
And I click on following column name dropdown arrow to remove a column
| Column Name  | Property   | Value         |
| MRN          | Columns    | Site MRN      |
And I click "All Intelerad" from "My Worklist"
Then I verify column is removed
|Site MRN  |

@Regression
@automated
@TC_WT-028
Scenario: Verify Assign exam via Lock Exam to Me icon works properly
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I hover on exam "14308" with "Unread" Status
And I click on icon "Lock Exam to Me"
And I click "Assigned to Me and My Groups" from "My Worklist"
Then exam is displayed "14308"
When I hover on exam "14308" with "Unread" Status
And I click on icon "Unassign Exam"
And I click "Assigned to Me and My Groups" from "My Worklist"
Then No records are displayed


@Regression
@automated
@TC_WT-029
Scenario: Verify Assign exam via Assign/Lock Exam icon works properly
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I hover on exam "10765" with "Unread" Status
And I click on icon "Assign/Lock Exam"
And I select "User" as "Test123, test123" on Assign Dialog box
And I set reason as "Test Assign/Lock Exam icon" on Assign Dialog box
And I click on "Assign" button on Assign Dialog box
And I click "Assigned to Me and My Groups" from "My Worklist"
Then exam is displayed "10765"
When I hover on exam "10765" with "Unread" Status
And I click on icon "Assign/Lock Exam"
And I click on "Unassign" button on Assign Dialog box
And I click "Assigned to Me and My Groups" from "My Worklist"
Then No records are displayed


@Regression
@automated
@TC_WT-030
Scenario: Verify exam assignment works properly
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I right click on "54564204" exam
And I select "Assign" from the right click option
And I select "User" as "Test123, test123" on Assign Dialog box
And I set reason as "Test Assign option from properties" on Assign Dialog box
And I click on "Assign" button on Assign Dialog box
And I click "Assigned to Me and My Groups" from "My Worklist"
Then exam is displayed "54564204"
And I right click on "54564204" exam
And I select "Assign" from the right click option
And I click on "Unassign" button on Assign Dialog box
And I click "Assigned to Me and My Groups" from "My Worklist"
Then No records are displayed

@automated
@Regression
@4.1
@TC_C60682197
Scenario: Procedure search by name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Procedure" dropdown on Advanced Search Page
And I search for "CT Abdomen Pelvis w+w/o Contrast" in dropdown on Advanced Search Page
And  I click "OK" button on Advanced Search Page dropdown
Then "CT Abdomen Pelvis w+w/o Contrast" is visible in text box

@automated
@Regression
@4.1
@TC_C60682198
Scenario: Procedure code search by code
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Procedure" dropdown on Advanced Search Page
And I search for "CT100" in dropdown on Advanced Search Page
And  I click "OK" button on Advanced Search Page dropdown
Then "A_Test_Procedure_Name" is visible in text box

@automated
@Regression
@4.1
@TC_C60682199
Scenario: Procedure second page code search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I expand the "Procedure" section in Advanced Search
And I click on "Procedure" dropdown on Advanced Search Page
And I click "Next Page" button on Advanced Search dropdown
And I search for "CT100" in dropdown on Advanced Search Page
And  I click "OK" button on Advanced Search Page dropdown
Then "A_Test_Procedure_Name" is visible in text box

@automated
@Regression
@4.1
@TC_C60682200
Scenario: Procedure click "Reset" button after search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Procedure" dropdown on Advanced Search Page
And I search for "CT100" in dropdown on Advanced Search Page
And I click "Reset" button on Advanced Search Page dropdown

@automated
@Regression
@4.1
@TC_C60682201
Scenario: Procedure Paging
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Procedure" dropdown on Advanced Search Page
And I search for "CT" in dropdown on Advanced Search Page
And I click "Next Page" button on Advanced Search dropdown
Then I verify "CT100: A_Test_Procedure_Name" is visible in first row of dropdown
When I click "Previous Page" button on Advanced Search dropdown
Then I verify "CT100: A_Test_Procedure_Name" is visible in first row of dropdown
When I click "Last Page" button on Advanced Search dropdown
Then I verify "CT100: A_Test_Procedure_Name" is visible in first row of dropdown
When I click "Next Page" button on Advanced Search dropdown
Then I verify "CT100: A_Test_Procedure_Name" is visible in first row of dropdown
When  I click "OK" button on Advanced Search Page dropdown
Then "A_Test_Procedure_Name" is visible in text box

@automated
@Regression
@4.1
@TC_C60682196
Scenario: Site Procedure search by code
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Site Procedure" dropdown on Advanced Search Page
And I search for "test101" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
Then "Test_Procedure_Name" is visible in text box

@automated
@Regression
@4.1
@TC_C59950503
Scenario: Site Procedure second page code search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Site Procedure" dropdown on Advanced Search Page
And I click "Next Page" button on Advanced Search dropdown
And I search for "test101" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
Then "Test_Procedure_Name" is visible in text box

@automated
@Regression
@4.1
@TC_C59950502
Scenario: Site Procedure search by name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Site Procedure" dropdown on Advanced Search Page
And I search for "CT Abdomen Pelvis w+w/o Contrast" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
Then "CT Abdomen Pelvis w+w/o Contrast" is visible in text box

@automated
@Regression
@4.1
@TC_C59960809
Scenario: Site Procedure verify page button funcationality
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Site Procedure" dropdown on Advanced Search Page
Then I search for "test101" in dropdown on Advanced Search Page
When I click "Next Page" button on Advanced Search dropdown
Then I verify "test101: Test_Procedure_Name" is visible in first row of dropdown
When I click "Previous Page" button on Advanced Search dropdown
Then I verify "test101: Test_Procedure_Name" is visible in first row of dropdown
When I click "Last Page" button on Advanced Search dropdown
Then I verify "test101: Test_Procedure_Name" is visible in first row of dropdown
When I click "Next Page" button on Advanced Search dropdown
Then I verify "test101: Test_Procedure_Name" is visible in first row of dropdown
When I click "OK" button on Advanced Search Page dropdown
Then "Test_Procedure_Name" is visible in text box

@automated
@Regression
@4.1
@TC_C59960808
Scenario: Site Procedure click "Reset" button after search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I expand the "Procedure" section in Advanced Search
And I click on "Site Procedure" dropdown on Advanced Search Page
And I search for "CT" in dropdown on Advanced Search Page
And I click "Reset" button on Advanced Search Page dropdown
Then "" is visible in text box

@automated
@Regression
@4.1
@TC_C60682202
Scenario: Enterprise Site search by name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Site" dropdown on Advanced Search Page
And I search for "Test Crm" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
Then "Test Crm" is visible in text box

@automated
@Regression
@4.1
@TC_C60682204
Scenario: Enterprise Site Search second page search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Site" dropdown on Advanced Search Page
And I click "Next Page" button on Advanced Search dropdown
And I search for "Test Crm" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
Then "Test Crm" is visible in text box

@automated
@Regression
@4.1
@TC_C60682205
Scenario: Enterprise Site click "Reset" button after search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Site" dropdown on Advanced Search Page
And I search for "Test Crm" in dropdown on Advanced Search Page
And I click "Reset" button on Advanced Search Page dropdown

@automated
@Regression
@4.1
@TC_C60682206
Scenario: Enterprise Site verify page button funcationality
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Site" dropdown on Advanced Search Page
Then I search for "Test" in dropdown on Advanced Search Page
When I click "Next Page" button on Advanced Search dropdown
Then I verify "Name Test1" is visible in first row of dropdown
When I click "Previous Page" button on Advanced Search dropdown
Then I verify "Name Test1" is visible in first row of dropdown
When I click "Last Page" button on Advanced Search dropdown
Then I verify "Name Test1" is visible in first row of dropdown
When I click "Next Page" button on Advanced Search dropdown
Then I verify "Name Test1" is visible in first row of dropdown
When I click "OK" button on Advanced Search Page dropdown
Then "Name Test1" is visible in text box

@automated
@Regression
@4.1
@TC_C60682207
Scenario: Location search by name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Location" dropdown on Advanced Search Page
And I search for "Test_1" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
Then "Test_1" is visible in text box

@automated
@Regression
@4.1
@TC_C60682209
Scenario: Location Search second page search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Location" dropdown on Advanced Search Page
And I click "Next Page" button on Advanced Search dropdown
And I search for "Test_10" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
Then "Test_10" is visible in text box

@automated
@Regression
@4.1
@TC_C60682210
Scenario: Location click "Reset" button after search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Location" dropdown on Advanced Search Page
And I search for "Test" in dropdown on Advanced Search Page
Then I click "Reset" button on Advanced Search Page dropdown

@automated
@Regression
@4.1
@TC_C60682211
Scenario: Location verify page button funcationality
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I click on "Location" dropdown on Advanced Search Page
Then I search for "Test" in dropdown on Advanced Search Page
When I click "Next Page" button on Advanced Search dropdown
Then I verify "Test_1" is visible in first row of dropdown
When I click "Previous Page" button on Advanced Search dropdown
Then I verify "Test_1" is visible in first row of dropdown
When I click "Last Page" button on Advanced Search dropdown
Then I verify "Test_1" is visible in first row of dropdown
When I click "First Page" button on Advanced Search dropdown
Then I verify "Test_1" is visible in first row of dropdown
When I click "OK" button on Advanced Search Page dropdown
Then "Test_1" is visible in text box

@automated
@Regression
@4.1
@TC_C61726133
Scenario: Exam - Search without date fields error message verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61726134
Scenario: Exam - Search without date fields after successful search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61726135
Scenario: Exam - Search without date fields after empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61726124
Scenario: Exam - Test Unlimited Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
And I enter the details for Exam Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
| Priority       | Stat                     | Exam       |
| Modality       | CT - Computed Tomography | Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61726125
Scenario: Exam - Test Unlimited Count
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
And I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
When I enter the details for Exam Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
| Priority       | Stat                     | Exam       |
| Modality       | CT - Computed Tomography | Exam       |
And I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61726126
Scenario: Exam - Execute Count without date fields
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
And I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
And I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61726127
Scenario: Exam - Count without date fields after successful search 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
And I click on "Clear" action
And I click on "Count" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61726131
Scenario: Verify Unlimited Search & Unlimited Count are equal
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
When I enter the details for Exam Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
| Priority       | Stat                     | Exam       |
| Modality       | CT - Computed Tomography | Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page
When I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page
Then I compare the results of Search and Count

@automated
@Regression
@4.1
@TC_C61726128
Scenario: Exam-Count without date fields after date limitation error is displayed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Exam"
And I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
When I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C60207231
Scenario: Communication - Search without date fields error message verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
Then I click on "Clear" action
When I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C60207232
Scenario: Communication - Search without date fields after successful search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
And I select Advanced Search module "Communication"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Communication" Advanced Search page 
And I click on "Search" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C60207233
Scenario: Communication - Search without date fields after empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
And I click on "Clear" action
And I click on "Search" action
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C59740379
Scenario: Communication - Test Unlimited Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
And I enter the details for Communication Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C59740380
Scenario: Communication - Test Unlimited Count
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
And I enter the details for Communication Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C59740534
Scenario: Communication - Execute Count without date fields
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
Then I click on "Clear" action
When I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C59740535
Scenario: Communication - Count without date fields after successful search 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
Then I click on "Clear" action
When I select "1y" as date range in Exam Date on the "Communication" Advanced Search page 
Then I click on "Count" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C59908345
Scenario: Communication - Verify Unlimited Search & Unlimited Count are equal
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Communication"
And I enter the details for Communication Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
And Search Results are displayed on WorkList Page
And I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page
Then I compare the results of Search and Count

@automated
@Regression
@4.1
@TC_C59740536
Scenario: Communication - Count without date fields after date limitation error is displayed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Communication"
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750225
Scenario: Peer Review- Search without date fields error message verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750226
Scenario: Peer Review - Search without date fields after successful search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I expand the "Exam Date" section in Advanced Search
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Peer Review" Advanced Search page
And I click on "Search" action
And I click on "Clear" action 
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750227
Scenario: Peer Review - Search without date fields after empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750216
Scenario: Peer Review - Test Unlimited Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I enter the details for Peer Review Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61750217
Scenario: Peer Review - Test Unlimited Count
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I enter the details for Peer Review Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61750218
Scenario: Peer Review - Execute Count without date fields
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750219
Scenario: Peer Review - Count without date fields after successful search 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I expand the "Exam Date" section in Advanced Search
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Peer Review" Advanced Search page 
And I click on "Count" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750220
Scenario: Peer Review - Count without date fields after date limitation error is displayed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Peer Review"
And I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C59908323
Scenario: Peer Review - Verify Unlimited Search & Unlimited Count are equal
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Peer Review"
And I enter the details for Peer Review Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page
When I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page
And I compare the results of Search and Count

@automated
@Regression
@4.1
@TC_C61750210
Scenario: Tech QA - Search without date fields error message verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Tech QA"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750211
Scenario: Tech QA - Search without date fields after successful search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Tech QA"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Tech QA" Advanced Search page 
And I click on "Search" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750212
Scenario: Tech QA - Search without date fields after empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Tech QA"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750201
Scenario: Tech QA - Test Unlimited Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Tech QA"
And I enter the details for Tech QA Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61750202
Scenario: Tech QA - Test Unlimited Count
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Tech QA"
And I enter the details for Tech QA Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61750203
Scenario: Tech QA - Execute Count without date fields
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Tech QA"
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750204
Scenario: Tech QA - Count without date fields after successful search 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Tech QA"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Tech QA" Advanced Search page 
And I click on "Count" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750205
Scenario: Tech QA - Count without date fields after date limitation error is displayed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Tech QA"
And I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750208
Scenario: Tech QA - Verify Unlimited Search & Unlimited Count are equal
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Tech QA"
And I enter the details for Tech QA Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page
When I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page
Then I compare the results of Search and Count

@automated
@Regression
@4.1
@TC_C61750255
Scenario: Teaching Note - Search without date fields error message verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Teaching Note"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750256
Scenario: Teaching Note - Search without date fields after successful search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Teaching Note"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Teaching Note" Advanced Search page 
And I click on "Search" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750257
Scenario: Teaching Note - Search without date fields after empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
And I click on "Clear" action
When I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750246
Scenario: Teaching Note - Test Unlimited Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note"
And I enter the details for Teaching Note Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61750247
Scenario: Teaching Note - Test Unlimited Count
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note"
And I enter the details for Teaching Note Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C6175024
Scenario: Teaching Note - Execute Count without date fields
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note"
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750250
Scenario: Teaching Note - Count without date fields after successful search 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Teaching Note" Advanced Search page 
And I click on "Count" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750249
Scenario: Teaching Note - Count without date fields after date limitation error is displayed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note"
And I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750253
Scenario: Teaching Note - Verify Unlimited Search & Unlimited Count are equal
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note"
And I enter the details for Teaching Note Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page
When I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page
Then I compare the results of Search and Count

@automated
@Regression
@4.1
@TC_C61750240
Scenario: Teaching Note (anonymized) - Search without date fields error message verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750241
Scenario: Teaching Note (anonymized) - Search without date fields after successful search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Teaching Note (anonymized)" Advanced Search page 
And I click on "Search" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750242
Scenario: Teaching Note (anonymized) - Search without date fields after empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
And I click on "Search" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750231
Scenario: Teaching Note (anonymized) - Test Unlimited Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I enter the details for Teaching Note Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
When I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61750232
Scenario: Teaching Note (anonymized) - Test Unlimited Count
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I enter the details for Teaching Note Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page

@automated
@Regression
@4.1
@TC_C61750233
Scenario: Teaching Note (anonymized) - Execute Count without date fields
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
When I select Advanced Search module "Teaching Note (anonymized)"
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750234
Scenario: Teaching Note (anonymized) - Count without date fields after successful search 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I click on "Clear" action
And I select "1y" as date range in Exam Date on the "Teaching Note (anonymized)" Advanced Search page 
And I click on "Count" action
Then Search Results are displayed on WorkList Page
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750235
Scenario: Teaching Note (anonymized) - Count without date fields after date limitation error is displayed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed
When I click on "Clear" action
And I click on "Count" action
Then Error Message "Your search requires a date range. Your search has been limited to 3 days" is displayed

@automated
@Regression
@4.1
@TC_C61750238
Scenario: Teaching Note (anonymized) - Verify Unlimited Search & Unlimited Count are equal
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click Advanced Search
And I select Advanced Search module "Teaching Note (anonymized)"
And I enter the details for Teaching Note Advanced Search
| Label          | Value          					| Enclosure  |
| Status         | Unread         					| Exam       |
And I click on "Unlimited Search" action
Then Search Results are displayed on WorkList Page
When I click on "Unlimited Count" action
Then Search Results Count are displayed on WorkList Page
Then I compare the results of Search and Count


@Regression
@RadPartner
@automated
@TC_C65066658
Scenario: Check Worklist sort properties by first sort order
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value          |
| Status         | Unread         |
| Modality       | ES - Endoscopy |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Special Test Worklist" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue
And I right click on "Special Test Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I change properties 
| Label            | Value        |
| First sort order | Patient Name |
And I click on "Change" button from Properties dialogue
And I click "Special Test Worklist" from "InteleViewer Worklists"
And I close the Advanced Search if it is loaded
Then exams are sorted by first sort order


@Regression
@RadPartner
@automated
@TC_C65201362
Scenario: Check Worklist sort properties by second sort order
#Pre-requisite: Use Worklist created in @TC_C65066658
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I right click on "Special Test Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I change properties 
| Label            | Value  |
|First sort order  | Status |
|Second sort order | Site   |
And I click on "Change" button from Properties dialogue
And I click "Special Test Worklist" from "InteleViewer Worklists"
Then exams are sorted by second sort order


@Regression
@RadPartner
@automated
@TC_C65201363
Scenario: Check Worklist sort properties by third sort order
#Pre-requisite: Use Worklist created in @TC_C65066658
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I right click on "Special Test Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I change properties 
| Label            | Value    |
|First sort order  | Status   |
|Second sort order | Modality |
|Third sort order  | MRN      |
And I click on "Change" button from Properties dialogue
And I click "Special Test Worklist" from "InteleViewer Worklists"
Then exams are sorted by third sort order


@Regression
@RadPartner
@automated
@TC_C65480563
Scenario: Check Worklist active functionality by threshold value
#Pre-requisite: Use Worklist created in @TC_C65066658
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I right click on "Special Test Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I set threshold values as below
|Label | Value |
|Lower |3      |
|Upper |4      |
And I click on "Change" button from Properties dialogue
Then "Special Test Worklist" worklist is "de-activated"
And "Special Test Worklist" worklist counter is "de-activated" with exam count as "3"
When I right click on "Special Test Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I change properties 
| Label                    | Value        |
| Refresh rate in seconds  | 10           |
And I set threshold values as below
|Label | Value |
|Lower |1      |
|Upper |3      |
And I click on "Change" button from Properties dialogue
Then "Special Test Worklist" worklist is "activated"
And "Special Test Worklist" worklist counter is "activated" with exam count as "3"
When I right click on "Special Test Worklist" from "InteleViewer Worklists"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Special Test Worklist" worklist
Then "Special Test Worklist" worklist is deleted from "InteleViewer Worklists"

@Regression
@RadPartner
@automated
@TC_C65480562
Scenario: Worklist active functionality by Active Time
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value          |
| Status         | Unread         |
| Modality       | ES - Endoscopy |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Time Based Worklist" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue
And I close the Advanced Search if it is loaded
And I right click on "Time Based Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I click on "Active Time" tab on Properties dialog
And I uncheck 24x7 option
And I select "Thursday" as day 
And I set Active Time of day
And I click Add new active time icon on Properties dialogue
And I click on "Change" button from Properties dialogue
Then "Time Based Worklist" worklist is "de-activated"
And I right click on "Time Based Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I click on "Active Times" tab on Properties dialog
And I delete added Active Time for "Thursday"
And I click on "Change" button from Properties dialogue
Then "Time Based Worklist" worklist is "activated"

@Regression
@RadPartner
@automated
@TC_C66138722
Scenario: Worklist count active/de-active functionality
#Pre-requisite: Use Worklist created in @TC_C65480562
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Management" application
And I open "Configuration" module
And I click "Worklist Tuning" from "Worklist" Configuration Page
And I search for "Time Based Worklist" worklist in Worklist panel
And I select the "Time Based Worklist" worklist checkbox to enable count
And I click on "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Time Based Worklist" from "InteleViewer Worklists"
Then I verify the worklist "Time Based Worklist" tree count is enabled with exam count 3
When I switch to "Management" tab
And I click "Worklist Tuning" from "Worklist" Configuration Page
And I search for "Time Based Worklist" worklist in Worklist panel
And I de-select the "Time Based Worklist" worklist checkbox to disable count
And I switch to "Worklist" tab
And I click "Time Based Worklist" from "InteleViewer Worklists"
Then I verify the worklist "Time Based Worklist" tree count is disabled


@automated
@Regression
@4.1
@TC_C68190391
Scenario Outline: Verify <value> - auto next button functionality after worklist sort order for Single Worklist
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
And I right click "My Reading Queue"
And I click Queue Menu Option "Edit"
And I check "Intelerad - All Active" worklist checkBox under "Worklist Count Testing" worklist from Queue dialog
And I click on "Create" button from Properties dialogue
And I right click "My Reading Queue"
And I click Queue Menu Option "Properties"
Then I verify properties dialogue is open
When I change properties <label>, <value>
And I clear "Second sort order"
And I click on "Change" button from Properties dialogue
And I click on "Start Auto-next" button on Worklist tab
Then "SSN:" is visible on auto next page
Examples:
| label                    	 | value        					|
| "First sort order"         | "Priority"	 						|
| "First sort order"         | "AI Status"			  		|
| "First sort order"         | "Accession"						|
| "First sort order"         | "Acquisition Time"			|
| "First sort order"         | "Assigned"							|
| "First sort order"         | "Body Part"						|
| "First sort order"         | "Combined"							|
| "First sort order"         | "Exam Time"						|
| "First sort order"         | "Last Modified"				|
| "First sort order"         | "Location"							|
| "First sort order"         | "MRN"									|
| "First sort order"         | "Modality"							|
| "First sort order"         | "Patient Age"					|
| "First sort order"         | "Patient Name"					|
| "First sort order"         | "Radiologist"					|
| "First sort order"         | "Signed Time"					|
| "First sort order"         | "Site"									|
| "First sort order"         | "Site Accession"				|
| "First sort order"         | "Site Procedure"				|
| "First sort order"         | "Site Procedure Code"	|
| "First sort order"         | "Status"								|
| "First sort order"         | "Time Remaining"				|	
| "First sort order"         | "Unread Time"					|

@automated
@Regression
@4.1
@TC_C65056073
Scenario Outline: Verify <value> - auto next button functionality after worklist sort order for Combined Worklist
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I right click "My Reading Queue"
And I click Queue Menu Option "Edit"
And I uncheck "Unread" worklist checkBox under "Worklist Count Testing" worklist from Queue dialog
And I check "Assigned to Me and My Groups" worklist checkBox under "Worklist Count Testing" worklist from Queue dialog
And I click on "Create" button from Properties dialogue
And I right click "My Reading Queue"
And I click Queue Menu Option "Properties"
Then I verify properties dialogue is open
When I change properties <label>, <value>
And I clear "Second sort order"
And I click on "Change" button from Properties dialogue
And I click on "Start Auto-next" button on Worklist tab
Then "SSN:" is visible on auto next page
Examples:
| label                    	 | value        					|
| "First sort order"         | "Location"							|


@automated
@Regression
@4.1
@TC_C66494917
Scenario: Create My Support Queue
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click on create "Create a Support Queue" button on Worklist tab
And I check "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I click on "Create" button on Create My Support Queue dialog
Then "Waiting Comms" worklist is added under "My Support Queue" worklist

@automated
@Regression
@4.1
@TC_C66494918
Scenario: Add 'My Support Queue' to shift
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I click on "Scheduling" application
And I open "Shift Management" module
And I click on "Demo Shift 1" shift
And I click on "My Support Queue" from worklist 
And I uncheck "Waiting Comms" from "Operations Worklists"
And I click to save worklist
And I click on "save" button to save shift
And I switch to "Worklist" tab
And I click on "Shift" button on Worklist tab
And I select the "Select Shift" radio button
And I select the ShiftType and Shift as "Radiologist" and "Demo Shift 1" respectively
And I click on Save button
Then "Waiting Comms" worklist is deleted from "My Support Queue"

@automated
@Regression
@4.1
@TC_C66494919
Scenario: Change Shift
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I click on "Scheduling" application
And I open "Shift Management" module
And I click on "Demo Shift 1" shift
And I click on "My Support Queue" from worklist 
And I uncheck "Waiting Comms" from "Operations Worklists"
And I check "Critical Issues" from "Operations Worklists"
And I click on sort worklist checkox
And I click to save worklist
And I click on "save" button to save shift
And I click on "Demo Shift 2" shift
And I click on "My Support Queue" from worklist 
And I check "Waiting Comms" from "Operations Worklists"
And I uncheck "Critical Issues" from "Operations Worklists"
And I click on sort worklist checkox
And I click to save worklist
And I click on "save" button to save shift
And I switch to "Worklist" tab
And I click on "Shift" button on Worklist tab
When I select the ShiftType and Shift as "Radiologist" and "Demo Shift 1" respectively
And I click on Save button
Then "Waiting Comms" worklist is deleted from "My Support Queue"
And "Critical Issues" worklist is added under "My Support Queue" worklist
When I click on "Shift" button on Worklist tab
And I select the ShiftType and Shift as "Radiologist" and "Demo Shift 2" respectively
And I click on Save button
Then "Critical Issues" worklist is deleted from "My Support Queue"
And "Waiting Comms" worklist is added under "My Support Queue" worklist

@automated
@Regression
@4.1
@TC_C66494921
Scenario: Remove a Worklist from 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I uncheck "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I click on "Create" button on Create My Support Queue dialog
Then "Waiting Comms" worklist is deleted from "My Support Queue"

@automated
@Regression
@4.1
@TC_C67582134
Scenario: Verify functionality of 'Collapse All' and 'Expand All' for 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I click on "Collapse All" button on Create My Support Queue dialog
Then "Critical Results" is not visible
And I click on "Expand All" button on Create My Support Queue dialog
Then "Critical Results" is visible

@automated
@Regression
@4.1
@TC_C67582135
Scenario: Verify functionality of 'Uncheck' for 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I uncheck "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I click on "Uncheck" button on Create My Support Queue dialog
Then I verify the checkbox "Waiting Comms" is not selected

@automated
@Regression
@4.1
@TC_C67582136
Scenario: Verify functionality of 'Cancel' for 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I uncheck "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I click on "Cancel" button on Create My Support Queue dialog
Then "My Support Queue" is visible

@automated
@Regression
@4.1
@TC_C67582137
Scenario: Verify functionality of 'Refresh' for 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I click on create "Create a Support Queue" button on Worklist tab
And I check "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I click on "Create" button on Create My Support Queue dialog
Then "Waiting Comms" worklist is added under "My Support Queue" worklist
When I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I check "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I click on "Uncheck" button on Create My Support Queue dialog
And I click Edit My Support Queue refresh button
Then I verify the checkbox "Waiting Comms" is selected

@automated
@Regression
@4.1
@TC_C67582138
Scenario: Verify functionality of 'My Shift' for 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I click on "Scheduling" application
And I open "Shift Management" module
And I click on "Demo Shift 1" shift
And I click on "My Support Queue" from worklist 
And I check "Waiting Comms" from "Operations Worklists"
And I check "Critical Results Worklists" from "Operations Worklists"
And I check "Critical Issues" from "Worklist Count Testing"
And I click to save worklist
And I click on "save" button to save shift
And I switch to "Worklist" tab
And I click on "Shift" button on Worklist tab
And I select the ShiftType and Shift as "Radiologist" and "Demo Shift 1" respectively
And I click on Save button
And I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I uncheck "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I uncheck "Critical Issues" worklist checkBox under "Worklist Count Testing" worklist from Queue dialog
And I check "Critical Results Worklists" worklist checkBox under "Worklist Count Testing" worklist from Queue dialog
Then I verify the checkbox "Waiting Comms" is not selected
And I verify the checkbox "Critical Issues" is not selected
And I verify the checkbox "Critical Results Worklists" is selected
When I click on "Create" button on Create My Support Queue dialog
And I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I click on "My Shift" button on Create My Support Queue dialog
Then I verify the checkbox "Waiting Comms" is selected
And I verify the checkbox "Critical Issues" is selected
And I verify the checkbox "Critical Results Worklists" is selected

@automated
@Regression
@4.1
@TC_C67582139
Scenario: Verify functionality of 'My Support' dropdown for 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I select shift "Metro Hospital" from Edit My Support Queue dialogue box
Then I verify the checkbox "Waiting Comms" is not selected
And I verify the checkbox "Critical Issues" is not selected
And I verify the checkbox "Critical Results Worklists" is not selected

@automated
@Regression
@4.1
@TC_C66494928
Scenario: Delete 'My Support Queue' and assign shift, verify new 'My Support Queue' is reassigned
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Delete"
And I click on yes option to delete "My Support Queue" worklist
Then "My Support Queue" is not visible
When I click on "Shift" button on Worklist tab
And I select the ShiftType and Shift as "Radiologist" and "Demo Shift 1" respectively
And I click on Save button
Then "My Support Queue" is visible

@automated
@Regression
@4.1
@TC_C66494930
Scenario: Verify 'My Support Queue' Properties Cancel Button works
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
Then I verify properties dialogue is open
When I click on "Cancel" button from My Support Queue Properties
Then I verify properties dialogue is closed

@automated
@Regression
@4.1
@TC_C66494931
Scenario:  My Support Queue Properties Clear Button
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
And I click on "Clear" button from My Support Queue Properties
Then I verify properties are changed 
| Label                    	 | Value        |
| Refresh rate in seconds  	 | 	60         	|
| First sort order      	   |  [blank]			|

@automated
@Regression
@4.1
@TC_C67251620
Scenario: Subscribe to My Support Queue - Cancel button verification
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Subscribe"
And I click Subscribe button "Cancel"
Then I verify properties dialogue is closed

@automated
@Regression
@4.1
@TC_C66494932
Scenario: Subscribe to My Support Queue
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Subscribe"
And I click Subscribe button "Subscribe"
Then "Worklist has been subscribed" message is displayed
And I verify properties dialogue is closed

@automated
@Regression
@4.1
@TC_C66494933
Scenario: UnSubscribe to My Support Queue
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Subscribe"
And I click Subscribe button "Subscribe"
Then "Worklist has been subscribed" message is displayed
When I right click "My Support Queue"
And I click Queue Menu Option "Unsubscribe"
Then "Worklist has been unsubscribed" message is displayed
And I verify properties dialogue is closed

@automated
@Regression
@4.1
@TC_C66589903
Scenario: Update My Support Queue Properties 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
And I change properties 
| Label                    | Value        |
| Refresh rate in seconds  | 10           |
| First sort order         | Created By  	|
And I click Status checkbox
And I click Wrap content checkbox
And I click on "Change" button from Properties dialogue
Then I verify properties dialogue is closed
When I right click "My Support Queue"
And I click Queue Menu Option "Properties"
Then I verify Status checkbox is selected
And I verify Wrap Content checkbox is selected
And I verify properties are changed
| Label                    | Value        |
| Refresh rate in seconds  | 10           |
| First sort order         | Created By  	|

@automated
@Regression
@4.1
@TC_C67146571
Scenario: Update default configuration My Support Queue Properties
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "My Support Queue" from "Worklist" Configuration Page
When I change properties on the Worklist Configuration 
| Label                    | Value        |
| Refresh rate in seconds  | 99          |
| First sort order         | MRN			  	|
And I uncheck Wrap content checkbox for My Support Queue configuration
And I uncheck Status checkbox for My Support Queue configuration
And I click "Save" button from My Support Queue configuration
And I click on "Smart Worklist" application
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
Then I verify Status checkbox is not checked
And I verify Wrap Content checkbox is not checked
And I verify properties are changed
| Label                    | Value        |
| Refresh rate in seconds  | 99           |
| First sort order         | MRN			  	|

@automated
@Regression
@4.1
@TC_C67582142
Scenario: Check Persist user settings between login sessions checkbox
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "My Support Queue" from "Worklist" Configuration Page
And I check "Allow users to change properties for themselves" checkbox from My Support Queue configuration
And I check "Persist user settings between login sessions" checkbox from My Support Queue configuration
And I click "Save" button from My Support Queue configuration
And I click header to open "Smart Worklist" module
And I click on "Smart Worklist" application
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
And I change properties 
| Label                    | Value        |
| Refresh rate in seconds  | 23           |
And I click on "Change" button from Properties dialogue
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I right click "My Support Queue"
And I click Queue Menu Option "Properties"
Then I verify properties are changed
| Label                    | Value        |
| Refresh rate in seconds  | 23           |

@automated
@Regression
@4.1
@TC_C67582143
Scenario: Uncheck Persist user settings between login sessions checkbox
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "My Support Queue" from "Worklist" Configuration Page
And I check "Allow users to change properties for themselves" checkbox from My Support Queue configuration
And I uncheck "Persist user settings between login sessions" checkbox from My Support Queue configuration
And I click "Save" button from My Support Queue configuration
And I click on "Smart Worklist" application
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
And I change properties 
| Label                    | Value        |
| Refresh rate in seconds  | 24           |
And I click Status checkbox
And I click Wrap content checkbox
And I click on "Change" button from Properties dialogue
Then I verify properties dialogue is closed
When log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
And I verify properties are changed
| Label                    | Value        |
| Refresh rate in seconds  | 99           |

@automated
@Regression
@4.1
@TC_C67582144
Scenario: Uncheck 'Allow users to change properties for themselves'
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "My Support Queue" from "Worklist" Configuration Page
And I uncheck "Allow users to change properties for themselves" checkbox from My Support Queue configuration
Then "Persist user settings between login sessions" is not visible
When I click "Save" button from My Support Queue configuration
And I click on "Smart Worklist" application
Then worklist is loaded
When I right click "My Support Queue"
And I click Queue Menu Option "Properties"
Then "My Reading Queue cannot be changed" is visible

@automated
@Regression
@4.1
@TC_C67582145
Scenario: Check 'Allow users to change properties for themselves'
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "My Support Queue" from "Worklist" Configuration Page
And I click on "My Support Queue" menu option
And I check "Allow users to change properties for themselves" checkbox from My Support Queue configuration
Then "Persist user settings between login sessions" is visible
When I click "Save" button from My Support Queue configuration
And I click on "Smart Worklist" application
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Properties"
And I click on "Change" button from Properties dialogue
Then "My Reading Queue cannot be changed" is not visible

@automated
@Regression
@4.1
@TC_C66494934
Scenario: Check 'Reset Queue' button functionality of 'My Support Queue'
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I right click "My Support Queue"
And I click Queue Menu Option "Edit"
And I uncheck "Waiting Comms" worklist checkBox under "Operations Worklists" worklist from Queue dialog
And I click on "Create" button on Create My Support Queue dialog
Then "Waiting Comms" worklist is deleted from "My Support Queue"
When I right click "My Support Queue"
And I click Queue Menu Option "Reset Queue"
Then "Waiting Comms" worklist is visible under "My Support Queue" worklist

@automated
@Regression
@4.1
@TC_C68314865
Scenario: Delete 'My Support Queue' and assign 'Set Work Hours' 
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I right click "My Support Queue"
And I click Queue Menu Option "Delete"
And I click on yes option to delete "My Support Queue" worklist
Then "My Support Queue" is not visible
When I click on "Shift" button on Worklist tab
And I select the "Set Working Hours" radio button
And I click on Save button
Then "My Support Queue" is not visible


@automated
@Smoke
@Regression
@TC_WT-002
Scenario: Verify assignment of Shift, Reading Location and Reading Room on Smart Worklist launch.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And worklist launches
Then "Select Current Shift and Location" dialog is launched
When I select the ShiftType and Shift as "Radiologist" and "IV Body" respectively
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button
Then worklist is loaded
When I click on the Select Shift button in the Worklist navigation
And I select the "Set Working Hours" radio button
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button in the Shift selection from Worklist
Then worklist is loaded
When log out of the application
And log into the application as "admin" user
And I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
Then "admin" user is logged out


@automated
@BVT
@Smoke
@TC_WT-003
Scenario: Verify - My Reading Queue creation
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And worklist launches
Then "Select Current Shift and Location" dialog is launched
When I select the ShiftType and Shift as "Radiologist" and "IV Body" respectively
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
Then following worklists are displayed under the Inteleviewer Worklists
|Unread My Subspecialty | 
|Unread Body            |
|Signing Queue          | 
|All Intelerad          | 
When I click on the Select Shift button in the Worklist navigation
And I select the "Set Working Hours" radio button
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button in the Shift selection from Worklist
Then worklist is loaded
When log out of the application
And log into the application as "admin" user
And I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
Then "admin" user is logged out


@automated
@Regression
@TC_WT-011
Scenario: Worklist - shift selection
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then "Select Current Shift" dialog is launched
When I select the ShiftType and Shift as "Radiologist" and "IV Body" respectively
And I click on Save button
Then worklist is loaded
And shift is added to My Reading queue worklist
When I click on "Scheduling" application
And I open "Shift Management" module
And I click on "IV Body" shift
And I click on "My Reading Queue" from worklist
Then My Reading queue created as per shift management
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
And I click on the Select Shift button in the Worklist navigation
When I select the "Set Working Hours" radio button
And I click on Save button in the Shift selection from Worklist
Then worklist is loaded


@Regression
@RadPartner
@automated
@TC_C65649575
Scenario: Adding a Worklist to My Reading Queue
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value          |
| Status         | Unread         |
| Modality       | ES - Endoscopy |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Reading Queue Worklist" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue
And I click on create "Create a Reading Queue" button on Worklist tab
And I select "Reading Queue Worklist" worklist under "InteleViewer Worklists" worklist from Create My Reading Queue dialog
And I click on "Create" button on Create My Reading Queue dialog
Then "Reading Queue Worklist" worklist is added in My Reading Queue worklist
When I close the Advanced Search if it is loaded
And I click on create "Create a Reading Queue" button on Worklist tab
And I de-select "Reading Queue Worklist" worklist under "InteleViewer Worklists" worklist from Create My Reading Queue dialog
And I click on "Create" button on Create My Reading Queue dialog
Then "Reading Queue Worklist" worklist is removed from My Reading Queue worklist
When I click on "Scheduling" application
And I open "Shift Management" module
And I click on "IV Body" shift
And I click on "My Reading Queue" from worklist 
And I select "Reading Queue Worklist" from "InteleViewer Worklists" 
And I click to save worklist
And I click on "save" button to save shift
And I switch to "Worklist" tab
And I click on "Shift" button on Worklist tab
And I select the "Select Shift" radio button
And I select the ShiftType and Shift as "Radiologist" and "IV Body" respectively
And I click on Save button
Then "Reading Queue Worklist" worklist is added in My Reading Queue worklist
When log out of the application
And log into the application as "radiologist" user
And I open "Smart Worklist" application
And worklist launches
And I click on "Shift" button on Worklist tab
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded


@Regression
@RadPartner
@automated
@TC_C66097637
Scenario: Deleting a Worklist associated with a shift
#Pre-requisite: Use Worklist created in @TC_C65649575
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
When I expand "InteleViewer Worklists" worklist node
And I right click on "Reading Queue Worklist" from "InteleViewer Worklists"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Reading Queue Worklist" worklist
And I refresh the page
And I select the "Set Working Hours" radio button
And I click on Save button
Then "Reading Queue Worklist" worklist is deleted from "InteleViewer Worklists"


@automated
@Regression
@RadPartner
@TC_C66195298
Scenario: Stack based Worklist activation/de-activation functionality by Active Time
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I check if Advanced Search is open
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value          |
| Status         | Unread         |
| Modality       | ES - Endoscopy |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Combined Worklist1" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue
And I click on "Clear" action
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label          | Value                     |
| Status         | Unread                    |
| Modality       | CR - Computed Radiography |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page
And I click on "Search" to search results
And I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Combined Worklist2" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue
And I expand "InteleViewer Worklists" worklist node
And I drag drop "Combined Worklist2" on "Combined Worklist1"
And I set "Combined Demo Worklist" as worklist name
And I click on "Combine" button of "Combine Worklist" dialogue
Then "Combined Demo Worklist" worklist is added under "InteleViewer Worklists"
When I right click on "Combined Demo Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I click on "Active Time" tab on Properties dialog
And I uncheck 24x7 option
And I select "Thursday" as day 
And I set Active Time of day
And I click Add new active time icon on Properties dialogue
And I click on "Change" button from Properties dialogue
Then "Combined Demo Worklist" worklist is "de-activated"
When I right click on "Combined Demo Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I click on "Active Times" tab on Properties dialog
And I delete added Active Time for "Thursday"
And I click on "Change" button from Properties dialogue
Then "Combined Demo Worklist" worklist is "activated"


@automated
@Regression
@RadPartner
@TC_C66195299
Scenario: Combined Worklist count active/de-active functionality
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "Worklist Tuning" from "Worklist" Configuration Page
And I search for "Combined Demo Worklist" worklist in Worklist panel
And I select the "Combined Demo Worklist" worklist checkbox to enable count
And I click on "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "Combined Demo Worklist" from "InteleViewer Worklists"
Then I verify the worklist "Combined Demo Worklist" tree count is enabled with exam count 23
When I switch to "Management" tab
And I click "Worklist Tuning" from "Worklist" Configuration Page
And I search for "Combined Demo Worklist" worklist in Worklist panel
And I de-select the "Combined Demo Worklist" worklist checkbox to disable count
And I switch to "Worklist" tab
And I click "Combined Demo Worklist" from "InteleViewer Worklists"
Then I verify the worklist "Combined Demo Worklist" tree count is disabled


@automated
@Regression
@RadPartner
@TC_C69554746
Scenario: Stack based Worklist under MRQ activation/de-activation functionality by Active Time
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I right click on "Combined Demo Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I click on "Active Time" tab on Properties dialog
And I uncheck 24x7 option
And I select "Thursday" as day 
And I set Active Time of day
And I click Add new active time icon on Properties dialogue
And I click on "Change" button from Properties dialogue
Then "Combined Demo Worklist" worklist is "de-activated"
When I click on create "Create a Reading Queue" button on Worklist tab
And I select "Combined Demo Worklist" worklist under "InteleViewer Worklists" worklist from Create My Reading Queue dialog
And I click on "Create" button on Create My Reading Queue dialog
Then "Combined Demo Worklist" worklist is added in My Reading Queue worklist
And "Combined Demo Worklist" under My Reading Queue is "de-activated"
When I right click on "Combined Demo Worklist" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
And I click on "Active Times" tab on Properties dialog
And I delete added Active Time for "Thursday"
And I click on "Change" button from Properties dialogue
Then "Combined Demo Worklist" worklist is "activated"
And "Combined Demo Worklist" under My Reading Queue is "activated"
When I click on create "Create a Reading Queue" button on Worklist tab
And I de-select "Combined Demo Worklist" worklist under "InteleViewer Worklists" worklist from Create My Reading Queue dialog
And I click on "Create" button on Create My Reading Queue dialog
Then "Combined Demo Worklist" worklist is removed from My Reading Queue worklist
When I click on "Scheduling" application
And I open "Shift Management" module
And I click on "IV Body" shift
And I click on "My Reading Queue" from worklist 
And I select "Combined Demo Worklist" from "InteleViewer Worklists" 
And I click to save worklist
And I click on "save" button to save shift
And I switch to "Worklist" tab
And I click on "Shift" button on Worklist tab
And I select the "Select Shift" radio button
And I select the ShiftType and Shift as "Radiologist" and "IV Body" respectively
And I click on Save button
Then "Combined Demo Worklist" worklist is added in My Reading Queue worklist
When log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And worklist launches
And I click on "Shift" button on Worklist tab
And I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded

@automated
@Regression
@TC_C70618202
Scenario: Assign/Unassign exam through a single user
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I hover on exam "354697231" with "Unread" Status
And I click on icon "Lock Exam to Me"
Then Assigned icon is displayed for exam "354697231"
When I hover on Assigned icon for exam "354697231" 
Then Assigned icon tooltip with "Locked to: " value "Test123, test123" is displayed
When log out of the application
And log into the application as "other" user
And I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I close the Advanced Search if it is loaded
And I perform Quick Search 
| Label     | Value     |
| accession | 354697231 |
And I click on Search button in Quick Search
Then exam is displayed "354697231"
When I click on exam "354697231"
Then exam is open "DAN, SIMMONDS"
When I mouse hover on "CT BRAIN/HEAD W/O CONTRAST ----->  [ NOT FOR US ; CE ]" procedure in exam panel
And I click on "Open in Dictation" icon
Then "Unable to Start Dictation" dialog is visible
When I click "Cancel" on "Unable to Start Dictation" dialog
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I hover on exam "354697231" with "Unread" Status
And I click on icon "Unassign Exam"
And I hover on exam "354697231" with "Unread" Status
Then Assigned icon is not displayed for exam "354697231"
And Assigned icon tooltip with "Locked to: " value "Test123, test123" is not displayed 
When log out of the application
And log into the application as "other" user
And I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I close the Advanced Search if it is loaded
And I perform Quick Search 
| Label     | Value     |
| accession | 354697231 |
And I click on Search button in Quick Search
Then exam is displayed "354697231"
When I click on exam "354697231"
Then exam is open "DAN, SIMMONDS"
When I mouse hover on "CT BRAIN/HEAD W/O CONTRAST ----->  [ NOT FOR US ; CE ]" procedure in exam panel
And I click on "Open in Dictation" icon
Then "Unable to Start Dictation" dialog is not visible
And Dictation is enabled
When I expand patient view panel
And I click on "Cancel Report" icon of report
And I collapse patient view panel
And log out of the application
Then "admin" user is logged out


@automated
@Regression
@TC_C71910099
Scenario: Assign/Unassign exam through two different users
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I hover on exam "354697231" with "Unread" Status
And I click on icon "Lock Exam to Me"
Then Assigned icon is displayed for exam "354697231"
When I hover on Assigned icon for exam "354697231" 
Then Assigned icon tooltip with "Locked to: " value "Test123, test123" is displayed
When log out of the application
And log into the application as "other" user
And I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I close the Advanced Search if it is loaded
And I perform Quick Search 
| Label     | Value     |
| accession | 354697231 |
And I click on Search button in Quick Search
Then exam is displayed "354697231"
When I click on exam "354697231"
Then exam is open "DAN, SIMMONDS"
When I mouse hover on "CT BRAIN/HEAD W/O CONTRAST ----->  [ NOT FOR US ; CE ]" procedure in exam panel
And I click on "Open in Dictation" icon
Then "Unable to Start Dictation" dialog is visible
When I click "OK" on "Unable to Start Dictation" dialog
Then Dictation is enabled
When I expand patient view panel
And I click on "Cancel Report" icon of report
And I collapse patient view panel
And I click "All Intelerad" from "My Worklist"
And I close the Advanced Search if it is loaded
And I perform Quick Search 
| Label     | Value     |
| accession | 354697231 |
And I click on Search button in Quick Search
Then exam is displayed "354697231"
When I hover on exam "354697231" with "Unread" Status
And I click on icon "Unassign Exam"
And I hover on exam "354697231" with "Unread" Status
Then Assigned icon is not displayed for exam "354697231"
And Assigned icon tooltip with "Locked to: " value "Test123, test123" is not displayed 
When log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I hover on exam "354697231" with "Unread" Status
Then Assigned icon is not displayed for exam "354697231"
And Assigned icon tooltip with "Locked by: " value "Tech, Citius" is not displayed
When log out of the application
Then "admin" user is logged out


@Sikuli
@automated
@Regression
@TC_C69915290
Scenario: Double Click to Read Exam from History panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
And I click on exam "10430"
And I click on "Tech QA Note" note icon
And I create "Tech QA Note" with following details
|Note Type           | Message         |
|Documentation error |Test Tech QA Note|
And I click on "Submit" button to add note
And I click "All Intelerad" from "My Worklist"
And I double-click on "10759" exam from history panel
Then exam is open "HAN, JANE"
And Dictation is enabled
When I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click "All Intelerad" from "My Worklist"
Then worklist is loaded


@Sikuli
@automated
@Regression
@TC_C71369559
Scenario: Report cancelling workflow - Read exam using the History panel record
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I expand History panel 
And I double-click on "10759" exam from history panel
Then exam is open "HAN, JANE"
And Dictation is enabled
When I expand patient view panel
And I click on "Cancel Report" icon of report
And I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on exam from history panel "10759"
And I collapse patient view panel
And I collapse History panel 
And I click "All Intelerad" from "My Worklist"
Then worklist is loaded

@Sikuli
@automated
@Regression
@TC_C71369560
Scenario: Report Save workflow - Read exam using the History panel record
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I expand History panel 
And I double-click on "10759" exam from history panel
Then exam is open "HAN, JANE"
And Dictation is enabled
When I expand patient view panel
And I click on "Save Report" icon of report
And I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on exam from history panel "10759"
And I collapse patient view panel
And I collapse History panel 
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
Then Exam "10759" with Status "Draft" is displayed
When I right click on exam "10759"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
And I hover on exam "10759" with "Unread" Status
And I click on icon "Unassign Exam"
Then Exam "10759" with Status "Unread" is displayed

@Sikuli
@automated
@Regression
@TC_C71369561
Scenario: Report Signing Workflow - Read exam using the History panel record
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I expand History panel 
And I double-click on "10759" exam from history panel
Then exam is open "HAN, JANE"
And Dictation is enabled
When I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Sign Report" option of Select Report Action dialog
And I click "Ignore and Sign" option of Sign Report dialog
And I collapse History panel
And I click Advanced Search
And I click on "Clear" action
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label    | Value           |
| Status   | Signed          |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" to search results
Then Exam "10759" with Status "Signed" is displayed 
When I right click on exam "10759"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
And I hover on exam "10759" with "Unread" Status
And I click on icon "Unassign Exam"
And I click "All Intelerad" from "My Worklist"
Then Exam "10759" with Status "Unread" is displayed

@Sikuli
@automated
@Regression
@TC_C71369562
Scenario: Discard Report workflow - Read exam using the History panel record
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I expand History panel 
And I double-click on "10759" exam from history panel
Then exam is open "HAN, JANE"
And Dictation is enabled
When I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click on exam from history panel "10759"
And I collapse patient view panel
And I collapse History panel 
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
Then Exam "10759" with Status "Unread" is displayed



@Sikuli
@Regression
@automated
@TC_C69915293
Scenario:  Launching Viewer (Viewer icon) for an exam based on Site configuration
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click on "Site" menu option
And I select site "Clinic C"
And I select the following "Viewer" for site
|InteleViewer Bi-Directional |
And I select "Default Viewer" for the site as "InteleViewer Bi-Directional"
And I click on "Save" button of site properties
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I perform worklist search "COOK, ISABELLE"
And I click "Open in Viewer" icon for the exam
And I close Document View window in InteleViewer if visible
Then "COOK, ISABELLE" details are displayed in InteleViewer
When I exit from InteleViewer
And log out of the application
Then "admin" user is logged out


@Sikuli
@Regression
@automated
@TC_C72328564
Scenario:  Launching Viewer (Context menu) for an exam based on Site configuration
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click on "Site" menu option
And I select site "Clinic C"
And I select the following "Viewer" for site
|InteleViewer Bi-Directional |
And I select "Default Viewer" for the site as "InteleViewer Bi-Directional"
And I click on "Save" button of site properties
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I perform worklist search "COOK, ISABELLE"
And I right click on exam "10118"
And I select "Open in Viewer" from the right click option
And I close Document View window in InteleViewer if visible
Then "COOK, ISABELLE" details are displayed in InteleViewer
When I exit from InteleViewer
And log out of the application
Then "admin" user is logged out

@Sikuli
@automated
@Regression
@TC_C48774339
Scenario: Discard Report workflow - Double click exam from Worklist
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
And I double-click on "10430" exam
Then exam is open "HAN, JANE"
And Dictation is enabled
When I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Discard Report" option of Select Report Action dialog
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
Then Exam "10759" with Status "Unread" is displayed


@Sikuli
@automated
@Regression
@TC_C71369571
Scenario: Report cancelling workflow - Double click exam from Worklist
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
And I double-click on "10430" exam
Then exam is open "HAN, JANE"
And Dictation is enabled
When I expand patient view panel
And I click on "Cancel Report" icon of report
And I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on exam "10430"
And I collapse patient view panel
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
Then Exam "10759" with Status "Unread" is displayed

@Sikuli
@automated
@Regression
@TC_C71369572
Scenario: Save Report workflow - Double click exam from Worklist
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
And I double-click on "10430" exam
Then exam is open "HAN, JANE"
When I expand patient view panel
And I click on "Save Report" icon of report
And I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on exam "10430"
And I collapse patient view panel
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
Then Exam "25889" with Status "Draft" is displayed
When I right click on exam "25889"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
And I hover on exam "25889" with "Unread" Status
And I click on icon "Unassign Exam"
Then Exam "25889" with Status "Unread" is displayed


@Sikuli
@automated
@Regression
@TC_C71369573
Scenario: Sign Report workflow - Double Click Exam from Worklist
#Pre-requisite: @TC_C69915290 should execute to add exam in history panel
Given Clario application is launched
And log into the application as "radiologist" user
When I click on user dropdown in the page header
And I select "Profile Management" option from user dropdown menu
And I select "Enter Patient View" values as 
|Launch Viewer   |
|Launch Dictation|
And I click on "Save" button on Profile Management window
And I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "HAN, JANE"
And I double-click on "10430" exam
Then exam is open "HAN, JANE"
And Dictation is enabled
When I close Document View window in InteleViewer if visible
Then "HAN, JANE" details are displayed in InteleViewer
When I exit from InteleViewer
And I click on "Sign Report" option of Select Report Action dialog
And I click "Ignore and Sign" option of Sign Report dialog
And I click "All Intelerad" from "My Worklist"
And I click Advanced Search
And I click on "Clear" action
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
| Label    | Value           |
| Status   | Signed          |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" to search results
Then Exam "25889" with Status "Signed" is displayed 
When I right click on exam "25889"
And I select "Change Status" from the right click option
And I set Exam Status as "Unread"
And I click "OK" button to save changed Status
And I hover on exam "25889" with "Unread" Status
And I click on icon "Unassign Exam"
And I click "All Intelerad" from "My Worklist"
Then Exam "25889" with Status "Unread" is displayed


@automated
@Regression
@TC_C70918798
Scenario: Single Click exam in Folder to enter Patient View
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click on "Folder" tab in Worklist page
And I click on "Create" icon under folder tab
And I set "ExamFolder" as new folder name
And I click on "Save" button on Create Folder dialog
Then "ExamFolder" folder is created
When I click on "Worklist" tab in Worklist page
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "COOK, ISABELLE"
And I right click on exam "10982"
And I select "Add to Folder" from the right click option
And I set "Exam added in folder" as Title to assign exam to folder 
And I set "Message in folder" as Note Message to assign exam to folder 
And I check "ExamFolder" checkbox to assign exam to folder 
And I click on "Add" button on Assign Exam to Folder dialog
And I click on "Folder" tab in Worklist page
And I click on "ExamFolder" folder
And I click on exam "10118"
Then exam is open "COOK, ISABELLE"


@automated
@Regression
@TC_C70918799
Scenario: Remove Folder and SubFolders
#Pre-requisite: @TC_C70918798 should executed to create a folder
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click on "Folder" tab in Worklist page
And I open context menu of "ExamFolder" folder
And I select "Create Sub Folder" option from context menu
And I set "Sub Folder" as name in Create Sub-Folder dialog
And I click on "Save" button on Create Sub-Folder dialog
Then "Sub Folder" sub-folder is created
When I open context menu of "ExamFolder" folder
And I select "Delete Folder & Sub Folders" option from context menu
And I select "Yes" option to delete all folders
Then "Sub Folder" sub-folder is deleted from parent folder
And "ExamFolder" folder is deleted