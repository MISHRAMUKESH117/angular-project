@PatientView
Feature: Patient View

@automated
@BVT
@Smoke
@TC_PV-001
Scenario: Verify Worklist - Patient view.
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
Then exam is open "LISA, FISCHER"


@automated
@Smoke
@Regression
@TC_PV-002
Scenario: Verify Worklist - Patient view after change in patient details in Patient History
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
When I click on exam "12990"
Then exam is open "HAMILTON, JUSTIN"
When I collapse the note section
And in the "Patient" panel I click on "HAMILTON, JUSTIN"
Then "Patient History" window is opened
When I click on the "Patient History" edit button
And I edit the following details of patient
|Label Name  |Value      |
|Birth Date  |1984-11-16 |
And I save the "Patient History" changes
And I close the "Patient History" window
Then I check the following details
|Demographic Identifier  |Value      |
|Age                     |35y        |
|DOB                     |11/16/1984 |
When in the "Patient" panel I click on "HAMILTON, JUSTIN"
Then "Patient History" window is opened
When I click on the "Patient History" edit button
And I edit the following details of patient
|Label Name  |Value      |
|Birth Date  |1983-11-16 |
And I save the "Patient History" changes
And I close the "Patient History" window
Then I check the following details
|Demographic Identifier  |Value      |
|Age                     |36y        |
|DOB                     |11/16/1983 |
When log out of the application
And log into the application as "radiologist" user
And I open "Management" application
And I open "Configuration" module
And I click "Edit Field" from "Worklist" Configuration Page
And I select "Patient View Information" from the Configuration dropdown
And I delete the "Age" field
And log out of the application
And log into the application as "radiologist" user
And I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
When I click on exam "12990"
Then exam is open "HAMILTON, JUSTIN"
And I check the absence of the following
|Label Name |Value |
|Age        |36y   |
When log out of the application
And log into the application as "radiologist" user
And I open "Management" application
And I open "Configuration" module
And I click "Edit Field" from "Worklist" Configuration Page
And I select "Patient View Information" from the Configuration dropdown
And I drag and drop the "Age" field to "MRN" field
And log out of the application
And log into the application as "radiologist" user
And I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
When I click on exam "12990"
Then exam is open "HAMILTON, JUSTIN"
And I check the following details
|Demographic Identifier |Value |
|Age                    |36y   |
When I expand note section
And log out of the application
Then "radiologist" user is logged out


@automated
@Smoke
@Regression
@TC_PV-003
Scenario: Verify Exam Panel - Actions
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "MERCER"
Then exam is displayed "10075"
When I click on exam "10075"
Then exam is open "MERCER, ROSEMARY JOANN JOANN"
When I collapse the note section
And I collapse patient view panel 
And in the Exam panel I click on accession "10307"
Then "Exam History" window is opened
When I close the "Exam History" window
And I open the context menu for exam with Site Procedure as "CR FEMUR" 
Then I check the following list
|Read Exam                   |
|Open in Viewer              |
|Open in Dictation           |
|Assign                      |
|Add Note                    |
|Assign to Peer Review       |
|Change Status               |
|Change Priority             |
|Cancel Exam                 |
|Delete Exam                 |
|Edit Subspecialty           |
|Add to Folder               |
|Print Report                |
When I perform a click for dismissing the context menu
And I drag and drop the "Site Procedure" column on "Exam Time"
Then I check the immediate next column of "Site Procedure" is "Modality"
When I drag and drop the "Exam Time" column on "Site Procedure"
And I fetch the text for the column "4" for sorting check
And I click on the "Site Procedure" column for sorting
Then I check the sorting has worked for column "4"
When I expand note section
And I expand patient view panel
And log out of the application
Then "radiologist" user is logged out


@automated
@Smoke
@Regression
@TC_PV-004
Scenario: Verify Patient View - Editing exam information in Exam History
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
When I click on exam "12990"
Then exam is open "HAMILTON, JUSTIN"
When I collapse the note section
And I collapse patient view panel
And in the Exam panel I click on accession "16006"
Then "Exam History" window is opened
When I expand the Information section
And I click on the "Exam History" edit button
And I edit the following details in exam
|Label Name |Value              |
|Reason     |Reason For Study 2 |
And I save the "Exam History" changes
And I close the "Exam History" window
Then I check the following details
|Demographic Identifier  |Value              |
|Exam Reason             |Reason For Study 2 |
|Accession               |16006              |
When in the Exam panel I click on accession "16006"
And I click on the "Exam History" edit button
And I edit the following details in exam
|Label Name  |Value            |
|Reason      |Reason For Study |
And I save the "Exam History" changes
And I close the "Exam History" window
And I expand note section
And I expand patient view panel
And log out of the application
Then "radiologist" user is logged out


@automated
@Regression
@TC_PV-006
Scenario: Verify Patient View - Editing order information in Exam History
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Management" application
And I open "Configuration" module
And I click "Edit Field" from "Worklist" Configuration Page
And I select "Patient View Order tab" from the Configuration dropdown
And I drag and drop the "Ordered Time" field to "Radiologist" field
And I store field values in list
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
Then exam is open "HAMILTON, JUSTIN"
When in the Exam panel I click on accession "16006"
Then "Exam History" window is opened
When I expand the Information section
And I click on the "Exam History" edit button
And I edit the following details in exam
|Label Name  |Value      |
|Order       |2020-07-04 |
And I save the "Exam History" changes
And I close the "Exam History" window
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
Then exam is open "HAMILTON, JUSTIN"
Then I check the following details of exam
|Demographic Identifier  |Value      |
|Ordered Time            |07/04/2020 |
And order tab list is same of configuration field list
When in the Exam panel I click on accession "16006"
Then "Exam History" window is opened
When I expand the Information section
And I click on the "Exam History" edit button
And I edit the following details in exam
|Label Name  |Value      |
|Order       |2020-07-06 |
And I save the "Exam History" changes
And I close the "Exam History" window
And log out of the application
And log into the application as "radiologist" user
When I open "Management" application
And I open "Configuration" module
And I click "Edit Field" from "Worklist" Configuration Page
And I select "Patient View Order tab" from the Configuration dropdown
And I delete the "Ordered Time" field
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
Then exam is open "HAMILTON, JUSTIN"
And I check the absence of the following in order tab
|Label Name   |Value      |
|Ordered Time |07/04/2020 |
