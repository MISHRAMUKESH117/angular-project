@Dashboard
Feature: Dashboard feature

@automated
@BVT
@TC_DT-001
@Smoke
Scenario: Verify dashboard appliaction is launcing properly
Given Clario application is launched
And  log into the application as "admin" user
When I open "Dashboard" application
Then I am on "Dashboard" application
When I click on "Service" menu
Then service items load properly
When I click on "DICOM Routing" menu
Then DICOM Routing items load properly
When I click on "User Monitoring" menu
Then User Monitoring items load properly