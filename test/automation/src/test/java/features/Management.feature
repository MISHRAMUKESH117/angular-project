@Management
Feature: Management UI


@automated
@BVT
@Smoke
@RadPartner
@TC_MUI-001
Scenario:  Verify that new user role added and existing user role edited saved properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And create New Role with following details
|Role name |Role Description      |Permission Type   |Permission Name|
|Test User |Test User Description |Analytics Reports |Exam Assignment|
Then new user role "Test User" is added
When I click on number of Users hyperlink of user role "Test User"
And set user name "Gunn, Martin" to search
And I click on search button to search the users
And I select the user "Gunn, Martin"
And click on Save button on Users dialouge box
And I click on number of Users hyperlink of user role "Test User"
And set user name "Gunn, Martin" to search
And I click on search button to search the users
Then user name "Gunn, Martin" is added
When I deselect the user "Gunn, Martin"
And click on Save button on Users dialouge box
And I click on number of Users hyperlink of user role "Test User"
And set user name "Gunn, Martin" to search
And I click on search button to search the users
Then user name "Gunn, Martin" is removed
When click on Save button on Users dialouge box
And I select "Test User" user role
And I click on Edit user role icon
And I set new user role name "New Test User"
And click on save button
Then new user role "New Test User" is added
When I select "New Test User" user role
And I click on Edit user role icon
And I click in Delete user role icon
Then user role "New Test User" is deleted


@BVT
@automated
@Smoke
@TC_MUI-003
Scenario:  Verify that new user added and existing user edited saved properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Management" module
And create New User with following details
|First Name       |Last Name       |Login Name      |Password       |Verify Password   |
|TestingRadUser   |TestingRadUser  |TestingRadUser  |TestingRadUser |TestingRadUser    |
And I select Local User Roles
|Active  |
And I click on save user button
And I search user "TestingRadUser"
Then new user "TestingRadUser, TestingRadUser" is added
When I select "TestingRadUser, TestingRadUser" user 
And I click on Edit user icon
And I set user last name to "NewTestingRadUser"
And I click on save user button
And I search user "TestingRadUser"
Then new user "NewTestingRadUser, TestingRadUser" is added

@BVT
@Smoke
@automated
@TC_MUI-005
Scenario:  Verify that assignment rule is added, existing assignment rule is edited and rule is deleted.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Assign Rule Management" module
And I click on Add new Rule icon
And I click on Assignment Property add icon in Properties panel
And I set assignment rule name "Test Assign Rule"
And create new Assignment rule with following details
|Label            |Value              	 |
|Trigger          |Exam status is Prelim |
|First sort order |Time Remaining 		 |
|User  			  |Test0410, Test0410  	 |
And click on save assignment rule button
Then assignment rule "Test Assign Rule" is added
When I select "Test Assign Rule" assignment rule
And I click on Edit assignment rule icon
And I set assignment rule name "New Test Assign Rule"
And click on save assignment rule button
Then assignment rule "New Test Assign Rule" is added
When I select "New Test Assign Rule" assignment rule
And I click on Edit assignment rule icon
And I click in Delete assignment rule icon
Then assignment rule "New Test Assign Rule" is deleted


@Sikuli
@Smoke
@Regression
@automated
@TC_MUI-009
Scenario:  Loading of Procedure Management modules and importing of a Procedure
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Procedure Management" module
And I click on "Procedure" tab name on the left panel in Procedure Management page
Then "Procedure" page under Procedure Management is loaded properly
When I click on "Site Procedure" tab name on the left panel in Procedure Management page
Then "Site Procedure" page under Procedure Management is loaded properly
When I click on "Configuration" tab name on the left panel in Procedure Management page
Then "Configuration" page under Procedure Management is loaded properly
When I click on "Template" tab name on the left panel in Procedure Management page
Then "Template" page under Procedure Management is loaded properly
When I click on "Procedure" tab name on the left panel in Procedure Management page
And I select "Export_ProcedureNames" excel to upload in Procedure Management module, Configuration of Procedure page
And I set Procedure name "Test Procedure Name" to search on Procedure Management module, Configuration of Procedure page
Then "Test Procedure Name" procedure is "visible"
When I delete "Test Procedure Name" procedure from the grid
And I set Procedure name "Test Procedure Name" to search on Procedure Management module, Configuration of Procedure page
Then "Test Procedure Name" procedure is "not visible"


@BVT
@Smoke
@automated
@TC_MUI-010
Scenario:  Verify that all configuration modules load correctly without errors.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "LDAP Groups" from "Active Directory" Configuration Page
Then "LDAP Groups" page under Configuration is loaded properly
When I click "LDAP Server" from "Active Directory" Configuration Page
Then "LDAP Server" page under Configuration is loaded properly
When I click "Body Parts" from "Configuration" Configuration Page
Then "Body Parts" page under Configuration is loaded properly
When I click "Credentialing" from "Configuration" Configuration Page
Then "Credentialing" page under Configuration is loaded properly
When I click "Custom Reports" from "Configuration" Configuration Page
Then "Custom Reports" page under Configuration is loaded properly
When I click "Database" from "Configuration" Configuration Page
Then "Database" page under Configuration is loaded properly
When I click "Mail Server" from "Configuration" Configuration Page
Then "Mail Server" page under Configuration is loaded properly
When I click "Reading Location" from "Configuration" Configuration Page
Then "Reading Location" page under Configuration is loaded properly
When I click "Status" from "Exam Workflow" Configuration Page
Then "Status" page under Configuration is loaded properly
When I click "Status Change Icons" from "Exam Workflow" Configuration Page
Then "Status Change Icons" page under Configuration is loaded properly
When I click "Communication Type" from "Notes" Configuration Page
Then "Communication Type" page under Configuration is loaded properly
When I click "Note" from "Notes" Configuration Page
Then "Note" page under Configuration is loaded properly
When I click "Note Message" from "Notes" Configuration Page
Then "Note Message" page under Configuration is loaded properly
When I click "Tech QA Rating" from "Notes" Configuration Page
Then "Tech QA Rating" page under Configuration is loaded properly
When I click "Location" from "Organization" Configuration Page
Then "Location" page under Configuration is loaded properly
When I click "Practice" from "Organization" Configuration Page
Then "Practice" page under Configuration is loaded properly
When I click "Site" from "Organization" Configuration Page
Then "Site" page under Configuration is loaded properly
When I click "Site Report Template" from "Organization" Configuration Page
Then "Site Report Template" page under Configuration is loaded properly
When I click "Site System" from "Organization" Configuration Page
Then "Site System" page under Configuration is loaded properly
When I click "Portal Order Generation" from "Portal" Configuration Page
Then "Portal Order Generation" page under Configuration is loaded properly
When I click "Calendar Event Type" from "Productivity Gauge" Configuration Page
Then "Calendar Event Type" page under Configuration is loaded properly
When I click "RVU Task" from "Productivity Gauge" Configuration Page
Then "RVU Task" page under Configuration is loaded properly
When I click "Workload Gauge" from "Productivity Gauge" Configuration Page
Then "Workload Gauge" page under Configuration is loaded properly
When I click "Clario Commands" from "Reporting" Configuration Page
Then "Clario Commands" page under Configuration is loaded properly
When I click "Multi Factor Authentication" from "Security" Configuration Page
Then "Multi Factor Authentication" page under Configuration is loaded properly
When I click "WebSocket Certificate" from "Security" Configuration Page
Then "WebSocket Certificate" page under Configuration is loaded properly
When I click "AI Navigator" from "Services" Configuration Page
Then "AI Navigator" page under Configuration is loaded properly
When I click "Application Groups" from "Worklist" Configuration Page
Then "Application Groups" page under Configuration is loaded properly
When I click "Edit Field" from "Worklist" Configuration Page
Then "Edit Field" page under Configuration is loaded properly
When I click "Images/Icons" from "Worklist" Configuration Page
Then "Images/Icons" page under Configuration is loaded properly
When I click "My Reading Queue" from "Worklist" Configuration Page
Then "My Reading Queue" page under Configuration is loaded properly
When I click "My Support Queue" from "Worklist" Configuration Page
Then "My Support Queue" page under Configuration is loaded properly
When I click "Search Result" from "Worklist" Configuration Page
Then "Search Result" page under Configuration is loaded properly
When I click "Styling" from "Worklist" Configuration Page
Then "Styling" page under Configuration is loaded properly
When I click "Worklist Groups" from "Worklist" Configuration Page
Then "Worklist Groups" page under Configuration is loaded properly
When I click "Worklist Tuning" from "Worklist" Configuration Page
Then "Worklist Tuning" page under Configuration is loaded properly


@BVT
@Smoke
@automated
@RadPartner
@TC_C62635086
Scenario:  Verify the creation of a new Group
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I click on Create New Group button
And I set Group name as "Test Group"
And I set Group description as "Test Group description"
And I click on Save Group icon
Then new group "Test Group" is added


@BVT
@Regression
@Smoke
@automated
@RadPartner
@TC_C62635087
Scenario:  Verify the Group name is edited and saved properly
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I select "Test Group" from the grid
And I click on Edit Group icon
And I set Group name as "New Test Group"
And I click on Save Group icon
Then new group "New Test Group" is added

@BVT
@Smoke
@automated
@RadPartner
@TC_C63418733
Scenario:  Verify the Group name is deleted
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I select "New Test Group" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
Then group "New Test Group" is deleted


@Obsolete
@Regression
@automated
@RadPartner
@TC_C63832748
Scenario:  Rearrange the Group rank
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I click on "edit" button of the Subspecialty or Group grid
And I drag and drop the Group "Plain Film Readers" to rank 28
And I click on "save" button of the Subspecialty or Group grid
Then the Group "Plain Film Readers" is at rank 28
When I click on "edit" button of the Subspecialty or Group grid
And I drag and drop the Group "Plain Film Readers" back to the original rank
And I click on "save" button of the Subspecialty or Group grid
Then Group "Plain Film Readers" is moved back to its original rank


@BVT
@Smoke
@automated
@RadPartner
@TC_C63454784
Scenario:  Verify the creation of a new Subspecialty
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I click on Create New Subspecialty button
And I set Subspecialty name as "Testing Subspecialty"
And I set Subspecialty description as "Test Subspecialty description"
And I click on Save Group icon
Then new Subspecialty "Testing Subspecialty" is added


@Regression
@automated
@RadPartner
@TC_C63454785
Scenario:  Verify the Subspecialty name is edited and saved properly
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I click on Create New Subspecialty button
And I set Subspecialty name as "Test Subspecialty"
And I set Subspecialty description as "Test Subspecialty description"
And I click on Save Group icon
And I select "Test Subspecialty" from the grid
And I click on Edit Group icon
And I set Group name as "New Test Subspecialty"
And I click on Save Group icon
Then new group "New Test Subspecialty" is added


@Regression
@automated
@RadPartner
@TC_C63465877
Scenario:  Verify multiple Exam Type Definitions can be created in a Subspecialty
#Pre-requisite : TC_C63454785 should be executed.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I select "New Test Subspecialty" from the grid
And I click on Edit Group icon
And I create new Exam Type Definitions with Name
|Definition 1 |
|Definition 2 |
And I click on Save Group icon
And I select "New Test Subspecialty" from the grid
And I click on Edit Group icon
And I expand the Name dropdown in Exam Type Definition panel
Then following Exam Type Definitions are added
|Definition 1 |
|Definition 2 |


@Regression
@automated
@RadPartner
@TC_C63454786
Scenario:  Verify the Subspecialty is deleted
#Pre-requisite : TC_C63454785 should be executed.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I select "New Test Subspecialty" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
Then subspecialty "New Test Subspecialty" is deleted


@Obsolete
@Regression
@automated
@RadPartner
@TC_C63577871
Scenario:  Rearrange the Subspecialty rank
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I click on "edit" button of the Subspecialty or Group grid
And I drag and drop the Subspecialty "Pain" to rank 15
And I click on "save" button of the Subspecialty or Group grid
Then the Subspecialty "Pain" is at rank 15
When I click on "edit" button of the Subspecialty or Group grid
And I drag and drop the Subspecialty "Pain" back to the original rank
And I click on "save" button of the Subspecialty or Group grid
Then Subspecialty "Pain" is moved back to its original rank


@Regression
@automated
@RadPartner
@TC_C64309279
Scenario:  Deactivate a Custom Report
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "Custom Reports" from "Configuration" Configuration Page
And I select "Radiology Custom Report" in the Custom Reports panel
And I activate the Custom Report
And I click on "Save" button in the bottom panel of Custom Reports
Then Custom Report "Radiology Custom Report" is activated
When I select "Radiology Custom Report" in the Custom Reports panel
And I deactivate the Custom Report
And I click on "Save" button in the bottom panel of Custom Reports
Then Custom Report "Radiology Custom Report" is deactivated


@automated
@Regression
@RadPartner
@TC_C64089346
Scenario: Create a Notification Rule for Trigger: Exam Assignment
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I click on Add new Notification icon
And I enter name as "Exam Assignment Alert" in Properties window
And I enter description as "Check alert for Exam Assignment" in Properties window
And I select following values from multichoice dropdown
|Label |Value           |
|User  |Test123, test123|
And I select below values from singlechoice dropdown
|Label    |Value             |
|Trigger  |Exam Assignment   |
And I click on save icon in Properties window
Then Notification rule with "Exam Assignment Alert" name is created
When I click "Exam Assignment Alert" alert
And I click on edit icon on Properties window
And I click on delete icon on Properties window
Then "Exam Assignment Alert" Notification rule is deleted

@automated
@Regression
@RadPartner
@TC_C64577076
Scenario: Set Time of Day for an alert
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I click on Add new Notification icon
And I enter name as "Time of Day Alert" in Properties window
And I enter description as "Check alert for Time of Day" in Properties window
And I select following values from multichoice dropdown
|Label |Value           |
|User  |Test123, test123|
And I set Time of Day with a duration of 5 minutes
And I select below values from singlechoice dropdown
|Label    |Value             |
|Trigger  |Exam Assignment   |
And I click on save icon in Properties window
Then Notification rule with "Time of Day Alert" name is created
When I click "Time of Day Alert" alert
And I click on edit icon on Properties window
Then Start Time for the Notification rule is saved
And Finish Time for the Notification rule is saved
When I click on delete icon on Properties window
Then "Exam Assignment Alert" Notification rule is deleted


@Regression
@automated
@RadPartner
@TC_C65034908
Scenario:  Edit Exam Note
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "Note" from "Notes" Configuration Page
And I click edit icon for "Exam Note"
Then "Exam Note" edit window is visible

@automated
@Regression
@4.1
@TC_C60052984
Scenario: Verify that filtering/search AI Navigator by name only.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "AI Navigator" module
And I search for "Spine fracture" on AI Navigator Page
Then AI Navigator "Spine fracture" is present

@automated
@Regression
@4.1
@TC_C60052987
Scenario: Verify that filtering/search AI Navigator by special character.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "AI Navigator" module
And I search for "Test Test22-_" on AI Navigator Page
Then AI Navigator "Test Test22-_" is added

@automated
@Regression
@4.1
@TC_C60052990
Scenario: Verify that filtering/search AI Navigator by empty search.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "AI Navigator" module
And I search for " " on AI Navigator Page
Then AI Navigator " " is added

@automated
@Regression
@4.1
@TC_C60052991
Scenario: AI Navigator verify that page buttons work.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "AI Navigator" module
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page
And I search for "Spine fracture" on AI Navigator Page
Then AI Navigator "Spine fracture" is present

@automated
@Regression
@4.1
@TC_C60052999
Scenario: Verify that filtering/search AI Navigator works on second page.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "AI Navigator" module
And I click "Next Page" button on Managment Page
And I search for "Spine fracture" on AI Navigator Page
Then AI Navigator "Spine fracture" is present

@automated
@Regression
@4.1
@TC_C60052959
Scenario: Verify filtering/search Assign Rule by name only.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "Assign Rule Management" module
And I click on Add new Rule icon
And I set assignment rule name "Test Unique Assign Rule"
And I click on Assignment Property add icon in Properties panel
And create new Assignment rule with following details
|Label            |Value              	  |
|Trigger          |Exam status is Prelim  |
|First sort order |Time Remaining 		  |
|User  			  |Administrator, Initial |
And click on save assignment rule button
Then assignment rule "Test Unique Assign Rule" is present
When I select "Test Unique Assign Rule" assignment rule
And I search for "Test Unique Assign Rule" on Assignment Rule Page
Then assignment rule "Test Unique Assign Rule" is present
When I select "Test Unique Assign Rule" assignment rule
And I click on Edit assignment rule icon
And I click in Delete assignment rule icon
And I search for "Test Unique Assign Rule" on Assignment Rule Page
Then assignment rule "Test Unique Assign Rule" is deleted

@automated
@Regression
@4.1
@TC_C60052962
Scenario: Verify filtering/search Assign Rule by special character.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "Assign Rule Management" module
And I click on Add new Rule icon
And I set assignment rule name "Test _-"
And I click on Assignment Property add icon in Properties panel
And create new Assignment rule with following details
|Label            |Value              	  |
|Trigger          |Exam status is Prelim  |
|First sort order |Time Remaining 		  |
|User  			  |Administrator, Initial |
And click on save assignment rule button
Then assignment rule "Test _-" is present
When I select "Test _-" assignment rule
And I search for "Test _-" on Assignment Rule Page
Then assignment rule "Test _-" is present
When I select "Test _-" assignment rule
And I click on Edit assignment rule icon
And I click in Delete assignment rule icon
And I search for "Test _-" on Assignment Rule Page
Then assignment rule "Test _-" is deleted

@automated
@Regression
@4.1
@TC_C60052964
Scenario: Verify filtering/search Assign Rule by empty search.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "Assign Rule Management" module
And I search for " " on Assignment Rule Page
Then assignment rule " " is present

@automated
@Regression
@4.1
@TC_C60052965
Scenario: Verify page buttons work.
Given Clario application is launched
And log into the application as "admin" user
Then I open "Management" application
When I open "Assign Rule Management" module
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page

@automated
@Regression
@4.1
@TC_C60052966
Scenario: Verify filtering/search Assign Rule works on second page.
Given Clario application is launched
When log into the application as "admin" user
Then I open "Management" application
When I open "Assign Rule Management" module
And I click on Add new Rule icon
And I set assignment rule name "Test Unique Assign Rule"
And I click on Assignment Property add icon in Properties panel
And create new Assignment rule with following details
|Label            |Value              	  |
|Trigger          |Exam status is Prelim  |
|First sort order |Time Remaining 		  |
|User  			  |Administrator, Initial |
And click on save assignment rule button
Then assignment rule "Test Unique Assign Rule" is present
When I click "Next Page" button on Managment Page
And I search for "Test Unique Assign Rule" on Assignment Rule Page
Then assignment rule "Test Unique Assign Rule" is present
When I select "Test Unique Assign Rule" assignment rule
And I click on Edit assignment rule icon
And I click in Delete assignment rule icon
And I search for "Test Unique Assign Rule" on Assignment Rule Page
Then assignment rule "Test Unique Assign Rule" is deleted
	
@automated
@Regression
@4.1
@TC_C60052943
Scenario: Verify that filtering/search Group Management by name only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I click on Create New Group button
And I set Group name as "Test Group Filter"
And I set Group description as "Test Group Description"
And I click on Save Group icon
And I search for "Test Group Filter" on Group Management Page
Then new group "Test Group Filter" is present
When I select "Test Group Filter" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "Test Group Filter" on Group Management Page
Then group "Test Group Filter" is deleted

@automated
@Regression
@4.1
@TC_C60052945	
Scenario: Verify that filtering/search Group Management by description only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I click on Create New Group button
And I set Group name as "Test Group Filter"
And I set Group description as "Test Group Description"
And I click on Save Group icon
And I search for "Test Group Description" on Group Management Page
Then new group "Test Group Description" is present
When I select "Test Group Filter" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "Test Group Filter" on Group Management Page
Then group "Test Group Filter" is deleted

@automated
@Regression
@4.1
@TC_C60052946
Scenario: Verify that filtering/search Group Management by special characters
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I click on Create New Group button
And I set Group name as "TestData _-"
And I set Group description as "Test Group Description"
And I click on Save Group icon
And I search for "TestData _-" on Group Management Page
Then new group "TestData _-" is present
When I select "TestData _-" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "TestData _-" on Group Management Page
Then group "TestData _-" is deleted

@automated
@Regression
@4.1
@TC_C60052949	
Scenario: Verify that filtering/search Group Management by empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I search for " " on Group Management Page
Then new group " " is present

@automated
@Regression
@4.1
@TC_C60052950
Scenario: Verify Group Management pages button work.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page

@automated
@Regression
@4.1
@TC_C64758024
Scenario: Verify Group Management search funtionality works on second page
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Group" toggle button
And I click on Create New Group button
And I set Group name as "Test Group Filter"
And I set Group description as "Test Group Description"
And I click on Save Group icon
And I click "Next Page" button on Managment Page
And I search for "Test Group Filter" on Group Management Page
Then new Subspecialty "Test Group Filter" is present
When I select "Test Group Filter" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "Test Group Filter" on Group Management Page
Then group "Test Group Filter" is deleted

@automated
@Regression
@4.1
@TC_C64758025
Scenario: Verify that filtering/search Subspecialty name only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I click on Create New Subspecialty button
And I set Group name as "Test Subspecialty Filter"
And I set Group description as "Test Subspecialty Description"
And I click on Save Group icon
And I search for "Test Subspecialty Filter" on Group Management Page
Then new Subspecialty "Test Subspecialty Filter" is present
When I select "Test Subspecialty Filter" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "Test Subspecialty Filter" on Group Management Page
Then group "Test Subspecialty Filter" is deleted

@automated
@Regression
@4.1
@TC_C64758026
Scenario: Verify that filtering/search Subspecialty description only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on Create New Subspecialty button
And I set Group name as "Test Subspecialty Filter"
And I set Group description as "Test Subspecialty Description"
And I click on Save Group icon
And I search for "Test Subspecialty Description" on Group Management Page
Then new Subspecialty "Test Subspecialty Description" is present
When I select "Test Subspecialty Filter" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "Test Subspecialty Description" on Group Management Page
Then group "Test Subspecialty Description" is deleted

@automated
@Regression
@4.1
@TC_C64758027
Scenario: Verify that filtering/search Subspecialty special characters
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I click on Create New Subspecialty button
And I set Group name as "TestName _-"
And I set Group description as "Test Group Description"
And I click on Save Group icon
And I search for "TestName _-" on Group Management Page
Then new Subspecialty "TestName _-" is present
When I select "TestName _-" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "TestName _-" on Group Management Page
Then group "TestName _-" is deleted

@automated
@Regression
@4.1
@TC_C64758029
Scenario: Verify that filtering/search Subspecialty empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I search for " " on Group Management Page
Then new Subspecialty " " is present

@automated
@Regression
@4.1
@TC_C64758030
Scenario: Verify that pages button work on Subspeciality page
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page

@automated
@Regression
@4.1
@TC_C64758031
Scenario: Subspecialty verify that search funtionality works on second page
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Group Management" module
And I click on "Subspecialty" toggle button
And I click on Create New Subspecialty button
And I set Group name as "Test Subspecialty Filter"
And I set Group description as "Test Subspecialty Description"
And I click on Save Group icon
And I click "Next Page" button on Managment Page
And I search for "Test Subspecialty Filter" on Group Management Page
Then new Subspecialty "Test Subspecialty Filter" is present
When I select "Test Subspecialty Filter" from the grid
And I click on Edit Group icon
And I click on Delete Group icon
And I search for "Test Subspecialty Filter" on Group Management Page

@automated
@Regression
@4.1
@TC_C60052967
Scenario: Verify that filtering/search Notification Management by name only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I search for "Test Prelim Note" on Notification Management Page
Then Notification rule with "Test Prelim Note" name is created

@automated
@Regression
@4.1
@TC_C60052969
Scenario: Verify that filtering/search Notification Management by description only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I search for "Test Description" on Notification Management Page
Then Notification rule with "Test Description" name is created

@automated
@Regression
@4.1
@TC_C60052970
Scenario: Verify that filtering/search Notification Management by special character
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I search for "Test Test22! $%&’()+,-./:;=>?@[]^_`{}~" on Notification Management Page
Then Notification rule with "Test Test22! $%&’()+,-./:;=>?@[]^_`{}~" name is created

@automated
@Regression
@4.1
@TC_C60052972
Scenario: Verify that filtering/search Notification Management by empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I search for " " on Notification Management Page
Then Notification rule with " " name is created

@automated
@Regression
@4.1
@TC_C60052973
Scenario: Verify Notification Management pages button work
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page
And I search for "Test Description" on Notification Management Page
Then Notification rule with "Test Description" name is created

@automated
@Regression
@4.1
@TC_C60052974
Scenario: Verify that filtering/search Notification Management works on 2nd page
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I click "Next Page" button on Managment Page
And I search for "Test Description" on Notification Management Page
Then Notification rule with "Test Description" name is created

@automated
@Regression
@4.1
@TC_C59939672
Scenario: Verify that filtering/search SLA Management by name only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "SLA Management" module
And I search for "Stroke Protocol" on SLA Management Page
Then SLA "Stroke Protocol" is present

@automated
@Regression
@4.1
@TC_C59939675
Scenario: Verify that filtering/search SLA Management by name with special characters
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "SLA Management" module
And I search for "Test Name !$%&’()+,-./:;=>?@[]^_`{}~" on SLA Management Page
Then SLA "Test Name !$%&’()+,-./:;=>?@[]^_`{}~" is present

@automated
@Regression
@4.1
@TC_T27373309
Scenario: Verify that filtering/search SLA Management by empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "SLA Management" module
And I search for " " on SLA Management Page
Then SLA " " is present

@automated
@Regression
@4.1
@TC_T27373311
Scenario: Verify that pages button work
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "SLA Management" module
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page
And I search for "Stroke Protocol" on SLA Management Page
Then SLA "Stroke Protocol" is present

@automated
@Regression
@4.1
@TC_T27373313
Scenario: Verify that filtering/search SLA Management works on 2nd page
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "SLA Management" module
And I click "Next Page" button on Managment Page
And I search for "Stroke Protocol" on SLA Management Page
Then SLA "Stroke Protocol" is present

@automated
@Regression
@4.1
@TC_C59646125
Scenario: Verify that filtering/search UserRole Management by name only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I search for "Clario Service Admin" on User Role Management Page
Then User Role Management "Clario Service Admin" is present

@automated
@Regression
@4.1
@TC_C59656849
Scenario: Verify that filtering/search User Role Management by description only
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I search for "User Description" on User Role Management Page
Then User Role Management "User Description" is present

@automated
@Regression
@4.1
@TC_C59646126
Scenario: Verify that filtering/search UserRole Management by special character
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I search for "Test User _!$%&’()+,-./:;=>?@[]^_`{}~" on User Role Management Page
Then User Role Management "Test User _!$%&’()+,-./:;=>?@[]^_`{}~" is present

@automated
@Regression
@4.1
@TC_C59656850
Scenario: Verify that filtering/search UserRole Management by empty search
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I search for " " on User Role Management Page
Then User Role Management contains " "

@automated
@Regression
@4.1
@TC_C59705593
Scenario: Verify that pages button work
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I click "Next Page" button on Managment Page
And I click "Previous Page" button on Managment Page
And I click "Last Page" button on Managment Page
And I click "First Page" button on Managment Page
And I search for "Clario Service Admin" on User Role Management Page
Then User Role Management "Clario Service Admin" is present

@automated
@Regression
@4.1
@TC_C59705594
Scenario: Verify that filtering/search UserRole Management works on 2nd page
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I click "Next Page" button on Managment Page
And I search for "Clario Service Admin" on User Role Management Page
Then User Role Management "Clario Service Admin" is present	

@Regression
@automated
@RadPartner
@TC_C65034910
Scenario:  Edit Worklist Group
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "Worklist Groups" from "Worklist" Configuration Page
And I add sites to "Partner Worklists" Worklist Group
|CuraCloud |
|Fuji      |
And I click on "Save" button in the bottom panel of Worklist Groups
Then following sites are added for "Partner Worklists" Worklist Group
|CuraCloud |
|Fuji      |
When I remove sites from "Partner Worklists" Worklist Group
|CuraCloud |
|Fuji      |
And I click on "Save" button in the bottom panel of Worklist Groups
Then following sites are removed from "Partner Worklists" Worklist Group
|CuraCloud |
|Fuji      |


@Regression
@automated
@RadPartner
@TC_C65480564
Scenario:  Create, Edit and Delete a SLA Rule
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "SLA Management" module
And I click on Add new SLA Rule button
And I set SLA Rule name as "Test SLA Rule" 
And I set Escalation Minutes as 5
And I select Escalation Priority as "Stat"
And I click on "save" SLA Rule icon
Then SLA rule with name "Test SLA Rule" is created
When I select "Test SLA Rule" SLA Rule from the grid
And I click on "edit" SLA Rule icon
And I set SLA Rule name as "New Test SLA Rule"
And I click on "save" SLA Rule icon
Then SLA rule with name "New Test SLA Rule" is created
When I select "New Test SLA Rule" SLA Rule from the grid
And I click on "edit" SLA Rule icon
And I delete SLA Rule
Then "New Test SLA Rule" SLA Rule is deleted


@Regression
@automated
@RadPartner
@TC_C65649574
Scenario:  SLA Rule- Sorting, Rank re-arrange
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "SLA Management" module
And I click on "edit" icon on the SLA Rules grid
And I click on "Name" column header dropdown arrow
And I select "Sort Descending" dropdown option
And I click on "save" icon on the SLA Rules grid
Then SLA rules are sorted in Descending order by Name
When I click on "edit" icon on the SLA Rules grid
And I click on "Name" column header dropdown arrow
And I select "Sort Ascending" dropdown option
And I click on "save" icon on the SLA Rules grid
Then SLA rules are sorted in Ascending order by Name
When I click on "edit" icon on the SLA Rules grid
And I drag and drop the SLA Rule "STAT" to rank 3
And I click on "save" icon on the SLA Rules grid
Then the SLA Rule "STAT" is at rank 3
When I click on Add new SLA Rule button
And I set SLA Rule name as "Testing Rule" 
And I set Escalation Minutes as 5
And I select Escalation Priority as "Stat"
And I click on "save" SLA Rule icon
And I click on "edit" icon on the SLA Rules grid
And I click on "Name" column header dropdown arrow
And I select "Sort Descending" dropdown option
And I click on "save" icon on the SLA Rules grid
Then SLA rules are sorted in Descending order by Name
When I click on "edit" icon on the SLA Rules grid
And I click on "Name" column header dropdown arrow
And I select "Sort Ascending" dropdown option
And I click on "save" icon on the SLA Rules grid
Then SLA rules are sorted in Ascending order by Name
When I click on "edit" icon on the SLA Rules grid
And I drag and drop the SLA Rule "Testing Rule" to rank 4
And I click on "save" icon on the SLA Rules grid
Then the SLA Rule "Testing Rule" is at rank 4
When I select "Testing Rule" SLA Rule from the grid
And I click on "edit" SLA Rule icon
And I delete SLA Rule
Then "Testing Rule" SLA Rule is deleted



@automated
@Regression
@RadPartner
@TC_C65948360
Scenario: Delete a Notification Rule
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Notification Management" module
And I click on Add new Notification icon
And I enter name as "Test Notification Rule" in Properties window
And I enter description as "Test Notification Rule Description" in Properties window
And I select following values from multichoice dropdown
|Label |Value           |
|User  |Test123, test123|
And I select below values from singlechoice dropdown
|Label    |Value             |
|Trigger  |Exam Assignment   |
And I click on save icon in Properties window
Then Notification rule with "Test Notification Rule" name is created
When I click "Test Notification Rule" alert
And I click on edit icon on Properties window
And I click on delete icon on Properties window
Then "Test Notification Rule" Notification rule is deleted


@automated
@Regression
@RadPartner
@TC_C65837341
Scenario:  Assign Rule- Rank re-arrange
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Assign Rule Management" module
And I click on Add new Rule icon
And I click on Assignment Property add icon in Properties panel
And I set assignment rule name "Test Rule"
And create new Assignment rule with following details
|Label            |Value              	 |
|Trigger          |Exam status is Prelim |
|First sort order |Time Remaining 		 |
|User  			  |Test0410, Test0410  	 |
And click on save assignment rule button
Then assignment rule "Test Rule" is added
When I click on "edit" icon on the Assign Rules grid
And I drag and drop the Assign Rule "Test Rule" to rank 4
And I click on "save" icon on the Assign Rules grid
Then the Assign Rule "Test Rule" is at rank 4
When I select "Test Rule" assignment rule
And I click on Edit assignment rule icon
And I click in Delete assignment rule icon
Then assignment rule "Test Rule" is deleted


@Regression
@automated
@RadPartner
@TC_C65034909
Scenario:  Add RVU Task
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "RVU Task" from "Productivity Gauge" Configuration Page
And I click on "Add" button in RVU Task page
And I create a new RVU Task with following details
|Task Name     |RVU/minute  |Work Units/minute  |
|TestingTask   |0.2         |0.3         		 |
And I click on "Save" button in RVU Task page
Then RVU Task "TestingTask" is added
When I connect to database
Then RVU Task "TestingTask" is added in database
When I delete RVU Task "TestingTask" from database
Then RVU Task "TestingTask" is deleted from database
And I close database connection 


@Regression
@automated
@RadPartner
@TC_C64089348
Scenario:  Create a Communication Type
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click "Communication Type" from "Notes" Configuration Page
And I click on "Add" button in Communication Type page
And I create a Communication Type with following details
|Name  			|Waiting |Final  |
|TestCommType   |142     |142  	 |
And I click on "Save" button in Communication Type page
Then Communication Type "TestCommType" is added
When I connect to database
Then Communication Type "TestCommType" is added in database
When I delete Communication Type "TestCommType" from database
Then Communication Type "TestCommType" is deleted from database
And I close database connection 
