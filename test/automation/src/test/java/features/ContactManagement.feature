@ContactManagement
Feature: ContactManagement

@automated
@BVT
@Smoke
@RadPartner
@TC_C48774342
Scenario: Verify contact search successful and displays appropriate results
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I enter "Doe" as "lastName"
And I enter "John" as "firstName"
And I click on "Search" to button to search contact
Then "John  Doe" search results are displayed

@automated
@BVT
@Smoke
@Regression
@RadPartner
@TC_C48774343
Scenario: Verify contact is added
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I click on create new contact icon
And I create new contact with following details
|First Name |Last Name |Mobile Number |Type |Email                |
|Test       |Contact   |012-345-6789  |Cell |testcontact@gmail.com|
And I click on "Save" button to save contact
Then "Contact, Test" contact is created successfully

@automated
@Regression
@Smoke
@RadPartner
@TC_C48774344
Scenario: Verify contact is edited
#Pre-requisite : Use contact created in TC_C48774343
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I open "Contact, Test" contact
And I click on "Edit Contact" icon to edit contact
And I edit following details
|Birth Date |Communication Preference |Groups |
|11/12/1985 |Phone                    |General|
And I click on "Save" button to save contact
Then "Contact, Test" contact is updated successfully

@automated
@Regression
@Smoke
@RadPartner
@BVT
@TC_C62631582
Scenario: Verify two contacts are merged
#Pre-requisite : Use contact created in TC_C48774343
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I click on create new contact icon
And I create new contact with following details
|First Name |Last Name |Mobile Number |Type |Email                |
|Test       |Merge     |012-678-6789  |Cell |testmerge@gmail.com|
And I click on "Save" button to save contact
Then "Merge, Test" contact is created successfully
When I select "Contact, Test" and "Merge, Test" contacts to merge
And I click on "Merge" button to start merge
And I click on "Merge" button to merge contacts
Then "Merge, Test" contact is merged

@automated
@BVT
@Smoke
@RadPartner
@TC_C48774345
Scenario: Verify contact is deleted
#Pre-requisite : Use contact created in TC_C48774343
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I select "Merge, Test" contact to delete
And I click to "Delete" button to delete a contact
And I click on "Delete" confirmation button
Then "Merge, Test" contact is deleted

@automated
@Regression
@Smoke
@RadPartner
@TC_C48774346
Scenario: Verify contact is exported
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I select "Hospital B" as site
And I click on "Export" button to export file
And I select "Contacts" option to export
Then "Contact - Hospital B" file is downloaded with "Contact" sheet

@automated
@Regression
@Smoke
@RadPartner
@TC_C48774347
Scenario: Verify contact is imported
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I edit "Contact - Hospital B" site excel file
And I select "Hospital B" as site
And I click on "Import" button to import file
And I select "Contacts" option to import
And I select "Contact - Hospital B" file to import
Then file is imported with "User, Test"


@automated
@Regression
@RadPartner
@TC_C62631583
Scenario: Presence of Exam Note under Site in Patient View
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I select "Hospital A" as site
And I click on Edit Site icon
And I set "Exam Note for Site" as Note in Site Details
And I add Exam Notes under Edit Site Details window with below values
|Label       |Value  |
|Subspecialty|Body CT|
|Modality    |CT     |
And I click on plus icon to add Exam Note
And I click on "Save" button to save Site Details
Then "Exam Note for Site" is added under site
When I click on "Smart Worklist" application 
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I click Advanced Search
And I expand the "Enterprise" section in Advanced Search
And I click on "Site" dropdown on Advanced Search Page
And I search for "Hospital A" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
|Label       |Value                   |
|Modality    |CT - Computed Tomography|
|Subspecialty|Body CT                 |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" to search results
Then exam is displayed "10050"
When I click on exam "10050"
Then "Exam Note for Site" is present under Site
And log out of the application
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I select "Hospital A" as site
And I click on Edit Site icon
And I delete "Exam Note for Site" from Site Edit Details window
And I click on "Save" button to save Site Details 
Then "Exam Note for Site" is deleted from site


@automated
@Regression
@RadPartner
@TC_C62635088
Scenario: Presence of Exam Note under Referring Physician in Patient View
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I enter "Physician" as "lastName"
And I enter "Referring" as "firstName"
And I click on "Search" to button to search contact
And I open contact by site name "Clinic B"
And I click on "Edit Contact" icon to edit contact
And I set "Exam Note for Contact" as Note in Edit Contact 
And I add Exam Notes under Edit Contact window with below values
|Label       |Value  |
|Subspecialty|Body CT|
|Modality    |CT     |
And I click on plus icon to add Exam Note
And I click on "Save" button to save contact
Then "Exam Note for Contact" is added in contact
When I click on "Smart Worklist" application 
And I select the "Set Working Hours" radio button
And I click on Save button
And I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I click Advanced Search
And I expand the "Enterprise" section in Advanced Search
And I click on "Site" dropdown on Advanced Search Page
And I search for "Clinic B" in dropdown on Advanced Search Page
And I click "OK" button on Advanced Search Page dropdown
And I expand the "Exam" section in Advanced Search
And I perform "Exam" Advanced Search with following details
|Label       |Value                   |
|Modality    |CT - Computed Tomography|
|Subspecialty|Body CT                 |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date on the "Exam" Advanced Search page 
And I click on "Search" to search results
Then exam is displayed "12956"
When I click on exam "12956"
Then "Exam Note for Contact" is present under Ordering
And log out of the application
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I enter "Physician" as "lastName"
And I enter "Referring" as "firstName"
And I click on "Search" to button to search contact
And I open contact by site name "Clinic B"
And I click on "Edit Contact" icon to edit contact
And I delete "Exam Note for Contact" from Edit Contact window
And I click on "Save" button to save contact 
Then "Exam Note for Contact" is deleted from contact
 