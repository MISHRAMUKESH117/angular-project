@SWLInstallers
Feature: SWL Installers


@DBTest1
Scenario: Install SWL Database
Given "SWL Database Installer" is launched
When I get database properties for "Clario Database Setup"
And I click "Next" button
And I click "Install" button
And I click "Finish" button
Then SWL Database is installed

@DBTest2
Scenario: Uninstall SWL Database
Given "Control Panel" is launched 
When I uninstall "Clario Database"
Then SWL Database is uninstalled


@WebTest1 
Scenario: Install SWL Web
Given "SWL Web Installer" is launched
When I get database properties for "Clario SmartWorklist Setup"
And I click "Next" button
And I click "Confirm Upgrade" checkbox
And I click "Upgrade" button
And I click "Install" button
And I click "OK" button
And I click "Finish" button
Then SWL Web is installed

@WebTest2
Scenario: Uninstall SWL Web
Given "Control Panel" is launched 
When I uninstall "Clario SmartWorklist"
Then SWL Web is uninstalled

@WebTest3
Scenario: Install SWL Web as virtual directory
Given "SWL Web Installer" is launched
When I get database properties for "Clario SmartWorklist Setup"
And I click "Next" button
And I click "Confirm Upgrade" checkbox
And I click "Upgrade" button
And I click "Virtual directory on default website" radio button
And I enter "QA_AUTO" as Virtual directory alias
And I click "Install" button
And I click "Finish" button
Then SWL Web is installed

@WebTest4
Scenario: Install SWL Web as virtual directory
Given "SWL Web Installer" is launched
When I get database properties for "Clario SmartWorklist Setup"
And I click "Next" button
And I click "Confirm Upgrade" checkbox
And I click "Upgrade" button
And I click "Virtual directory on default website" radio button
And I enter "QA_AUTO" as Virtual directory alias
And I click "Install" button
And I click "OK" button
And I click "Finish" button
Then SWL Web is installed

@SvcTest1
Scenario: Install SWL Services
Given "SWL Services Installer" is launched
When I provide "QA_AUTO" servies configuration
Then SWL Services are installed

@SvcTest2
Scenario: Install SWL Services - in testing
Given "SWL Services Installer" is launched
When I get database properties for "SWL Services Dashboard"
And I click "hamburger" icon on "SWL Services Dashboard"
And I get "SWL" elements in "SWL Services Dashboard"
Then SWL Services are installed

