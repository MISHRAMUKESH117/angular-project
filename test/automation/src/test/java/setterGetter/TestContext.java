package setterGetter;

import java.util.HashMap;
import java.util.Map;

import utilities.Driver;
import utilities.WinDriver;

public class TestContext {

	Driver driver;
	WinDriver winDriver;

	public TestContext() {

	}

	private Map<String, Object> testContexts = new HashMap<String, Object>();

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Driver getDriver() {
		return this.driver;
	}	

	@SuppressWarnings("unchecked")
	public <T> T get(String name) {
		return (T) testContexts.get(name);
	}

	public <T> T set(String name, T object) {
		testContexts.put(name, object);
		return object;
	}

	public void setWinDriver(WinDriver winDriver) {
		this.winDriver = winDriver;
	}
	
	public WinDriver getWinDriver() {
		return this.winDriver;
	}
}
