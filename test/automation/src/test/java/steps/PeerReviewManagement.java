package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static utilities.Common.sleep;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class PeerReviewManagement {

	pages.PeerReviewManagement peerReviewManagementPage;
	TestContext context;

	public PeerReviewManagement(TestContext context) {
		this.context = context;
		peerReviewManagementPage = new pages.PeerReviewManagement(context.getDriver());
	}

	@Then("Peer Review Management is displayed")
	public void peer_review_management_is_displayed() {
		assertTrue(peerReviewManagementPage.isPeerReviewManagementPageDisplayed());
	}

	@When("I click on {string} menu option in Peer Review Management page")
	public void click_on_something_menu_options_in_peer_review_management_home_page(String menuOption) {
		peerReviewManagementPage.clickOnPeerReviewMenuOption(menuOption);
	}

	@Then("following sections are displayed under Note Configuration")
	public void something_section_is_displayed(DataTable sections) {
		List<String> sectionNames = sections.asList();
		for (String sectionName : sectionNames) {
			assertTrue(peerReviewManagementPage.isSectionHeaderVisible(sectionName));
			Log.printInfo("Verified Section " + sectionName);
		}
	}
	
	@Then("following sections are displayed under Note Configuration Note Preview")
	public void isSectionsValueVisible(DataTable sections) {
		List<String> sectionNames = sections.asList();
		for (String sectionName : sectionNames) {
			assertTrue(peerReviewManagementPage.isNoteNameVisible(sectionName));
			Log.printInfo("Verified Section " + sectionName);
		}
	}
	
	@Then("following sections are not displayed under Note Configuration Note Preview")
	public void sectionsValueisNotVisible(DataTable sections) {
		List<String> sectionNames = sections.asList();
		for (String sectionName : sectionNames) {
			assertFalse(peerReviewManagementPage.isNoteNameVisible(sectionName));
			Log.printInfo("Verified Section " + sectionName);
		}
	}
	
	@Then("the following dropdown values are not visible under Peer Review Note")
	public void dropdownValueIsNotVisible(DataTable peerReviewDropdownValues) {
        List<Map<String,String>> dropdownValues = peerReviewDropdownValues.asMaps();
        for (Map<String,String> dropdownValue: dropdownValues) {
	            peerReviewManagementPage.openPeerReviewNoteConfigurationDropdownButton(dropdownValue.get("Label"));
	            assertFalse(peerReviewManagementPage.isDropdownValueVisible(dropdownValue.get("Value")));
				Log.printInfo("Verified DropDownValue " + dropdownValue.get("Value"));
	        }
	}

	@Then("the following dropdown values and labels are visible under Peer Review Note")
	public void isPeerReviewDropdownValueVisible(DataTable peerReviewDropdownValues) {		
        List<Map<String,String>> dropdownValues = peerReviewDropdownValues.asMaps();
        for (Map<String,String> dropdownValue: dropdownValues) {
            peerReviewManagementPage.openPeerReviewNoteConfigurationDropdownButton(dropdownValue.get("Label"));
            assertTrue(peerReviewManagementPage.isDropdownValueVisible(dropdownValue.get("Value")));       
        }
	}
	
	@When("add a new Exam Type Definition {string}")
	public void add_a_new_exam_type_definition(String newExamTypeDefinitionName) {
		peerReviewManagementPage.clickAddNewExamTypeDefinitionButton();
		peerReviewManagementPage.setNewExamTypeDefinitionName(newExamTypeDefinitionName);
		peerReviewManagementPage.clickNewExamTypeDefinitionOKButton();
	}

	@When("I select Time Period as {string}")
	public void select_assignment_properties_time_period_as_something(String timePeriod) {
		peerReviewManagementPage.clickAssignmentPropertiesTimePeriodDropdownArrow();
		peerReviewManagementPage.selectAssignmentPropertiesTimePeriod(timePeriod);
	}

	@When("I click on Ongoing Assignment Save button")
	public void i_click_on_ongoing_assignment_save_button() {
		//To Do
	}

	@When("I select assignment properties Definition name  as {string}")
	public void select_assignment_properties_definition_name_as_something(String definitionName) {
		peerReviewManagementPage.clickAssignmentPropertiesDefinitionNameDropdownArrow();
		peerReviewManagementPage.selectAssignmentPropertiesDefinitionName(definitionName);
	}

	@Then("Time Period is set to {string}")
	public void time_period_is_set_to_something(String timePeriod) {
		assertEquals(timePeriod, peerReviewManagementPage.getAssignmentPropertiesTimePeriod());
		Log.printInfo("Time Period is set to " + timePeriod);
	}

	@Then("assignment properties Definition name {string} is deleted")
	public void assignment_properties_definition_name_something_is_deleted(String definitionName) {
		peerReviewManagementPage.clickAssignmentPropertiesDefinitionNameDropdownArrow();
		assertTrue(peerReviewManagementPage.isAssignmentPropertiesDefinitionNameDeleted(definitionName));
		Log.printInfo("assignment properties Definition name " + definitionName + " is deleted");
	}

	@When("create New Rule with following details")
	public void create_new_rule_with_following_details(DataTable newRuleDetails) {
		List<Map<String, String>> userDetails = newRuleDetails.asMaps();

		for (Map<String, String> ruleDetail : userDetails) {
			peerReviewManagementPage.clickFocusedAssignmentAddRuleButton();
			peerReviewManagementPage.setFocusedAssignmentRuleName(ruleDetail.get("Rule name"));
			peerReviewManagementPage.setFocusedAssignmentRuleDescription(ruleDetail.get("Rule Description"));
			peerReviewManagementPage
					.setFocusedAssignmentRulePercentageExamsToAssign(ruleDetail.get("Percentage Exams to Assign"));

		}
	}

	@When("I select user {string}")
	public void i_select_user_something(String userName) {
		peerReviewManagementPage.setFocusedAssignmentRuleUserName(userName);
		peerReviewManagementPage.selectFocusedAssignmentUserNameCheckbox(userName);
	}

	@When("I click on focused assignment rule save button")
	public void i_click_on_focused_assignment_rule_save_button() {
		peerReviewManagementPage.clickFocusedAssignmentRuleSaveButton();
	}

	@Then("new rule {string} is added")
	@Then("new rule {string} is present")
	public void new_rule_something_is_added(String ruleName) {
		assertTrue(peerReviewManagementPage.isRuleVisible(ruleName));
		Log.printInfo("New rule:- " + ruleName + " is added.");
	}

	@When("I select {string} rule")
	public void i_select_something_rule(String ruleName) {
		peerReviewManagementPage.clickRuleInGrid(ruleName);
	}

	@When("I click on Edit rule icon")
	public void i_click_on_edit_rule_icon() {
		peerReviewManagementPage.clickEditRuleleButton();
	}

	@When("I set new rule name {string}")
	public void i_set_new_rule_name_something(String ruleName) {
		peerReviewManagementPage.setFocusedAssignmentRuleName(ruleName);
	}

	@When("I click on Delete rule icon")
	public void i_click_on_delete_rule_icon() {
		peerReviewManagementPage.clickDeleteRuleButton();
		peerReviewManagementPage.clickDeleteRulePopUpYesButton();
	}

	@Then("rule {string} is deleted")
	public void rule_something_is_deleted(String ruleName) {
		assertTrue(peerReviewManagementPage.isRuleDeleted(ruleName));
		Log.printInfo("rule:- " + ruleName + " is deleted.");
	}

	@Then("{string} page loads properly")
	public void something_page_loads_properly(String menuOptionPageName) {
		assertTrue(peerReviewManagementPage.isMenuOptionsHeaderVisible(menuOptionPageName));
		Log.printInfo(menuOptionPageName + " page loads properly");
	}

	@When("I select {string} user for Prompting")
	public void i_select_something_user_for_Prompting(String userName) {
		peerReviewManagementPage.setPromptingUserName(userName);
		peerReviewManagementPage.selectPromptingUserName(userName);
	}

	@When("I click on save button")
	public void i_click_on_save_button() {
		peerReviewManagementPage.clickPromptingUserSelectionSaveButton();
	}

	@When("I add an Indicator with following details")
	public void i_add_an_indicator_with_following_details(DataTable newIndicatorDetails) {
		List<Map<String, String>> indicatorDetails = newIndicatorDetails.asMaps();

		for (Map<String, String> indicatorDetail : indicatorDetails) {
			peerReviewManagementPage.clickPromptingIndicatorDayDropdownArrow();
			peerReviewManagementPage.selectPromptingIndicatorDay(indicatorDetail.get("Day"));
			peerReviewManagementPage.clickPromptingIndicatorFromDropdownArrow();
			peerReviewManagementPage.selectPromptingIndicatorFromTime(indicatorDetail.get("From Time"));
			peerReviewManagementPage.clickPromptingIndicatorToDropdownArrow();
			peerReviewManagementPage.selectPromptingIndicatorToTime(indicatorDetail.get("To Time"));
			peerReviewManagementPage.clickPromptingIndicatorAddButton();
		}

	}

	@Then("{string} is added to Prompting Summary")
	public void new_user_is_added_to_prompting(String userName) {
		assertTrue(peerReviewManagementPage.isPromptingSummaryUserNamePresent(userName));
		Log.printInfo(userName + " is present in Prompting Summary");
	}

	@Then("an indicator is added for user {string} as {string}")
	public void an_indicator_is_added_for_user_something(String userName, String indicatorTime) {
		assertTrue(peerReviewManagementPage.isPromptingSummaryUserIndicatorPresent(userName, indicatorTime));
		Log.printInfo("Indicator added to an user in Prompting summary is verified");
	}

	@When("I delete an user {string} from Prompting Summary")
	public void i_delete_an_user_something_from_prompting_summary(String userName) {
		peerReviewManagementPage.clickPromptingSummaryDeleteUser(userName);
		peerReviewManagementPage.clickPopUpDeleteYesButton();
	}

	@Then("user {string} is deleted from Prompting Summary")
	public void user_is_deleted_from_prompting_summary(String userName) {
		assertTrue(peerReviewManagementPage.isPromptingSummaryUserNameDeleted(userName));
		Log.printInfo(userName + " is deleted from Prompting Summary");
	}

	@When("I click on delete Assignment Property")
	public void i_click_on_delete_assignment_property() {
		peerReviewManagementPage.clickExamTypeDefinitionDeleteIcon();
	}

	@Then("following tables are displayed under the Menu")
	public void following_tables_are_displayed_under_the_menu(DataTable menuTableNames) {
		List<String> menuNames = menuTableNames.asList();

		for (String menuName : menuNames) {
			assertTrue(peerReviewManagementPage.isMenuOptionTableNameVisible(menuName));
			Log.printInfo("Verified Peer Review Management page menu option table name" + menuName);
		}

	}

	@Then("following parameters are displayed under Peer Review Alert")
	public void following_parameters_are_displayed_under_peer_review_alert(DataTable reviewAlertParameterNames) {
		List<String> parameterNames = reviewAlertParameterNames.asList();

		for (String parameterName : parameterNames) {
			assertTrue(peerReviewManagementPage.isPeerReviewAlertParameterNameVisible(parameterName));
			Log.printInfo(parameterName + " parameter is displayed under Peer Review Alert");
		}

	}

	@Then("following property names are displayed under Focused Assignment")
	public void following_property_names_are_displayed_under_focused_assignment(DataTable focusedAssignmentPropertyNames) {
		List<String> propertyNames = focusedAssignmentPropertyNames.asList();

		for (String propertyName : propertyNames) {
			assertTrue(peerReviewManagementPage.isFocusedAssignmentPropertyNameVisible(propertyName));
			Log.printInfo(propertyName + " property is displayed under Focused Assignment");
		}
	}
	
	@Then("Focused Assignment rules section is displayed")
    public void focused_assignment_rules_section_is_displayed() {
		assertTrue(peerReviewManagementPage.isFocusedAssignmentRulesSectionVisible());
		Log.printInfo("Focused Assignment rules section is displayed");
    }
	
	@When("I set Peer Review Alert name as {string}")
	public void i_set_peer_review_alert_name_as_something(String alertName) {
		peerReviewManagementPage.setPeerReviewAlertName(alertName);
	}

	@When("I set Peer Review Alert description as {string}")
	public void i_set_peer_review_alert_description_as_something(String alertDescription) {
		peerReviewManagementPage.setPeerReviewAlertDescription(alertDescription);
	}

	@When("I click on Peer Review Alert save button")
	public void i_click_on_peer_review_alert_save_button() {
		peerReviewManagementPage.clickPeerReviewAlertSaveButton();
	}

	@Then("Peer Review Alert name is set to {string}")
	public void peer_review_alert_name_is_set_to_something(String alertName) {
		assertEquals(alertName, peerReviewManagementPage.getPeerReviewAlertName());
		Log.printInfo("Peer Review Alert name is set to " + alertName);
	}

	@Then("Peer Review Alert description is set to {string}")
	public void peer_review_alert_description_is_set_to_something(String alertDescription) {
		assertEquals(alertDescription, peerReviewManagementPage.getPeerReviewAlertDescription());
		Log.printInfo("Peer Review Alert description is set to " + alertDescription);
	}

	@When("I search for {string} on Peer Review Management Page")
	public void i_search_page_for_something_group_managment(String input) {
		peerReviewManagementPage.setFilterValue(input);
	}
	
	@When("I click add icon for {string} on Note Configuration Page")
	public void clickNoteConfigurationAddButton(String configurationType) {
		peerReviewManagementPage.clickNoteConfigurationAddButton(configurationType);	
	}
	
	@When("I create {string} with name {string} on Note Configuration Page")
	public void createNoteConfiguration(String configurationType, String name) {
		peerReviewManagementPage.createNoteConfiguration(configurationType,name);	
	}
	
	@When("I update {string} name to {string} on Note Configuration Page")
	public void updateNoteConfiguration(String configurationType, String name) {
		peerReviewManagementPage.createNoteConfiguration(configurationType,name);	
	}
	
    @When("I click Note Configuration {string} button")
    public void clickNoteConfigurationButton(String button) {
    	peerReviewManagementPage.clickNoteConfigurationButton(button);
    }
    
    @When("I click Peer Review Note Configuration {string} button")
    public void clickPeerReviewNoteConfigurationButton(String button) {
    	peerReviewManagementPage.clickPeerReviewNoteConfigurationButton(button);
    }
    
    @When("I click Peer Review Note Configuration {string} clear button")
    public void clickPeerReviewNoteConfigurationClearButton(String configuration) {
    	peerReviewManagementPage.removePeerReviewNoteConfigurationDefault(configuration);
    }

    @When("I click Note Configuration {string} Active checkbox")
    public void checkNoteConfigurationActiveCheckbox(String configuration) {
    	peerReviewManagementPage.checkNoteConfigurationActiveCheckbox(configuration);
    }
    
    @When("I uncheck Note Configuration {string} Active checkbox")
    public void uncheckNoteConfigurationActiveCheckbox(String configuration) {
    	peerReviewManagementPage.uncheckNoteConfigurationActiveCheckbox(configuration);
    }
    
    @When("I select Default Value {string} from {string} in Note Settings")
    public void clickDefaultValueNoteSettings(String value,String configuration) {
    	peerReviewManagementPage.clickDefaultValueDropdown(value,configuration);
    } 
    
    @Then("Default Value {string} from {string} is not Visable in Peer Review")
    public void defaultValueVisibleisPresent(String value,String configuration) {
    	peerReviewManagementPage.isDefaultValueVisible(value,configuration);
    } 
    
    
    @When("I click Note Configuration {string} Required checkbox")
    public void clickNoteConfigurationRequiredCheckbox(String configuration) {
    	peerReviewManagementPage.clickNoteConfigurationRequiredCheckbox(configuration);
    }
    
    @When("I uncheck Note Configuration {string} Required checkbox")
    public void uncheckNoteConfigurationRequiredCheckbox(String configuration) {
    	peerReviewManagementPage.uncheckNoteConfigurationRequiredCheckbox(configuration);
    }  
    
    @When("I click Note Configuration {string} delete icon")
    public void clickNoteConfigurationDeleteIcon(String configuration) {
    	sleep(2, TimeUnit.SECONDS);
    	peerReviewManagementPage.clickNoteConfigurationDeleteIcon(configuration);
    }
    
    @Then("{string} with name {string} is displayed on Note Configuration Page")
	@Then("{string} with name {string} is created on Note Configuration Page")
	public void isNoteConfigurationPresent(String configurationType, String name) {
		assertTrue(peerReviewManagementPage.isNoteConfigurationPresent(name));	
	}
	
	@Then("{string} with name {string} is removed from Note Configuration Page")
	public void isNoteConfigurationNotPresent(String configurationType, String name) {
		assertFalse(peerReviewManagementPage.isNoteConfigurationPresent(name));	
	}
	
	@Then("Message is displayed {string}")
	public void isMessageVisible(String message) {
		assertTrue(peerReviewManagementPage.isMessageVisible(message));	
	}
	
	@Then("{string} field error message is displayed")
	public void isFieldErrorDisplayed(String configuration) {
		assertTrue(peerReviewManagementPage.isFieldErrorDisplayed(configuration));	
	}
	
	@Then("{string} field error message is not displayed")
	public void FieldErrorIsNotDisplayed(String configuration) {
		assertFalse(peerReviewManagementPage.isFieldErrorDisplayed(configuration));	
	}
	
	@Then("the following default value is selected under Peer Review Note")
	public void isDefaultValueSelected(DataTable peerReviewDropdownValues) {		
        List <Map<String,String>> dropdownValues = peerReviewDropdownValues.asMaps();
        for (Map<String,String> dropdownValue: dropdownValues) {
            assertTrue(peerReviewManagementPage.isDefaultDropdownValueSelected(dropdownValue.get("Label"),dropdownValue.get("Value")));    
			Log.printInfo("Verified DropDownValue " + dropdownValue);
        }
	}
	
	@Then("the following default value is not selected under Peer Review Note")
	public void defaultValueisNotVisible(DataTable peerReviewDropdownValues) {		
        List <Map<String,String>> dropdownValues = peerReviewDropdownValues.asMaps();
        for (Map<String,String> dropdownValue: dropdownValues) {
            assertFalse(peerReviewManagementPage.isDefaultDropdownValueSelected(dropdownValue.get("Label"),dropdownValue.get("Value")));    
			Log.printInfo("Verified DropDownValue " + dropdownValue);
        }
	}
	
	@When("I click clear button for Default Value from {string} in Note Settings")
	public void clickNotePreviewDefaultValueClearButton(String configuration) {
         peerReviewManagementPage.removeNotePreviewDefaultValue(configuration);    
    }
	
	@When("I click Peer Review Note {string} button")
	public void clickNotePreviewNoteButton(String button) {
         peerReviewManagementPage.clickPeerReviewNoteButton(button);    
    }
	
	@When("Note Detail {string} is displayed")
	public void isNoteDetailsVisible(String details) {
		peerReviewManagementPage.isNoteDetailsVisible(details);
	}
}
 