package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;
import utilities.Log;

public class SmartReporting {

	TestContext context;
	pages.SmartReporting smartReportingPage;

	public SmartReporting(TestContext context) {
		this.context = context;
		smartReportingPage = new pages.SmartReporting(context.getDriver());
	}

	@When("I click on plus icon to create new template")
	public void i_click_on_plus_icon_to_create_new_template() {
		smartReportingPage.clickCreateNewTemplate();
	}

	@When("I enter {string} as name to create new template")
	public void i_enter_as_to_create_new_template(String templateName) {
		smartReportingPage.enterValuesInTexbox(templateName);
		context.set("TemplateName", templateName);
	}

	@When("I select following values in filter")
	public void i_select_following_values_in_filter(DataTable newTemplateDetails) {
		List<Map<String, String>> newTemplateDetail = newTemplateDetails.asMaps();
		for (Map<String, String> templateDetail : newTemplateDetail) {
			smartReportingPage.selectFilterValues("Modality", templateDetail.get("Modality"));
			smartReportingPage.selectFilterValues("Procedure Name", templateDetail.get("Procedure Name"));
			smartReportingPage.selectFilterValues("Body Part", templateDetail.get("Body Part"));
			smartReportingPage.selectFilterValues("Subspecialty", templateDetail.get("Subspecialty"));
			smartReportingPage.selectFilterValues("Site", templateDetail.get("Site"));
			smartReportingPage.selectFilterValues("Radiologist", templateDetail.get("Radiologist"));
		}
	}

	@When("I click on add icon to add filter")
	public void i_click_on_add_icon_to_these_filters() {
		smartReportingPage.addFilterDropdownValues();
	}

	@Then("new template is created")
	public void new_template_is_created() {
		assertTrue(smartReportingPage.isTemplateCreated(context.get("TemplateName")));
		Log.printInfo("New template is created");
	}

	@When("I search {string} template")
	public void i_select_template_to_delete(String templateName) {
		smartReportingPage.searchTemplate(templateName);
		context.set("TemplateName", templateName);
	}

	@When("I click on delete icon to delete template")
	public void i_click_on_delete_icon_to_delete_template() {
		smartReportingPage.clickDeleteIcon();
	}

	@Then("{string} is deleted")
	public void is_deleted(String templateName) {
		assertTrue(smartReportingPage.isTemplateDeleted(templateName));
		Log.printInfo(templateName + "template is deleted");
	}

	@Then("I verify below template proprties")
	public void i_verify_below_template_proprties(DataTable templateProperty) {
		List<String> property = templateProperty.row(0);
		for (String propertyName : property) {
			assertTrue(smartReportingPage.isTemplatePropertiesVisible(propertyName));
			Log.printInfo("Template is present with" + propertyName + "property");
		}
	}

	@When("I select {string} as {string}")
	public void i_select_as(String dropdownValue, String textbox) {
		smartReportingPage.selectFilterValues(textbox, dropdownValue);
	}

	@Then("procedure should be visible with below details")
	@Then("filters are added with below procedure")
	public void procedure_should_be_visible_with_below_details(DataTable newProcedureValues) {
		List<Map<String, String>> newProcedureValue = newProcedureValues.asMaps();
		for (Map<String, String> procedureValue : newProcedureValue) {
			assertTrue(smartReportingPage.isProcedureValueVisible(procedureValue.get("Procedure Name")));
			assertTrue(smartReportingPage.isProcedureValueVisible(procedureValue.get("Procedure Code")));
			assertTrue(smartReportingPage.isProcedureValueVisible(procedureValue.get("Modality")));
		}
	}

	@Then("template is cloned with {string} name")
	@Then("{string} template is present")
	public void template_is_present(String templateName) {
		assertTrue(smartReportingPage.isTemplateCreated(templateName));
		Log.printInfo("Template is present with" + templateName);
	}

	@Then("template table should have below columns")
	public void template_table_should_have_below_columns(DataTable templateTable) {
		List<String> templateTableHeaders = templateTable.asList();
		for (String headerName : templateTableHeaders) {
			assertTrue(smartReportingPage.isTemplateTableHeadersPresent(headerName));
			Log.printInfo("Template is present with" + headerName + "header");
		}
	}

	@When("I add follwing values in template")
	public void i_add_follwing_values_in_template(DataTable addTemplateDetails) {
		List<Map<String, String>> addTemplateDetail = addTemplateDetails.asMaps();
		for (Map<String, String> templateDetail : addTemplateDetail) {
			smartReportingPage.selectFilterValues("Modality", templateDetail.get("Modality"));
			smartReportingPage.selectFilterValues("Body Part", templateDetail.get("Body Part"));
			smartReportingPage.selectFilterValues("Subspecialty", templateDetail.get("Subspecialty"));
		}
	}
	
	@Then("{string} is created in template builder")
	public void is_created_in_template_builder(String templateName) {
		assertEquals(smartReportingPage.getTemplateName(), templateName);
		Log.printInfo("Template is created with" + templateName);
	}

	@When("I click on {string} tab to open new page")
	public void i_click_on_tab_to_open_build(String menuOption) {
		smartReportingPage.clickOnTemplateMenu(menuOption);
	}

	@When("I search for {string} template")
	public void i_search_for_template(String templateName) {
		smartReportingPage.searchBuildTemplate(templateName);
	}

	@When("I add following section in template")
	public void i_add_following_sections_in_template(DataTable sectionNamesList) {
		List<String> sectionNames = sectionNamesList.asList();
		for (String sectionName : sectionNames) {
			smartReportingPage.addBuildSectionToTemplate(sectionName);
		}
	}

	@Then("below sections are added in template")
	public void below_sections_are_added_in_template(DataTable sectionNamesList) {
		List<String> sectionNames = sectionNamesList.asList();
		for (String sectionName : sectionNames) {
			assertTrue(smartReportingPage.isSectionAddedInTemplate(sectionName));
			Log.printInfo(sectionName + "section is added in template");
		}
	}

	@When("I click on plus icon to create new block")
	public void i_click_on_plus_icon_to_create_new_block() {
		smartReportingPage.clickNewBlockIcon();
	}

	@When("I create new block with below details")
	public void i_create_new_block_with_below_details(DataTable newBlockDetails) {
		List<Map<String, String>> newBlockDetail = newBlockDetails.asMaps();
		for (Map<String, String> BlockDetail : newBlockDetail) {
			smartReportingPage.setBlockName(BlockDetail.get("Name"));
			smartReportingPage.selectQualityMeasure(BlockDetail.get("Quality Measure"));
			smartReportingPage.setBlockContent(BlockDetail.get("Block Content"));
		}
	}

	@When("I click on {string} icon on macro content")
	@When("I click on {string} icon on block content")
	public void i_click_on_icon_to_save_block(String action) {
		smartReportingPage.clickBlockContentAction(action);
	}

	@When("I search {string} by name")
	public void i_search_by(String blockValue) {
		smartReportingPage.searchBlock(blockValue);
	}

	@Then("block with {string} name is present")
	@Then("block with {string} is created")
	public void block_with_is_created(String blockName) {
		assertTrue(smartReportingPage.isBlockPresent(blockName));
		context.set("BlockName", blockName);
		Log.printInfo(blockName + "block is present");
	}

	@When("I select {string} as {string} of template")
	public void i_select_as_modality(String dropdownValue, String dropdownLabel) {
		smartReportingPage.selectTemplateLabelDropdown(dropdownValue, dropdownLabel);
		context.set("DropdownValue", dropdownValue);
		context.set("DropdownLabel", dropdownLabel);
	}

	@Then("template is present")
	public void template_is_present() {
		assertTrue(smartReportingPage.isTemplatePrsesent(context.get("TemplateName")));
		Log.printInfo("Template is present");
	}

	@When("I clear name from search result of template")
	public void i_clear_name_from_search_result_of_template() {
		smartReportingPage.clearTemplateNameValue();
	}

	@When("I clear dropdown value of template")
	public void i_clear_dropdown_value_of_template() {
		smartReportingPage.selectTemplateLabelDropdown(context.get("DropdownValue"), context.get("DropdownLabel"));
	}

	@Then("all templates are loaded")
	public void all_templates_are_loaded() {
		assertFalse(smartReportingPage.isAllTemplateListLoaded(context.get("TemplateName")));
		Log.printInfo("All templates are loaded");
	}

	@When("I check publish checkbox to publish template")
	public void i_check_publish_checkbox_to_publish_template() {
		smartReportingPage.clickPublishCheckbox();
	}

	@When("I set {string} as updated by date of template")
	public void i_set_as_date_of_template(String date) {
		smartReportingPage.setDateInTemplate(date);
	}

	@When("I clear name from search result of block")
	public void i_clear_name_from_search_result_of_block() {
		smartReportingPage.clearBlockName();
	}

	@Then("all blocks are loaded")
	public void all_blocks_are_loaded() {
		assertFalse(smartReportingPage.isAllBlocksLoaded(context.get("BlockName")));
		Log.printInfo("All blocks are loaded");
	}

	@When("I select {string} as {string} of block")
	public void i_select_as_of_block(String dropdownValue, String dropdownLabel) {
		smartReportingPage.selectBlockLabelDropdown(dropdownValue, dropdownLabel);
		context.set("DropdownValue", dropdownValue);
		context.set("DropdownLabel", dropdownLabel);
	}

	@When("I clear dropdown value of block")
	public void i_clear_dropdown_value_of_block() {
		smartReportingPage.selectBlockLabelDropdown(context.get("DropdownValue"), context.get("DropdownLabel"));
	}

	@When("I set today as updated by date of block")
	public void i_set_today_as_updated_by_date_of_block() {
		smartReportingPage.setUpdatedOnDateInBlock();
	}

	@When("I clear date value of block")
	public void i_clear_date_value_of_block() {
		smartReportingPage.clearDateOfBlock();
	}

	@When("I clear date value of template")
	public void i_clear_date_value_of_template() {
		smartReportingPage.clearDateOfTemplate();
	}

	@When("I search block {string} by block content")
	public void i_search_block_by_block_content(String blockContent) {
		smartReportingPage.setTextByBlockContent(blockContent);
	}

	@When("I add {string} in {string} section")
	public void i_add_in_section(String blockContent, String section) {
		smartReportingPage.addBlockSections(blockContent, section);
	}

	@When("I click on plus icon to create new macro")
	public void i_click_on_plus_icon_to_create_new_macro() {
		smartReportingPage.clickCreateNewMacro();
	}

	@When("I enter {string} as name to create new macro")
	public void i_enter_as_name_to_create_new_macro(String macroName) {
		smartReportingPage.setMacroName(macroName);
	}

	@When("I set {string} as macro content")
	public void i_set_as_macro_content(String macroContent) {
		smartReportingPage.setMacroContent(macroContent);
	}

	@When("I select following values to add in macro filter")
	public void i_select_following_values_to_add_in_macro_filter(DataTable newMacroDetails) {
		List<Map<String, String>> newMacroDetail = newMacroDetails.asMaps();
		for (Map<String, String> macroDetail : newMacroDetail) {
			smartReportingPage.selectMacroFilterValues("Modality", macroDetail.get("Modality"));
			smartReportingPage.selectMacroFilterValues("Procedure Name", macroDetail.get("Procedure Name"));
			smartReportingPage.selectMacroFilterValues("Body Part", macroDetail.get("Body Part"));
			smartReportingPage.selectMacroFilterValues("Subspecialty", macroDetail.get("Subspecialty"));
			smartReportingPage.selectMacroFilterValues("Site", macroDetail.get("Site"));
			smartReportingPage.selectMacroFilterValues("Radiologist", macroDetail.get("Radiologist"));
		}
	}

	@When("I click on add icon to add filter to macro")
	public void i_click_on_add_icon_to_add_filter_to_macro() {
		smartReportingPage.clickMacroFilterAddIcon();
	}

	@When("I check publish checkbox to publish macro")
	public void i_check_publish_checkbox_to_publish_macro() {
		smartReportingPage.clickMacroPublishCheckbox();
	}

	@When("I search {string} by macro name")
	public void i_search_by_macro_name(String macroName) {
		smartReportingPage.searchMacroByName(macroName);
	}

	@Then("macro with {string} is created")
	@Then("{string} macro is present")
	public void macro_with_is_created(String macroName) {
		assertTrue(smartReportingPage.isMacroPresent(macroName));
		context.set("MacroName", macroName);
		Log.printInfo(macroName + "macro is present");
	}

	@When("I select {string} as {string} of macro")
	public void i_select_as_of_macro(String dropdownValue, String dropdownLabel) {
		smartReportingPage.selectMacroLabelDropdown(dropdownValue, dropdownLabel);
		context.set("DropdownValue", dropdownValue);
		context.set("DropdownLabel", dropdownLabel);
	}

	@When("I clear dropdown value of macro")
	public void i_clear_dropdown_value_of_macro() {
		smartReportingPage.selectMacroLabelDropdown(context.get("DropdownValue"), context.get("DropdownLabel"));
	}

	@When("I clear name from search result of macro")
	public void i_clear_name_from_search_result_of_macro() {
		smartReportingPage.clearMacroName();
	}

	@Then("all macro are loaded")
	public void all_macro_are_loaded() {
		assertFalse(smartReportingPage.isAllMacroLoaded(context.get("MacroName")));
		Log.printInfo("all macros are loaded");
	}

	@When("I open {string} block")
	public void i_open_block(String blockName) {
		smartReportingPage.openBlock(blockName);
	}

	@When("I click on delete icon of block property")
	public void i_click_on_icon_of_block_property() {
		smartReportingPage.deleteBlock();
	}

	@Then("{string} block is deleted")
	public void block_is_deleted(String blockName) {
		assertFalse(smartReportingPage.isBlockPresent(blockName));
		Log.printInfo(blockName + "block is deleted");
	}

	@When("I set today as updated by date of macro")
	public void i_set_today_as_updated_by_date_of_macro() {
		smartReportingPage.setUpdatedOnDateInMacro();
	}
	
	@When("I open {string} macro")
	public void i_open_macro(String macroName) {
		smartReportingPage.openMacro(macroName);
	}

	@When("I click on delete icon of macro property")
	public void i_click_on_delete_icon_of_macro_property() {
		smartReportingPage.deleteMacro();
	}

	@Then("{string} macro is deleted")
	public void macro_is_deleted(String macroName) {
		assertFalse(smartReportingPage.isMacroPresent(macroName));
		Log.printInfo(macroName + "block is deleted");
	}
	
	@When("I click on {string} icon of template property")
	public void i_click_on_icon_of_template_property(String action) {
		smartReportingPage.clickTemplatePropertyIcon(action);
	}
	
	@Then("{string} default template file is downloaded")
	@Then("{string} template file is downloaded")
	public void template_file_is_downloaded(String fileName) {
		assertTrue(smartReportingPage.isTemplateDownloaded(fileName));
	}	
}
