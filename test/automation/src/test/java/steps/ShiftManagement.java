package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class ShiftManagement {
	TestContext context;
	pages.Schedule schedulePage;
	pages.Worklist worklistPage;

	public ShiftManagement(TestContext context) {
		this.context = context;
		schedulePage = new pages.Schedule(context.getDriver());
		worklistPage = new pages.Worklist(context.getDriver());
	}

	@When("I click on {string} button")
	public void i_click_on_button(String button) {
		schedulePage.clickShiftManagementButton(button);
	}

	@When("I click on {string} button to delete {string} shift")
	@When("I click on {string} button to create {string} shift")
	public void i_click_on_button_to_create_shift(String button, String shiftName) {
		schedulePage.clickShiftButton(button, shiftName);
	}

	@When("create New Shift with following details")
	public void create_New_Shift_with_following_details(DataTable newUserDetails) {
		List<Map<String, String>> userDetails = newUserDetails.asMaps();
		for (Map<String, String> userDetail : userDetails) {
			schedulePage.createShift("name", userDetail.get("Name"));
			schedulePage.createShift("timeStart", userDetail.get("Start Time"));
			schedulePage.createShift("timeEnd", userDetail.get("End Time"));
			schedulePage.createShift("dateStart", userDetail.get("Start Date"));
		}
	}

	@When("I click on {string} shift")
	public void i_click_on_shift(String shiftName) {
		schedulePage.clickOnShift(shiftName);
	}

	@When("I click on {string} button to save shift")
	public void i_click_on_button_to_save_shift(String button) {
		schedulePage.clickSaveShiftButton(button);
		context.set("Save", button);
	}

	@Then("{string} new shift is created")
	public void new_shift_is_created(String shiftName) {
		assertTrue(schedulePage.isNewShiftRowCreated(shiftName));
	}

	@Then("shift is deleted")
	public void shift_is_deleted() {
		assertTrue(schedulePage.isShiftDeleted());
	}

	@Then("{string} shift is edited")
	public void shift_is_edited(String shiftName) {
		assertTrue(schedulePage.isShiftEdited(shiftName, context.get("DescriptionTextbox"), context.get("DescriptionValue")));
		schedulePage.clickSaveShiftButton(context.get("Save"));
	}

	@When("I click on {string} row to assign user")
	public void i_click_on_row_to_assign_user(String shiftName) {
		schedulePage.clickRowAssignUserToShift(shiftName);
	}

	@When("I assign {string} as user")
	public void i_assign_as_user(String assignUserValue) {
		schedulePage.setAssignUserValue(assignUserValue);
	}

	@When("I click on done button to save")
	public void i_click_on_done_button_to_save_user() {
		schedulePage.saveAssignUser();
	}

	@Then("{string} user is assigned to {string} shift")
	public void user_is_assigned_to_shift(String userName, String shiftName) {
		assertTrue(schedulePage.getAssignUserValue(userName, shiftName));
	}

	@Then("{string} user is deleted from {string} shift")
	public void user_is_deleted(String userName, String shiftName) {
		assertTrue(schedulePage.isUserDeleted(shiftName, userName));
	}

	@When("I set {string} as {string}")
	public void i_edit(String value, String textbox) {
		schedulePage.setDescriptionValue(value, textbox);
		context.set("DescriptionTextbox", textbox);
		context.set("DescriptionValue", value);
	}

	@When("I select {string} from {string}")
	public void i_select_from(String worklistName, String worklistType) {
		schedulePage.selectWorklist(worklistName, worklistType);
	}

	@When("I click to save worklist")
	public void i_click_to_save_worklist() {
		schedulePage.saveWorklist();
	}

	@When("I click on sort worklist checkox")
	public void i_click_on_sort_worklist_checkox() {
		schedulePage.selectSortWorklist();
	}
	
	@When("I select {string} user to delete from {string} shift")
	public void i_click_on_delete_icon_to_delete_user_from_shift(String userName,String shiftName) {
		schedulePage.deleteAssignUser(userName,shiftName);
	}

	@When("I click on {string} application")
	public void i_click_on_application(String applicationName) {
		schedulePage.clickApplication(applicationName);
	}

	@Then("{string} worklist is added under {string} worklist")
	@Then("{string} worklist is visible under {string} worklist")
	public void worklist_is_added_in_worklist(String worklistName, String worklistType) {
		assertTrue(worklistPage.isShiftWorklistAdded(worklistName, worklistType));
	}

	@Then("My Reading queue created as per shift management")
	public void created_as_per_shift_management() {
		assertEquals(context.get("ReadingQueueList"), schedulePage.getShiftWorklistList());
	}

	@When("I click on Template file for import hyperlink")
	public void i_click_on_Template_file_for_import_link() {
		schedulePage.clickImportTemplateFileHyperlink();
	}

	@When("I set below values in {string} file in {string}")
	public void setValueInExcelSheet(String fileName, String sheetName, DataTable excelValues) {
		List<Map<String, String>> newExcelValues = excelValues.asMaps();
		for (Map<String, String> newExcelValue : newExcelValues) {
			schedulePage.setValueInExcelSheet(fileName, sheetName, newExcelValue.get("Header"), newExcelValue.get("Value"));			
		}
	}

	@When("I click on {string} button on Import dialog")
	public void clickImportDialogButton(String buttonName) {
		schedulePage.clickImportDialogButton(buttonName);
	}

	@When("I import {string} file")
	public void importExcelFile(String fileName) {
		schedulePage.importExcelFile(fileName);
	}

	@Then("{string} user is added under the current date against {string}")
	public void user_is_added_under_the_current_date_against(String userName, String shiftName) {
		assertTrue(schedulePage.getAssignUserValue(userName, shiftName));
		assertTrue(schedulePage.getShiftDate());
	}
	
	@When("I click on {string} from worklist")
	public void i_click_on_from_worklist(String label) {
		schedulePage.addShiftToWorklist(label);
	}
	
	@When("I check {string} from {string}")
	public void checkWorklistCheckBox(String worklistName, String worklistType) {
		schedulePage.checkWorklistCheckBox(worklistName, worklistType);
	}
	
	@When("I uncheck {string} from {string}")
	public void uncheckWorklistCheckBox(String worklistName, String worklistType) {
		schedulePage.uncheckWorklistCheckBox(worklistName, worklistType);
	}

}
