package steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static utilities.Common.sleep;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class SiteConfiguration{

	pages.SiteConfiguration sitePage;
	TestContext context;

	public SiteConfiguration(TestContext context) {
		this.context = context;
		sitePage = new pages.SiteConfiguration(context.getDriver());
	}

	@When("I search for {string} on Site Configuration Page")
	public void i_search_page_for_site(String site) {
		sitePage.setFilterValue(site);
	}

	@Then("Site {string} is added")
	@Then("Site {string} is present")
	public void site_is_added(String site) {
		assertTrue(sitePage.isSiteVisible(site));
		Log.printInfo("Site:- " + site + " is added.");
	}
	 
}
