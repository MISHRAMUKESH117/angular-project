package steps;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import Actions.Sikuli;
import utilities.Launcher;
import utilities.Log;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.Status;
import io.cucumber.java.en.Given;
import setterGetter.TestContext;

public class Launch {

	Launcher launcher;
	TestContext context;
	Sikuli sikuli;
	public static boolean dunit = false;

	public Launch(TestContext context) {
		this.context = context;
		launcher = new Launcher();
	}

	@Given("Clario application is launched")
	public void clario_application_is_launched() throws IOException {
		launcher.initializeWebDriver();
		launcher.launchApplication();
		Log.printInfo("Clario application is launched");
		context.setDriver(launcher.getDriver());
	}

	@SuppressWarnings("deprecation")
	@After(order = 100)
	public void afterTest(Scenario scenario) {
		if ((scenario.isFailed() || scenario.getStatus().equals(Status.SKIPPED)) && launcher.webDriverIsActive()) {
		scenario.embed(((TakesScreenshot)context.getDriver().getWebDriver()).getScreenshotAs(OutputType.BYTES), "image/png");
			Log.printInfo("screenshot is captured");
		}
		if(launcher.webDriverIsActive())
		launcher.quitApplication();
	}
}
