package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import utilities.ExcelOperations;
import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class Analytics {
	pages.Analytics analyticsPage;
	TestContext context;

	public Analytics(TestContext context) {
		this.context = context;
		analyticsPage = new pages.Analytics(context.getDriver());
	}

	@Then("Analytics is launched")
	public void isAnalyticsLaunched() {
		assertTrue(analyticsPage.isAnalyticsLaunched());
	}

	@When("I create the following widgets")
	public void clickOnWidgetName(DataTable listWidgetNames) {
		List<String> widgetNames = listWidgetNames.asList();
		for (String widgetName : widgetNames) {
			analyticsPage.selectWidget(widgetName);
		}
	}

	@Then("following widgets are displayed")
	public void isWidgetCreated(DataTable listWidgetNames) {
		List<String> widgetNames = listWidgetNames.asList();
		for (String widgetName : widgetNames) {
			assertTrue(analyticsPage.isWidgetDashboardDisplayed(widgetName));
		}
	}

	@Then("changed parameter {string} is displayed for {string}")
	public void isChangedWidgetCreated(String param, String widgetName) {
		assertTrue(analyticsPage.isChangedParamReflected(param));
	}

	@When("I edit {string} widget")
	public void editWidget(String widgetName) {
		analyticsPage.editWidgetDialogue(widgetName);
		assertTrue(analyticsPage.isWidgetDialogueDisplayed(widgetName));
	}

	@When("I set the values for the below single choice parameters")
	public void setValues(DataTable searchParameters) {
		List<Map<String, String>> searchParameter = searchParameters.asMaps();
		for (Map<String, String> search : searchParameter) {
			analyticsPage.selectSingleChoiceDropdownValue(search.get("Label"), search.get("Value"));
		}
	}

	@When("I set the values for the below multi choice parameters")
	public void setValuesMultiChoice(DataTable searchParametersMultiChoice) {
		List<Map<String, String>> searchParameter = searchParametersMultiChoice.asMaps();
		for (Map<String, String> search : searchParameter) {
			analyticsPage.selectMultiChoiceDropdownValue(search.get("Label"), search.get("Value"));
		}
	}
	
	@When("I select the Exam Time as {string}")
	public void setExamTime(String Time) {
		analyticsPage.editInputParameterTime(Time);
	}

	
	@When("I click on {string} button when editing of {string} widget is complete")
	public void clickButton(String button, String widgetName) {
		analyticsPage.actionWidget(widgetName, button);
	}

	@When("count {string} from graph")
	public void getCount(String widgetName) {
		Integer graphCount = analyticsPage.hoverCountUnreadExam();
		context.set("graphCount", graphCount);
	}

	@When("I {string} the following widgets")
	public void actOnWidget(String action, DataTable listWidgetNames) {
		List<String> widgetNames = listWidgetNames.asList();
		for (String widgetName : widgetNames) {
			analyticsPage.editWidgetDialogue(widgetName);
			assertTrue(analyticsPage.isWidgetDialogueDisplayed(widgetName));
			analyticsPage.actionWidget(widgetName, action);
		}
	}

	@Then("widgets are deleted")
	public void isWidgetDeleted(DataTable listWidgetNames) {
		List<String> widgetNames = listWidgetNames.asList();
		for (String widgetName : widgetNames) {
			assertFalse(analyticsPage.isWidgetDialogueDisplayed(widgetName));
		}
	}

	@When("get all modalities")
	/* get all modalities from graph */
	public List<String> getModalitiesFromGraph() {
		List<String> modalities = new ArrayList<String>();
		modalities = analyticsPage.getAllModality();
		context.set("Modality", modalities);
		return modalities;
	}

	@When("I export the report for {string} widget")
	public void clickOnExportReport(String widgetName) {
		analyticsPage.clickOnExport(widgetName);
		analyticsPage.isExportAlertPresent();
		analyticsPage.clickToExportSheet();
	}

	@Then("report {string} download is successfull")
	public void isReportDownloaded(String reportName) {
		assertTrue(analyticsPage.isFileDownloaded(reportName));
	}

	@When("I click on edit {string} widget")
	public void i_click_on_edit_something_widget(String widgetName) {
		analyticsPage.editWidgetDialogue(widgetName);
	}

	@When("I click on {string} label dropdown arrow in widget")
	public void i_click_on_something_label_dropdown_arrow_in_widget(String labelName) {
		analyticsPage.clickWidgetLabelDropdown(labelName);
	}

	@Then("widget label dropdown contains below options")
	public void widget_label_dropdown_contains_below_options(DataTable widgetLabelOptions) {
		List<String> labelOptions = widgetLabelOptions.asList();
		List<String> dropdownOptions = analyticsPage.getWidgetLabelDropdownOptions();
		for (String labelOption : labelOptions)
			assertTrue(dropdownOptions.contains(labelOption));
		Log.printInfo("Widget label dropdown contains below options: " + analyticsPage.getWidgetLabelDropdownOptions());
	}

	@When("I set the start date to {string}")
	public void i_set_the_start_date_to_something(String startDate) {
		analyticsPage.setEditWidgetWindowStartDate(startDate);
	}

	@Then("{string} file doesnot contains values from below sites in the {string} tab")
	public void something_file_doesnot_contains_values_from_below_sites(String fileName, String sheetName, DataTable siteNames) {
		List<String> sites = siteNames.asList();
		for (String site : sites) {
			assertTrue(analyticsPage.verifySiteNameNotPresentInExcelValue(fileName, sheetName, site));
			Log.printInfo("Excel does not contains value from site " + site);
		}
	}

	@Then("I delete the file {string}")
	public void i_delete_the_file_something(String fileName) {
		assertTrue(ExcelOperations.deleteFile(fileName));
	}

	@When("I select {string} from the {string} dropdown in widget")
	public void i_select_something_from_the_something_dropdown_in_widget(String dropdownValue, String dropdownName) {
		analyticsPage.selectSingleChoiceDropdownValue(dropdownName, dropdownValue);
	}

	@When("I get number of exams from {string} widget")
	public void i_get_number_of_exams_from_something_widget(String widgetName) {
		context.set("examCount", analyticsPage.widgetExamCount(widgetName));
	}

	@Then("the number of exams in the {string} sheet inside {string} file for the site {string} matches the number shown in widget")
	public void getNumberOfExamFromWidgetExcelSheet(String sheetName, String fileName, String siteName) {
		assertEquals(String.valueOf(analyticsPage.getNumberOfExamsPresentInExcel(fileName, sheetName, siteName)),  context.get("examCount").toString());
	}

	@When("I get the number of exams from the {string} sheet of {string} file for the Modality {string}")
	@When("I get the Note Count from the {string} sheet of {string} file for the Communication Type {string}")
	@When("I get the Note Message from the {string} sheet of {string} file for the Communication Type {string}")
	public void getExamCount(String sheetName, String fileName, String filterCellValue) {
		context.set("excelCellValue", analyticsPage.getCellValueFromExcel(fileName, sheetName, filterCellValue));
	}

	@When("I Drag drop {string} on {string} horizontally")
	public void dragDropHorizontal(String source, String destination) {
		analyticsPage.dragDropWidgetsHorizontal(source, destination);
	}

	@When("I Drag drop {string} on {string} vertically")
	public void dragDropVertical(String source, String destination) {
		analyticsPage.dragDropWidgetsVertical(source, destination);
	}

	@Then("Advanced Search count matches with the widget excel count")
	@Then("Advanced Search Note Count matches with the widget excel Note Count")
	public void compareExcelData() {
		assertTrue(context.get("advanceSearchCount").toString().concat(".0").equals(context.get("excelCellValue").toString()));
	}

	@When("I click on {string} the widget {string}")
	public void i_something_the_widget_something(String exportType, String widgetName) {
		analyticsPage.exportWidget(widgetName, exportType);
	}

	@Then("Communication Note Message matches with the widget excel Note Message")
	public void communication_Note_Message_matches_with_the_widget_Note_Message() {
		assertEquals(context.get("excelCellValue"), context.get("NoteText").toString());
	}
}
