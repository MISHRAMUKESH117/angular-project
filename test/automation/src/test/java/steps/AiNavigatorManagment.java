package steps;

import static org.junit.Assert.assertTrue;
import static utilities.Common.sleep;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class AiNavigatorManagment {

	pages.AiNavigatorManagement aiNavigatorPage;
	TestContext context;

	public AiNavigatorManagment(TestContext context) {
		this.context = context;
		aiNavigatorPage = new pages.AiNavigatorManagement(context.getDriver());
	}

	@When("I search for {string} on AI Navigator Page")
	public void i_search_page_for_ai_navigator(String input) {
		aiNavigatorPage.setFilterValue(input);
	}

	@Then("AI Navigator {string} is added")
	@Then("AI Navigator {string} is present")
	public void ai_navigator_is_added(String aiNavigatorName) {
		assertTrue(aiNavigatorPage.isAiNavigatorFilterValueVisible(aiNavigatorName));
		Log.printInfo("AiNavigator:- " + aiNavigatorName + " is added.");
	}
	
	
}
