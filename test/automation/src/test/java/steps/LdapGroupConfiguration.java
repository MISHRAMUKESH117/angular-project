package steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static utilities.Common.sleep;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class LdapGroupConfiguration{

	pages.LdapGroupConfiguration ldapGroupPage;
	TestContext context;

	public LdapGroupConfiguration(TestContext context) {
		this.context = context;
		ldapGroupPage = new pages.LdapGroupConfiguration(context.getDriver());
	}
	
	@When("I search for {string} on LDAP Configuration Page")
	public void i_search_page_for_ldap(String ldap) {
		ldapGroupPage.setFilterValue(ldap);
	}

	@Then("LDAP {string} is added")
	@Then("LDAP {string} is present")
	public void ldap_is_added(String ldapName) {
		assertTrue(ldapGroupPage.isLdapGroupFilterValueVisible(ldapName));
		Log.printInfo("LDAP:- " + ldapName + " is added.");
	}
	 
}
