package steps;

import java.util.List;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class UserCredentialing {

	pages.UserCredentialing userCredentialingPage;
	TestContext context;

	public UserCredentialing(TestContext context) {
		this.context = context;
		userCredentialingPage = new pages.UserCredentialing(context.getDriver());
	}

	@When("I click on {string} tab")
	public void i_click_on_something_tab(String tabName) {
		userCredentialingPage.clickUserCredentialingPageLeftPanelTabs(tabName);
	}

	@When("I search username {string}")
	public void i_search_username_something(String userName) {
		userCredentialingPage.setUserNameToSearch(userName);
	}

	@When("I click on user {string}")
	public void i_click_on_user_something(String userName) {
		userCredentialingPage.clickUserFromUsersTable(userName);
	}

	@When("I remove membership for all users")
	public void i_remove_membership_for_all_users() {
		userCredentialingPage.doubleClickCommonMemberCheckbox();
		userCredentialingPage.isRemoveGroupMembershipMessageDisplayed();
		userCredentialingPage.clickUserCredentialingSaveButton();
	}

	@When("I remove Final credentials for all users")
	public void i_remove_final_credentials_for_all_users() {
		userCredentialingPage.doubleClickCommonFinalRadioButton();
		userCredentialingPage.isRemoveAllCredentialingMessageDisplayed();
		userCredentialingPage.clickUserCredentialingSaveButton();

	}

	@When("I check Member checkbox")
	public void i_check_member_checkbox_for_something(DataTable valueNames) {
		List<String> names = valueNames.asList();
		for (String valueName : names)
			userCredentialingPage.clickSpecificMemberCheckbox(valueName);
	}

	@When("I select Final radio button")
	public void i_select_final_radio_button_for_something(DataTable valueNames) {
		List<String> names = valueNames.asList();
		for (String valueName : names)
			userCredentialingPage.clickSpecificFinalRadioButton(valueName);
	}

	@When("I save user credentialing")
	public void i_save_user_credentialing() {
		userCredentialingPage.clickUserCredentialingSaveButton();
	}

	@When("I click on {string} tab under User Tab in right panel")
	public void i_click_on_something_tab_under_user_tab_in_right_panel(String tabName) {
		userCredentialingPage.clickUserTabRightPanelTabs(tabName);
	}
	
	@When("I add membership for all users")
    public void i_add_membership_for_all_users() {
		userCredentialingPage.clickCommonMemberCheckbox();
		userCredentialingPage.clickUserCredentialingSaveButton();
    }
}
