package steps;

import static org.junit.Assert.assertEquals;

import java.util.List;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;
import io.cucumber.datatable.DataTable;

public class Home {

	pages.Home homePage;
	TestContext context;

	public Home(TestContext context) {
		this.context = context;
		homePage = new pages.Home(context.getDriver());
	}

	@When("I open {string} application")
	public void i_open_application(String applicationName) {
		homePage.openhomePageApplication(applicationName);
	}

	@When("I open {string} module")
	public void i_open_module(String moduleName) {
		homePage.openhomePageApplicationModulesButton(moduleName);
	}
	
	@When("I click header to open {string} module")
	public void openhomePageApplicationHeaderButton(String moduleName) {
		homePage.openhomePageApplicationHeaderButton(moduleName);
	}

	@Then("I am on {string} application")
	public void i_am_on_application(String applicationTitle) {
		assertEquals(applicationTitle, homePage.getApplicationTitle());
	}

	@When("I click on user dropdown in the page header")
	public void clickUserDropdown() {
		homePage.clickUserDropdown();
	}

	@When("I select {string} option from user dropdown menu")
	public void clickUserDropdownMenuOption(String dropdownValue) {
		homePage.clickUserDropdownMenuOption(dropdownValue);
	}
	
	@When("I set {string} as External Schedule ID")
	public void setUserSettingsScheduleId(String scheduleId) {
		homePage.setUserSettingsScheduleId(scheduleId);
	}
	
	@When("I click on {string} button on User Settings window")
	public void clickUserSettingsActionButton(String buttonName) {
		homePage.clickUserSettingsActionButton(buttonName);
	}
	
	
	@When("I click on {string} button on Profile Management window")
	public void clickProfileManagementButton(String buttonName) {
		homePage.clickProfileManagementActionButton(buttonName);
	}

	@When("I click {string} checkbox on Profile Management Window")
	public void clickCheckboxProfileManagement(String checkBoxName) {
		homePage.clickCheckboxProfileManagement(checkBoxName);
	}
	
	@When("I uncheck {string} checkbox on Profile Management Window")
	public void uncheckCheckboxProfileManagement(String checkBoxName) {
		homePage.uncheckCheckboxProfileManagement(checkBoxName);
	}
	
	@When("I switch to Window number {int}")
	public void switchWindowTab(int tabNumber) {
		homePage.uncheckCheckboxProfileManagement(tabNumber);
	}
	
	@When("I navigate to {string} module")
	public void navigateToModule(String moduleName) {
		homePage.clickModuleMenuItemDropdown();
		homePage.clickModuleMenuItemDropdownOption(moduleName);
	}
	
	@When("I select {string} values as")
	public void selectProfileManagementMultiChoiceDropdownValue(String label, DataTable dropdownValues) {
		List<String> dropdownValue = dropdownValues.asList();
		for (String value : dropdownValue) {
			homePage.selectProfileManagementMultiChoiceDropdownValue(label, value);
		}
	}

}
