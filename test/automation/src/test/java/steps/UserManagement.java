package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class UserManagement {

	pages.UserManagement userManagementPage;
	TestContext context;

	public UserManagement(TestContext context) {
		this.context = context;
		userManagementPage = new pages.UserManagement(context.getDriver());
	}

	@When("create New User with following details")
	public void create_new_role_with_following_details(DataTable newUserDetails) {
		List<Map<String, String>> userDetails = newUserDetails.asMaps();

		for (Map<String, String> userDetail : userDetails) {
			userManagementPage.clickAddUserButton();
			userManagementPage.setUserFirstName(userDetail.get("First Name"));
			userManagementPage.setUserLastName(userDetail.get("Last Name"));
			userManagementPage.setUserLoginName(userDetail.get("Login Name"));
			userManagementPage.setPassword(userDetail.get("Password"));
			userManagementPage.setVerifyPassword(userDetail.get("Verify Password"));
		}
	}

	@When("I select Local User Roles")
	public void i_select_local_user_roles(DataTable localUserRoles) {
		List<String> userRoles = localUserRoles.asList();

		for (String userRole : userRoles) {
			userManagementPage.selectLocalUserRoles(userRole);
		}
	}

	@When("I click on save user button")
	public void i_click_on_save_user_button() {
		userManagementPage.clickSaveUserButton();
	}

	@When("I search user {string}")
	public void i_search_user_something(String userName) {
		userManagementPage.searchUser(userName);
	}

	@Then("new user {string} is added")
	public void new_user_something_is_added(String userName) {
		assertTrue(userManagementPage.isUserVisible(userName));
		Log.printInfo("New user:- " + userName + " is added.");
	}

	@When("I select {string} user")
	public void i_select_something_user(String userName) {
		userManagementPage.clickUserInGrid(userName);
	}

	@When("I click on Edit user icon")
	public void i_click_on_edit_user_icon() {
		userManagementPage.clickEditUserButton();
	}
	
	@When("I set user last name to {string}")
    public void i_set_user_last_name_to_something(String lastName) {
		userManagementPage.setUserLastName(lastName);
    }
	
	@When("I generate the following user License\\/s")
	public void generateUserLicense(DataTable licenseTable) {
		List<String> licenses = licenseTable.asList();
		userManagementPage.generateUserLicense(licenses);	
	}

}
