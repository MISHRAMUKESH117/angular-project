package steps;

import pages.Login;
import utilities.Driver;
import setterGetter.TestContext;

public class AfterHooks {

	Login loginPage;
	TestContext testContext;
	Driver driver;

	public AfterHooks(TestContext testContext) {
		this.testContext = testContext;
		driver = testContext.getDriver();
		loginPage = new Login(driver);
	}

}
