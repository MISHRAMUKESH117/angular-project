package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static utilities.Common.sleep;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;
import utilities.Log;

public class Worklist {
	pages.Worklist worklistPage;
	TestContext context;

	public Worklist(TestContext context) {
		this.context = context;
		worklistPage = new pages.Worklist(context.getDriver());
	}

	@Then("worklist is launched")
	public void worklistIsLaunched() {
		assertTrue(worklistPage.isWorklistLoaded());
		worklistPage.cancelShift();		
	}

	@Then("worklist is loaded")
	public void worklistIsLoaded() {
		assertTrue(worklistPage.isWorklistLoaded());
	}

	@When("I select examtype {string}")
	public void userSelectExamType(String examType) {
		worklistPage.selectExamType(examType);
	}

	@When("I perform Quick Search")
	public void performSearch(DataTable quickSearch) {
		List<Map<String, String>> quickSearches = quickSearch.asMaps();
		for (Map<String, String> search : quickSearches) {
			String label = search.get("Label");
			String value = search.get("Value");
			worklistPage.setQuickSearchAttribute(label, value);
		}
	}

	@When("I perform worklist search {string}")
	public void userPerformWorklistSearch(String text) {
		worklistPage.setWorklistSearchAttribute(text);
	}

	@Then("note message {string} is displayed")
	@Then("exam is displayed {string}")
	public void examisLoaded(String patient) {
		assertTrue(worklistPage.isExamLoaded(patient));
	}

	@When("I click on exam {string}")
	public void userClickOnExam(String exam) {
		worklistPage.selectExam(exam);
	}

	@Then("next exam is loaded with {string}")
	@Then("exam is open {string}")
	public void examIsOpen(String patient) {
		assertTrue(worklistPage.isExamOpen(patient));
	}

	@When("I click Advanced Search")
	public void iClickAdvancedSearch() {
		worklistPage.clickAdvancedSearch();
	}

	@When("worklist launches")
	public void worklistLaunchTest() {
		assertTrue(worklistPage.isWorklistLoaded());
		// assertTrue(worklistPage.isZvExtenderDisplayed());
		// worklistPage.clickZvAlert();
	}

	@Then("{string} dialog is launched")
	public void currentShiftSelectionisCreated(String dialogName) {
		assertTrue(worklistPage.isCurrentShiftPageLoaded(dialogName));
	}

	@When("I select the ShiftType and Shift as {string} and {string} respectively")
	public void selectShift(String shiftType, String shift) {
		worklistPage.selectShift(shiftType, shift);
	}

	@When("I select the location as {string} and reading room as {string} respectively")
	public void selectLocationReadingRoom(String location, String readingRoom) {
		worklistPage.selectLocationReadingRoom(location, readingRoom);
	}

	@When("I click on Save button")
	public void saveShift() {
		worklistPage.saveShiftonWorklistLaunch();
	}

	@Then("selected shift is loaded and {string} is displayed")
	public void isShiftLoaded_Worklist(String worklistName) {
		assertTrue(worklistPage.isSelectedShiftLoaded(worklistName));
	}

	@When("I click on the Select Shift button in the Worklist navigation")
	public void userClickSelectShiftOnWorklist() {
		worklistPage.selectShiftLoadFromWorklist();
	}

	@When("I select the {string} radio button")
	public void selectSetWorkingHoursSave(String radioBtnSelect) {
		worklistPage.selectShiftRadioBtn(radioBtnSelect);
	}

	@When("I click on Save button in the Shift selection from Worklist")
	public void saveShiftFromWorklist() {
		worklistPage.saveShiftFromWorkList();
	}

	@Then("exam should open in patient view")
	public void exam_should_open_in_patient_view() {
		assertTrue(worklistPage.isExamOpenInPatientView());
	}

	@Then("following worklists are displayed under the Inteleviewer Worklists")
	public void workistCheck(DataTable worklists) {
		List<String> worklistNames = worklists.asList();
		for (String worklistName : worklistNames) {
			assertTrue(worklistPage.isWorklistVisible(worklistName));
			Log.printInfo("Verified worklist presence" + worklistName);
		}
	}

	@When("I click on worklist {string}")
	public void i_click_on_worklist_something(String worklistName) {
		worklistPage.clickOnWorklist(worklistName);
	}

	@Then("worklist contains records only from site {string}")
	public void worklist_contains_records_only_from_site_something(String siteName) {
		List<String> siteNames = worklistPage.getWorklistGridSiteNames();
		for (String gridSiteName : siteNames) {
			assertTrue(gridSiteName.equals(siteName));
			Log.printInfo("Site Name present is: " + gridSiteName);
		}
	}

	@When("I click on Quick Search {string} dropdown")
	public void i_click_on_quick_search_something_dropdown(String dropdownOption) {
		worklistPage.clickQuickSearchDropdown(dropdownOption);
	}

	@Then("following sites are displayed in Quick Search dropdown")
	public void following_sites_are_displayed_in_quick_search_dropdown(DataTable quickSearchSiteNames) {
		List<String> siteNames = quickSearchSiteNames.asList();
		assertTrue(siteNames.equals(worklistPage.getQuickSearchSiteDropdownNames()));
		Log.printInfo("Following sites are displayed in Quick Search dropdown: "
				+ worklistPage.getQuickSearchSiteDropdownNames());
	}

	@When("I expand the {string} section in Advanced Search")
	public void i_expand_the_something_section_in_advanced_search(String sectionName) {
		worklistPage.expandAdvancedSearchSection(sectionName);
	}

	@When("I click on {string} label dropdown arrow")
	public void i_click_on_something_label_dropdown_arrow(String labelName) {
		worklistPage.clickAdvancedSearchLabelDropdown(labelName);
	}

	@Then("following options are displayed under Advanced Search label dropdown")
	public void following_options_are_displayed_under_advanced_search_label_dropdown(
			DataTable advancedSearchLabelOptions) {
		List<String> labelOptions = advancedSearchLabelOptions.asList();
		assertTrue(labelOptions.equals(worklistPage.getAdvancedSearchLabelDropdownOptions()));
		Log.printInfo("Following options are displayed under Advanced Serach label dropdown: "
				+ worklistPage.getAdvancedSearchLabelDropdownOptions());
	}

	@When("I close the Advanced Search if it is loaded")
	public void isAdvancedSearchLoaded_Close() {
		worklistPage.check_AdvancedSearch_Close();
	}

	@When("I click on Search button in Quick Search")
	public void i_click_on_search_button_in_quick_search() {
		worklistPage.clickQuickSearchButton();
	}

	@Then("worklist contains records only for patient {string}")
	public void worklist_contains_records_only_for_patient_something(String patientName) {
		List<String> patientNames = worklistPage.getWorklistGridPatientNames();
		for (String gridPatientName : patientNames) {
			assertTrue(gridPatientName.equals(patientName));
			Log.printInfo("Patient Name present is: " + gridPatientName);
		}
	}

	@Then("Advanced Search label dropdown contains below options")
	public void advanced_search_label_dropdown_contains_below_options(DataTable advancedSearchLabelOptions) {
		List<String> labelOptions = advancedSearchLabelOptions.asList();
		List<String> dropdownOptions = worklistPage.getAdvancedSearchLabelDropdownOptions();
		for (String labelOption : labelOptions)
			assertTrue(dropdownOptions.contains(labelOption));
		Log.printInfo("Following options are displayed under Advanced Serach label dropdown: "
				+ worklistPage.getAdvancedSearchLabelDropdownOptions());
	}

	@Then("I verify {string} worklist tree and tab count")
	public void verifyWorklistTreeAndTabCount(String worklistName) {
		Integer treeCount = Integer.parseInt(worklistPage.getWorklistTreeCount(worklistName));
		Integer tabCount = worklistPage.getWorklistTabCount();
		assertEquals(treeCount, tabCount);
	}

	@Then("I verify worklist load time is less than {string} seconds")
	public void verifyWorklistLaodTime(String actualTime) {
		Double loadTime = worklistPage.getWorklistLoadTime();
		Double actualTimeDouble = Double.parseDouble(actualTime);
		assertTrue(loadTime < actualTimeDouble);
	}


	@When("I click on export worklist")
	public void clickOnExportWorklist() {
		worklistPage.clickOnExportWorklist();
	}

	@Then("{string} worklist is appropriately exported")
	public void verifyExportedWorklist(String worklistName) {
		assertTrue(worklistPage.isWorklistExported(worklistName));
	}

	@When("I click on {string} {string} button to add new worklist")
	public void i_click_on_button_to_add_new_worklist(String worklistName, String button) {
		worklistPage.saveWorklistButton(worklistName, button);
	}

	@Then("I verify properties dialogue is open")
	public void verifyPropertiesDialogueIsOpen() {
		assertTrue(worklistPage.verifyPropertiesDialogue());
	}

	@Then("I verify columns sort order")
	public void verifyColumnsSortOrder(DataTable properties) {
		List<Map<String, String>> property = properties.asMaps();
		for (Map<String, String> singleProperty : property) {
			assertTrue(
					worklistPage.verifySortOrder(singleProperty.get("Column Name"), singleProperty.get("Property")));
		}
	}

	@When("I change properties")
	public void changeProperties(DataTable properties) {
		List<Map<String, String>> property = properties.asMaps();
		for (Map<String, String> singleProperty : property) {
			worklistPage.changeProperties(singleProperty.get("Label"), singleProperty.get("Value"));
		}
	}

	@When("I change properties {string}, {string}")
	public void changePropertiesOutline(String label, String value) {
		worklistPage.changeProperties(label, value);
	}

	@Then("I verify properties are changed")
	public void verifyPropertiesAreChanged(DataTable properties) {
		List<Map<String, String>> property = properties.asMaps();
		for (Map<String, String> singleProperty : property) {
			assertTrue(
					worklistPage.verifyPropertiesAreChanged(singleProperty.get("Label"), singleProperty.get("Value")));
		}
	}

	@When("I click following columns to sort them")
	public void clickColumnsToSort(DataTable sorting) {
		List<Map<String, String>> sort = sorting.asMaps();
		for (Map<String, String> singleProperty : sort) {
			worklistPage.clickColumnName(singleProperty.get("Column Name"));
			worklistPage.selectProperty(singleProperty.get("Property"));
		}
	}

	@When("I click on {string} button of {string} dialogue")
	public void addNewWorklist(String button, String worklistName) {
		worklistPage.saveWorklistButton(worklistName, button);
	}

	@When("I click on {string} button from Properties dialogue")
	public void clickPropertiesOptionFromDialogue(String option) {
		worklistPage.clickPropetiesCancelChangeClear(option);
	}

	@When("I drag drop {string} on {string}")
	public void worklistDragDrop(String worklistName1, String worklistName2) {
		worklistPage.dragDropWorklists(worklistName1, worklistName2);
	}

	@When("I expand {string}")
	public void expandWorklist(String name) {
		worklistPage.expandWorklist(name);
	}

	@Then("shift is added to My Reading queue worklist")
	public void shift_is_added_to_worklist() {
		context.set("ReadingQueueList", worklistPage.getReadingQueueList());
	}

	@When("I click on exam from history panel {string}")
	public void clickFromHistoryPanel(String accessionNo) {
		worklistPage.clickExamInHistoryPanel(accessionNo);
	}

	@When("I select {string} from the right click option")
	public void i_click_on_something_from_the_right_click_options_available_for_exams(String rightClickOption) {
		worklistPage.clickWorklistExamRightClickOptions(rightClickOption);
	}

	@When("I select the radiologist name as {string} to assign the Peer review")
	public void i_select_the_radiologist_name_as_something_to_assign_the_peer_review(String radiologistName) {
		worklistPage.selectAssignToRadiologistName(radiologistName);
	}

	@When("I set {string} as note message in Assign Peer Review dialog box")
	@When("I set the note message in Peer Review window as {string}")
	public void i_set_something_as_note_message_in_assign_peer_review_popup(String noteMessage) {
		worklistPage.setAssignPeerReviewPopupNoteMessage(noteMessage);
	}

	@When("I click on assign button on Assign Peer Review dialog box")
	public void i_click_on_assign_button_on_assign_peer_review_popup() {
		worklistPage.clickAssignPeerReviewPopupAssignButton();
	}

	@Then("{string} exam details are displayed in Peer Review window")
	public void something_exam_details_are_displayed_in_peer_review_window(String patientName) {
		assertTrue(worklistPage.verifyPatientLoadedInPeerReviewWindow(patientName));
		Log.printInfo(patientName + "is displayed in Peer Review window");
	}

	@When("I click on {string} button in the bottom panel")
	public void i_click_on_something_button_in_the_bottom_panel(String buttonName) {
		worklistPage.clickBottomButton(buttonName);
	}

	@When("I set the following values under Note section in Peer Review window")
	public void i_set_the_following_values_under_note_section_in_peer_review_window(
			DataTable peerReviewWindowNoteDetails) {
		List<Map<String, String>> noteDetails = peerReviewWindowNoteDetails.asMaps();
		for (Map<String, String> noteDetail : noteDetails) {
			worklistPage.selectPeerReviewWindowNoteDropdownValue(noteDetail.get("DropdownName"),
					noteDetail.get("DropdownValue"));
		}
	}

	@Then("the dropdown values under Note section in Peer Review window is set to default as")
	public void the_dropdown_values_under_note_section_in_peer_review_window_is_set_to_default_as(
			DataTable peerReviewWindowNoteSectionDetails) {
		List<Map<String, String>> noteDetails = peerReviewWindowNoteSectionDetails.asMaps();
		for (Map<String, String> noteDetail : noteDetails) {
			assertEquals(worklistPage.getPeerReviewWindowNoteSectionDropdownValue(noteDetail.get("DropdownName")),
					noteDetail.get("DropdownValue"));
			Log.printInfo(noteDetail.get("DropdownName") + "is set to " + noteDetail.get("DropdownValue"));
		}
	}

	@Then("message field under the Note section in Peer Review window is cleared")
	public void message_field_under_the_note_section_in_peer_review_window_is_cleared() {
		assertTrue(worklistPage.isPeerReviewWindowNoteMessageCleared());
	}

	@When("I set the Dismiss Peer Review reason as {string}")
	public void i_set_the_dismiss_peer_review_reason_as_something(String dismissReason) {
		worklistPage.setDismissPeerReviewReason(dismissReason);
	}

	@Then("exam {string} is not displayed in the worklist")
	public void exam_something_is_not_displayed_in_the_worklis(String examMRNNumber) {
		assertTrue(worklistPage.isExamNotVisibleInWorklist(examMRNNumber));
	}

	@When("I switch to {string} tab")
	public void i_switch_to_something_page(String tabName) {
		worklistPage.switchToTab(tabName);
	}

	@When("I delete exam {string} from the worklist")
	public void i_delete_exam_from_the_worklist(String exam) {
		worklistPage.deleteExamFromWorklist(exam);
	}

	@When("I reject exam {string} from the worklist")
	public void i_reject_exam_from_the_worklist(String exam) {
		worklistPage.clickRejectExamFromWorklist(exam);
	}

	@When("set reject reason as {string}")
	public void set_reject_reason_as(String rejectReason) {
		worklistPage.setRejectPeerReviewReason(rejectReason);
	}

	@Then("I verify all the displayed exam values")
	public void verifyDisplayedValues(DataTable quickSearch) {
		List<Map<String, String>> quickSearches = quickSearch.asMaps();
		for (Map<String, String> search : quickSearches) {
			worklistPage.readAllColumnValues(search.get("Value"), Integer.parseInt(search.get("Index")));
		}
	}

	@When("I check if Advanced Search is open")
	public void isAdvancedSearchLoaded_Open() {
		if ((worklistPage.check_AdvancedSearch_Open())) {
		} else
			worklistPage.clickAdvancedSearch();
	}

	@When("I check Auto-next checkbox in Peer Review window")
	public void i_check_Auto_next_checkbox_in_Peer_Review_window() {
		worklistPage.checkPeerReviewWindowAutoNextCheckbox();
	}

	@When("I launch the Advanced Search from Quick Search panel")
	public void launchAdvancedSearch() {
		worklistPage.launchAdvancedSearchFromQuickSearch();
	}

	@When("I click on following column name dropdown arrow to remove a column")
	@When("I click on following column name dropdown arrow to add new column")
	public void clickColumnsToAddRemoveColumn(DataTable sorting) {
		List<Map<String, String>> sort = sorting.asMaps();
		for (Map<String, String> singleProperty : sort) {
			worklistPage.clickColumnName(singleProperty.get("Column Name"));
			worklistPage.selectProperty(singleProperty.get("Property"));
			worklistPage.selectDeselectColumn(singleProperty.get("Value"));
		}
	}

	@Then("I verify new column is added")
	public void verifyColumnAdded(DataTable colName) {
		List<String> column = colName.asList();
		for (String columnName : column)
			assertTrue(worklistPage.verifyNewColumnAdded(columnName));
	}

	@Then("I verify column is removed")
	public void verifyColumnRemoved(DataTable colName) {
		List<String> column = colName.asList();
		for (String columnName : column)
			assertFalse(worklistPage.verifyColumnRemoved(columnName));
	}

	@When("I click on Open in Viewer icon in Peer Review window")
	public void i_click_on_Open_in_Viewer_icon_on_Peer_Review_window() {
		worklistPage.clickPeerReviewWindowOpenInViewerIcon();
	}

	@Then("{string} details are displayed in InteleViewer")
	public void details_are_displayed_in_InteleViewer(String patientName) {
		sleep(3, TimeUnit.SECONDS); // Adding a sleep for the InteleViewer to open.
		worklistPage.clickOnViewerButton("iVAcknowledgeButton");
		assertTrue(worklistPage.isPatientDetailsDisplayedInViewer(patientName));
		worklistPage.clickOnViewerButton("iVCloseButton");
	}

	@When("I exit from InteleViewer")
	public void i_exit_from_InteleViewer() {
		worklistPage.clickOnViewerButton("exitInteleViewerButton");
		worklistPage.clickOnViewerButton("exitViewerConfirmationYesButton");
	}

	@When("I check Always Launch Viewer checkbox in Peer Review window for exam {string}")
	public void i_check_Always_Launch_Viewer_checkbox_in_Peer_Review_window(String exam) {
		worklistPage.checkAlwaysLaunchViewerCheckbox(exam);
	}

	@When("I close Document View window in InteleViewer if visible")
	public void i_close_Document_View_window_in_InteleViewer_if_visible() {
		sleep(50, TimeUnit.SECONDS);
		if (worklistPage.isElementVisibleInInteleviewer("Document Viewer"))
			worklistPage.clickOnViewerButton("exitInteleViewerButton");
	}

	@When("I hover on exam {string} with {string} Status")
	public void mouseHover(String accessionNo, String status) {
		worklistPage.mouseHoverStatusColumn(accessionNo, status);
		context.set("accessionNo", accessionNo);
	}

	@When("I click on icon {string}")
	public void clickStatusIcon(String hoverIcon) {
		worklistPage.clickExamStatusIcon(context.get("accessionNo"), hoverIcon);
	}

	@When("I select {string} as {string} on Assign Dialog box")
	public void selectUserRad(String label, String value) {
		worklistPage.selectAssignDropDownValue(label, value);
	}

	@When("I set reason as {string} on Assign Dialog box")
	public void setReasonInAssignWindow(String reason) {
		worklistPage.setReasonInAssignWindow(reason);
	}

	@When("I click on {string} button on Assign Dialog box")
	public void clickButton(String button) {
		worklistPage.clickAssignWindowButton(button);
	}

	@When("I uncheck Always Launch Viewer checkbox in Peer Review window for exam {string}")
	public void i_uncheck_Always_Launch_Viewer_checkbox_in_Peer_Review_window(String exam) {
		worklistPage.uncheckAlwaysLaunchViewerCheckbox(exam);
	}

	@When("I deselect {string} for Auto-Next")
	public void selectAutoNext(String option) {
		worklistPage.selectAutoNextOption(option, "Auto-NextMenu");
	}

	@When("I click {string} on Auto-Next panel")
	public void clickOnAutoNextTab(String buttonName) {
		worklistPage.clickOnAutoNextTab(buttonName);
	}

	@Then("Worklist is loaded after Auto-Next ends")
	public void isWorklistLoadedAfterAutoNext() {
		assertTrue(worklistPage.isWorklistLoadedAfterAutoNext());
	}

	@Then("Exam {string} with Status {string} is displayed")
	public void isExamStatusChanged(String accessionNo, String status) {
		assertTrue(worklistPage.isExamStatusChanged(accessionNo, status));
	}

	@Then("InteleViewer is not launched")
	public void isInteleViewerLaunched() {
		assertFalse(worklistPage.isInteleViewerLaunched());
	}

	@When("I right click on exam {string}")
	public void rightClickOnExam(String accessionNo) {
		worklistPage.rightClickOnExam(accessionNo);
	}

	@When("I set Exam Status as {string}")
	public void setExamStatus(String newStatus) {
		worklistPage.changeExamStatus(newStatus);
	}

	@When("I click {string} button to save changed Status")
	public void saveChangedStatus(String buttonName) {
		worklistPage.saveChangedStatus(buttonName);
	}

	@Then("exams are sorted by first sort order")
	public void sortExamBySortOrder() {
		assertTrue(worklistPage.getPatientNameList());
	}

	@Then("exams are sorted by second sort order")
	public void exams_are_sorted_by_second_sort_order() {
		assertTrue(worklistPage.getSiteNameList());
	}

	@Then("exams are sorted by third sort order")
	public void exams_are_sorted_by_third_sort_order() {
		assertTrue(worklistPage.getMrnNumberList());
	}

	@When("I set threshold values as below")
	public void setThresholdValue(DataTable newThresholdValues) {
		List<Map<String, String>> newThresholdValue = newThresholdValues.asMaps();
		for (Map<String, String> thresholdValue : newThresholdValue) {
			worklistPage.setThresholdValue(thresholdValue.get("Label"), thresholdValue.get("Value"));
		}
	}

	@Then("{string} worklist is {string}")
	public void isWorklistActivated(String worklistName, String status) {
		if (status.equals("de-activated")) {
			assertTrue(worklistPage.isWorklistActivated(worklistName));
		} else {
			assertFalse(worklistPage.isWorklistActivated(worklistName));
		}
	}

	@Then("{string} worklist counter is {string} with exam count as {string}")
	public void isCounterActivated(String worklistName, String status, String examCount) {
		if (status.equals("de-activated")) {
			assertTrue(worklistPage.isCounterActivated(worklistName, examCount));
		} else {
			assertFalse(worklistPage.isCounterActivated(worklistName, examCount));
		}
	}

	@When("I click {string} icon for exam {string}")
	public void clickSkipOrNextIcon(String iconName, String exam) {
		worklistPage.clickSkipOrNext(iconName, exam);
	}

	@When("I clear {string}")
	public void clearSortOrder(String sortOrder) {
		worklistPage.clearSortOrder(sortOrder);
	}

	@When("I select {string} worklist under {string} worklist from Create My Reading Queue dialog")
	public void expandCreateMRQWorklist(String subWorklistName, String worklistName) {
		worklistPage.selectWorklistFromCreateMRQ(subWorklistName, worklistName);
	}
	
	@When("I click on {string} button on Create My Reading Queue dialog")
	public void clickCreateMRQButton(String buttonName) {
		worklistPage.clickCreateMRQActionButton(buttonName);
	}

	@Then("{string} worklist is removed from My Support Queue worklist")
	@Then("{string} worklist is removed from My Reading Queue worklist")
	public void isWorklistRemovedFromMRQ(String subWorklistName) {
		assertFalse(worklistPage.isWorklistAddedInMRQ(subWorklistName));
	}

	@Then("{string} worklist is added in My Reading Queue worklist")
	@Then("{string} worklist is added in My Support Queue worklist")
	public void isWorklistAddedInMRQ(String subWorklistName) {
		assertTrue(worklistPage.isWorklistAddedInMRQ(subWorklistName));
	}

	@When("I click on {string} button on Worklist tab")
	public void myReadingQueueButtons(String buttonName) {
		worklistPage.clickReadingQueueButton(buttonName);
	}

	@When("I click {string} from {string}")
	public void i_click_from(String worklistName, String worklistType) {
		worklistPage.expandWorklist(worklistName, worklistType);
	}

	@Then("{string} worklist is added under {string}")
	public void worklist_is_added_in(String worklistName, String worklistType) {
		assertTrue(worklistPage.isWorklistPresent(worklistName, worklistType));
	}

	@When("I right click on {string} exam")
	public void i_right_click_on_exam(String exam) {
		worklistPage.rightClickOnExam(exam);
	}

	@When("I mouse hover on {string} option")
	public void i_mouse_hover_on_option(String menuOption) {
		worklistPage.mouseHoveOnAddNoteOption(menuOption);
	}

	@When("I click on {string} option")
	public void i_click_on_option(String subMenuOption) {
		worklistPage.clickOnNoteSubMenu(subMenuOption);
	}

	@Then("{string} exam is present in history panel")
	public void exam_is_present_in_history_panel(String accessionNo) {
		assertTrue(worklistPage.isExamPresentInHistoryPanel(accessionNo));
	}

	@When("I right click on {string} from {string}")
	public void i_right_click_on_from(String worklistName, String worklistType) {
		worklistPage.rightClickOnWorkList(worklistName, worklistType);
	}

	@When("I select {string} option from worklist menu")
	public void i_select_option_to_delete_worklist(String menuOption) {
		worklistPage.clickOnWorklistMenuOption(menuOption);
	}

	@When("I click on yes option to delete {string} worklist")
	public void i_click_on_option_to_delete_worklist(String worklistName) {
		worklistPage.deleteWorklist(worklistName);
	}

	@Then("{string} worklist is deleted from {string}")
	public void worklist_is_deleted(String worklistName, String worklistType) {
		assertFalse(worklistPage.isWorklistPresent(worklistName, worklistType));
	}

	@When("I refresh the page")
	public void refreshPage() {
		worklistPage.refreshPage();
	}

	@When("I click on {string} tab on Properties dialog")
	public void clickWorklistPropertyTab(String tabName) {
		worklistPage.clickWorklistPropertyTab(tabName);
	}

	@When("I uncheck 24x7 option")
	public void clickActiveTimeCheckbox() {
		worklistPage.clickActiveTimeCheckbox();
	}

	@When("I click Add new active time icon on Properties dialogue")
	public void clickActiveNewTimeIcon() {
		worklistPage.clickActiveNewTimeIcon();
	}

	@When("I select {string} as day")
	public void selectActiveTimeDay(String day) {
		worklistPage.selectActiveTimeDay(day);
	}

	@When("I set Active Time of day")
	public void setActiveTimeRange() {
		worklistPage.setActiveTimeRange("From", 60);
		worklistPage.setActiveTimeRange("To", 70);
	}

	@Then("I delete added Active Time for {string}")
	public void deleteAddedActiveTime(String day) {
		worklistPage.deleteAddedActiveTime(day);
	}

	@Then("I verify the worklist {string} tree count is enabled with exam count {int}")
	public void i_verify_the_worklist_tree_count_is_with_exam_count(String worklistName, Integer examCount) {
		sleep(60, TimeUnit.SECONDS);
		assertEquals(new Integer(worklistPage.getWorklistTreeCount(worklistName)), examCount);
	}
	
	@Then("{string} exam details are displayed in Peer Review window number {int}")
	public void verifyPatientLoadedInPeerReviewWindow(String patientName,int windowNumber) {
		assertTrue(worklistPage.verifyPatientLoadedInPeerReviewWindow(patientName,windowNumber));
		Log.printInfo(patientName + "is displayed in Peer Review window");
	}
	
	@When("I confirm Delete worklist")
	public void confirmDeleteWorklist() {
		worklistPage.confirmDeleteWorklist();
	}
	
	@Then("{string} worklist is not visible under {string}")
	public void worklist_is_removed_in(String worklistName, String worklistType) {
		assertFalse(worklistPage.isWorklistPresent(worklistName, worklistType));
	}
	
	@When("I click on create {string} button on Worklist tab")
	public void createQueueButtons(String buttonName) {
		worklistPage.clickCreateQueueQueueButton(buttonName);
	}
	@When("I select {string} worklist under {string} worklist from Queue dialog")
	public void expandCreateQueWorklist(String subWorklistName, String worklistName) {
		worklistPage.selectWorklistFromCreateQueue(subWorklistName, worklistName);
	}
	
	@When("I switch to {string} tab in window number {int}")
	public void switchToPage(String tabName, int tabNumber) {
		worklistPage.switchToTab(tabName,tabNumber);
	}
	
	@When("I click {string} shift button")
	public void shiftButton(String shift) {
		worklistPage.shiftButton(shift);
	}
	
	@When("I right click {string}")
	public void rightClickQueue(String shift) {
		worklistPage.rightClickOnQueue(shift);
	}
	
	@When("{string} is not visible")
	public void queueIsNotVisible(String queue) {
		assertFalse(worklistPage.queueName(queue));
	}
	
	@When("I click {string}")
	public void clickQueue(String shift) {
		worklistPage.clickOnQueue(shift);
	}
	
	@When("I click on {string} button from My Support Queue Properties")
	public void clickPropertiesQueueButton(String buttonName) {
		worklistPage.clickPropetiesQueueButton(buttonName);
	}
	
	@When("I click Queue Menu Option {string}")
	public void clickQueueOption(String menuOption) {
		worklistPage.clickQueueMenuOption(menuOption);
	}
	
	@Then("I verify properties dialogue is closed")
	public void verifyPropertiesDialogueIsClosed() {
		assertFalse(worklistPage.verifyPropertiesDialogue());
	}
	
	@When("I click Status checkbox")
	public void clickStatusCheckbox() {
		worklistPage.clickStatusCheckbox();
	}
	
	@When("I click Wrap content checkbox")
	public void wrapContent() {
		worklistPage.wrapContent();
	}
	
	@When("I uncheck Status checkbox")
	public void uncheckStatusCheckbox() {
		worklistPage.uncheckStatusCheckbox();
	}
	
	@When("I uncheck Wrap content checkbox")
	public void uncheckWrapContent() {
		worklistPage.uncheckWrapContent();
	}
	
	@Then("I verify Status checkbox is selected")
	public void isStatusCheckboxSelected() {
		assertTrue(worklistPage.isStatusCheckboxSelected());
	}
	
	@Then("I verify Wrap Content checkbox is selected")
	public void isWrapContentCheckBoxSelected() {
		assertTrue(worklistPage.isWrapContentCheckBoxSelected());
	}
	
	@Then("I verify Status checkbox is not checked")
	public void statusCheckboxIsNotSelected() {
		assertFalse(worklistPage.isStatusCheckboxSelected());
	}
	
	@Then("I verify Wrap Content checkbox is not checked")
	public void wrapContentCheckBoxIsNotSelected() {
		assertFalse(worklistPage.isWrapContentCheckBoxSelected());
	}
	
	@When("I click delete icon for Timeshift Queue")
	public void clickTimeShiftQueue() {
		worklistPage.timeShiftDeleteIcon();
	}
	
	@When("I click Subscribe button {string}")
	public void clickSubscribeButtonQueue(String button) {
		worklistPage.clickSubscribeButtonQueue(button);
	}
	
	@Then("{string} is visible")
	@Then("{string} message is displayed")
	@Then("{string} button is displayed")
	@Then("{string} is visible on auto next page")
	public void messageDisplayed(String message) {
		assertTrue(worklistPage.displayedString_Xpath(message));
	}
	
	@When("I verify the checkbox {string} is not selected")
	public void checkBoxItemNotSelected(String checkboxName) {
		assertFalse(worklistPage.checkBoxItemSelected(checkboxName));
	}
	
	@When("I verify the checkbox {string} is selected")
	public void checkBoxItemIsSelected(String checkboxName) {
		assertTrue(worklistPage.checkBoxItemSelected(checkboxName));
	}
	
	@When("I click Edit My Support Queue refresh button")
	public void clickMyQueueRefreshIcon() {
		worklistPage.clickMyQueueRefreshIcon();	
	}
	
	@When("I select shift {string} from Edit My Support Queue dialogue box")
	public void selectDropDownItem(String shift) {
			worklistPage.selectShiftDropdown(shift);
	}
	
	@When("I click on {string} button on Create My Support Queue dialog")
	public void clickCreateSupportQueueButton(String buttonName) {
		worklistPage.clickCreateSupportQueueButton(buttonName);
	}

	@When("I check {string} worklist checkBox under {string} worklist from Queue dialog")
	public void checkQueueWorklist(String subWorklistName, String worklistName) {
		worklistPage.checkWorklistCheckBoxFromCreateQueue(subWorklistName, worklistName);
	}
	
	@When("I uncheck {string} worklist checkBox under {string} worklist from Queue dialog")
	public void uncheckQueueWorklist(String subWorklistName, String worklistName) {
		worklistPage.uncheckWorklistCheckBoxFromCreateQueue(subWorklistName, worklistName);
	}
	
	@When("I expand {string} worklist node")
	public void expandWorklistNode(String worklistName) {
		worklistPage.expandWorklistNode(worklistName);
	}
	
	@When("I expand History panel")
	public void expandHistoryPanel() {
		worklistPage.expandHistoryPanel();
	}
	
	@When("I collapse History panel")
	public void collapseHistoryPanel() {
		worklistPage.collpaseHistoryPanel();
	}
	
	@Then("{string} under My Reading Queue is {string}")
	public void isMRQWorklistActivated(String worklistName, String status) {
		if (status.equals("de-activated")) {
			assertTrue(worklistPage.isMRQWorklistActivated(worklistName));
		} else {
			assertFalse(worklistPage.isMRQWorklistActivated(worklistName));
		}
	}
	
	@Then("I verify the worklist {string} tree count is disabled")
	public void i_verify_the_worklist_tree_count_is(String worklistName) {
		sleep(60, TimeUnit.SECONDS);
		assertEquals(worklistPage.getWorklistTreeCount(worklistName), "");
	}

	@When("I de-select {string} worklist under {string} worklist from Create My Reading Queue dialog")
	public void i_de_select_worklist_under_worklist_from_Create_My_Reading_Queue_dialog(String subWorklistName, String worklistName) {
		worklistPage.deselectWorklistFromCreateMRQ(subWorklistName, worklistName);
	}
	
	@Then("Assigned icon is not displayed for exam {string}")
	public void isAssignedIconDisplayed1(String accesssionNo) {
		assertFalse(worklistPage.isAssignedIconDisplayed(accesssionNo));
	}
	
	@Then("Assigned icon tooltip with {string} value {string} is not displayed")
	public void isAssignedTooltipDisplayed1(String tooltipLabel, String tooltipMessage) {
		assertFalse(worklistPage.isAssignedTooltipPresent(tooltipLabel,tooltipMessage));
	}
	
	@Then("Assigned icon is displayed for exam {string}")
	public void isAssignedIconDisplayed(String accesssionNo) {
		assertTrue(worklistPage.isAssignedIconDisplayed(accesssionNo));
	}
	
	@Then("Assigned icon tooltip with {string} value {string} is displayed")
	public void isAssignedTooltipDisplayed(String tooltipLabel, String tooltipMessage) {
		assertTrue(worklistPage.isAssignedTooltipPresent(tooltipLabel,tooltipMessage));
	}
	
	@When("I hover on Assigned icon for exam {string}")
	public void mouseHoverAssignedIcon(String accessionNo) {
		worklistPage.mouseHoverAssignedIcon(accessionNo);
	}
	
	@When("I double-click on {string} exam from history panel")
	public void doubleClickExamInHistoryPanel(String accessionNo) {
		worklistPage.doubleClickExamInHistoryPanel(accessionNo);
	}
	
	@When("I click {string} icon for the exam")
	public void clickWorklistGridExamIcon(String iconName) {
		worklistPage.clickWorklistGridExamIcon(iconName);
	}
	
	@When("I double-click on {string} exam")
	public void doubleClickOnExamFromWorklist(String exam) {
		worklistPage.doubleClickOnExamFromWorklist(exam);
	}
	
	@When("I click on {string} tab in Worklist page")
	public void clickWorklistPageTab(String tabName) {
		worklistPage.clickWorklistPageTab(tabName);
	}
	
	@When("I click on {string} icon under folder tab")
	public void clickFolderToolbarActionButton(String buttonName) {
		worklistPage.clickFolderToolbarOption(buttonName);
	}

	@When("I set {string} as new folder name")
	public void setFolderName(String folderName) {
		worklistPage.setFolderName(folderName);
	}

	@When("I click on {string} button on Create Folder dialog")
	public void clickCreateFolderActionButton(String buttonName) {
		worklistPage.clickCreateFolderActionButton(buttonName);
	}

	@Then("{string} folder is created")
	@Then("{string} sub-folder is created")
	public void isFolderVisible(String folderName) {
		assertTrue(worklistPage.isFolderVisible(folderName));
	}

	@When("I set {string} as Title to assign exam to folder")
	public void setTitleInAssignExamToFolder(String title) {
		worklistPage.setAssignExamToFolderTitle(title);
	}

	@When("I set {string} as Note Message to assign exam to folder")
	public void setMessageInAssignExamToFolder(String message) {
		worklistPage.setAssignExamToFolderMessage(message);
	}

	@When("I check {string} checkbox to assign exam to folder")
	public void selectFolderToAssignExam(String folderName) {
		worklistPage.clickAssignExamToFolderCheckbox(folderName);
	}

	@When("I click on {string} button on Assign Exam to Folder dialog")
	public void i_click_on_button_on_Assign_Exam_to_Folder_dialog(String buttonName) {
		worklistPage.clickAssignExamToFolderButton(buttonName);
	}

	@When("I click on {string} folder")
	public void i_click_on_folder(String folderName) {
		worklistPage.clickOnFolderName(folderName);
	}
	
	@When("I open context menu of {string} folder")
	public void openFolderContextMenu(String folderName) {
		worklistPage.openFolderContextMenu(folderName);
	}

	@When("I select {string} option from context menu")
	public void selectContextMenuOption(String option) {
		worklistPage.selectContextMenuOption(option);
	}

	@When("I set {string} as name in Create Sub-Folder dialog")
	public void setSubFolderName(String subFolderName) {
		worklistPage.setSubFolderName(subFolderName);
	}

	@When("I click on {string} button on Create Sub-Folder dialog")
	public void clickCreateSubFolderActionButton(String buttonName) {
		worklistPage.clickCreateSubFolderActionButton(buttonName);
	}

	@When("I select {string} option to delete all folders")
	public void i_select_option_to_delete_all_folders(String option) {
		worklistPage.clickDeleteFoldersConfirmationOption(option);
	}

	@Then("{string} sub-folder is deleted from parent folder")
	@Then("{string} folder is deleted")
	public void isSubFolderDeleted(String folderName) {
		assertFalse(worklistPage.isFolderVisible(folderName));
	}	
}
