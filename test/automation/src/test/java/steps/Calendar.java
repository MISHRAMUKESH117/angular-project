package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class Calendar {
	
	TestContext context;
	pages.Schedule schedulePage; 
	
	public Calendar(TestContext context) {
		this.context = context;
		schedulePage = new pages.Schedule(context.getDriver());
	}

	@When("I click on date to create event")
	public void i_click_on_date_to_create_event() {
		schedulePage.clickOnDate();
	}

	@When("I select {string}")
	public void i_select(String checkbox) {
		schedulePage.selectEventTime(checkbox);
	}
	
	@When("I click on {string}")
	public void i_click_on_save_event(String button) {
		schedulePage.clickEventButton(button);
	}

	@Then("event is created")
	public void event_is_created() {
		assertEquals(context.get("Title"), schedulePage.isEventCreated());     
	}

	@When("I select {string} from list")
	public void i_select_from_list(String textbox) {
		schedulePage.selectRadiologist(textbox);
	}
	
	@When("create New event with following details")
	public void create_New_event_with_following_details(DataTable newEventDetails) {
		List<Map<String, String>> eventDetails = newEventDetails.asMaps();
		for (Map<String, String> eventDetail : eventDetails) {
			schedulePage.createNewEvent("title",eventDetail.get("Title"));
			schedulePage.selectValueFromDropdownForEvent("Event Type",eventDetail.get("Event Type"));
			schedulePage.selectEventTime(eventDetail.get("Event Time"));			
			schedulePage.createNewEvent("location",eventDetail.get("Location"));
			schedulePage.createNewEvent("note",eventDetail.get("Notes"));
			schedulePage.selectValueFromDropdownForEvent("Chat status during meeting",eventDetail.get("Chat Status"));	
			context.set("Title", eventDetail.get("Title"));
		}
	}
	
	@When("I click on delete to delete a event")
	public void i_click_on_delete_to_delete_a_event() {
		schedulePage.deleteEvent();
	}

	@Then("event is deleted")
	public void event_is_deleted() {
		assertTrue(schedulePage.isEventDeleted());
	}
}
