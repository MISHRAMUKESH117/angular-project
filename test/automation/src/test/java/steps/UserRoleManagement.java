package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class UserRoleManagement {

	pages.UserRoleManagement userRoleManagementPage;
	TestContext context;

	public UserRoleManagement(TestContext context) {
		this.context = context;
		userRoleManagementPage = new pages.UserRoleManagement(context.getDriver());
	}

	@When("create New Role with following details")
	public void create_new_role_with_following_details(DataTable newUserRoleDetails) {
		List<Map<String, String>> userRoleDetails = newUserRoleDetails.asMaps();

		for (Map<String, String> userRoleDetail : userRoleDetails) {
			userRoleManagementPage.clickAddUserRoleButton();
			userRoleManagementPage.setUserRoleName(userRoleDetail.get("Role name"));
			userRoleManagementPage.setUserRoleDescription(userRoleDetail.get("Role Description"));
			userRoleManagementPage.clickPermissionsCheckbox(userRoleDetail.get("Permission Type"),
					userRoleDetail.get("Permission Name"));
			userRoleManagementPage.clickSaveNewRoleButton();
		}
	}

	@Then("new user role {string} is added")
	@Then("User Role Management {string} is present")
	public void new_user_role_something_is_added(String userRoleName) {
		assertTrue(userRoleManagementPage.userRoleDoesNotExists());
		assertTrue(userRoleManagementPage.isUserRoleVisible(userRoleName));
		Log.printInfo("New user role:- " + userRoleName + " is added.");
	}

	@When("I select {string} user role")
	public void user_selects_something_user_role_and_clicks_on_edit_icon(String userRoleName) {
		userRoleManagementPage.clickUserRoleInGrid(userRoleName);
	}

	@When("I click on Edit user role icon")
	public void i_click_on_edit_user_role_icon() {
		userRoleManagementPage.clickEditUserRoleButton();
	}

	@When("I set new user role name {string}")
	public void sets_new_user_role_name_something(String userRoleName) {
		userRoleManagementPage.setUserRoleName(userRoleName);
	}

	@When("click on save button")
	public void click_on_save_button() {
		userRoleManagementPage.clickSaveNewRoleButton();
	}

	@When("I click on number of Users hyperlink of user role {string}")
	public void i_click_on_number_of_users_hyperlink_of_user_role_something(String userRoleName) {
		userRoleManagementPage.numberOfUsersHyperlink(userRoleName);
	}

	@When("set user name {string} to search")
	public void set_user_name_something_to_search(String userName) throws Throwable {
		userRoleManagementPage.setUserNameToSearch(userName);
	}

	@When("I click on search button to search the users")
	public void i_click_on_search_button_to_search_the_users() {
		userRoleManagementPage.clickSearchButtonOnUsersDialougeBox();
	}

	@When("I select the user {string}")
	public void i_select_the_user_something(String userName) {
		userRoleManagementPage.selectUsersFromUsersDialougeBox(userName);
	}

	@When("click on Save button on Users dialouge box")
	public void click_on_save_button_on_users_dialouge_box() {
		userRoleManagementPage.clickSaveButtonOnUsersDialougeBox();
	}

	@Then("user name {string} is added")
	public void user_name_something_is_added(String userName) {
		assertTrue(userRoleManagementPage.isUserNameSelected(userName));
		Log.printInfo("User " + userName + " is selected.");
	}

	@When("I deselect the user {string}")
	public void i_deselect_the_user_something(String userName) {
		userRoleManagementPage.deSelectUsersFromUsersDialougeBox(userName);
	}

	@Then("user name {string} is removed")
	public void user_name_something_is_removed(String userName) {
		assertTrue(userRoleManagementPage.isUserNameNotSelected(userName));
		Log.printInfo("User " + userName + " is selected.");
	}

	@When("I click in Delete user role icon")
	public void i_click_in_delete_user_role_icon() {
		userRoleManagementPage.clickDeleteUserRoleButton();
		userRoleManagementPage.clickDeleteUserRolePopUpYesButton();
	}
	
	@Then("user role {string} is deleted")
    public void user_role_something_is_deleted(String userRole) {
		assertTrue(userRoleManagementPage.isUserRoleDeleted(userRole));
		Log.printInfo("User Role:- " + userRole + " is deleted.");
    }
	
	@When("I set the permission as follows")
	public void search_userRole_setPermissions(DataTable existingUserDetails) {
		List<Map<String, String>> existUserDetails = existingUserDetails.asMaps();
		
		for (Map<String, String> existUserDetail : existUserDetails) {
			userRoleManagementPage.clickPermissionsCheckbox(existUserDetail.get("Permission Type"),
					existUserDetail.get("Permission Name"));			
		}
	}
	
	@When("I search for {string} on User Role Management Page")
	public void i_search_page_for_user_role_managment(String input) {
		userRoleManagementPage.setFilterValue(input);
	}
	
	
	@Then("User Role Management contains {string}")
	public void userRoleContains(String userRoleName) {
		assertTrue(userRoleManagementPage.userRoleContains(userRoleName));
		Log.printInfo("New user role:- " + userRoleName + " is added.");
	}
}
