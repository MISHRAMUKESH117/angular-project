package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class ContactManagement {

	TestContext context;
	pages.ContactManagement contactManagementPage;

	public ContactManagement(TestContext context) {
		this.context = context;
		contactManagementPage = new pages.ContactManagement(context.getDriver());
	}

	@When("I enter {string} as {string}")
	public void i_enter_as_last_name(String contactName, String textbox) {
		contactManagementPage.enterContactDetails(contactName, textbox);
	}

	@When("I click on {string} to button to search contact")
	@When("I click on {string} button to start merge")
	@When("I click to {string} button to delete a contact")
	public void i_click_on_search_to_button_to_search_contact(String button) {
		contactManagementPage.clickContactButton(button);
	}

	@Then("{string} search results are displayed")
	public void search_results_are_displayed(String contactName) {
		assertTrue(contactManagementPage.isSearchSuccessfull(contactName));
	}

	@When("I click on create new contact icon")
	public void i_click_on_create_new_contact_icon() {
		contactManagementPage.clickCreateNewContact();
	}

	@When("I create new contact with following details")
	public void i_create_new_contact_with_following_details(DataTable newUserDetails) {
		List<Map<String, String>> userDetails = newUserDetails.asMaps();
		for (Map<String, String> userDetail : userDetails) {
			contactManagementPage.createNewContact(userDetail.get("First Name"), "firstName");
			contactManagementPage.createNewContact(userDetail.get("Last Name"), "lastName");
			contactManagementPage.createNewContact(userDetail.get("Mobile Number"), "phoneNumber");
			contactManagementPage.createNewContact(userDetail.get("Type"), "phoneType");
			contactManagementPage.createNewContact(userDetail.get("Email"), "email");
			context.set("ContactEmail", userDetail.get("Email"));
		}
	}

	@When("I click on {string} button to save contact")
	@When("I click on {string} button to merge contacts")
	public void i_click_on_button_to_save_contact(String button) {
		contactManagementPage.clickSaveContact(button);
	}

	@Then("{string} contact is created successfully")
	public void contact_is_created_successfully(String contactName) {
		assertTrue(contactManagementPage.isContactCreated(contactName));
	}

	@When("I open {string} contact")
	public void i_open_contact(String contactName) {
		contactManagementPage.openContactToEdit(contactName);
	}

	@When("I edit following details")
	public void i_edit_following_details(io.cucumber.datatable.DataTable editUserDetails) {
		List<Map<String, String>> userDetails = editUserDetails.asMaps();
		for (Map<String, String> userDetail : userDetails) {
			contactManagementPage.createNewContact(userDetail.get("Birth Date"), "birthDate");
			contactManagementPage.editDetailsFromDropdown(userDetail.get("Communication Preference"), "communicationPreferenceID");
			contactManagementPage.editDetailsFromDropdown(userDetail.get("Groups"), "entityGroup");
			context.set("CommunicationPreference", userDetail.get("Communication Preference"));
		}
	}

	@Then("{string} contact is updated successfully")
	public void contact_is_updated_successfully(String string) {
		assertEquals(contactManagementPage.isContactEdit(), context.get("CommunicationPreference"));
	}

	@When("I select {string} and {string} contacts to merge")
	public void i_select_and_contacts_to_merge(String firstContact, String secondContact) {
		contactManagementPage.clickContactToMerge(firstContact, secondContact);
	}

	@Then("{string} contact is merged")
	public void contacs_are_merged(String contactName) {
		assertTrue(contactManagementPage.isContactMerged(contactName,context.get("ContactEmail")));
	}

	@When("I select {string} contact to delete")
	public void i_select_contact_to_delete(String contactName) {
		contactManagementPage.selectContactToDelete(contactName);
	}

	@When("I click on {string} icon to edit contact")
	public void i_click_to_button_to_edit_a_contact(String button) {
		contactManagementPage.clickEditContactButton(button);
	}

	@When("I click on {string} confirmation button")
	public void i_click_on_confirmation_button(String button) {
		contactManagementPage.clickDeleteConfirmation(button);
	}

	@Then("{string} contact is deleted")
	public void contact_is_deleted(String contactName) {
		assertTrue(contactManagementPage.isContactDeleted(contactName));
	}

	@When("I select {string} as site")
	public void i_select_as_site(String siteName) {
		contactManagementPage.enterSite(siteName);
	}

	@When("I click on {string} button to export file")
	@When("I click on {string} button to import file")
	public void i_click_on_button_to_export_file(String button) {
		contactManagementPage.clickExportImportButton(button);
	}

	@When("I select {string} option to export")
	@When("I select {string} option to import")
	public void i_select_to_export(String exportDropdownValue) {
		contactManagementPage.selectExportImportDropdownOption(exportDropdownValue);
	}

	@Then("{string} file is downloaded with {string} sheet")
	public void file_is_downloaded(String fileName, String sheetName) {
		assertTrue(contactManagementPage.isExcelSheetDownlaoded(fileName, sheetName));
	}

	@When("I edit {string} site excel file")
	public void i_edit_Hospital_B_site_excel_file(String fileName) {
		contactManagementPage.writeDataInExcel(fileName);
	}

	@When("I select {string} file to import")
	public void i_select_file_to_import(String fileName) {
		contactManagementPage.uploadFile(fileName);
	}

	@Then("file is imported with {string}")
	public void file_is_imported_with(String contactName) {
		assertTrue(contactManagementPage.isExcelSheetUploaded(contactName));
	}

	@When("I click on Edit Site icon")
	public void i_click_on_Edit_Site_icon() {
		contactManagementPage.clickEditSiteIcon();
	}

	@When("I set {string} as Note in Edit Contact")
	@When("I set {string} as Note in Site Details")
	public void i_set_as_Note_in_Site_Details(String text) {
		contactManagementPage.setExamNoteText(text);
	}

	@When("I add Exam Notes under Edit Contact window with below values")
	@When("I add Exam Notes under Edit Site Details window with below values")
	public void i_add_Exam_Notes_under_Edit_Site_Details_with_below_values(DataTable selectExamNoteValues) {
		List<Map<String, String>> selectExamNoteValue = selectExamNoteValues.asMaps();
		for (Map<String, String> selectValue : selectExamNoteValue) {
			contactManagementPage.selectExamNoteDropdownValue(selectValue.get("Label"), selectValue.get("Value"));
		}
	}

	@When("I click on plus icon to add Exam Note")
	public void i_click_on_plus_icon_to_addExam_Note() {
		contactManagementPage.clickAddExamNoteIcon();
	}

	@When("I click on {string} button to save Site Details")
	public void i_click_on_button_to_save_Site_Details(String button) {
		contactManagementPage.clickContactSiteWindowButton(button);
	}

	@Then("{string} is added under site")
	public void is_added_under_site(String value) {
		assertTrue(contactManagementPage.isValuePresentUnderSiteInformationPanel(value));
	}

	@When("I delete {string} from Edit Contact window")
	@When("I delete {string} from Site Edit Details window")
	public void i_delete_from_Site_Edit_Details_window(String text) {
		contactManagementPage.clickNoteDeleteIcon(text);
	}

	@Then("{string} is deleted from site")
	public void is_deleted_from_site(String value) {
		assertFalse(contactManagementPage.isValuePresentUnderSiteInformationPanel(value));
	}
	
	@When("I open contact by site name {string}")
	public void i_open_contact_by_site_name(String siteName) {
		contactManagementPage.openContactBySiteName(siteName);
	}	

	@Then("{string} is added in contact")
	public void is_added_in_contact(String value) {
		assertTrue(contactManagementPage.isValuePresentUnderContactInformationPanel(value));
	}

	@Then("{string} is deleted from contact")
	public void is_deleted_from_contact(String value) {
		assertFalse(contactManagementPage.isValuePresentUnderContactInformationPanel(value));
	}
}
