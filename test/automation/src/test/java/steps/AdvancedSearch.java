package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import Actions.Robo;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class AdvancedSearch {
    pages.AdvancedSearch advancedSearchPage;
    TestContext context;
    Robo robotActions;

    public AdvancedSearch(TestContext context) {
        this.context = context;
      
        advancedSearchPage = new pages.AdvancedSearch(context.getDriver());
    }

    @Then("Advanced Search is launched")
    public void isAdvancedSearchLaunched() {
        assertTrue(advancedSearchPage.isAdvancedSearchLaunched());
    }

    @When("I enter the details for Exam Advanced Search")
    public void examAdvancedSearch(DataTable advanceSearch) {
        List < Map < String, String >> advanceSearches = advanceSearch.asMaps();
        for (Map < String, String > search: advanceSearches) {
            String label = search.get("Label");
            String value = search.get("Value");
            String subModuleEnclosure = search.get("Enclosure");
            advancedSearchPage.setExamAdvancedSearch(subModuleEnclosure, label, value);
        }
    }

    @When("I enter the details for Communication Advanced Search")
    public void commAdvancedSearch(DataTable advanceSearch) {
        List < Map < String, String >> advanceSearches = advanceSearch.asMaps();
        for (Map < String, String > search: advanceSearches) {
            String label = search.get("Label");
            String value = search.get("Value");
            String subModuleEnclosure = search.get("Enclosure");
            advancedSearchPage.setCommAdvancedSearch(subModuleEnclosure, label, value);
        }
    }

    @When("I enter the details for Task Advanced Search")
    public void taskAdvancedSearch(DataTable advanceSearch) {
        List < Map < String, String >> advanceSearches = advanceSearch.asMaps();
        for (Map < String, String > search: advanceSearches) {
            String label = search.get("Label");
            String value = search.get("Value");
            String subModuleEnclosure = search.get("Enclosure");
            advancedSearchPage.setTaskAdvancedSearch(subModuleEnclosure, label, value);
        }
    }

    @When("I enter the details for Peer Review Advanced Search")
    public void peerReviewAdvancedSearch(DataTable advanceSearch) {
        List < Map < String, String >> advanceSearches = advanceSearch.asMaps();
        for (Map < String, String > search: advanceSearches) {
            String label = search.get("Label");
            String value = search.get("Value");
            String subModuleEnclosure = search.get("Enclosure");
            advancedSearchPage.setPeerReviewAdvancedSearch(subModuleEnclosure, label, value);
        }
    }

    @When("I enter the details for Tech QA Advanced Search")
    public void techQAAdvancedSearch(DataTable advanceSearch) {
        List < Map < String, String >> advanceSearches = advanceSearch.asMaps();
        for (Map < String, String > search: advanceSearches) {
            String label = search.get("Label");
            String value = search.get("Value");
            String subModuleEnclosure = search.get("Enclosure");
            advancedSearchPage.setTechQAAdvancedSearch(subModuleEnclosure, label, value);
        }
    }

    @When("I enter the details for Teaching Note Advanced Search")
    public void teachingNoteAdvancedSearch(DataTable advanceSearch) {
        List < Map < String, String >> advanceSearches = advanceSearch.asMaps();
        for (Map < String, String > search: advanceSearches) {
            String label = search.get("Label");
            String value = search.get("Value");
            String subModuleEnclosure = search.get("Enclosure");
            advancedSearchPage.setTeachingNoteAdvancedSearch(subModuleEnclosure, label, value);
        }
    }

    @When("I enter the details for Analytics Advanced Search")
    public void analyticsAdvancedSearch(DataTable advanceSearch) {
        List < String > values = context.get("Modality");
        List < Map < String, String >> advanceSearches = advanceSearch.asMaps();
        for (Map < String, String > search: advanceSearches) {
            String label = search.get("Label");
            String subModuleEnclosure = search.get("Enclousre");
            if (label.contentEquals("Modality")) {
                String value1 = search.get("Value");
                if (values.contains(value1)) {
                    advancedSearchPage.setExamAdvancedSearch(subModuleEnclosure, label, search.get("Abbreviations"));
                }
            } else {
                String value2 = search.get("Value");
                advancedSearchPage.setExamAdvancedSearch(subModuleEnclosure, label, value2);
            }
        }
    }

    @When("I select Advanced Search module {string}")
    public void setAdvanceSearchType(String module) {
        advancedSearchPage.selectAdvancedSearchType(module);
        advancedSearchPage.expandAdvancedSearchType(module);
    }

    @When("I click on {string} action")
    public void iClickOnAction(String action) {
        advancedSearchPage.actionAdvanceSearch(action);
    }

    @Then("result count is displayed")
    public void countResults() {
        context.set("advanceSearchCount",advancedSearchPage.countResults());
    }

    @Then("I check the result")
    public void countResultsFromAnalytics() {
        Integer countFromAnalyticsGraph = context.get("graphCount");
        Integer countFromAdvancedSearch = advancedSearchPage.countResults();
        assertEquals(countFromAnalyticsGraph, countFromAdvancedSearch);
    }

    @When("I clear results")
    public void clearResults() {
        advancedSearchPage.clearResults();
    }

    @Then("No records are displayed")
    public void isClearResults() {
        advancedSearchPage.verifyCount(0);
    }

    @Then("exam with note is not loaded")
    public void exam_with_teaching_note_is_not_loaded() {
        assertTrue(advancedSearchPage.isExamFromAdvanceSearchPresent());
    }

    @When("I enter {string} in keyword textbox of {string}")
    public void i_enter_in_textbox(String message, String noteType) {
        advancedSearchPage.enterValueInKeyword(message, noteType);
    }

    @When("I click on {string} to search results")
    public void i_click_on_to_search_results(String button) {
        advancedSearchPage.clickAdvanceSearchButton(button);
    }

    @Then("verify count of rows displayed is {string}")
    public void verify_count_of_rows(String count) {
        advancedSearchPage.verifyCount(Integer.parseInt(count));
    }
    
    @When("I perform {string} Advanced Search with following details")
	public void i_perform_Advanced_Search_with_following_details(String sectionName, DataTable searchValues) {
		List<Map<String, String>> searchValue = searchValues.asMaps();
		for (Map<String, String> value : searchValue) {			
			advancedSearchPage.selectMultiChoiceDropdownValue(sectionName, value.get("Label"),value.get("Value"));			
		}
	}

	@When("I set {string} Advanced Search with below details")
	public void i_set_Advanced_Search_with_below_details(String sectionName, DataTable searchValues) {
		List<Map<String, String>> searchValue = searchValues.asMaps();
		for (Map<String, String> value : searchValue) {			
			advancedSearchPage.setTextValue(sectionName, value.get("Label"),value.get("Value"));			
		}
	}
	
	@When("I click on {string} dropdown on Advanced Search Page")
	public void open_dropDownLabel(String label) {
		advancedSearchPage.clickDropDownLabel(label);
	}
	
	@And("I search for {string} in dropdown on Advanced Search Page")
	public void item_search_dropdown(String dropdownItem) {
		advancedSearchPage.enterDropDownOption(dropdownItem);
	}
	
	@And("I click {string} button on Advanced Search Page dropdown")
	public void click_okReset_button(String button) {
		advancedSearchPage.clickOkResetButton(button);	
	}
	
	@Then("{string} is visible in text box")
	public void verify_item_added(String searchValue) {
		assertTrue(advancedSearchPage.isSelectedCheckBoxItemVisible(searchValue));
	}
	
	@When("I click {string} button on Advanced Search dropdown")
	public void click_page_button(String button) {
		advancedSearchPage.clickDropdownPageButton(button);
	}
	
	@Then("I verify {string} is visible in first row of dropdown")
	public void item_firstrow_dropdown(String value) {
		assertTrue(advancedSearchPage.isItemInFirstRowSelectedVisible(value));
	}
	
	@Then("Error Message {string} is displayed")
	public void verify_error_advanced_search(String message) {
		assertTrue(advancedSearchPage.advancedSearchErrorMessage(message));
		assertTrue(advancedSearchPage.advancedSearchErrorBox());
	}
    
	@When("I perform {string} Advanced Search with below details")
	public void i_perform_Advanced_Search_with_below_details(String sectionName, DataTable searchValues) {
		List<Map<String, String>> searchValue = searchValues.asMaps();
		for (Map<String, String> value : searchValue) {			
			advancedSearchPage.selectSingleChoiceDropdownValue(sectionName, value.get("Label"),value.get("Value"));			
		}
	}

	@When("I select {string} as date range in Exam Date on the {string} Advanced Search page")
	public void i_select_as_Exam_date_in_Advanced_Search(String examDateRange,String subModule) {
		advancedSearchPage.selectExamDateRange(examDateRange,subModule);
		assertTrue(advancedSearchPage.isPopulatedFromFieldCheck(subModule));
	}
	
	@When("Search Results are displayed on WorkList Page")
	public void search_results_verification() {
		context.set("searchResultCount", advancedSearchPage.getResultCount("Search Results"));
		assertTrue(advancedSearchPage.verifyResultString("Search Results"));
	}
	
	@When("Search Results Count are displayed on WorkList Page")
	public void count_results_verification() {
		context.set("countResults", advancedSearchPage.getResultCount("Search Results Count"));
		assertTrue(advancedSearchPage.verifyResultString("Search Results Count"));
	}
	
	@When("I compare the results of Search and Count")
	public void compare_search_count() {
		assertTrue(context.get("searchResultCount").equals(context.get("countResults")));
	}
	
	@When("I select the date as {int} and I set the month to {int} months ago from todays date in the From Calendar")
	public void selectFromCalendarDate(int date,int numberOfMonths) {
		advancedSearchPage.clickFromCalenderMonthBackButton(numberOfMonths);
		advancedSearchPage.clickFromCalendarDate(date);
	}
	
	@When("I click the {string} calender icon")
	public void clickCalanderIcon(String calendarType) {
		advancedSearchPage.clickCalendarIcon(calendarType);
	}

	@When("I click Today button on the {string} Calendar")
	public void clickDateToCalendar(String calendarType) {
		advancedSearchPage.clickCalendarTodayButton(calendarType);
	}
	
	@When("I select the time {string} in the {string} Calendar section")
	public void selectTimeToCalendar(String time,String calendarType) {
		advancedSearchPage.selectTime(time,calendarType);
	}
	
	@When("I remove the date in the {string} Calendar section")
	public void clearExamDateField(String calendarType) {
		advancedSearchPage.clearExamDateField(calendarType);
	}
	
	@Then("I verify the {string} calendar date field date is {int} and the month is {int} months ago")
	public void verifyDateFieldValueMatchesExpectedDate(String calendarType,int date, int month) {
		assertTrue(advancedSearchPage.dateFieldValueMatchesExpectedDate(calendarType,date,month));
	}
	
	@Then("I verify the {string} calendar date field is set to todays date")
	public void verifyToCalendarIsDateFieldTodaysDate(String calendarType) {
		assertTrue(advancedSearchPage.isDateFieldTodaysDate(calendarType));
	}
	
	@Then("I verify the {string} calendar time field is {string} is visible")
	public void verifyCalendarTimeFieldIsPopulated(String calendarType,String time) {
		assertTrue(advancedSearchPage.isTimeFieldPresent(calendarType,time));
	}
	
	@Then("I verify the {string} calendar date field is empty")
	public void verifyCalendarDateFieldIsNotPopulated(String calendarType) {
		assertTrue(advancedSearchPage.isDateFieldEmpty(calendarType));
	}
	
	@Then("I verify the {string} calendar time field is empty")
	public void verifyCalendarTimeFieldIsNotPopulated(String calendarType) {
		assertTrue(advancedSearchPage.isTimeFieldEmpty(calendarType));
	}
	
}