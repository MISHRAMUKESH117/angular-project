package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import setterGetter.TestContext;
import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Notes {

	pages.Notes notesPage;
	TestContext context;

	public Notes(TestContext context) {
		this.context = context;
		notesPage = new pages.Notes(context.getDriver());
	}

	@When("I click on {string} note icon")
	public void i_click_on_note_icon(String noteType) {
		notesPage.clickOnNoteType(noteType);
	}

	@When("I add {string} in note box")
	public void i_add_in_note_box(String text) {
		notesPage.addNote(text);
		context.set("NoteText", text);
	}

	@When("I click on {string} button to add note")
	public void i_click_on_button_to_add_note(String noteEditAction) {
		notesPage.saveNote(noteEditAction);
	}

	@Then("note is created")
	@Then("I can see note")
	public void note_is_added() {
		assertTrue(notesPage.isNoteCreated(context.get("NoteText")));
	}

	@When("I open {string} to edit")
	public void i_open_to_edit(String note) {
		notesPage.openNote(note);
	}

	@When("I add {string} in comment textbox")
	public void i_add_in_comment_textbox(String comment) {
		notesPage.addCommentInCommunicationNote(comment);
	}

	@Then("{string} is seen in note")
	@Then("{string} is added in teaching note")
	@Then("I can see {string} note")
	public void is_seen_in_note(String comment) {
		assertTrue(notesPage.isCommentAdded(comment));
	}

	@When("I change note status to {string}")
	@When("I click on {string} button to add comment")
	public void i_click_on_button_to_add_comment(String button) {
		notesPage.saveNote(button);
	}

	@Then("note status is changed to {string}")
	public void note_status_is_changed_to(String noteStatus) {
		assertTrue(notesPage.isNoteStatusChanged(context.get("NoteText"), noteStatus));
	}

	@When("I delete {string} status from note")
	@When("I delete {string} from note")
	public void i_delete_status_from_note(String noteStatus) {
		notesPage.deleteNote(noteStatus);
	}

	@Then("note is not present")
	@Then("note is deleted")
	public void note_is_deleted() {
		notesPage.isNoteDeleted(context.get("NoteText"));
	}

	@Then("communication note edit window is not opening")
	public void note_edit_window_is_not_opening() {
		assertTrue(notesPage.isCommunicationNoteWindowNotVisible());
	}

	@When("I create teaching note with below details")
	public void i_created_note_with_below_details(DataTable newNoteDetails) {
		List<Map<String, String>> noteDetails = newNoteDetails.asMaps();
		for (Map<String, String> noteDetail : noteDetails) {
			notesPage.createTeachingNote("Title", noteDetail.get("Title"));
			notesPage.createTeachingNote("Message", noteDetail.get("Message"));
			context.set("NoteText", noteDetail.get("Message"));
		}
	}

	@When("I add comment {string} in {string}")
	public void i_add_comment_in_teaching_note(String comment, String noteType) {
		notesPage.addMessageInNote(comment, noteType);
	}

	@When("I click on {string} button to save comment")
	public void i_click_on_button_to_save_comment(String button) {
		notesPage.clickSaveTeachingNoteComment(button);
	}

	@Then("{string} edit window is not opening")
	public void teaching_note_edit_window_is_not_opening(String noteType) {
		assertTrue(notesPage.isEditNoteWindowNotVisible(noteType));
	}

	@When("I create {string} with following details")
	public void i_create_patient_note_with_following_details(String noteName, DataTable newNoteDetails) {
		List<Map<String, String>> noteDetails = newNoteDetails.asMaps();
		for (Map<String, String> noteDetail : noteDetails) {
			notesPage.selectExamNoteType(noteDetail.get("Note Type"));
			notesPage.addMessageInNote(noteDetail.get("Message"), noteName);
			context.set("NoteText", noteDetail.get("Message"));
		}
	}

	@When("I click on Add to Worklist to create new worklist")
	public void i_click_on_to_add_note_in_worklist() {
		notesPage.clickAddToWorklist();
	}

	@When("I select {string} from worklist group to add new worklist")
	public void i_select_to_add_note_in_worklist(String worklistType) {
		notesPage.selectWorklist(worklistType);
	}

	@When("I set {string} as worklist name")
	public void i_set_as_worklist(String worklistName) {
		notesPage.setNewWorklistName(worklistName);
	}

	@When("I click on {string} button to add new worklist")
	public void i_click_on_button_to_add_new_worklist(String button) {
		notesPage.clickNewWorklistButton(button);
	}

	@When("I mouse hover on {string} indicator")
	public void i_mouse_hover_on_exam_note_indicator(String exam) {
		notesPage.mouseHoverOnExamIndicator(exam);
	}

	@Then("note tooltip with {string} message is {string}")
	public void exam_note_tooltip_with_message_is_present(String message, String indicatorStatus) {
		if (indicatorStatus.equals("present")) {
			assertTrue(notesPage.isExamNoteTooltipPresent(message));
		} else {
			assertFalse(notesPage.isExamNoteTooltipPresent(message));
		}
	}

	@When("I click on {string} button to delete note from exam")
	public void i_click_on_button_to_delete_exam_note(String noteEditAction) {
		notesPage.clickExamNoteButton(noteEditAction);
	}

	@When("I mouse hover on {string} Tech QA note indicator")
	public void i_mouse_hover_on_Tech_QA_note_indicator(String exam) {
		notesPage.mouseHoverOnTechQANoteIndicator(exam);
	}

	@When("I enter today's date in {string}")
	public void i_enter_today_s_date_in(String textbox) {
		notesPage.setCurrentDate(textbox);
	}

	@When("I enter comment {string} in {string}")
	public void i_enter_comment_in(String comment, String noteType) {
		notesPage.addMessageInNote(comment, noteType);
		context.set("NoteText", comment);
	}

	@When("I mouse hover on {string} Follow-Up note indicator")
	public void i_mouse_hover_on_Follow_Up_note_indicator(String exam) {
		notesPage.mouseHoverOnFollowUpNoteIndicator(exam);
	}

	@When("I mouse hover on {string} communication note indicator")
	public void i_mouse_hover_on_communication_note_indicator(String exam) {
		notesPage.mouseHoverOnCommunicationNoteIndicator(exam);
	}

	@Then("note status is changed to {string} with current date")
	public void note_status_is_changed_to_with_current_date_and_time(String noteStatus) {
		assertTrue((notesPage.isNoteChangedWithCurrentDate(noteStatus))
				&& (notesPage.isNoteStatusChanged(context.get("NoteText"), noteStatus)));
		Log.printInfo("Note status is changed to" + noteStatus);
	}

	@Then("note with message {string} is in {string} state")
	public void note_with_message_is_in_state(String noteMessage, String noteStatus) {
		assertEquals(notesPage.getNoteStatus(noteMessage), noteStatus);
	}

	@When("I mouse hover on {string} Peer Review note indicator")
	public void i_mouse_hover_on_Peer_Review_note_indicator(String exam) {
		notesPage.mouseHoveOnPeerReviewNoteIndicator(exam);
	}

	@When("I delete Peer Review Note")
	public void i_delete_Peer_Review_Note() {
		notesPage.deletePeerReviewNote();
	}

	@When("I mouse hover on {string} ED Prelim note indicator")
	public void i_mouse_hover_on_ED_Prelim_note_indicator(String exam) {
		notesPage.mouseHoverOnEDPrelimNoteIndicator(exam);
	}

	@When("I select {string} as Communication Note type")
	public void i_select_as_Communication_Note_type(String noteType) {
		notesPage.selectCommunicatioNoteType(noteType);
	}

	@Then("alert popup with {string} message is present")
	public void note_alert_window_with_message_popup(String noteMessage) {
		assertTrue(notesPage.isNoteAlertMessagePresent(noteMessage));
	}

	@Then("I click on Acknowledge button")
	public void i_click_on_Acknowledge_button() {
		notesPage.clickAcknowledgeButton();
	}
}
