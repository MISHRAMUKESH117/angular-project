package steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class Configuration {

	pages.Configuration configurationPage;
	TestContext context;

	public Configuration(TestContext context) {
		this.context = context;
		configurationPage = new pages.Configuration(context.getDriver());
	}

	@When("I click on {string} menu option")
	public void clickConfiguration(String menuOption) {
		configurationPage.selectMenuOption(menuOption);
	}

	@When("I select {string} from the Configuration dropdown")
	public void selectDropdownOption_EditField(String dropdownOption) {
		configurationPage.selectEditFieldConfigurationDropdownOption(dropdownOption);
	}

	@When("I delete the {string} field")
	public void deleteField_EditField(String fieldName) {
		configurationPage.deleteFieldFromEditField(fieldName);
	}

	@When("I drag and drop the {string} field to {string} field")
	public void dragDrop_EditField(String fromFieldName, String toFieldName) {
		configurationPage.dragDropFieldToColumn_EditField(fromFieldName, toFieldName);
	}

	@Then("{string} page under Configuration is loaded properly")
	public void something_page_under_configuration_is_loaded_properly(String menuName) {
		assertTrue(configurationPage.isConfigurationMenuHeaderVisible(menuName));
		Log.printInfo(menuName + " page under Configuration is loaded properly");
	}

	@When("I store field values in list")
	public void i_store_field_values_in_list() {
		context.set("ConfigurationTableList", configurationPage.getConfigurationTableList());
	}

	@When("I select {string} in the Custom Reports panel")
	public void i_select_in_the_Custom_Reports_panel(String customReportName) {
		configurationPage.selectCustomReportsPageReport(customReportName);
	}

	@When("I activate the Custom Report")
	public void i_activate_the_Custom_Report() {
		if (!configurationPage.isCustomReportsPagePropertiesActiveCheckboxChecked())
			configurationPage.clickCustomReportsPagePropertiesActiveCheckbox();
	}

	@When("I click on {string} button in the bottom panel of Custom Reports")
	public void i_click_on_button_in_the_bottom_panel_of_Custom_Reports(String buttonName) {
		configurationPage.clickCustomReportsPageBottomPanelButton(buttonName);
	}

	@Then("Custom Report {string} is activated")
	public void custom_Report_is_activated(String customReportName) {
		assertTrue(configurationPage.isCustomReportsPageReportActive(customReportName));
	}

	@When("I deactivate the Custom Report")
	public void i_deactivate_the_Custom_Report() {
		if (configurationPage.isCustomReportsPagePropertiesActiveCheckboxChecked())
			configurationPage.clickCustomReportsPagePropertiesActiveCheckbox();
	}

	@Then("Custom Report {string} is deactivated")
	public void custom_Report_is_deactivated(String customReportName) {
		assertFalse(configurationPage.isCustomReportsPageReportActive(customReportName));
	}

	@When("I click edit icon for {string}")
	public void clickNotePageEditNoteIcon(String noteName) {
		configurationPage.clickNotePageEditNoteIcon(noteName);
	}

	@Then("{string} edit window is visible")
	public void isNotePageEditNoteWindowVisible(String noteName) {
		assertTrue(configurationPage.isNotePageEditNoteWindowVisible(noteName));
	}

	@When("I click {string} from {string} Configuration Page")
	public void i_click_from(String configurationName, String configurationType) {
		configurationPage.selectConfigurationMenuOption(configurationName, configurationType);
	}

	@When("I connect to database")
	public void connectToDatabase() {
		configurationPage.connectToDatabase();
	}

	@Then("RVU Task {string} is added in database")
	public void isRVUTaskPresentInDatabase(String taskName) {
		assertTrue(configurationPage.isRVUTaskPresentInDatabase(taskName));
	}

	@When("I delete RVU Task {string} from database")
	public void deleteRVUTaskFromDatabase(String taskName) {
		configurationPage.deleteRVUTaskFromDatabase(taskName);
	}

	@Then("RVU Task {string} is deleted from database")
	public void isRVUTaskdeletedFromDatabase(String taskName) {
		assertFalse(configurationPage.isRVUTaskPresentInDatabase(taskName));
	}

	@When("I close database connection")
	public void closeDatabaseConnection() {
		configurationPage.closeDatabaseConnection();
	}

	@When("I update {string} task name as  {string}")
	public void i_update_task_name_as(String oldTaskName, String newTaskName) {
		configurationPage.updateRVUTaskNameInDatabase(oldTaskName, newTaskName);
	}

	@When("I add sites to {string} Worklist Group")
	public void addSitesToWorklistGroup(String worklistGroupName, DataTable sites) {
		configurationPage.clickWorklistGroupsPageSiteColumn(worklistGroupName);
		configurationPage.clickWorklistGroupsPageMultiChoiceDropdownArrow();
		List<String> siteNames = sites.asList();
		for (String siteName : siteNames)
			configurationPage.selectWorklistGroupsPageMultiChoiceDropdownValue(siteName);
		configurationPage.clickWorklistGroupsPageMultiChoiceDropdownArrow();
		configurationPage.clickConfigurationPageHeader("Worklist Groups");
	}

	@When("I click on {string} button in the bottom panel of Worklist Groups")
	public void clickWorklistGroupsPageBottomPanelButton(String buttonName) {
		configurationPage.clickWorklistGroupsPageBottomPanelButton(buttonName);
	}

	@Then("following sites are added for {string} Worklist Group")
	public void isWorklistsGroupsPageSiteAddedForGroup(String worklistGroupName, DataTable sites) {
		List<String> siteNames = sites.asList();
		for (String siteName : siteNames)
			assertTrue(configurationPage.isWorklistsGroupsPageSiteAddedForGroup(worklistGroupName, siteName));
	}

	@When("I remove sites from {string} Worklist Group")
	public void removeSitesFromWorklistGroup(String worklistGroupName, DataTable sites) {
		configurationPage.clickWorklistGroupsPageSiteColumn(worklistGroupName);
		configurationPage.clickWorklistGroupsPageMultiChoiceDropdownArrow();
		List<String> siteNames = sites.asList();
		for (String siteName : siteNames)
			configurationPage.deSelectWorklistGroupsPageMultiChoiceDropdownValue(siteName);
		configurationPage.clickWorklistGroupsPageMultiChoiceDropdownArrow();
		configurationPage.clickConfigurationPageHeader("Worklist Groups");
	}

	@Then("following sites are removed from {string} Worklist Group")
	public void following_sites_are_removed_from_worklist_group(String worklistGroupName, DataTable sites) {
		List<String> siteNames = sites.asList();
		for (String siteName : siteNames)
			assertFalse(configurationPage.isWorklistsGroupsPageSiteAddedForGroup(worklistGroupName, siteName));
	}

	@When("I set the Custom Report name as {string}")
	public void setReportName(String reportName) {
		configurationPage.setReportName(reportName);
	}

	@When("I set the Worksheet name as {string}")
	public void setWorksheetName(String worksheetName) {
		configurationPage.setWorksheetName(worksheetName);
	}

	@When("I get the Query from {string} excel {string} Worksheet for Custom Report creation")
	public void getWorksheetQuery(String fileName, String sheetName) {
		context.set("Query", configurationPage.getWorksheetQuery(fileName, sheetName));
	}

	@When("I set the Query for Custom Report creation")
	public void setWorksheetQuery() {
		configurationPage.setWorksheetQuery(context.get("Query"));
	}

	@When("I click on Active checkbox to activate query")
	public void activateWorksheet() {
		configurationPage.clickCustomReportsPageQueryActiveCheckbox();
	}

	@When("I drag-drop below columns to add in the Worksheet")
	public void addColumns(DataTable columnValues) {
		List<Map<String, String>> columnValue = columnValues.asMaps();
		for (Map<String, String> value : columnValue) {
			configurationPage.dragDropColumnNamesToAddInReport(value.get("Section"), value.get("Column Name"));
		}
	}

	@When("I click the delete icon of {string}")
	public void clickDeleteIcon(String reportName) {
		configurationPage.clickDeleteCustomReportIcon(reportName);
	}

	@When("I confirm the deletion of Custom Report")
	public void confirmDelete() {
		configurationPage.confirmDeleteCustomReport();
	}

	@Then("Custom Report {string} is deleted")
	public void isReportDeleted(String customReportName) {
		assertFalse(configurationPage.isReportDeleted(customReportName));
	}

	@When("I click on {string} button in RVU Task page")
	public void clickRVUTaskPageButton(String buttonName) {
		configurationPage.clickRVUTaskPageButton(buttonName);
	}

	@When("I create a new RVU Task with following details")
	public void createNewRVUTask(DataTable newRVUTaskDetails) {
		List<Map<String, String>> rvuTaskDetails = newRVUTaskDetails.asMaps();
		for (Map<String, String> rvuTaskDetail : rvuTaskDetails) {
			configurationPage.setRVUTaskPageNewTaskName(rvuTaskDetail.get("Task Name"));
			configurationPage.setRVUTaskPageNewTaskRVUPerMinute(rvuTaskDetail.get("RVU/minute"));
			configurationPage.setRVUTaskPageNewTaskWorkUnitsPerMinute(rvuTaskDetail.get("Work Units/minute"));
			configurationPage.clickConfigurationPageHeader("RVU Task");
		}
	}

	@Then("RVU Task {string} is added")
	public void isRVUTaskPageTaskVisible(String taskName) {
		assertTrue(configurationPage.isRVUTaskPageTaskVisible(taskName));
	}

	@When("I select the {string} worklist checkbox to enable count")
	public void selectWorklistCheckboxEnableCount(String worklistName) {
		configurationPage.selectWorklistCheckboxInWorklistTunning(worklistName);
	}
	
	@When("I click on {string} button in Communication Type page")
	public void clickCommunicationTypePageButton(String buttonName) {
		configurationPage.clickCommunicationTypePageButton(buttonName);
	}

	@When("I create a Communication Type with following details")
	public void createNewCommunicationType(DataTable newComunicationTypeDetails) {
		List<Map<String, String>> comunicationTypeDetails = newComunicationTypeDetails.asMaps();
		for (Map<String, String> comunicationTypeDetail : comunicationTypeDetails) {
			configurationPage.setCommunicationTypePageNewTypeName(comunicationTypeDetail.get("Name"));
			configurationPage.selectCommunicationTypePageNewTypeWaitingAttribute(comunicationTypeDetail.get("Waiting"));
			configurationPage.selectCommunicationTypePageNewTypeFinalAttribute(comunicationTypeDetail.get("Final"));
			configurationPage.clickConfigurationPageHeader("Communication Type");
		}
	}

	@Then("Communication Type {string} is added")
	public void isCommunicationTypeVisible(String typeName) {
		assertTrue(configurationPage.isCommunicationTypeVisible(typeName));
	}

	@Then("Communication Type {string} is added in database")
	public void isCommunicationTypePresentInDatabase(String typeName) {
		assertTrue(configurationPage.isCommunicationTypePresentInDatabase(typeName));
	}

	@When("I delete Communication Type {string} from database")
	public void deleteCommunicationTypeFromDatabase(String typeName) {
		context.set("CommunicationTypeID", configurationPage.getCommunicationTypeIDFromDatabase(typeName));
		configurationPage.deleteCommunicationTypeFromDatabase(context.get("CommunicationTypeID"), typeName);
	}

	@Then("Communication Type {string} is deleted from database")
	public void isCommunicationTypeDeletedFromDatabase(String typeName) {
		assertFalse(configurationPage.isCommunicationTypePresentInDatabase(typeName));
	}
	
	@When("I change properties on the Worklist Configuration")
	public void changeProperties(DataTable properties) {
		List<Map<String, String>> property = properties.asMaps();
		for (Map<String, String> singleProperty : property) {
			configurationPage.changeProperties(singleProperty.get("Label"), singleProperty.get("Value"));
		}
	}
	
	@When("I click Status checkbox for My Support Queue configuration")
	public void clickStatusCheckbox() {
		configurationPage.clickStatusCheckbox();
	}
	
	@When("I click Wrap content checkbox for My Support Queue configuration")
	public void wrapContent() {
		configurationPage.wrapContent();
	}
	
	@When("I uncheck Status checkbox for My Support Queue configuration")
	public void uncheckStatusCheckbox() {
		configurationPage.uncheckStatusCheckbox();
	}
	
	@When("I uncheck Wrap content checkbox for My Support Queue configuration")
	public void uncheckWrapContent() {
		configurationPage.uncheckWrapContent();
	}
	
	@When("I verify Status checkbox is selected for My Support Queue configuration")
	public void isVerifyStatusCheckboxSelected() {
		assertTrue(configurationPage.isStatusCheckboxSelected());
	}
	
	@Then("I verify Wrap Content checkbox is selected for My Support Queue configuration")
	public void isWrapContentCheckBoxSelected() {
		assertTrue(configurationPage.isWrapContentCheckBoxSelected());
	}
	
	@When("I click {string} button from My Support Queue configuration")
	public void clickMySupportQueueButton(String button) {
		configurationPage.clickMySupportQueueButton(button);
	}
	
	@When("I check {string} checkbox from My Support Queue configuration")
	public void checkMySupportQueuePermissionsCheckbox(String permissionName) {
		configurationPage.checkMySupportQueuePermissionsCheckbox(permissionName);
	}
	
	@When("I uncheck {string} checkbox from My Support Queue configuration")
	public void uncheckMySupportQueuePermissionsCheckbox(String permissionName) {
		configurationPage.uncheckMySupportQueuePermissionsCheckbox(permissionName);
	}
	
	@When("I de-select the {string} worklist checkbox to disable count")
	public void deselectWorklistCheckboxInWorklistTunning(String worklistName) {
		configurationPage.deselectWorklistCheckboxInWorklistTunning(worklistName);
	}

	@When("I search for {string} worklist in Worklist panel")
	public void setWorklistNameInWorklistTunning(String worklistName) {
		configurationPage.setWorklistNameInWorklistTunning(worklistName);
	}

	@When("I add a Body Part Keyword {string}")
	public void addBodyPartKeyword(String keywordName) {
		configurationPage.setBodyPartsPageAddKeywordTextbox(keywordName);
		configurationPage.clickBodyPartsPageAddNewKeywordAddIcon();
	}

	@When("I search a Body Part Keyword {string}")
	public void setBodyPartsPageKeywordSearch(String keywordName) {
		configurationPage.setBodyPartsPageKeywordSearch(keywordName);
	}

	@Then("Body Part Keyword {string} is present")
	public void isBodyPartsPageKeywordPresentOnSearch(String keywordName) {
		assertTrue(configurationPage.isBodyPartsPageKeywordPresentOnSearch(keywordName));
	}
	
	@When("I set {string} as new Body Part")
	public void setBodyPartsPageAddBodyPart(String bodyPartName) {
		configurationPage.setBodyPartsPageAddBodyPart(bodyPartName);
	}

	@When("I add Keyword to the Body Part")
	public void addKeywordToBodyPart(DataTable newBodyPartKeywords) {
		List<String> bodyPartKeywords = newBodyPartKeywords.asList();
		configurationPage.clickBodyPartsPageKeywordSearchDropdownArrow();
		for (String bodyPartKeyword : bodyPartKeywords) 
			configurationPage.selectBodyPartsPageKeywordDropdownResult(bodyPartKeyword);
	}

	@When("I click on new Body Part Add icon")
	public void clickNewBodyPartAddIcon() {
		configurationPage.clickBodyPartsPageNewBodyPartAddIcon();
	}

	@Then("{string} is added to the Body Parts grid")
	public void isValuePresentInBodyPartsGrid(String bodyPartGridValue) {
		assertTrue(configurationPage.isValuePresentInBodyPartsGrid(bodyPartGridValue));
	}
	
	@When("I select site {string}")
	public void clickSitePageSiteName(String siteName) {
		configurationPage.clickSitePageSitesGridSiteName(siteName);
	}

	@When("I select the following {string} for site")
	public void selectSitePageSitePropertyMultiChoiceDropdownValues(String propertyName, DataTable dropdownValues) {
		List<String> values = dropdownValues.asList();
		configurationPage.clickSitePageSitePropertyMultiChoiceDropdownArrow(propertyName);
		for (String value : values) 
			configurationPage.selectSitePageSitePropertyMultiChoiceDropdownOption(value);
		configurationPage.clickSitePageSitePropertyMultiChoiceDropdownArrow(propertyName);
	}

	@When("I select {string} for the site as {string}")
	public void selectSitePageSitePropertySingleChoiceDropdownValue(String propertyName, String dropdownValue) {
		configurationPage.selectSitePageSitePropertySingleChoiceDropdownValue(propertyName, dropdownValue);
	}

	@When("I click on {string} button of site properties")
	public void clickSitePageSitePropertiesButton(String buttonName) {
		configurationPage.clickSitePageSitePropertyBottomButton(buttonName);
	}
	
	@When("I click {string} button on configuration page")
	public void clickConfigurationPageButton(String button) {
		configurationPage.clickConfigurationPageButton(button);
	}
	
	
	@When("I create Ldap Server configuration")
	public void createLdapServerConfiguration(DataTable dt) {
		List<Map<String,String>> ldapConfigs = dt.asMaps();
		for(Map<String,String> ldapConfig: ldapConfigs) {
			configurationPage.setLdapServerName(ldapConfig.get("Server"));
			configurationPage.setLdapServerPort(ldapConfig.get("Port"));
			configurationPage.setLdapServerAuthGroup(ldapConfig.get("AuthGroup"));
			configurationPage.setLdapServerDomain(ldapConfig.get("Domain"));
			configurationPage.setLdapServerAuthUser(ldapConfig.get("AuthUser"));
			configurationPage.setLdapServerAuthPassword(ldapConfig.get("Server"));
			configurationPage.setLdapServerUserDomain(ldapConfig.get("UserDomain"));	
		}
	}
	
	@When("I create Ldap Group configuration")
	public void createLdapGroupConfiguration(DataTable dt) {
		List<Map<String,String>> ldapGroupConfigs = dt.asMaps();
		for(Map<String,String> ldapGroupConfig: ldapGroupConfigs) {
			configurationPage.setLdapGroupName(ldapGroupConfig.get("Group Name"));
			configurationPage.setLdapGroupServer(ldapGroupConfig.get("Group Server"));
			configurationPage.setLdapGroupSite(ldapGroupConfig.get("Site"));
			configurationPage.setLdapGroupRoles(ldapGroupConfig.get("AuthGroup"));	
		}
	}
	
	@When("I click the add button for configuring the following License\\/s")
	public void iSelectLicense(DataTable licenseTable) {
		List<String> licenses = licenseTable.asList();
		configurationPage.addLicense(licenses);	
	}
	
	@When("I click {string} button on License Page")
	public void iClickLicenseButton(String button) {
		configurationPage.clickButton(button);	
	}
	
}
