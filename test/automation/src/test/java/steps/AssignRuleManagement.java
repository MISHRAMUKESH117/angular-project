package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static utilities.Common.sleep;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class AssignRuleManagement {

	pages.AssignRuleManagement assignRuleManagementPage;
	TestContext context;

	public AssignRuleManagement(TestContext context) {
		this.context = context;
		assignRuleManagementPage = new pages.AssignRuleManagement(context.getDriver());
	}

	@When("create new Assignment rule with following details")
	public void create_new_assignment_rule_with_following_details(DataTable newAssignRuleDetails) {
		List<Map<String, String>> assignRuleDetails = newAssignRuleDetails.asMaps();
		for (Map<String, String> assignRuleDetail : assignRuleDetails) 
			assignRuleManagementPage.selectSingleChoiceDropdownValue(assignRuleDetail.get("Label"), assignRuleDetail.get("Value"));
	}


	@Then("assignment rule {string} is added")
	@Then("assignment rule {string} is present")
	public void assign_rule_something_is_added(String ruleName) {
		assertTrue(assignRuleManagementPage.ruleDoesNotExists());
		assertTrue(assignRuleManagementPage.isRuleVisible(ruleName));
		Log.printInfo("Rule:- " + ruleName + " is added.");
	}

	@When("I select {string} assignment rule")
	public void i_select_something_rule(String ruleName) {
		assignRuleManagementPage.clickRuleInGrid(ruleName);
	}

	@When("I click on Edit assignment rule icon")
	public void i_click_on_edit_rule_icon() {
		assignRuleManagementPage.clickEditRuleButton();
	}

	@When("I set assignment rule name {string}")
	public void i_set_assignment_rule_name_something(String ruleName) {
		assignRuleManagementPage.setAssignRuleName(ruleName);
	}

	@When("click on save assignment rule button")
	public void click_on_save_assignment_rule_button() {
		assignRuleManagementPage.clickSaveAssignmentRuleButton();
	}

	@When("I click in Delete assignment rule icon")
	public void i_click_in_delete_assign_rule_icon() {
		assignRuleManagementPage.clickDeleteAssignmentRuleButton();
	}

	@Then("assignment rule {string} is deleted")
	public void assignment_rule_something_is_deleted(String ruleName) {
		sleep(3, TimeUnit.SECONDS);
		assertTrue(assignRuleManagementPage.isAssignmentRuleDeleted(ruleName));
		Log.printInfo("Assignment rule:- " + ruleName + " is deleted.");
	}

	@When("I search for {string} on Assignment Rule Page")
	public void i_search_page_for_assin_rule_managment(String input) {
		assignRuleManagementPage.setFilterValue(input);
	}

	@When("I click {string} button on Managment Page")
	public void click_page_button(String button) {
		assignRuleManagementPage.clickPageButton(button);
	}
	
	@When("I click on {string} icon on the Assign Rules grid")
	public void clickRulesGridHeaderButton(String buttonName) {
		assignRuleManagementPage.clickRulesGridHeaderButton(buttonName);
	}

	@When("I drag and drop the Assign Rule {string} to rank {int}")
	public void dragAndDropAssignRuleToRank(String ruleName, Integer destinationRank) {
		context.set("OriginalRank", assignRuleManagementPage.getRuleRankInGrid(ruleName));
		assignRuleManagementPage.rearrangeRule(context.get("OriginalRank"), destinationRank);
	}
	
	@Then("the Assign Rule {string} is at rank {int}")
	public void assignRuleIsAtRank(String ruleName, Integer rank) {
		assertEquals(assignRuleManagementPage.getRuleRankInGrid(ruleName), rank);
	}
	
	@When("I click on Add new Rule icon")
	public void clickAddAssignRuleButton() {
		assignRuleManagementPage.clickAddAssignRuleButton();
	}
	
	@When("I click on Assignment Property add icon in Properties panel")
	public void clickAssignmentPropertyAddIcon() {
		assignRuleManagementPage.clickAssignmentPropertyAddIcon();
	}
	
}
