package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class SLAManagement {

	pages.SLAManagement slaManagementPage;
	TestContext context;

	public SLAManagement(TestContext context) {
		this.context = context;
		slaManagementPage = new pages.SLAManagement(context.getDriver());
	}
	
	
	@When("I click on Add new SLA Rule button")
	public void clickAddNewRuleButton() {
		slaManagementPage.clickAddNewRuleButton();
	}

	@When("I set SLA Rule name as {string}")
	public void setRuleName(String ruleName) {
		slaManagementPage.setRuleName(ruleName);
	}

	@When("I set Escalation Minutes as {int}")
	public void setEscalationMinutes(Integer escalationMinutes) {
		slaManagementPage.setEscalationMinutes(escalationMinutes);
	}

	@When("I select Escalation Priority as {string}")
	public void selectEscalationPriority(String escalationPriority) {
		slaManagementPage.selectEscalationPriority(escalationPriority);
	}

	@When("I click on {string} SLA Rule icon")
	public void clickPropertiesPanelButton(String buttonName) {
		slaManagementPage.clickPropertiesPanelButton(buttonName);
	}

	@Then("SLA {string} is present")
	@Then("SLA rule with name {string} is created")
	public void isSLARuleVisible(String ruleName) {
		assertTrue(slaManagementPage.isSLARuleVisible(ruleName));
	}
	
	@When("I select {string} SLA Rule from the grid")
	public void clickRuleInGrid(String ruleName) {
		slaManagementPage.clickRuleInGrid(ruleName);
	}
	
	@When("I delete SLA Rule")
	public void deleteSLARule() {
		slaManagementPage.deleteSLARule();
	}

	@Then("{string} SLA Rule is deleted")
	public void slaRuleIsDeleted(String ruleName) {
		assertFalse(slaManagementPage.isSLARuleVisible(ruleName));
	}
	
	@When("I search for {string} on SLA Management Page")
	public void setFilterValue(String input) {
		slaManagementPage.setFilterValue(input);
	}
	
	@When("I click on {string} icon on the SLA Rules grid")
	public void clickRulesGridHeaderButton(String buttonName) {
		slaManagementPage.clickRulesGridHeaderButton(buttonName);
	}

	@When("I click on {string} column header dropdown arrow")
	public void clickRulesGridColumnHeaderDropdownArrow(String columnHeaderName) {
		slaManagementPage.clickRulesGridColumnHeaderDropdownArrow(columnHeaderName);
	}

	@When("I select {string} dropdown option")
	public void clickRulesGridColumnHeaderDropdownOption(String dropdownOption) {
		slaManagementPage.clickRulesGridColumnHeaderDropdownOption(dropdownOption);
	}

	@Then("SLA rules are sorted in Ascending order by Name")
	public void areRulesSortedInAscendingOrder() {
		assertEquals(slaManagementPage.getAllRulesNameInGrid(),
				slaManagementPage.sortRulesInAscendingOrder(slaManagementPage.getAllRulesNameInGrid()));
	}
	
	@Then("SLA rules are sorted in Descending order by Name")
	public void areRulesSortedInDescendingOrder() {
		assertEquals(slaManagementPage.getAllRulesNameInGrid(),
				slaManagementPage.sortRulesInDescendingOrder(slaManagementPage.getAllRulesNameInGrid()));
	}
	
	@When("I drag and drop the SLA Rule {string} to rank {int}")
	public void dragAndDropSLARuleToRank(String ruleName, Integer destinationRank) {
		context.set("OriginalRank", slaManagementPage.getRuleRankInGrid(ruleName));
		slaManagementPage.rearrangeRule(context.get("OriginalRank"), destinationRank);
	}

	@Then("the SLA Rule {string} is at rank {int}")
	public void sLARuleIsAtRank(String ruleName, Integer rank) {
		assertEquals(slaManagementPage.getRuleRankInGrid(ruleName), rank);
	}
}
