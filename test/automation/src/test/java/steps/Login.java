package steps;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import utilities.Log;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class Login {

	pages.Login loginPage;
	TestContext context;

	public Login(TestContext context) {
		this.context = context;
		loginPage = new pages.Login(context.getDriver());
	}

	@Given("log into the application as {string} user")
	public void loginAsUser(String userType) {
		loginPage.loginAsUser(userType);
//		assertTrue(loginPage.isUserLoggedIn(userType));
		Log.printInfo(userType + " has logged into the Clario application");
	}

	@When("Click on login button")
	public void clickOnLoginButton() {
		loginPage.clickLoginButton();
		Log.printInfo("Clicked on login button");
	}

	@Then("log out of the application")
	public void logout() {
		loginPage.logout();
	}

	@When("log into the application with username {string} and password {string}")
	public void loginAsUser(String username, String password) {
		loginPage.login(username, password);
		Log.printInfo(username + " has logged into the Clario application");
		context.set("OperatorUsername", username);
	}

	@Then("I am logged in as user {string}")
	public void verifyLoggedInUser(String username) {
		assertTrue(loginPage.isUserLoggedIn(username));
		Log.printInfo(username+ " has logged in.");
	}

	@Then("{string} user is logged out")
	public void isUserLoggedOut(String userName) {
		assertTrue(loginPage.isUserOnLoginPage());
		Log.printInfo(userName+ " has been logged out.");
	}
	
	@When("I create a {string} file for writing build version")
	public void getAppVersion(String fileName) throws IOException {
		loginPage.getBuildVersion(fileName);
	}
	
	@Then("{string} file is created")
	public void isFileExisting(String fileName) {
		assertTrue(loginPage.isFileExisting(fileName));
	}
	
	@Then("Unsupported broswer message is displayed containing {string}")
	public void unsupportedBroswer(String errorMessage) {
		assertTrue(loginPage.isBrowserDialogueBoxDisplayed());
		assertTrue(loginPage.isBrowserCheckMessageDisplayed(errorMessage));
	}
	
	@When("I update the {string} user's password from {string} to {string}")
	public void updateUserPassword(String username, String currentPassword, String newPassword) {
		loginPage.login(username,currentPassword);
		loginPage.updateUserPassword(currentPassword,newPassword);
	}
}
