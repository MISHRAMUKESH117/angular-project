package steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

@SuppressWarnings("unused")
public class Reports {
	pages.Reports reportsPage;
	TestContext context;
	

	public Reports(TestContext context) {
		this.context = context;
		reportsPage = new pages.Reports(context.getDriver());
	}

	@When("I select the following report")
	public void selectReportName(DataTable listReportsName) {
		List<String> reportsNames = listReportsName.asList();
		for (String reportName : reportsNames) {
		reportsPage.selectReportName(reportName);
		}
	}
	
	@When("I click the Reports tab")
	public void isReportsLaunched() {
		reportsPage.clickReportsTab();
	}

	@Then("Reports are displayed")
	public void isReportsDisplayed() {
		assertTrue(reportsPage.isReportsPageDisplayed());
	}

	@When("I create the following report")
	public void iCreateReport(DataTable listReportsName) {
		List<String> reportsNames = listReportsName.asList();
		for (String reportName : reportsNames) {
			reportsPage.clickCreateReport();
			assertTrue(reportsPage.isReportDownloadAlertVisible(reportName));
			assertTrue(reportsPage.isReportDownloadSuccessful(reportName));
			reportsPage.closeReportDownloadAlert();
		}
	}
	
	@When("I set the following fields")
	public void editParams(DataTable listReportsName) {
		List<Map<String, String>> reportsData = listReportsName.asMaps();
		for (Map<String, String> search : reportsData) {
			String label = search.get("Parameter Name");
			String value = search.get("Value");
			reportsPage.selectEditInputParams(label, value);
		}
	}
	
	@Then("reports are created successfully")
	public void verifyReportCreation(DataTable listReportsNames) {
		List<String> reportsNames = listReportsNames.asList();
		for (String reportsName : reportsNames) {
		assertTrue(reportsPage.verifyReportInArchive(reportsName));
		}
	}
	
	@Then("datasheet shows appropriate information")
	public void verifyDataSheet(DataTable listReportsNames) {
		List<String> reportsNames = listReportsNames.asList();
		for (String reportsName : reportsNames) {
		assertTrue(reportsPage.isDataSheetDownloaded(reportsName));
		assertTrue(reportsPage.verifyExcelContents("Report Name", "Peer Review Analysis", reportsName));
		}
	}
	
	@When("I download the report")
	public void downloadReport(DataTable listReportsNames) {
		List<String> reportsNames = listReportsNames.asList();
		for (String reportsName : reportsNames) {
			reportsPage.clickReportDownloadIcon(reportsName);
		}
	}
	
	@Then("excel contains exported information")
	public void verifyEditedReport(DataTable listReportsData) {
		List<Map<String, String>> reportsData = listReportsData.asMaps();
		for (Map<String, String> search : reportsData) {
			reportsPage.verifyEditedReport(search.get("Column Name"), search.get("Value"), search.get("FileName"));
		}
	}
	
	@Then("Custom Report shows appropriate information")
	public void verifyCustomReportDataSheet(DataTable listReportsNames) {
		List<String> reportsNames = listReportsNames.asList();
		for (String reportsName : reportsNames) {
		assertTrue(reportsPage.isDataSheetDownloaded(reportsName));
		assertTrue(reportsPage.verifyExcelContents("Report Name", "Test Custom Report", reportsName));
		}
	}
	
	@When("I click on Save Report button")
	public void clickReportSaveButton() {
		reportsPage.clickReportSaveButton();
	}
	
	@When("I set the Report Name as {string} and schedule as {string}")
	public void setReportFields(String name, String schedule) {
		reportsPage.setReportFields(name, schedule);
	}
	
	@When("I click on Save schedule button")
	public void clickSaveSchedule() {
		reportsPage.clickSaveSchedule();
	}
	
	@When("I select {string} as exam date range for report generation")
	public void selectDateRange(String range) {
		reportsPage.clickExamDate(range);
	}
}
