package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;
import utilities.Log;

public class GroupManagement {

	pages.GroupManagement groupManagementPage;
	TestContext context;

	public GroupManagement(TestContext context) {
		this.context = context;
		groupManagementPage = new pages.GroupManagement(context.getDriver());
	}

	@When("I click on Create New Subspecialty button")
	@When("I click on Create New Group button")
	@When("I select {string} option from the Create Group icon dropdown")
	public void clickGroupOrSubspecialtyAddButton() {
		groupManagementPage.clickGroupOrSubspecialtyAddButton();
	}

	@When("I set Subspecialty name as {string}")
	@When("I set Group name as {string}")
	public void i_set_Group_Or_Subspecialty_name_as(String groupOrSubspecialtyName) {
		groupManagementPage.setGroupOrSubspecialtyName(groupOrSubspecialtyName);
	}

	@When("I set Subspecialty description as {string}")
	@When("I set Group description as {string}")
	public void i_set_Group__Or_Subspecialty_description_as(String groupOrSubspecialtyDescription) {
		groupManagementPage.setGroupOrSubspecialtyDescription(groupOrSubspecialtyDescription);
	}

	@When("I click on Save Group icon")
	public void i_click_on_save_Group_button() {
		groupManagementPage.clickSaveGroupButton();
	}

	@Then("new Subspecialty {string} is added")
	@Then("new group {string} is added")
	@Then("new group {string} is present")
	@Then("new Subspecialty {string} is present")
	public void new_group_Or_Subspecialty_is_added(String groupOrSubspecialtyName) {
		assertTrue(groupManagementPage.isGroupOrSubspecialtyVisible(groupOrSubspecialtyName));
		Log.printInfo("Group:- " + groupOrSubspecialtyName + " is added.");
	}
	
	@When("I select {string} from the grid")
	public void i_select_from_the_grid(String groupOrSubspecialtyName) {
		groupManagementPage.clickGroupOrSubspecialtyNameInGrid(groupOrSubspecialtyName);
	}
	
	@When("I click on Edit Group icon")
	public void i_click_on_Edit_Group_icon() {
		groupManagementPage.clickEditGroupIcon();
	}
	
	@When("I click on Delete Group icon")
	public void i_click_on_Delete_Group_icon() {
		groupManagementPage.deleteGroup();
	}

	@Then("subspecialty {string} is deleted")
	@Then("group {string} is deleted")
	public void group_Or_Subspecialty_is_deleted(String groupOrSubspecialtyName) {
		assertFalse(groupManagementPage.isGroupOrSubspecialtyVisible(groupOrSubspecialtyName));
	}
	
	@When("I create new Exam Type Definitions with Name")
	public void i_create_new_Exam_Type_Definitions_with_Name(DataTable newExamTypeDefinitionNames) {
		List<String> examTypeDefinitionNames = newExamTypeDefinitionNames.asList();
		for (String examTypeDefinitionName : examTypeDefinitionNames) {
			groupManagementPage.clickExamTypeDefinitionAddButton();
			groupManagementPage.setNewExamTypeDefinitionName(examTypeDefinitionName);
			groupManagementPage.clickOKButton();
		}
	}
	
	@When("I expand the Name dropdown in Exam Type Definition panel")
	public void i_click_on_Exam_Type_Definition_Name_dropdown() {
		groupManagementPage.clickExamTypeDefinitionNameDropdown();
	}
	
	@Then("following Exam Type Definitions are added")
	public void following_Exam_Type_Definitions_are_added(DataTable examTypeDefinitionNames) {
		List<String> examTypeDefinitions = examTypeDefinitionNames.asList();
		for (String examTypeDefinitionName : examTypeDefinitions) {
			assertTrue(groupManagementPage.isExamTypeDefinitionNameVisible(examTypeDefinitionName));
			Log.printInfo("Exam Type Definition: " + examTypeDefinitionName + " is added.");
		}
	}
	
	@When("I click on {string} button of the Subspecialty or Group grid")
	public void i_click_on_button_of_the_Subspecialty_or_Group_grid(String gridButtonName) {
		groupManagementPage.clickGroupOrSubspecialtyGridButton(gridButtonName);
	}

	@When("I drag and drop the Group {string} to rank {int}")
	@When("I drag and drop the Subspecialty {string} to rank {int}")
	public void i_drag_and_drop_the_Subspecialty_to_rank(String subspecialtyOrGroupName, Integer destinationRank) {
	   context.set("OriginalRank", groupManagementPage.getGroupOrSubspecialtyRankInGrid(subspecialtyOrGroupName));
	   groupManagementPage.rearrangeSubSpecialtyOrGroup(context.get("OriginalRank"), destinationRank);
	}


	@Then("the Group {string} is at rank {int}")
	@Then("the Subspecialty {string} is at rank {int}")
	public void the_Subspecialty_is_at_rank(String subspecialtyOrGroupName, Integer rank) {
		assertEquals(groupManagementPage.getGroupOrSubspecialtyRankInGrid(subspecialtyOrGroupName), rank);
	}
	
	@When("I drag and drop the Group {string} back to the original rank")
	@When("I drag and drop the Subspecialty {string} back to the original rank")
	public void i_drag_and_drop_the_Subspecialty_back_to_the_original_rank(String subspecialtyOrGroupName) {
		groupManagementPage.rearrangeSubSpecialtyOrGroup(groupManagementPage.getGroupOrSubspecialtyRankInGrid(subspecialtyOrGroupName), context.get("OriginalRank"));
	}
	
	@Then("Group {string} is moved back to its original rank")
	@Then("Subspecialty {string} is moved back to its original rank")
	public void subspecialty_is_moved_back_to_its_original_rank(String subspecialtyOrGroupName) {
		assertEquals(groupManagementPage.getGroupOrSubspecialtyRankInGrid(subspecialtyOrGroupName), context.get("OriginalRank"));
	}
	
	@When("I click on {string} toggle button")
	public void i_click_on_toggle_button(String toggle) {
		groupManagementPage.clickToggleButton(toggle);
	}
	
	@When("I search for {string} on Group Management Page")
	public void i_search_page_for_something_group_managment(String input) {
		groupManagementPage.setFilterValue(input);
	}
}
