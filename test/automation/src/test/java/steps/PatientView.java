package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;
import utilities.Log;

public class PatientView {

	pages.PatientView patientViewPage;
	TestContext context;
	private Logger logger = LogManager.getLogger(PatientView.class.getName());

	public PatientView(TestContext context) {
		this.context = context;
		patientViewPage = new pages.PatientView(context.getDriver());
	}

	@When("in the {string} panel I click on {string}")
	public void click_somelink_some_panel(String panelName, String patientName) {
		patientViewPage.clickVirtualLinkInPanel(panelName, patientName);
	}

	@Then("{string} window is opened")
	public void check_dialog_load(String dialogTitle) {
		assertTrue(patientViewPage.hasDialogLaunched(dialogTitle));
	}

	@When("I click on the {string} edit button")
	public void clickEditPatientExamHistory(String dialogName) {
		patientViewPage.clickEditPatientExamHistory(dialogName);
	}

	@Then("I check the following details")
	public void checkDetails(DataTable patientViewDetails) {
		List<Map<String, String>> checkDetails = patientViewDetails.asMaps();
		for (Map<String, String> checkDetail : checkDetails) {
			logger.info("Assertion starting for: " + checkDetail.get("Demographic Identifier"));
			assertTrue(patientViewPage.isPatientExamDetailVisible(checkDetail.get("Demographic Identifier"), checkDetail.get("Value")));
		}
	}

	@When("I edit the following details of patient")
	public void editDetails(DataTable editPatientHistoryDetails) {
		List<Map<String, String>> editDetails = editPatientHistoryDetails.asMaps();
		for (Map<String, String> editDetail : editDetails) {
			patientViewPage.editPatientDetails(editDetail.get("Label Name"), editDetail.get("Value"));
			logger.info("Edited the " + editDetail.get("Label Name") + " " + editDetail.get("Value"));
		}
	}

	@When("I save the {string} changes")
	public void savePatientExamDetails(String dialogName) {
		patientViewPage.clickOnSavePatientExamHistory(dialogName);
	}

	@When("I close the {string} window")
	public void closePatientExamHistory(String dialogName) {
		patientViewPage.closePatientExamHistory(dialogName);
	}

	@Then("I check the absence of the following")
	public void isFieldAbsent(DataTable absentFieldList) {
		List<Map<String, String>> absentFields = absentFieldList.asMaps();
		for (Map<String, String> absentField : absentFields) {
			assertTrue(patientViewPage.isFieldAbsence(absentField.get("Label Name"), absentField.get("Value")));
		}
	}

	@When("I expand the {string} panel for {string}")
	public void panelExpand(String panelName, String panelNameValue) {
		patientViewPage.panelCollapse_Expand(panelName, panelNameValue);
	}

	@Then("I check the following panel is loaded")
	public void checkPanelLoad(DataTable patientViewPanelLoads) {
		List<String> panelLoads = patientViewPanelLoads.asList();
		for (String panelName : panelLoads) {
			assertTrue(patientViewPage.isPanelLoaded(panelName));
		}
	}

	@Then("I check the {string} in site panel is {string}")
	public void checkSitePanelDetails(String labelName, String labelValue) {
		patientViewPage.isSiteDetailVisible(labelName, labelValue);
	}

	@When("in the Exam panel I click on accession {string}")
	public void clickAccessionLink(String accessionNo) {
		patientViewPage.clickAccessionVirtualLink(accessionNo);
	}

	@When("I expand the Information section")
	public void expandInformationSection() {
		patientViewPage.expandInformationSec();
	}

	@When("I open the context menu for exam with Site Procedure as {string}")
	public void contextMenuClick(String siteProcedure) {
		patientViewPage.contextMenuRightClick(siteProcedure);
		logger.info("Opened the context menu.");
	}

	@Then("I check the following list")
	public void contextMenuListCheck(DataTable contextMenuList) {
		List<String> contextMenuItems = contextMenuList.asList();
		for (String contextMenuItem : contextMenuItems) {
			assertTrue(patientViewPage.isContextMenuItemVisible(contextMenuItem));
			logger.info("Context Menu option " + contextMenuItem + " found.");
		}
	}

	@When("I drag and drop the {string} column on {string}")
	public void dragDropColumn(String dragDropColumn, String targetColumn) {
		patientViewPage.dragDropColumn(dragDropColumn, targetColumn);
	}

	@Then("I check the immediate next column of {string} is {string}")
	public void checkDragDropColumn(String columnName, String immediateNextColumn) {
		assertTrue(patientViewPage.checkImmediateNextColumn(columnName, immediateNextColumn));
	}

	@When("I fetch the text for the column {string} for sorting check")
	public void sortColumnList(String columnIndex) {
		List<String> columnValues = patientViewPage.sortList(columnIndex);
		context.set("ColumnValuesList", columnValues);
	}

	@When("I click on the {string} column for sorting")
	public void clickColumnToSort(String columnName) {
		patientViewPage.clickToSortColumn(columnName);
	}

	@Then("I check the sorting has worked for column {string}")
	public void columnValueListColumnSorted(String columnIndex) {
		List<String> columnSortedList = patientViewPage.fetchColumnValueList(columnIndex);
		List<String> sortedList = context.get("ColumnValuesList");
		assertTrue(sortedList.equals(columnSortedList));
	}

	@When("I edit the following details in exam")
	public void i_edit_the_following_details_in_exam(DataTable examViewDetails) {
		List<Map<String, String>> examDetails = examViewDetails.asMaps();
		for (Map<String, String> examDetail : examDetails) {
			patientViewPage.setExamHistoryInformation(examDetail.get("Label Name"), examDetail.get("Value"));
			logger.info("Edited the " + examDetail.get("Label Name") + " " + examDetail.get("Value"));
		}
	}

	@Then("I check the following details of exam")
	public void i_check_the_below_details_in_order_tab(DataTable examViewDetails) {
		List<Map<String, String>> examDetails = examViewDetails.asMaps();
		for (Map<String, String> examDetail : examDetails) {
			patientViewPage.isExamInformationVisible(examDetail.get("Label Name"), examDetail.get("Value"));
			logger.info("Edited the " + examDetail.get("Label Name") + " " + examDetail.get("Value"));
		}
	}

	@Then("order tab list is same of configuration field list")
	public void order_tab_list_is_same_of_configuration_field_list() {
		assertEquals(patientViewPage.getOrderTableList(), context.get("ConfigurationTableList"));
	}

	@Then("I check the absence of the following in order tab")
	public void i_check_the_absence_of_the_following_in_order_tab(DataTable orderTabValues) {
		List<Map<String, String>> orderValues = orderTabValues.asMaps();
		for (Map<String, String> orderValue : orderValues) {
			assertFalse(patientViewPage.isExamInformationVisible(orderValue.get("Label Name"), orderValue.get("Value")));
		}
	}

	@When("I mouse hover on {string} procedure in exam panel")
	public void i_mouse_hover_on_procedure_in_exam_panel(String procedureName) {
		patientViewPage.mouseHoverOnExamProcedure(procedureName);
	}

	@When("I click on {string} icon")
	public void i_click_on_icon(String iconName) {
		patientViewPage.clickExamGridAction(iconName);
	}

	@Then("exam is loaded in dictation with template")
	public void exam_is_loaded_in_dictation_with_template() {
		assertTrue(patientViewPage.isTemplateLoadedInDication());
		Log.printInfo("Template is launched in dictation");
	}

	@When("I click on {string} template")
	public void i_click_on_template(String templateName) {
		patientViewPage.clickTemplateInExam(templateName);
	}

	@Then("{string} template is launched")
	public void template_is_launched(String templateName) {
		assertTrue(patientViewPage.isMappedTemplateLoaded(templateName));
		Log.printInfo(templateName + "template is launched");
	}

	@When("I click on {string} icon of report")
	public void i_click_on_icon_to_discard_report(String iconName) {
		patientViewPage.clickReportActionIcon(iconName);
	}

	@When("I confirm {string} option of discard report dialog")
	public void i_confirm_option(String optionValue) {
		patientViewPage.clickDiscardReportDialog(optionValue);
	}

	@When("I make {string} template as favourite template")
	public void i_make_template_as_favourite_template(String templateName) {
		patientViewPage.clickTemplateFavouriteIcon(templateName);
	}

	@Then("{string} template is launched as favourite template")
	public void template_is_launched_as_favourite_template(String templateName) {
		assertTrue(patientViewPage.isTemplateSelectedAsFavourite(templateName));
		Log.printInfo(templateName + "is launched as favourite template");
	}

	@Then("template is launched with below sections")
	public void template_is_launched_with_below_sections(DataTable addedSectionNames) {
		List<String> sectionNames = addedSectionNames.asList();
		assertEquals(patientViewPage.getDicationBlockContentList(), sectionNames);
		Log.printInfo("Favourite template is launched");
	}
	
	@When("I make {string} template as unfavourite template")
	public void i_make_template_as_unfavourite_template(String templateName) {
		patientViewPage.clickTemplateUnfavouriteIcon(templateName);
	}

	@Then("{string} template changed to unfavourite template")
	public void template_is_launched_as_unfavourite_template(String templateName) {
		assertTrue(patientViewPage.isTemplateSelectedAsUnfavourite(templateName));
	}
	
	@Then("{string} is loaded as default template")
	public void is_loaded_as_default_template(String templateName) {
		assertTrue(patientViewPage.isTemplateSelectedAsDefault(templateName));
	}
	
	@When("I expand patient view panel")
	public void i_expand_patient_view_panel() {
		patientViewPage.expandPatienViewSplitter();
	}

	@Then("I collapse patient view panel")
	public void i_collapse_patient_view_panel() {
		patientViewPage.collapsePatienViewSplitter();
	}
	
	@When("I expand note section")
	public void i_expand_note_section() {
		patientViewPage.expandNotePanel();
	}

	@When("I collapse the note section")
	public void i_collapse_note_section() {
		patientViewPage.collapseNotePanel();
	}
	
	@Then("{string} is present under Site")
	public void is_present_under_Site(String value) {
		assertTrue(patientViewPage.isSiteDetailVisiblieUnderSite(value));
	}
	
	@Then("{string} is present under Ordering")
	public void is_present_under_Ordering(String value) {
		assertTrue(patientViewPage.isContactDetailVisiblieUnderOrdering(value));
	}
	
	@When("I click on {string} button on Dictation Cancelled window")
	public void clickDictationCancelled(String buttonName) {
		patientViewPage.clickDictationCancelled(buttonName);
	}
	
	@Then("Dictation is disabled")
	public void isDictationDisabled() {
		assertTrue(patientViewPage.isDictationDisabled());
	}
	
	@Then("Dictation is enabled")
	public void isDictationEnabled() {
		assertTrue(patientViewPage.isDictationEnabled());
	}
	
	@When("I click on {string} button on Dictation Suspended window")
	public void clickDictationSuspended(String optionValue) {
		patientViewPage.clickDictationSuspendedOption(optionValue);
	}
	
	@Then("{string} dialog is visible")
	public void isDictationDialogDisplayed(String dialogName) {
		assertTrue(patientViewPage.isDictationDialogDisplayed(dialogName));
	}
	
	@When("I click on {string} option of Select Report Action dialog")
	public void clickReportActionOption(String option) {
		patientViewPage.selectReportAction(option);
	}
	
	@When("I perform a click for dismissing the context menu")
	public void clickPatientPanel() {
		patientViewPage.clickToDismissContextMenu();
	}
	
	@When("I select the following details in exam history panel" )
	public void i_edit_the_following_details_in_exam_dropDown(DataTable examViewDetails) {
		List<Map<String, String>> examDetails = examViewDetails.asMaps();
		for (Map<String, String> examDetail : examDetails) {
			patientViewPage.selectExamHistoryInformation(examDetail.get("Label Name"), examDetail.get("Value"));
			logger.info("Edited the " + examDetail.get("Label Name") + " " + examDetail.get("Value"));
		}
	}
	
	@When("I click {string} on {string} dialog")
	public void iClickButtonOnDialog(String buttonName, String dialogName) {
		patientViewPage.clickDictationAction(buttonName, dialogName);
	}
	
	@Then("{string} dialog is not visible")
	public void isDictationDialogVisible(String dialogName) {
		assertFalse(patientViewPage.isDictationDialogDisplayed(dialogName));
	}
	
	@When("I click {string} option of Sign Report dialog")
	public void clickSignReportActionButton(String buttonName) {
		patientViewPage.clickSignReportActionButton(buttonName);
	}
}
