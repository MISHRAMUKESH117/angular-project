package steps;

import org.openqa.selenium.WebElement;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.WindowsLaunch;
import setterGetter.TestContext;
import utilities.DBConnection;

public class SWLInstallers {
	
	TestContext context;
	WindowsLaunch windowsLaunchPage;
	DBConnection dbConnection;
	
	public SWLInstallers(TestContext context) {
		this.context = context;
		windowsLaunchPage = new WindowsLaunch(context.getWinDriver());
		dbConnection = new DBConnection();
	}

	@When("I get database properties for {string}")
	public void getDbProperties(String installerWindow) {
		windowsLaunchPage.getDbProperties(installerWindow, dbConnection.getDatabaseProperty());
}
	
	@When("I click {string} button")
    public void clickButton(String btnName) {
		windowsLaunchPage.clickButton(btnName);
	}
	
	@When("I click {string} checkbox") 
	public void clickCheckBox(String chkbxName) {
		windowsLaunchPage.clickCheckBox(chkbxName); 
	}
	
	@When("I click {string} radio button")
	public void  clickRadioButton(String btnName) {
		windowsLaunchPage.clickRadioButton(btnName);
	}
	
	@When("I enter {string} as Virtual directory alias")
	public void  setTextEdit(String virtualDir) {
		windowsLaunchPage.setTextEdit(virtualDir) ;
	}
	
	@When("I click {string} window") 
	public void clickWindow(String winName) {
		windowsLaunchPage.clickWindow(winName); 
	}
	
	@When("I click {string} icon on {string}") 
	public void clickOnIconButton(String iconName, String winName) {
		windowsLaunchPage.clickOnIconButton(iconName, winName);
	}
	
	@When("I provide {string} servies configuration")
	public void i_get_servies_configuration(String config) {
		windowsLaunchPage.getServiceConfig(config);
	}


	@When("I uninstall {string}")
	public void uninstallApp(String appName) {
		windowsLaunchPage.uninstallApp(appName);
	}


	@Then("SWL Database is installed")
    public void swl_database_is_installed() {
    	System.out.println("Database Installed...");;
		System.out.println(windowsLaunchPage.getValueFromDatabase("conf_Common", "UQ_name","installedVersionNumber", 3));
	}

	@Then("SWL Web is installed")
	public void swl_Web_is_installed() {
		System.out.println("SWL Web is installed...to be implemented...");
	}
	

	@Then("SWL Services are installed")
	public void swl_Services_are_installed() {
		System.out.println("SWL Services are installed...to be implemented...");
}

	
	@Then("SWL Database is uninstalled")
	public void isDatabaseUninstalled() {
		System.out.println("SWL Database is uninstalled...to be implemented...");
	}
	
	@Then("SWL Web is uninstalled")
	public void isWebUninstalled() {
		System.out.println("SWL Web is uninstalled...to be implemented...");
	}
	
	@When("I get {string} elements in {string}")
	public void i_get_elements_in(String string, String string2) {
		System.out.println(""
				+ "...to be implemented...");

		//windowsLaunchPage.elementList(string, string2);
	}
	/*
	@When("I get {string} elememts in {string}")
	public void elementList(String winName, String controlName) {
		windowsLaunchPage.elementList(winName, controlName);
	}
	*/
}
