package steps;

import setterGetter.TestContext;
import io.cucumber.java.Before;

public class BeforeHooks {
	
	TestContext testContext;
	public static boolean isBeforeHookExecuted = false;
	private static boolean dunit = false;
	
	public BeforeHooks(TestContext testContext) {
		this.testContext = testContext;
	}
	
	@Before(order= 0 , value="@BeforeAll")
    public void beforeAll() {
        if(!dunit) {
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    System.out.println("After All");
                }
            });
            System.out.println("Before All");
            dunit = true;
        }
    }
}
