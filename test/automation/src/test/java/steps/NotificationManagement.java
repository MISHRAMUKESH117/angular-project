package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class NotificationManagement {
	pages.NotificationManagement notificationManagement;
	TestContext context;

	public NotificationManagement(TestContext context) {
		this.context = context;
		notificationManagement = new pages.NotificationManagement(context.getDriver());
	}

	@When("I click on Add new Notification icon")
	public void i_click_on_Create_New_Notification_icon() {
		notificationManagement.clickAddNewNotificationIcon();
	}

	@When("I enter name as {string} in Properties window")
	public void i_enter_name_as_in_Properties_window(String name) {
		notificationManagement.setPropertyName(name);
	}

	@When("I enter description as {string} in Properties window")
	public void i_enter_description_as_in_Properties_window(String description) {
		notificationManagement.setPropertyDescription(description);
	}

	@When("I select following values from multichoice dropdown")
	public void i_select_following_values_from_multichoice_dropdown(DataTable dropdownValues) {
		List<Map<String, String>> dropdownValue = dropdownValues.asMaps();
		for (Map<String, String> value : dropdownValue) {
			notificationManagement.selectMultiChoiceDropdownValue(value.get("Label"), value.get("Value"));
		}
	}

	@When("I select below values from singlechoice dropdown")
	public void i_select_below_values_from_singlechoice_dropdown(DataTable dropdownValues) {
		List<Map<String, String>> dropdownValue = dropdownValues.asMaps();
		for (Map<String, String> value : dropdownValue) {
			notificationManagement.selectSingleChoiceDropdownValue(value.get("Label"), value.get("Value"));
		}
	}

	@When("I click on save icon in Properties window")
	public void i_click_on_icon() {
		notificationManagement.clickPropertySaveIcon();
	}

	@Then("Notification rule with {string} name is created")
	public void alert_with_name_is_created(String alertName) {
		assertTrue(notificationManagement.isNotificationRulePresent(alertName));
	}

	@When("I click {string} alert")
	public void i_click_alert(String alertName) {
		notificationManagement.clickOnNotificationRuleName(alertName);
	}

	@When("I click on edit icon on Properties window")
	public void i_click_on_edit_icon_on_Properties_window() {
		notificationManagement.clickPropertyEditIcon();
	}

	@When("I click on delete icon on Properties window")
	public void i_click_on_delete_icon_to_delete_alert() {
		notificationManagement.clickPropertyDeleteIcon();
	}

	@Then("{string} Notification rule is deleted")
	public void alert_is_delete(String alertName) {
		assertFalse(notificationManagement.isNotificationRulePresent(alertName));
	}
	
	@When("I set Time of Day with a duration of {int} minutes")
	public void i_set_Time_of_Day_with_time_span_of_minutes(Integer minutesToAdd) {
		context.set("StartTime", notificationManagement.setPropertyTimeOfDayStartTime());
		context.set("FinishTime", notificationManagement.setPropertyTimeOfDayFinishTime(minutesToAdd));
	}
	
	@Then("Start Time for the Notification rule is saved")
	public void start_Time_for_the_Notification_rule_is_saved_properly() {
		assertEquals(notificationManagement.getPropertyTimeOfDayStartTime(),
				context.get("StartTime").toString().replaceAll("\\s", ""));
	}

	@Then("Finish Time for the Notification rule is saved")
	public void finish_Time_for_the_Notification_rule_is_saved() {
		assertEquals(notificationManagement.getPropertyTimeOfDayFinishTime(),
				context.get("FinishTime").toString().replaceAll("\\s", ""));
	}

	@When("I search for {string} on Notification Management Page")
	public void i_search_page_for_noification_managment(String input) {
		notificationManagement.setFilterValue(input);
	}
}
