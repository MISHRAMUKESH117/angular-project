package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class ProcedureManagement {

	pages.ProcedureManagement procedureManagementPage;
	TestContext context;

	public ProcedureManagement(TestContext context) {
		this.context = context;
		procedureManagementPage = new pages.ProcedureManagement(context.getDriver());
	}

	@When("I click on {string} tab name on the left panel in Procedure Management page")
	public void i_click_on_something_tab_name_on_the_left_panel_in_procedure_management_page(String tabName) {
		procedureManagementPage.clickLeftMenuTabs(tabName);
	}

	@Then("{string} page under Procedure Management is loaded properly")
	public void something_page_under_procedure_management_is_loaded_properly(String tabName) {
		assertTrue(procedureManagementPage.isProcedureManagementMenuHeaderVisible(tabName));
		Log.printInfo(tabName + " page under Procedure Management is loaded properly");
	}
	

    @When("I select {string} excel to upload in Procedure Management module, Configuration of Procedure page")
    public void i_select_something_excel_to_upload_in_procedure_management_module_configuration_of_procedure_page(String fileName) {
    	procedureManagementPage.procedureTabUploadProcedureListExcel(fileName);
    	Log.printInfo(fileName + " is uploaded in Procedure Management module, Configuration of Procedure page");
    }

    @When("I set Procedure name {string} to search on Procedure Management module, Configuration of Procedure page")
    public void i_set_procedure_name_something_to_search(String procedureName) {
    	procedureManagementPage.setConfigurationOfProcedurePageProcedureToSearch(procedureName);
    }
    
    @When("I click on search procedure button on Procedure Management module, Configuration of Procedure page")
    public void i_click_on_search_procedure_button_on_procedure_management_module_configuration_of_procedure_page() {
    	procedureManagementPage.clickConfigurationOfProcedurePageSearchProcedureButton();
    }
    
    @Then("{string} procedure is {string}")
    public void something_procedure_is_something(String procedureName, String procedureVisibility) {
    	assertTrue(procedureManagementPage.checkConfigurationOfProcedurePageProcedureVisibilityInGrid(procedureName, procedureVisibility));
    	Log.printInfo(procedureName + " procedure is "+ procedureVisibility);
    }
    
    @When("I delete {string} procedure from the grid")
    public void i_delete_something_procedure_from_the_grid(String procedureName) {
    	procedureManagementPage.clickConfigurationOfProcedurePageGridDeleteProcedureButton(procedureName);
    	procedureManagementPage.clickConfigurationOfProcedurePageProcedureGridSaveButton();
    	Log.printInfo(procedureName + " is deleted");
    }

    @When("I enter {string} as template name")
    public void i_enter_as_template_name(String templateName) {
    	procedureManagementPage.setTemplateNameInTemplatePanel(templateName);
    }
    
    @When("I click on {string} icon of template page")
    public void i_click_on_icon_of_template_page(String iconName) {
    	procedureManagementPage.clickTemplatePanelActionIcon(iconName);
    }

    @Then("{string} template is loaded")
    public void template_is_loaded(String templateName) {
    	assertTrue(procedureManagementPage.isTemplateLoaded(templateName));
    }

    @When("I select {string} as procedure name")
    public void i_select_as_procedure_name(String procedureName) {
    	procedureManagementPage.selectTemplateProcedureName(procedureName);
    }  

    @When("I select {string} template as default template")
    public void i_select_template_as_default_template(String templateName) {
    	procedureManagementPage.selectDefaultTemplate(templateName);
    }

    @When("I click on {string} button of template page")
    public void i_click_on_button_of_template_page(String buttonName) {
    	procedureManagementPage.clickTemplatePageButton(buttonName);
    }
    
    @When("I set new Procedure name {string}")
    public void setNewProcedureName(String procedureName) {
    	procedureManagementPage.setProcedurePageNewProcedureName(procedureName);
    }

    @When("I set Procedure code as {string} for new Procedure")
    public void setProcedureCodeFoNewProcedure(String procedureCode) {
    	procedureManagementPage.setProcedurePageNewProcedureCode(procedureCode);
    }
    
    @When("I click multi-choice dropdown arrow for {string}")
    public void clickProcedurePageMultiChoiceDropdownArrow(String dropdownName) {
    	procedureManagementPage.clickProcedurePageMultiChoiceDropdownArrow(dropdownName);
    }

    @When("I add Body Part to the Procedure")
    public void addBodyPartToProcedure(DataTable bodyPartOptions) {
    	List<String> bodyParts = bodyPartOptions.asList();
    	for (String bodyPart : bodyParts) 
			procedureManagementPage.clickProcedurePageNewProcedureBodyPartDropdownOption(bodyPart);
    }

    @When("I click on new Procedure Add icon")
    public void clickNewProcedureAddIcon() {
    	procedureManagementPage.clickNewProcedureAddIcon();
    }
}
