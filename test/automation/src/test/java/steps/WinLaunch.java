package steps;


import java.io.IOException;
import io.cucumber.java.en.Given;
import setterGetter.TestContext;
import utilities.WinAppLauncher;

public class WinLaunch {

	TestContext context;
	WinAppLauncher winAppLauncher;
	public static boolean dunit = false;

	public WinLaunch(TestContext context) {
		this.context = context;
		winAppLauncher = new WinAppLauncher();
	}

	@Given("{string} is launched")
	public void windows_application_is_launched(String appName) throws IOException {
		winAppLauncher.launchWinApp(appName);
		context.setWinDriver(winAppLauncher.getWinDriver());
	}
}

