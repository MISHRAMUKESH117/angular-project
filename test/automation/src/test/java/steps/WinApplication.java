package steps;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.WindowsLaunch;
import setterGetter.TestContext;

public class WinApplication {

	TestContext context;
	WindowsLaunch windowsLaunchPage;

	public WinApplication(TestContext context) {
		this.context = context;
		windowsLaunchPage = new WindowsLaunch(context.getWinDriver());
	}
	
	@When("I uninstall zvExtender")
	public void i_uninstallzvExtender() {
		windowsLaunchPage.clickUninstallIcon();
	}
	
	@Then("zvExtender is uninstalled")
	public void iszvExtenderUninstalled() {
		assertFalse(windowsLaunchPage.iszvUninstalled());
	}

	@When("I close Control Panel window")
	public void closeControlPanelWindow() {
		windowsLaunchPage.closeControlPanel();
	}
	
	
	@When("zvExtender window is closed")
	public void closezvExtender() {
		windowsLaunchPage.closeWindow();
	}
	
	@Then("zvExtender is installed")
	public void iszvExtenderInstalled() {
		assertTrue(windowsLaunchPage.iszvInstalled());
		windowsLaunchPage.closeControlPanel();
	}
	
	
	}

