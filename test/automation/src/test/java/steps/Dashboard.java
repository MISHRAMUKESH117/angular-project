package steps;

import static org.junit.Assert.assertTrue;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class Dashboard {
	TestContext context;
	pages.Dashboard dashboardPage;

	public Dashboard(TestContext context) {
		this.context = context;
		dashboardPage = new pages.Dashboard(context.getDriver());
	}
	
	@When("I click on {string} menu")
	public void i_click_on_menu(String menuOption) {
		dashboardPage.clickDashboardMenu(menuOption);
	}

	@Then("service items load properly")
	public void service_items_load_properly() {
		assertTrue(dashboardPage.isServiceMenuLoaded());
	}

	@Then("DICOM Routing items load properly")
	public void dicom_Routing_items_load_properly() {
		assertTrue(dashboardPage.isDicomRoutingLoaded());
	}

	@Then("User Monitoring items load properly")
	public void user_Monitoring_items_load_properly() {
		assertTrue(dashboardPage.isUserMonitoringLoaded());
	}

}
