package steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import utilities.Log;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setterGetter.TestContext;

public class ConfigurationNoDriver {

	Actions.ConfigurationNoDriver configuration;
	TestContext context;

	public ConfigurationNoDriver(TestContext context) {
		this.context = context;
		configuration = new Actions.ConfigurationNoDriver();
	}

	@Given("Connect to database and set browser compatibility as")
	public void changeBrowserCompatibilityConfiguration(DataTable browserTypes) {
		List<String> browserTypeList = browserTypes.asList();
		configuration.changeBrowserCompatibilityConfiguration(browserTypeList);
	}
	
	@When("I update automation property {string} to {string}")
	public void changeAutomationPropertyValue(String propertyType,String propertyValue) {
		configuration.changeAutomationPropertyValue(propertyType,propertyValue);
	}
	
	@Given("Connect to database and set browser compatibility as null")
	public void changeBrowserCompatibilityConfiguration() {
		configuration.changeBrowserCompatibilityConfigurationNull();
	}
	
	@Given("I delete user session from database")
	public void deleteUserSession() {
		configuration.deleteUserSession();
	}
}
