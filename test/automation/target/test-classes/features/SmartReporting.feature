@SmartReporting
Feature: Smart Reporting

@automated
@SmartReporting
@TC_SR-004
Scenario: Create a new template
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on plus icon to create new template
And I enter "Test Template" as name to create new template
And I select following values in filter
|Modality       |Procedure Name          |Body Part|Subspecialty|Site    |Radiologist|
|CP - Culposcopy|CT Abdomen with Contrast|abdomen  |Body CT     |Clinic B|Chan, Kathy|
And I click on add icon to add filter
And I check publish checkbox to publish template
Then new template is created


@automated
@SmartReporting
@TC_SR-002
Scenario: Search a template and edit template
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
Then template table should have below columns
|Name |Created By |Updated By |Updated On |Publish |
When I search "Test Template" template 
And I add follwing values in template
|Modality                |Body Part |Subspecialty|
|CT - Computed Tomography|abdomen   |Body CT     |
And I click on add icon to add filter
Then filters are added with below procedure
|Procedure Name                   |Procedure Code |Modality|
|CT Abdomen Pelvis w+w/o Contrast |CT100          |CT      | 
|CT Abdomen Pelvis with Contrast  |CT101          |CT      |
|CT Abdomen w/o Contrast          |CT102          |CT      | 


@automated
@SmartReporting
@TC_SR-003
Scenario: Apply template properties
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
Then I verify below template proprties
|Filter | Macro | Procedure|
When I search "Test Template" template 
And I select "CT Abdomen w/o Contrast" as "Procedure Name"
And I click on add icon to add filter
Then procedure should be visible with below details
|Procedure Name         |Procedure Code|Modality|
|CT Abdomen w/o Contrast|CT102         |CT      | 


@automated
@SmartReporting
@TC_SR-005
Scenario: Build a new template
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on plus icon to create new template
And I enter "CT with build" as name to create new template
And I select following values in filter
|Modality       |Procedure Name          |Body Part|Subspecialty|Site      |Radiologist    |
|CS - Cystoscopy|CT Abdomen w/o Contrast |elbow    |Body US     |Hospital A|Bradley, Shelby|
And I click on add icon to add filter
And I check publish checkbox to publish template
And I click on build icon to build template
Then "CT with build" is created in template builder


@automated
@SmartReporting
@TC_SR-006
Scenario: Add new sections in template
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Build" tab to open new page
And I search for "CT with build" template
And I add following section in template
|Study Description| Reason For Study| Exam Parameters| Comments|
Then below sections are added in template
|Study Description| Reason For Study| Exam Parameters| Comments|

@automated
@SmartReporting
@TC_SR-007
Scenario: Clone a existing template
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I search "Test Template" template 
And I click on clone icon to clone template
Then template is cloned with "Cloned Test Template" name

@automated
@SmartReporting
@TC_SR-008
Scenario: Delete a template
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I search "Test Template" template 
And I click on delete icon to delete template
Then "Cloned Test Template" is deleted 
And I search "Test Template" template 
And I click on delete icon to delete template
Then "Test Template" is deleted 

@automated
@SmartReporting
@TC_SR-009
Scenario: Create a new block
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I click on plus icon to create new block
And I create new block with below details
|Name                  | Quality Measure | Block Content         |
|New block for patient |195 - Radiology  |Block content is proper|
And I click on "Save Block Content" icon on block content
And I click on "Build" tab to open new page
And I search for "CT with build" template
And I search block "Block content is proper" by block content
And I add "Block content is proper" in "Orphan" section
And I click on "Block" tab to open new page
And I search "New block for patient" by name
Then block with "New block for patient" is created

@automated
@SmartReporting
@TC_SR-010
Scenario: Search a existing template with modality and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I select "CS - Cystoscopy" as "Modality" of template
And I search "CT with build" template 
Then template is present
When I clear dropdown value of template
And I clear name from search result of template
Then all templates are loaded

@automated
@SmartReporting
@TC_SR-011
Scenario: Search a existing template with procedure name and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I select "CT Abdomen w/o Contrast" as "Procedure Name" of template
And I search "CT with build" template 
Then template is present
When I clear dropdown value of template
And I clear name from search result of template
Then all templates are loaded

@automated
@SmartReporting
@TC_SR-012
Scenario: Search a existing template with body part and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I select "elbow" as "Body Part" of template
And I search "CT with build" template 
Then template is present
When I clear dropdown value of template
And I clear name from search result of template
Then all templates are loaded

@automated
@SmartReporting
@TC_SR-013
Scenario: Search a existing template with subspecialty and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I select "Body US" as "Subspecialty" of template
And I search "CT with build" template 
Then template is present
When I clear dropdown value of template
And I clear name from search result of template
Then all templates are loaded

@automated
@SmartReporting
@TC_SR-014
Scenario: Search a existing template with template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I search "CT with build" template 
Then template is present
When I clear name from search result of template
Then all templates are loaded


@automated
@SmartReporting
@TC_SR-015
Scenario: Search a existing template with created by and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I select " Test123, test123" as "Created By" of template
And I search "CT with build" template 
Then template is present
When I clear dropdown value of template
And I clear name from search result of template
Then all templates are loaded


@automated
@SmartReporting
@TC_SR-016
Scenario: Search a existing template with updated by and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I select "Larsen, Sara" as "Updated By" of template
And I search "CT with build" template 
Then template is present
When I clear dropdown value of template
And I clear name from search result of template
Then all templates are loaded


@automated
@SmartReporting
@TC_SR-017
Scenario: Search a existing template with updated on and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I set "01/05/2020" as updated by date of template
And I search "CT with build" template 
Then template is present
When I clear date value of template
And I clear name from search result of template
Then all templates are loaded

@automated
@SmartReporting
@TC_SR-018
Scenario: Search a existing template with publish and template name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I select "Published" as "Publish" of template
And I search "CT with build" template 
Then template is present
When I clear dropdown value of template
And I clear name from search result of template
Then all templates are loaded

@automated
@SmartReporting
@TC_SR-019
Scenario: Search a block by name and section
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I select "Comparison" as "Sections" of block
And I search "Empty" by name
Then block with "Empty" name is present
When I clear dropdown value of block
And I clear name from search result of block
Then all blocks are loaded


@automated
@SmartReporting
@TC_SR-020
Scenario: Search a block by name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I search "New block for patient" by name
Then block with "New block for patient" name is present
When I clear name from search result of block
Then all blocks are loaded


@automated
@SmartReporting
@TC_SR-021
Scenario: Search a block by name and created by
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I select " Test123, test123" as "Created By" of block
And I search "New block for patient" by name
Then block with "New block for patient" name is present
When I clear dropdown value of block
And I clear name from search result of block
Then all blocks are loaded


@automated
@SmartReporting
@TC_SR-022
Scenario: Search a block by name and updated by
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I select " Test123, test123" as "Updated By" of block
And I search "New block for patient" by name
Then block with "New block for patient" name is present
When I clear dropdown value of block
And I clear name from search result of block
Then all blocks are loaded


@automated
@SmartReporting
@TC_SR-023
Scenario: Search a block by name and updated on
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I set today as updated by date of block
And I search "New block for patient" by name
Then block with "New block for patient" name is present
When I clear date value of block
And I clear name from search result of block
Then all blocks are loaded


@automated
@SmartReporting
@TC_SR-024
Scenario: Search a block by name and used
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I select "Used" as "Used" of block
And I search "New block for patient" by name
Then block with "New block for patient" name is present
When I clear dropdown value of block
And I clear name from search result of block
Then all blocks are loaded

@automated
@SmartReporting
@TC_SR-025
Scenario: Create a new macro
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I click on plus icon to create new macro
And I enter "CT with new macro" as name to create new macro
And I select following values to add in macro filter
|Modality       |Procedure Name          |Body Part|Subspecialty|Site    |Radiologist|
|CP - Culposcopy|CT Abdomen with Contrast|abdomen  |Body CT     |Clinic B|Chan, Kathy|
And I click on add icon to add filter to macro
And I set "Macro content is set" as macro content
And I click on "Save Block Content" icon on macro content
And I check publish checkbox to publish macro
And I search "CT with new macro" by macro name
Then macro with "CT with new macro" is created


@automated
@SmartReporting
@TC_SR-026
Scenario: Search a macro by name and modality
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I select "CP - Culposcopy" as "Modality" of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded


@automated
@SmartReporting
@TC_SR-027
Scenario: Search a macro by name and procedure name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I select "CT Abdomen with Contrast" as "Procedure Name" of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded


@automated
@SmartReporting
@TC_SR-028
Scenario: Search a macro by name and body part
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I select "abdomen" as "Body Part" of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded


@automated
@SmartReporting
@TC_SR-029
Scenario: Search a macro by name and subspecialty
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I select "Body CT" as "Subspecialty" of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded

@automated
@SmartReporting
@TC_SR-030
Scenario: Search a macro by name
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear name from search result of macro
Then all macro are loaded


@automated
@SmartReporting
@TC_SR-031
Scenario: Search a macro by name and created by
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I select " Test123, test123" as "Created By" of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded

@automated
@SmartReporting
@TC_SR-032
Scenario: Search a macro by name and updated by
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I select " Test123, test123" as "Updated By" of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded

@automated
@SmartReporting
@TC_SR-033
Scenario: Search a macro by name and updated on
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I set today as updated by date of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded

@automated
@SmartReporting
@TC_SR-034
Scenario: Search a macro by name and published
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I select "Published" as "Publish" of macro
And I search "CT with new macro" by macro name
Then "CT with new macro" macro is present
When I clear dropdown value of macro
And I clear name from search result of macro
Then all macro are loaded


@automated
@SmartReporting
@TC_SR-036
Scenario: Delete a block
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Block" tab to open new page
And I search "New block for patient" by name
And I open "New block for patient" block
And I click on delete icon of block property
Then "New block for patient" block is deleted 
And I click on "Template" tab to open new page
And I search "CT with build" template 
And I click on delete icon to delete template
Then "CT with build" is deleted 


@automated
@SmartReporting
@TC_SR-037
Scenario: Delete a macro
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
When I open "Template" module
And I click on "Macro" tab to open new page
And I search "CT with new macro" by macro name
And I open "CT with new macro" macro
And I click on delete icon of macro property
Then "CT with new macro" macro is deleted 