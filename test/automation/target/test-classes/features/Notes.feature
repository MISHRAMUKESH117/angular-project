@Notes
Feature: Worklist Notes


@automated
@Notes
@BVT
@TC_NT-001
Scenario: Add Tech QA note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I click on "Tech QA Note" note icon
And I create "Tech QA Note" with following details
|Note Type           | Message         |
|Documentation error |Test Tech QA Note|
And I click on "Submit" button to add note
Then note is created
When I open "Test Tech QA Note" to edit
And I add comment "Test Tech QA Note" in "Tech QA"
And I change note status to "Acknowledge"
Then note status is changed to "Complete"
When I open "Test Tech QA Note" to edit
And I delete "Acknowledged" status from note
Then note status is changed to "Pending"
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Tech QA Note" section in Advanced Search
And I enter "Test Tech QA Note" in keyword textbox of "Tech QA Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
And I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Tech QA Note" as worklist name
And I click on "Save" button to add new worklist
Then "Tech QA Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "Tech QA Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Tech QA note indicator
Then note tooltip with "Test Tech QA Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
When I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Tech QA Note" to edit
Then "Tech QA Note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Tech QA        |View Tech QA notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Tech QA        |View Tech QA notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Tech QA Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Tech QA note indicator
Then note tooltip with "Test Tech QA Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
|Tech QA        |View Tech QA notes   |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
|Tech QA        |View Tech QA notes   |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Tech QA Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Tech QA Note" to edit
And I delete "Created" status from note
Then note is deleted
When I right click on "Tech QA Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Tech QA Note" worklist
Then "Tech QA Note" worklist is deleted from "My Worklist"


@automated
@Notes
@BVT
@TC_NT-002
Scenario: Add Follow-up Note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I click on "Follow-up Note" note icon
And I enter today's date in "dueDate" 
And I enter comment "Test Follow-up Note" in "Follow-up Note"
And I click on "Submit" button to add note
Then note is created
When I open "Test Follow-up Note" to edit
And I change note status to "Scheduled"
Then note status is changed to "Scheduled"
When I open "Test Follow-up Note" to edit
And I change note status to "Complete"
Then note status is changed to "Completed"
When I open "Test Follow-up Note" to edit
And I delete "Completed" status from note
Then note status is changed to "Scheduled"
When I open "Test Follow-up Note" to edit
And I delete "Scheduled" status from note
Then note status is changed to "Pending"
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Follow Up Note" section in Advanced Search
And I enter "Test Follow-up Note" in keyword textbox of "Follow Up Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
And I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Follow-up Note" as worklist name
And I click on "Save" button to add new worklist
Then "Follow-up Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "Follow-up Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Follow-Up note indicator
Then note tooltip with "Test Follow-up Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
When I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Follow-up Note" to edit
Then "Follow-up" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View Follow-up notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View Follow-up notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Follow-up Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Follow-Up note indicator
Then note tooltip with "Test Follow-up Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
|Note           |View Follow-up notes          |   
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
|Note           |View Follow-up notes          |  
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Follow-up Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Follow-up Note" to edit
And I delete "Pending" status from note
Then note is deleted
When I right click on "Follow-up Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Follow-up Note" worklist
Then "Follow-up Note" worklist is deleted from "My Worklist"


@automated
@Notes
@BVT
@TC_NT-003
Scenario: Add patient note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I click on "Patient Note" note icon
And I create "Patient Note" with following details
|Note Type | Message         |
|Info      |Test Patient Note|
And I click on "Submit" button to add note
Then note is created
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Patient Note" section in Advanced Search
And I enter "Test Patient Note" in keyword textbox of "Patient Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
And I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Patient Note" as worklist name
And I click on "Save" button to add new worklist
Then "Patient Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "Patient Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Patient Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
When I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Patient Note" to edit
Then "Patient note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Note           |View Patient notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Note           |View Patient notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Patient Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Patient Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
|Note           |View Patient notes          |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
|Note           |View Patient notes          |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Patient Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Patient Note" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted
When I right click on "Patient Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Patient Note" worklist
Then "Patient Note" worklist is deleted from "My Worklist"

@automated
@Notes
@BVT
@TC_NT-004
Scenario: Add exam note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I click on "Exam Note" note icon
And I create "Exam Note" with following details
|Note Type | Message      |
|Tech Note |Test Exam Note|
And I click on "Submit" button to add note
Then note is created
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Exam Note" section in Advanced Search
And I enter "Test Exam Note" in keyword textbox of "Exam Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Exam Note" as worklist name
And I click on "Save" button to add new worklist
Then "Exam Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "Exam Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Exam Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name          |
|Note           |Administrator Exam notes |
|Note           |Create Exam notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name          |
|Note           |Administrator Exam notes |
|Note           |Create Exam notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Exam Note" to edit
Then "Exam note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name |
|Note           |View Exam notes  |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name |
|Note           |View Exam notes  |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Exam Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Exam Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name         |
|Note           |Administrator Exam notes|
|Note           |Create Exam notes       |
|Note           |View Exam notes         |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name         |
|Note           |Administrator Exam notes|
|Note           |Create Exam notes       |
|Note           |View Exam notes         |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Exam Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Exam Note" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted
When I right click on "Exam Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Exam Note" worklist
Then "Exam Note" worklist is deleted from "My Worklist"

@automated
@Notes
@BVT
@TC_NT-006
Scenario: Add ED Prelim note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I click on "Prelim Note" note icon
And I create "Prelim Note" with following details
|Note Type | Message        |
|Normal    |Test Prelim Note|
And I click on "Submit" button to add note
Then note is created
And note status is changed to "Pending"
When I open "Test Prelim Note" to edit
And I change note status to "Minor Discrepancy"
Then note status is changed to "Discrepancy"
When I open "Test Prelim Note" to edit
And I change note status to "Acknowledge"
Then note status is changed to "Complete"
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "ED Prelim Note" section in Advanced Search
And I enter "Test Prelim Note" in keyword textbox of "ED Prelim Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "ED Prelim Note" as worklist name
And I click on "Save" button to add new worklist
Then "ED Prelim Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "ED Prelim Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Prelim Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Prelim Note" to edit
Then "ED Prelim" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View ED Prelim notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View ED Prelim notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "ED Prelim Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Prelim Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
|Note           |View ED Prelim notes          |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
|Note           |View ED Prelim notes          |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "ED Prelim Note" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Prelim Note" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted
When I right click on "ED Prelim Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "ED Prelim Note" worklist
Then "ED Prelim Note" worklist is deleted from "My Worklist"


@automated
@Notes
@BVT
@TC_NT-007
Scenario: Add teaching note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I click on "Teaching Note" note icon
And I create teaching note with below details
|Title        |Message           |
|Teaching Note|Teaching Note Test|
And I click on "Submit" button to add note
Then note is created
When I open "Teaching Note Test" to edit
And I add comment "Teaching Note Comment" in "Teaching Note"
And I click on "Save" button to save comment
Then "Teaching Note Comment" is added in teaching note
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Teaching Note" section in Advanced Search
And I enter "Teaching Note Test" in keyword textbox of "Teaching Note" 
And I click on "Search" to search results
And I click on exam "246930740"
Then I can see "Teaching Note Comment" note
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name       |
|Teaching Note  |Admin Teaching notes  |
|Teaching Note  |Create Teaching notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Teaching Note  |Admin Teaching notes |
|Teaching Note  |Create Teaching notes|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Teaching Note Test" to edit
Then "Teaching note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Teaching Note  |View Teaching notes|
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Teaching Note  |View Teaching notes|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type | Permission Name     |
|Teaching Note   |Admin Teaching notes |
|Teaching Note   |Create Teaching notes|
|Teaching Note   |View Teaching notes  |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name       |
|Teaching Note  |Admin Teaching notes  |
|Teaching Note  |Create Teaching notes |
|Teaching Note  |View Teaching notes   |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Teaching Note Test" to edit
And I delete "Teaching Note" from note
Then note is deleted
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Teaching Note" section in Advanced Search
And I enter "Teaching Note Test" in keyword textbox of "Teaching Note" 
And I click on "Search" to search results
Then exam with note is not loaded


@automated
@Notes
@BVT
@TC_NT-008
Scenario: Add communication note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I click on "Communication Note" note icon
And I add "Communication Note Test" in note box
And I click on "Submit" button to add note
Then note is created
When I open "Communication Note Test" to edit
And I add "Communication Note Comment" in comment textbox 
And I click on "Comment" button to add comment
Then "Communication Note Comment" is seen in note
When I open "Communication Note Test" to edit
And I change note status to "Attempted"
Then note status is changed to "Attempted"
When I open "Communication Note Test" to edit
And I delete "Attempted" status from note
When I open "Communication Note Test" to edit
And I change note status to "Complete"
Then note status is changed to "Completed"
When I open "Communication Note Test" to edit
And I delete "Complete" status from note
And I open "Communication Note Test" to edit
And I delete "Commented" status from note
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name                  |
|Note           |Administrator Communication notes|
|Note           |Create Communication notes       |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name                  |
|Note           |Administrator Communication notes|
|Note           |Create Communication notes       |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Communication Note Test" to edit
Then communication note edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name         |
|Note           |View Communication notes|
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name         |
|Note           |View Communication notes|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name                  |
|Note           |Administrator Communication notes|
|Note           |Create Communication notes       |
|Note           |View Communication notes         |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name                  |
|Note           |Administrator Communication notes|
|Note           |Create Communication notes       |
|Note           |View Communication notes         |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Communication Note Test" to edit
And I delete "Waiting" status from note
Then note is deleted

@automated
@Notes
@BVT
@TC_NT-009
Scenario: Add communication note to exam from worklist exam grid
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
When I right click on "246930740" exam
And I mouse hover on "Add Note" option
And I click on "Communication Note" option
And I add "Communication Note Test" in note box
And I click on "Submit" button to add note
And I click on exam "246930740"
Then note is created
When I click "My Reading Queue" from "My Worklist"
And exam is displayed "246930740"
And I mouse hover on "246930740" communication note indicator
Then note tooltip with "Communication Note Test" message is "present"
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Communication Note Test" to edit
And I add "Communication Note Comment" in comment textbox 
And I click on "Comment" button to add comment
Then "Communication Note Comment" is seen in note
When I open "Communication Note Test" to edit
And I change note status to "Attempted"
Then note status is changed to "Attempted" with current date
When I open "Communication Note Test" to edit
And I delete "Attempted" status from note
Then note status is changed to "Waiting"
When I open "Communication Note Test" to edit
And I change note status to "Complete"
Then note status is changed to "Completed" with current date
When I open "Communication Note Test" to edit
And I delete "Completed" status from note
Then note status is changed to "Waiting"
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Communication Note" section in Advanced Search
And I enter "Communication Note Test" in keyword textbox of "Communication Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
And I click on exam "246930740"
And I can see note
When I open "Communication Note Test" to edit
And I delete "Waiting" status from note
Then note is deleted
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Communication Note" section in Advanced Search
And I enter "Communication Note Test" in keyword textbox of "Communication Note" 
And I click on "Search" to search results
Then exam with note is not loaded

@automated
@Notes
@BVT
@TC_NT-010
Scenario: Add exam note to exam from worklist exam grid
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I click on "Exam Note" note icon
And I create "Exam Note" with following details
|Note Type | Message                   |
|Tech Note |Test Exam Note as non-admin|
And I click on "Submit" button to add note
Then note is created
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
When I right click on "246930740" exam
And I mouse hover on "Add Note" option
And I click on "Exam Note" option
And I create "Exam Note" with following details
|Note Type | Message      |
|Tech Note |Test Exam Note|
And I click on "Submit" button to add note
And I click on exam "246930740"
Then note is created
When I click "My Reading Queue" from "My Worklist"
And exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Exam Note" message is "present"
When I click "My Reading Queue" from "My Worklist"
And "246930741" exam is present in history panel
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Exam Note" section in Advanced Search
And I click on "Type" label dropdown arrow of "Exam Note"
And I select "Tech Note" from dropdown
And I enter "Test Exam Note" in keyword textbox of "Exam Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Exam Note" as worklist name
And I click on "Save" button to add new worklist
Then "Exam Note" worklist is added under "My Worklist"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name          |
|Note           |Administrator Exam notes |
|Note           |Create Exam notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name          |
|Note           |Administrator Exam notes |
|Note           |Create Exam notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Exam Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Exam Note" to edit
Then "Exam note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name |
|Note           |View Exam notes  |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name |
|Note           |View Exam notes  |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Exam Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Exam Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name         |
|Note           |Administrator Exam notes|
|Note           |Create Exam notes       |
|Note           |View Exam notes         |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name         |
|Note           |Administrator Exam notes|
|Note           |Create Exam notes       |
|Note           |View Exam notes         |
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name   |
|Note           |Create Exam notes |
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Exam Note" to edit
Then "Test Exam Note" edit window is not opening
And I can see note
When I click on "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name   |
|Note           |Create Exam notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Exam Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Exam Note" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted
When I right click on "Exam Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Exam Note" worklist
Then "Exam Note" worklist is deleted from "My Worklist"
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I open "Test Exam Note as non-admin" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted

@automated
@Notes
@BVT
@TC_NT-011
Scenario: Add Follow-up note to exam from worklist exam grid
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I click on "Follow-up Note" note icon
And I enter today's date in "dueDate" 
And I enter comment "Test Follow-up Note as non-admin" in "Follow-up Note"
And I click on "Submit" button to add note
Then note is created
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
When I right click on "246930740" exam
And I mouse hover on "Add Note" option
And I click on "Follow-up Note" option
And I enter today's date in "dueDate" 
And I enter comment "Test Follow-up Note" in "Follow-up Note"
And I click on "Submit" button to add note
And I click on exam "246930740"
Then note is created
When I open "Test Follow-up Note" to edit
And I change note status to "Scheduled" 
Then note status is changed to "Scheduled" with current date
When I open "Test Follow-up Note" to edit
And I change note status to "Complete"
Then note status is changed to "Completed" with current date
When I open "Test Follow-up Note" to edit
And I delete "Completed" status from note
Then note status is changed to "Scheduled" with current date
When I open "Test Follow-up Note" to edit
And I delete "Scheduled" status from note
Then note status is changed to "Pending"
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Follow Up Note" section in Advanced Search
And I click on "Status" label dropdown of "Follow Up Note"
And I select "Pending" from dropdown option
And I enter "Test Follow-up Note" in keyword textbox of "Follow Up Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Follow-up Note" as worklist name
And I click on "Save" button to add new worklist
Then "Follow-up Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "Follow-up Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Follow-Up note indicator
Then note tooltip with "Test Follow-up Note" message is "present"
When I click "My Reading Queue" from "My Worklist"
And "246930741" exam is present in history panel
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
When I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Follow-up Note" to edit
Then "Follow-up" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View Follow-up notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View Follow-up notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Follow-up Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Follow-Up note indicator
Then note tooltip with "Test Follow-up Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
|Note           |View Follow-up notes          |   
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator Follow-up notes |
|Note           |Create Follow-up notes        |
|Note           |View Follow-up notes          |  
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name       |
|Note           |Create Follow-up notes|
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Follow-up Note" to edit
Then "Test Follow-up Note" edit window is not opening
And I can see note
When I click on "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name        |
|Note           |Create Follow-up notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Follow-up Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Follow-up Note" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted
When I right click on "Follow-up Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Follow-up Note" worklist
Then "Follow-up Note" worklist is deleted from "My Worklist"
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I open "Test Follow-up Note as non-admin" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted


@automated
@Notes
@BVT
@TC_NT-012
Scenario: Add patient note to exam from worklist exam grid
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I click on "Patient Note" note icon
And I create "Patient Note" with following details
|Note Type | Message                      |
|Info      |Test Patient Note as non-admin|
And I click on "Submit" button to add note
Then note is created
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
When I right click on "246930740" exam
And I mouse hover on "Add Note" option
And I click on "Patient Note" option
And I create "Patient Note" with following details
|Note Type | Message         |
|Info      |Test Patient Note|
And I click on "Submit" button to add note
And I click on exam "246930740"
Then note is created
When I click "My Reading Queue" from "My Worklist"
And I click Advanced Search
And I expand the "Patient Note" section in Advanced Search
And I click on "Type" label dropdown arrow of "Patient Note"
And I select "Info" from dropdown
And I enter "Test Patient Note" in keyword textbox of "Patient Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Patient Note" as worklist name
And I click on "Save" button to add new worklist
Then "Patient Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "Patient Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Patient Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
When I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Patient Note" to edit
Then "Patient note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Note           |View Patient notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Note           |View Patient notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Patient Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Patient Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
|Note           |View Patient notes          |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name             |
|Note           |Administrator Patient notes |
|Note           |Create Patient notes        |
|Note           |View Patient notes          |
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name     |
|Note           |Create Patient notes|
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Patient Note" to edit
Then "Patient note" edit window is not opening
And I can see note
When I click on "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Note           |Create Patient notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Patient Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Patient Note" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted
When I right click on "Patient Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Patient Note" worklist
Then "Patient Note" worklist is deleted from "My Worklist"
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I open "Test Patient Note as non-admin" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted

@automated
@Notes
@BVT
@TC_NT-013
Scenario: Add ED Prelim note to exam from worklist exam grid
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I click on "Prelim Note" note icon
And I create "Prelim Note" with following details
|Note Type | Message                     |
|Normal    |Test Prelim Note as non-admin|
And I click on "Submit" button to add note
Then note is created
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
When I right click on "246930740" exam
And I mouse hover on "Add Note" option
And I click on "Prelim Note" option
And I create "Prelim Note" with following details
|Note Type | Message        |
|Normal    |Test Prelim Note|
And I click on "Submit" button to add note
And I click on exam "246930740"
Then note is created
And note status is changed to "Pending"
When I open "Test Prelim Note" to edit
And I change note status to "Minor Discrepancy"
Then note status is changed to "Discrepancy"
When I open "Test Prelim Note" to edit
And I change note status to "Acknowledge"
Then note status is changed to "Complete"
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "ED Prelim Note" section in Advanced Search
And I click on "Type" label dropdown of "Tech QA Note"
And I select "Normal" from dropdown option
And I enter "Test Prelim Note" in keyword textbox of "ED Prelim Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "ED Prelim Note" as worklist name
And I click on "Save" button to add new worklist
Then "ED Prelim Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "ED Prelim Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Prelim Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Prelim Note" to edit
Then "ED Prelim" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View ED Prelim notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name     |
|Note           |View ED Prelim notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "ED Prelim Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" indicator
Then note tooltip with "Test Prelim Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
|Note           |View ED Prelim notes          |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name               |
|Note           |Administrator ED Prelim notes |
|Note           |Create ED Prelim notes        |
|Note           |View ED Prelim notes          |
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name        |
|Note           |Create ED Prelim notes |
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Prelim Note" to edit
Then "ED Prelim" edit window is not opening
And I can see note
When I click on "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name       |
|Note           |Create ED Prelim notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "ED Prelim Note" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Prelim Note" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted
When I right click on "ED Prelim Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "ED Prelim Note" worklist
Then "ED Prelim Note" worklist is deleted from "My Worklist"
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I open "Test Prelim Note as non-admin" to edit
And I click on "Delete" button to delete note from exam
Then note is deleted

@automated
@Notes
@BVT
@TC_NT-014
Scenario: Add Tech QA note to exam from worklist exam grid
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I click on "Tech QA Note" note icon
And I create "Tech QA Note" with following details
|Note Type           | Message                      |
|Documentation error |Test Tech QA Note as non-admin|
And I click on "Submit" button to add note
Then note is created
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
When I right click on "246930740" exam
And I mouse hover on "Add Note" option
And I click on "Tech QA Note" option
And I create "Tech QA Note" with following details
|Note Type           | Message         |
|Documentation error |Test Tech QA Note|
And I click on "Submit" button to add note
And I click on exam "246930740"
Then note is created
When I open "Test Tech QA Note" to edit
And I add comment "Test Tech QA Note" in "Tech QA"
And I change note status to "Acknowledge"
Then note status is changed to "Complete"
When I open "Test Tech QA Note" to edit
And I delete "Acknowledged" status from note
Then note status is changed to "Pending"
When I click "My Reading Queue" from "My Worklist"
And I click Advanced Search
And I expand the "Tech QA Note" section in Advanced Search
And I click on "Rating" label dropdown of "Tech QA Note"
And I select "Documentation error" from dropdown option
And I click on "Status" label dropdown of "Tech QA Note"
And I select "Pending" from dropdown option
And I enter "Test Tech QA Note" in keyword textbox of "Tech QA Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Tech QA Note" as worklist name
And I click on "Save" button to add new worklist
Then "Tech QA Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see note
When I click "Tech QA Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Tech QA note indicator
Then note tooltip with "Test Tech QA Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
When I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Tech QA Note" to edit
Then "Tech QA Note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Tech QA        |View Tech QA notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Tech QA        |View Tech QA notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Tech QA Note" from "My Worklist"
Then exam is displayed "246930740"
When I mouse hover on "246930740" Tech QA note indicator
Then note tooltip with "Test Tech QA Note" message is "not present"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
|Tech QA        |View Tech QA notes   |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
|Tech QA        |View Tech QA notes   |
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name     |
|Tech QA        |Create Tech QA notes|
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Tech QA Note" to edit
Then "Tech QA note" edit window is not opening
And I can see note
When I click on "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Tech QA        |Create Tech QA notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Tech QA Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Test Tech QA Note" to edit
And I delete "Created" status from note
Then note is deleted
When I right click on "Tech QA Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Tech QA Note" worklist
Then "Tech QA Note" worklist is deleted from "My Worklist"
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I open "Test Tech QA Note as non-admin" to edit
And I delete "Created" status from note
Then note is deleted

@automated
@Notes
@BVT
@TC_NT-015
Scenario: Add teaching note to exam from worklist exam grid
Given Clario application is launched
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I click on "Teaching Note" note icon
And I create teaching note with below details
|Title        |Message                        |
|Teaching Note|Teaching Note Test as non-admin|
And I click on "Submit" button to add note
Then note is created
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
When I right click on "246930740" exam
And I mouse hover on "Add Note" option
And I click on "Teaching Note" option
And I create teaching note with below details
|Title        |Message            |
|Teaching Note|Teaching Note Test |
And I click on "Submit" button to add note
And I click on exam "246930740"
Then note is created
When I open "Teaching Note Test" to edit
And I add comment "Teaching Note Comment" in "Teaching Note"
And I click on "Save" button to save comment
Then "Teaching Note Comment" is added in teaching note
When I click "My Reading Queue" from "My Worklist"
When I click Advanced Search
And I expand the "Teaching Note" section in Advanced Search
And I enter "Teaching Note Test" in keyword textbox of "Teaching Note" 
And I click on "Search" to search results
Then exam is displayed "246930740"
When I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Teaching Note" as worklist name
And I click on "Save" button to add new worklist
Then "Teaching Note" worklist is added under "My Worklist"
When I click on exam "246930740"
Then I can see "Teaching Note Comment" note
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name       |
|Teaching Note  |Admin Teaching notes  |
|Teaching Note  |Create Teaching notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name      |
|Teaching Note  |Admin Teaching notes |
|Teaching Note  |Create Teaching notes|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Teaching Note Test" to edit
Then "Teaching note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Teaching Note  |View Teaching notes|
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name   |
|Teaching Note  |View Teaching notes|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type | Permission Name     |
|Teaching Note   |Admin Teaching notes |
|Teaching Note   |Create Teaching notes|
|Teaching Note   |View Teaching notes  |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name       |
|Teaching Note  |Admin Teaching notes  |
|Teaching Note  |Create Teaching notes |
|Teaching Note  |View Teaching notes   |
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name      |
|Teaching Note   |Create Teaching notes|
And click on save button
And log out of the application
And log into the application as "radiologist" user
When I open "Smart Worklist" application
Then worklist is launched
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "LISA, FISCHER"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Teaching Note Test" to edit
Then "Teaching note" edit window is not opening
And I can see "Teaching Note Comment" note
When I click on "Management" application
And I open "User Role Management" module
And I select "Testing Role - Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name      |
|Teaching Note   |Create Teaching notes|
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I click "Teaching Note" from "My Worklist"
Then exam is displayed "246930740"
And I click on exam "246930740"
When I open "Teaching Note Test" to edit
And I delete "Teaching Note" from note
Then note is deleted
When I right click on "Teaching Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Teaching Note" worklist
Then "Teaching Note" worklist is deleted from "My Worklist"
When I click "My Reading Queue" from "My Worklist"
And I perform worklist search "HAMILTON, JUSTIN"
Then exam is displayed "12990"
And I click on exam "12990"
When I open "Teaching Note Test as non-admin" to edit
And I delete "Teaching Note" from note
Then note is deleted


@automated
@Notes
@Regression
@TC_NT-016
Scenario: Add Peer Review note to exam
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I perform Quick Search  
| Label     | Value |
| firstName | John  |
| lastName  | Cook  |
And I click on Search button in Quick Search
Then exam is displayed "7739"
When I click on exam "7739"
Then exam is open "Cook, John"
When I expand note section
And I click on "Peer Review Note" note icon
And I enter comment "Test Peer Review Note" in "Peer Review Note"
And I click on "Submit" button to add note
Then note is created
When I click "My Reading Queue" from "My Worklist"
And I click Advanced Search
And I expand the "Peer Review" section in Advanced Search
And I enter "Test Peer Review Note" in keyword textbox of "Peer Review" 
And I perform "Peer Review" Advanced Search with following details
|Label  |Value        |
|Reason |User Selected|
|Status|Final         |
And I expand the "Exam Date" section in Advanced Search
And I select "1y" as date range in Exam Date 
And I click on "Search" to search results
Then exam is displayed "7739"
And I click on Add to Worklist to create new worklist
And I select "My Worklist" from worklist group to add new worklist
And I set "Peer Review Note" as worklist name
And I click on "Save" button to add new worklist
Then "Peer Review Note" worklist is added under "My Worklist"
When I click on exam "7739"
Then I can see note
When I click "Peer Review Note" from "My Worklist"
Then exam is displayed "7739"
When I mouse hover on "7739" Peer Review note indicator
Then note tooltip with "Test Peer Review Note" message is "present"
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name                 |
|Note           |Administrator Peer Review notes |
|Note           |Create Peer Review notes        |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type|Permission Name                 |
|Note           |Administrator Peer Review notes |
|Note           |Create Peer Review notes        |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "Peer Review Note" from "My Worklist"
Then exam is displayed "7739"
When I click on exam "7739"
Then exam is open "Cook, John"
When I open "Test Peer Review Note" to edit
Then "Peer Review Note" edit window is not opening
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name       |
|Note           |View Peer Review notes |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name       |
|Note           |View Peer Review notes |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "Peer Review Note" from "My Worklist"
Then exam is displayed "7739"
When I mouse hover on "7739" Peer Review note indicator
Then note tooltip with "Test Peer Review Note" message is "not present"
And I click on exam "7739"
Then note is not present
When I click on "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name                |
|Note           |Administrator Peer Review notes |
|Note           |Create Peer Review notes        |
|Note           |View Peer Review notes          |
And click on save button
And I select "Radiologist Admin" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type| Permission Name                |
|Note           |Administrator Peer Review notes |
|Note           |Create Peer Review notes        |
|Note           |View Peer Review notes          |
And click on save button
And log out of the application
And log into the application as "admin" user
And I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "Peer Review Note" from "My Worklist"
Then exam is displayed "7739"
When I click on exam "7739"
And I open "Test Peer Review Note" to edit
And I delete Peer Review Note
Then note is deleted
When I collapse note section
And I right click on "Peer Review Note" from "My Worklist"
And I select "Delete" option from worklist menu
And I click on yes option to delete "Peer Review Note" worklist
Then "Peer Review Note" worklist is deleted from "My Worklist"
