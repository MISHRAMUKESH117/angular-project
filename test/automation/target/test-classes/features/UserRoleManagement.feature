@UserRoleManagement
Feature: User Role Management

@automated
@Management
@TC_MUI-001
Scenario:  Verify that new user role added and existing user role edited saved properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And create New Role with following details
|Role name |Role Description      |Permission Type   |Permission Name|
|Test User |Test User Description |Analytics Reports |Exam Assignment|
Then new user role "Test User" is added
When I select "Test User" user role
And I click on Edit user role icon
And I set new user role name "New Test User"
And click on save button
Then new user role "New Test User" is added
When I select "New Test User" user role
And I click on Edit user role icon
And I click in Delete user role icon
Then user role "New Test User" is deleted



@automated
@Management
@TC_MUI-002
Scenario:  Verify that users added and removed appropriately.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
When I click on number of Users hyperlink of user role "Demo Users"
And set user name "Gunn, Martin" to search
And I click on search button to search the users
And I select the user "Gunn, Martin"
And click on Save button on Users dialouge box
Then I click on number of Users hyperlink of user role "Demo Users"
And set user name "Gunn, Martin" to search
And I click on search button to search the users
And user name "Gunn, Martin" is added
When I deselect the user "Gunn, Martin"
And click on Save button on Users dialouge box
Then I click on number of Users hyperlink of user role "Demo Users"
And set user name "Gunn, Martin" to search
And I click on search button to search the users
And user name "Gunn, Martin" is removed








	