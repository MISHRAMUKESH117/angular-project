@PeerReviewManagement
Feature: Peer Review Management


@automated
@Management
@TC_PR-001
Scenario: Verify that following tables are under the Menu: - Note Configuration, Ongoing Assignment, Prompting, Peer Review Alert, eRad Peer, Focused Assignment
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
Then following tables are displayed under the Menu
|Note Configuration | 
|Ongoing Assignment |
|Focused Assignment | 
|Prompting 		    | 
|Peer Review Alert 	|
|eRad Peer          |



@automated
@Management
@TC_PR-002
Scenario: Verify that following table  has following parameters: Name, Description, Type (alert/Notification), Notification Interval (hours, but allow floats), Day of Week, Time of Day, Users/Groups, Threshold: Unread ExamCount/WU/RVU divided by rads logged in: X"
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Peer Review Alert" menu option in Peer Review Management page
Then following parameters are displayed under Peer Review Alert
|Name            		 		| 
|Description 			 		| 
|Type				 			| 
|Notification Interval 			| 
|Day of Week 					|
|Time of Day 					|
|User 				 			|
|Group 				 			|
|Unread Threshold Type 		 	|
|Threshold [above type / rad]	|



@automated
@Management
@TC_PR-006
Scenario: Verify Focused Assignment List of rules on the left and propertieson the right visible
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
Then Focused Assignment rules section is displayed
And following property names are displayed under Focused Assignment
|Name:            		 		| 
|Description: 			 		| 
|Active:				 		| 
|Time period: 					| 
|% exams to assign: 			|
|Period(s) to assign (d/w/m): 	|
|User: 				 			|
|Group: 						|
|Priority: 		 	 			|
|Modality:						|
|Subspecialty:					|
|Site Procedure: 				|
|Site Procedure Code: 			|
|Site:			 				|
|Location:	 					|
|Gender:	 					|
|Age >=:	 					|
|Age <=:	 					|
|Radiologist:	 				|
|Resident Radiologist:		 	|
|Attending Radiologist: 		|


@Regression
@automated
@Management
@TC_PR-010
Scenario: Verify Notes Configuration loads properly with the following sections Ratings, Reasons, Assignment Reasons, Categories, Classifications.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Note Configuration" menu option in Peer Review Management page
Then following sections are displayed under Note Configuration
|Ratings            | 
|Reasons 			| 
|Assignment Reasons | 
|Categories 		| 
|Classifications 	|


@Regression
@Management
@TC_PR-011
Scenario: Verify that new exam type defination created and existing one edited and saved.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Ongoing Assignment" menu option in Peer Review Management page
And add a new Exam Type Definition "Test Exam"
And I select Time Period as "Monthly"
And I click on Ongoing Assignment Save button
And I click on "Note Configuration" menu option in Peer Review Management page
And I click on "Ongoing Assignment" menu option in Peer Review Management page
And I select assignment properties Definition name  as "Test Exam"
Then Time Period is set to "Monthly"
When I select Time Period as "Weekly"
And I click on Ongoing Assignment Save button
And I click on "Note Configuration" menu option in Peer Review Management page
And I click on "Ongoing Assignment" menu option in Peer Review Management page
And I select assignment properties Definition name  as "Test Exam"
Then Time Period is set to "Weekly"
When I click on delete Assignment Property
Then assignment properties Definition name "Test Exam" is deleted


@Regression
@automated
@Management
@TC_PR-012
Scenario: Verify that new rule created and Existing rule edited ,saved.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Focused Assignment" menu option in Peer Review Management page
And create New Rule with following details
|Rule name |Rule Description      |Percentage Exams to Assign |
|Test Rule |Test Rule Description |30                         |
And I select Time Period as "Monthly"
And I select user "Wood, Chris"
And I click on focused assignment rule save button
Then new rule "Test Rule" is added
When I select "Test Rule" rule
And I click on Edit rule icon
And I set new rule name "New Test Rule"
And I click on focused assignment rule save button
Then new rule "New Test Rule" is added
When I select "New Test Rule" rule
And I click on Edit rule icon
And I click on Delete rule icon
Then rule "New Test Rule" is deleted



@automated
@Management
@TC_PR-013
Scenario: Verify that new user added and indicater added.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Prompting" menu option in Peer Review Management page
And I select "Tech, Citius" user for Prompting
And I click on save button
And I add an Indicator with following details
|Day     |From Time   |To Time  |
|Tuesday |2:00 AM     |4:00 AM  |
Then "Tech, Citius" is added to Prompting Summary
And an indicator is added for user "Tech, Citius" as "2:00AM-4:00AM"
When I delete an user "Tech, Citius" from Prompting Summary
Then user "Tech, Citius" is deleted from Prompting Summary


@automated
@Management
@TC_PR-014
Scenario: Verify peer review alert created, edited and saved properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "Peer Review Alert" menu option in Peer Review Management page
And I set Peer Review Alert name as "Test Peer Review Alert Name"
And I set Peer Review Alert description as "Test Peer Review Alert Description"
And I click on Peer Review Alert save button
And I click on "Focused Assignment" menu option in Peer Review Management page
And I click on "Peer Review Alert" menu option in Peer Review Management page
Then Peer Review Alert name is set to "Test Peer Review Alert Name"
And Peer Review Alert description is set to "Test Peer Review Alert Description"
When I set Peer Review Alert name as "New Test Peer Review Alert Name"
And I click on Peer Review Alert save button
And I click on "Focused Assignment" menu option in Peer Review Management page
And I click on "Peer Review Alert" menu option in Peer Review Management page
Then Peer Review Alert name is set to "New Test Peer Review Alert Name"


@Regression
@automated
@Management
@TC_PR-015
Scenario: Verify that page loaded properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Peer Review Management" module
And I click on "eRad Peer" menu option in Peer Review Management page
Then "Configuration of eRad Peer" page loads properly


	