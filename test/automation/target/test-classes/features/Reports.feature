@Reports
Feature: Reports

@automated
@TC_AT006
Scenario: Create Reports
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
Then Reports are displayed
When I edit the input parameters
| Parameter Name| Value     |
| From          | 01/01/2020|
| To            | 05/18/2020|
And I create the following report
| Peer Review Analysis |
Then reports are created successfully
| Peer Review Analysis |

@automated
@TC_AT007
Scenario: Verify Reports data
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
Then Reports are displayed
When I edit the input parameters
| Parameter Name | Value       |
| From           | 01/01/2020  |
| To             | 05/18/2020  |
And I create the following report
| Peer Review Analysis  |
Then reports are created successfully
| Peer Review Analysis  |
When I download the report
| Peer Review Analysis  | 
Then datasheet shows appropriate information
| Peer Review Analysis  |


@automated
@TC_AT008
Scenario: Edit input parameters and verify reports data
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I click the Reports tab 
And I select the following report
| Peer Review Analysis |
Then Reports are displayed
When I edit the input parameters
| Parameter Name | Value           |
| From           | 01/01/2020      |
| To             | 05/18/2020      |
| Reason         | Ongoing         |
| Location       | University      |
| Priority       | Stat            |
| Site           | Imaging Center 1|
And I create the following report
| Peer Review Analysis |
Then reports are created successfully
| Peer Review Analysis |
When I download the report
| Peer Review Analysis | 
Then datasheet shows appropriate edited information
| Column Name | Value            | FileName             |
| Site        | Imaging Center 1 | Peer Review Analysis |


