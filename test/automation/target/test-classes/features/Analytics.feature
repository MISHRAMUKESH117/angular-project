@Analytics
Feature: Analytics widgets

@automated
@TC_AT001
Scenario: Widgets are created
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets 
|Unread Exam            |
|Practice Productivity  |
|Exam Backlog		        |
|Auto-next Productivity |
Then widgets are displayed
|Unread Exam            |
|Practice Productivity  |
|Exam Backlog		        |
|Auto-next Productivity |


@automated
@TC_AT002
Scenario: Widgets are deleted 
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I "Delete" the following widgets
|Unread Exam            |
|Practice Productivity  |
|Exam Backlog		        |
|Auto-next Productivity |
Then widgets are deleted
|Unread Exam            |
|Practice Productivity  |
|Exam Backlog		        |
|Auto-next Productivity |

@automated
@TC_AT003
Scenario: Verify unread Exam data
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Unread Exam |
Then widgets are displayed
|Unread Exam |
When count "Unread Exam" from graph
And get all modalities
Then I "Delete" the following widgets
|Unread Exam |
When I open "Smart Worklist" application
Then worklist is launched
When I click Advanced Search
Then Advanced Search is launched
When I select Advanced Search module "Exam"
When I perform search for Analytics
| Label    | Value  | Abbreviations                           |
| Status   | Unread | NA                                      |
| Modality | CR     | CR - Computed Radiography               |
| Modality | CT     | CT - Computed Tomography                |
| Modality | US     | US - Ultrasound                         |
| Modality | XR     | XR - X-Ray                              |
| Modality | EC     | EC - Echocardiography                   |
| Modality | ES     | ES - Endoscopy                          |
| Modality | IR     | IR - Interventional Radiology           |
| Modality | MG     | MG - Mammography                        |
| Modality | MR     | MR - Magnetic Resonance                 |
| Modality | NM     | NM - Nuclear Medicine                   |
| Modality | OT     | OT - Other                              |
| Modality | XA     | XA - X-Ray Angiography                  |
| Modality | UN     | UN - Unknown                            |
| Modality | RF     | RF - Radio Fluoroscopy                  |
| Modality | PT     | PT - Positron emission tomography (PET) |
| Modality | DD     | DD - Duplex Doppler                     |
| Modality | DG     | DG - Diaphanography                     |
| Modality | DM     | DM - Digital microscopy                 |
| Modality | DX     | DX - Digital Radiography                |
When I click on "Search" action
Then I check the result

@automated
@TC_AT004
Scenario: Edit Widget input parameters
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Unread Exam |
When I edit widget "Unread Exam","Plot By","Site" and "Save"
Then changed parameter "Site" is displayed for "Unread Exam"
Then I "Delete" the following widgets
|Unread Exam |


@automated
@TC_AT005
Scenario: Widget export report
Given Clario application is launched
And log into the application as "admin" user
When I open "Analytics" application
Then Analytics is launched
When I create the following widgets
|Exam Backlog |
Then widgets are displayed
|Exam Backlog |
When I export report
Then I "Delete" the following widgets
|Exam Backlog	|
Then report "Exam Backlog" downloaded successfully


