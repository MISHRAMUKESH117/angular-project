@SmartWorklist
Feature: Worklist


@automated
@BVT
@SmartWorklist
@TC_WT-001
Scenario: worklist launched
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched

@automated
@BVT
@SmartWorklist
@TC_WT-021
Scenario: Verify - Quick Search
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I perform quick search 
| Label     | Value     |
| mrn       | 246930740 |
| firstName | LISA      |
| lastName  | FISCHER   |
| accession | 246930741 |
| site      | Zebra     |
Then exam is displayed "246930740"
When I click on exam "246930740"
Then exam is open "Fischer, Lisa"

@automated
@BVT
@SmartWorklist
@TC_WT-005
Scenario: Verify - Advanced search(Single Field Search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I click Advanced Search
Then Advanced Search is launched
When I select Advanced Search module "Exam"
When I perform Advanced Search with following details
| Label          | Value          |
| Accession      | 246930741      |
When I click on "Search" action
Then result count is displayed
When I clear results
Then result is cleared 
| Accession | 

@automated
@BVT
@SmartWorklist
@TC_WT-006
Scenario: Verify - Advanced search(Combination Search)
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I click Advanced Search
Then Advanced Search is launched
When I select Advanced Search module "Exam"
When I perform Advanced Search with following details
| Label          | Value          |
| Status         | Unread         |
| Billing Status | Incomplete     |
| Billing Status | Submitted      |
| Billing Status | Rejected       |
| Accession      | 246930741      |
| Emergency      | Include Active |
When I click on "Search" action
Then result count is displayed
When I clear results
Then result is cleared 
| Status         |
| Billing Status |
| Accession      | 
| Emergency      |



@SmartWorklist
@automated
@BVT
@TC_WT-013
Scenario: Verify Worklist - Patient view sections load
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
When I select the "Set Working Hours" radio button
And I click on Save button
Then worklist is loaded
When I close the Advanced Search if it is loaded
And I click on worklist "My Worklist"
And I perform worklist search "DEMERS, LARRY"
Then exam is displayed "12748"
And I click on exam "12748"
When I expand the "Patient" panel for "DEMERS, LARRY"
And I expand the "Ordering" panel for "Referring Physician"
Then I check the following details
|Demographic Identifier         |Value                         |
|DOB         			|11/16/1980                    |
|Gender      			|Male                          |
|MRN         			|12748                         |
|Age        			|39y                           |
|Accession   			|14415                         |
|Location    			|NEURO                         |
|Modality    			|MR - Magnetic Resonance       |
|Procedure   			|MR HEAD BLACKFORD ICOMETRIX-DM|
|Priority    			|Stat                          |
|Exam Reason 			|Reason for study              |
|History     			|Relevant clinical information |
|Ordering    			|Referring Physician           |
|Unread Time 			|04/15/2020  2:50 PM           |
And I check the "Note" in site panel is "Preference Notes"
And I check the following panel is loaded
|Notes|
|Exams|


@automated
@BVT
@SmartWorklist
@TC_WT-002
Scenario: Verify assignment of Shift, Reading Location and Reading Room on Smart Worklist launch.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then "Select Current Shift and Location" dialog is launched
When I select the ShiftType and Shift as "Radiologist" and "IV Body" respectively
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button
Then worklist is loaded
And I click on the Select Shift button in the Worklist navigation
When I select the "Set Working Hours" radio button
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button in the Shift selection from Worklist
Then worklist is loaded
And log out of the application
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
Then "admin" user is logged out


@automated
@BVT
@SmartWorklist
@TC_WT-003
Scenario: Verify - My Reading Queue creation
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
And log into the application as "admin" user
When I open "Smart Worklist" application
And worklist launches
Then "Select Current Shift and Location" dialog is launched
When I select the ShiftType and Shift as "Radiologist" and "IV Body" respectively
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button
Then worklist is loaded
Then following worklists are displayed under the Inteleviewer Worklists
|Unread My Subspecialty | 
|Exams in Transcription |
|Signing Queue          | 
|All Intelerad          | 
And I click on the Select Shift button in the Worklist navigation
When I select the "Set Working Hours" radio button
And I select the location as "Headquarters" and reading room as "Room A" respectively
And I click on Save button in the Shift selection from Worklist
Then worklist is loaded
And log out of the application
And log into the application as "admin" user
When I open "Management" application
And I open "User Role Management" module
And I select "Radiologist" user role
And I click on Edit user role icon
And I set the permission as follows
|Permission Type |Permission Name          |
|Preferences     |Required Reading Location|
And click on save button
And log out of the application
Then "admin" user is logged out

@automated
@SmartWorklist
@BVT
@TC_WT-004
Scenario: Verify - worklist count and load time
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click on worklist "All Intelerad"
Then I verify "All Intelerad" worklist tree and tab count
Then I verify worklist load time is less than "2" seconds

@automated
@SmartWorklist
@TC_WT-008
Scenario: Verify - count should enable after one refresh cycle of the tree
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I click Advanced Search
Then Advanced Search is launched
When I select Advanced Search module "Exam"
When I perform Advanced Search with following details
| Label          | Value          |
| Accession      | 246930741      |
When I click on "Search" action
Then exam is displayed "246930741"
When I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Count Enable/Disable" as worklist name
And I click on "InteleViewer Worklists" "Save" button to add new worklist
Then "Count Enable/Disable" worklist is added under "InteleViewer Worklists"
Then I verify the worklist "Count Enable/Disable" tree count is "disabled"
When I open "Management" application
And I open "Configuration" module
When I click on config name
| Worklist Tuning |
And I select the "Count Enable/Disable" worklist checkbox to enable count
When I open "Smart Worklist" application
Then worklist is launched
Then I verify the worklist "Count Enable/Disable" tree count is "enabled"

@automated
@SmartWorklist
@TC_WT-007
Scenario: create worklists - single,combine, combine worklist with exclusions
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
When I click Advanced Search 
Then Advanced Search is launched 
When I select Advanced Search module "Exam" 
When I perform Advanced Search with following details 
| Label          | Value                    |
| Accession      | 354697231                |
| Priority       | Stat                     |
| Modality       | CT - Computed Tomography |
When I click on "Search" action
Then exam is displayed "354697231"
When I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Demo Test Worklist 1" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue 
Then "Demo Test Worklist 1" worklist is added under "InteleViewer Worklists" 
When I select Advanced Search module "Exam" 
When I perform Advanced Search with following details 
| Label          | Value                     |
| Accession      | 10307                     |
| Status         | Unread                    |
| Priority       | Stat                      |
| Modality       | CR - Computed Radiography |
When I click on "Search" action
Then exam is displayed "10307"
When I click on Add to Worklist to create new worklist
And I select "InteleViewer Worklists" from worklist group to add new worklist
And I set "Demo Test Worklist 2" as worklist name
And I click on "Save" button of "InteleViewer Worklists" dialogue 
Then "Demo Test Worklist 2" worklist is added under "InteleViewer Worklists"
When I click "Demo Test Worklist 2" from "InteleViewer Worklists"
Then exam is displayed "10307" 
When I drag drop "Demo Test Worklist 1" on "Demo Test Worklist 2"
And I set "Combined Demo Worklists" as worklist name
And I click on "Combine" button of "Combine Worklist" dialogue
Then "Combined Demo Worklists" worklist is added under "InteleViewer Worklists"
Then I click "Combined Demo Worklists" from "InteleViewer Worklists"
Then exam is displayed "354697231"
And exam is displayed "10307"
When I expand "Combined Demo Worklists"
When I click "Demo Test Worklist 2" from "Combined Demo Worklists"
And I right click on "Demo Test Worklist 2" from "Combined Demo Worklists"
And I select "Exclude" option from worklist menu
When I click "Combined Demo Worklists" from "InteleViewer Worklists"
Then exam is displayed "354697231"
And exam is not displayed "10307"

@automated
@SmartWorklist
@TC_WT-009
Scenario: Sort all Columns
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
When I click following columns to sort them 
| Column Name  | Sort Order |
| MRN          | Ascending  |
| Priority     | Descending |
Then I verify columns sort order
| Column Name  | Sort Order |
| MRN          | ASC        |


@automated
@SmartWorklist
@TC_WT-010
Scenario: Verify - properties of worklist are changed
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I right click on "Unread Routine" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
Then I verify properties dialogue is open
When I change properties 
| Label                    | Value        |
| refreshRate              | 10           |
| sortOrderName_1          | AI Priority  |
| colorSchema              | No style     |
And I click on "Change" button from properties dialogue
When I right click on "Unread Routine" from "InteleViewer Worklists"
And I select "Properties" option from worklist menu
Then I verify properties are changed
| Label                    | Value        |
| refreshRate              | 10           |
| sortOrderName_1          | AI Priority  |
| colorSchema              | No style     |



@automated
@SmartWorklist
@TC_WT-012
Scenario: Verify - worklist export
Given Clario application is launched
And log into the application as "admin" user
When I open "Smart Worklist" application
Then worklist is launched
When I close the Advanced Search if it is loaded
And I click "All Intelerad" from "My Worklist"
When I click on export worklist
Then "All Intelerad" worklist is appropriately exported


