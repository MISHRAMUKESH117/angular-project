@ContactManagement
Feature: ContactManagement

@automated
@ContactManagement
@TC_CT-001
Scenario: Verify Search successful and displays appropriate results
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I enter "Doe" as "lastName"
And I enter "John" as "firstName"
And I click on "Search" to button to search contact
Then "JohnDoe" search results are displayed

@automated
@ContactManagement
@TC_CT-002
Scenario: Verify that Contact added
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I click on create new contact icon
And I create new contact with following details
|First Name |Last Name |Mobile Number |Type |Email                |
|Test       |Contact   |012-345-6789  |Cell |testcontact@gmail.com|
And I click on "Save" button to save contact
Then "Contact, Test" contact is created successfully

@automated
@ContactManagement
@TC_CT-003
Scenario: Verify that Contact Edited
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I open "Contact, Test" contact
And I click on "Edit Contact" icon to edit contact
And I edit following details
|Birth Date |Communication Preference |Groups |
|11/12/1985 |Phone                    |General|
And I click on "Save" button to save contact
Then "Contact, Test" contact is updated successfully

@automated
@ContactManagement
@TC_CT-004
Scenario: Verify that contact is merged
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I click on create new contact icon
And I create new contact with following details
|First Name |Last Name |Mobile Number |Type |Email                |
|Test       |Merge     |012-678-6789  |Cell |testmerge@gmail.com|
And I click on "Save" button to save contact
Then "Merge, Test" contact is created successfully
When I select "Contact, Test" and "Merge, Test" contacts to merge
And I click on "Merge" button to start merge
And I click on "Merge" button to merge contacts
Then "Merge, Test" contact is merged

@automated
@ContactManagement
@TC_CT-005
Scenario: Verify that contact is deleted
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I select "Merge, Test" contact to delete
And I click to "Delete" button to delete a contact
And I click on "Delete" confirmation button
Then "Merge, Test" contact is deleted

@automated
@ContactManagement
@TC_CT-006
Scenario: Verify that contact is exported
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I select "Hospital B" as site
And I click on "Export" button to export file
And I select "Contacts" option to export
Then "Contact - Hospital B" file is downloaded with "Contact" sheet

@automated
@ContactManagement
@TC_CT-007
Scenario: Verify that contact is imported
Given Clario application is launched
And log into the application as "admin" user
When I open "CRM" application
Then I am on "CRM" application
When I open "Contact Management" module
And I edit "Contact - Hospital B" site excel file
And I select "Hospital B" as site
And I click on "Import" button to import file
And I select "Contacts" option to import
And I select "Contact - Hospital B" file to import
Then file is imported with "User, Test"

