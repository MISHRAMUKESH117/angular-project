@ShiftManagement
Feature: ShiftManagement Page

@automated
@Scheduling
@TC_ST-001
Scenario: Create new shift
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
And I click on "Create New Shift" button
And create New Shift with following details
|Name       |Start Time |End Time   |Start Date|
|Test New Shift |10:30 AM   |7:30 PM    |5/5/2020  | 
And I click on "Create" button to create a new shift
Then new shift is created
When I click on shift
And I set "Test" as "description" 
And I click on "save" button to save shift
Then shift is edited 
When I click on row to assign user
And I assign "Tech, Citius" as user
And I click on done button to save
Then user is assigned
When I click on delete icon to delete user
And I click on done button to save
Then "Tech, Citius" user is deleted


@automated
@Scheduling
@TC_ST-002
Scenario: Add worklist to a shift
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
When I click on worklist
When I select "Exams in Transcription" from "InteleViewer Worklists" 
And I click on sort worklist checkox
And I click to save worklist
And I click on "save" button to save shift
And I click on "Smart Worklist" application
Then worklist is launched
And "Exams in Transcription" worklist is added under "InteleViewer Worklists" worklist 

@automated
@Scheduling
@TC_ST-004
Scenario: Delete a shift
Given Clario application is launched
And log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application
When I open "Shift Management" module
And I click on "Delete" button to delete shift
Then shift is deleted