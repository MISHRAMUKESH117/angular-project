@UserRoleManagement
Feature: User Role Management


@Management
@TC_MUI-003
Scenario:  Verify that new user added and existing user edited saved properly.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "User Management" module
And create New User with following details
|First name |Last Name      |Login Name   |Password |Verify Password|
|Test0410   |Test0410		|Test0410     |Test0410 |Test0410       |
And I select Local User Roles
|Active  |
And I click on save user button
And I search user "Test0410, Test0410"
Then new user "Test0410, Test0410" is added
When I select "Test0410, Test0410" user 
And I click on Edit user icon
And I set user last name to "NewTest0410"
And I click on save user button
And I search user "NewTest0410, Test0410"
Then new user "NewTest0410, Test0410" is added



	