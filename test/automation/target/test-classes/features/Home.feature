@HomePage
Feature: HomePage feature


@HomePage
@TC_BVT7
Scenario: Verify user is on Schedule application
Given Clario application is launched
And  log into the application as "admin" user
When I open "Scheduling" application
Then I am on "Schedule" application


@HomePage
@TC_BVT7
Scenario: Verify user is on Smart Reporting application
Given Clario application is launched
And  log into the application as "admin" user
When I open "Smart Reporting" application
Then I am on "Smart Reporting" application
