@AssignRuleManagement
Feature: Assign Rule Management

@automated
@Management
@TC_MUI-005
Scenario:  Verify that assignment rule is added, existing assignment rule is edited and rule is deleted.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Assign Rule Management" module
And create new Assignment rule with following details
|Assignment Rule name |Trigger dropdown  |Trigger value         |First sort Order dropdown |First sort Order Value |User dropdown |User name           |
|Test Assign Rule     |Trigger			 |Exam status is Prelim |First sort order          |Time Remaining         |User          |Test0410, Test0410  |
Then assignment rule "Test Assign Rule" is added
When I select "Test Assign Rule" assignment rule
And I click on Edit assignment rule icon
And I set assignment rule name "New Test Assign Rule"
And click on save assignment rule button
Then assignment rule "New Test Assign Rule" is added
When I select "New Test Assign Rule" assignment rule
And I click on Edit assignment rule icon
And I click in Delete assignment rule icon
Then assignment rule "New Test Assign Rule" is deleted



@automated
@Management
@TC_MUI-009
Scenario:  Verify that all Procedure Management modules loads correctly without errors. Also verify that the imported template got added and the table is updated.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Procedure Management" module
And I click on "Procedure" tab name on the left panel in Procedure Management page
Then "Procedure" page under Procedure Management is loaded properly
When I click on "Site Procedure" tab name on the left panel in Procedure Management page
Then "Site Procedure" page under Procedure Management is loaded properly
When I click on "Configuration" tab name on the left panel in Procedure Management page
Then "Configuration" page under Procedure Management is loaded properly
When I click on "Template" tab name on the left panel in Procedure Management page
Then "Template" page under Procedure Management is loaded properly
When I click on "Procedure" tab name on the left panel in Procedure Management page
And I select "Export_ProcedureNames" excel to upload in Procedure Management module, Configuration of Procedure page
And I set Procedure name "Test Procedure Name" to search on Procedure Management module, Configuration of Procedure page
And I click on search procedure button on Procedure Management module, Configuration of Procedure page
Then "Test Procedure Name" procedure is "visible"
When I delete "Test Procedure Name" procedure from the grid
And I set Procedure name "Test Procedure Name" to search on Procedure Management module, Configuration of Procedure page
Then "Test Procedure Name" procedure is "not visible"


@automated
@Management
@TC_MUI-010
Scenario:  Verify that all configuration modules load correctly without errors.
Given Clario application is launched
And log into the application as "admin" user
When I open "Management" application
And I open "Configuration" module
And I click on "AI Navigator DICOM" menu option
Then "AI Navigator DICOM" page under Configuration is loaded properly
When I click on "Application Groups" menu option
Then "Application Groups" page under Configuration is loaded properly
When I click on "Body Parts" menu option
Then "Body Parts" page under Configuration is loaded properly
When I click on "Calendar Event Type" menu option
Then "Calendar Event Type" page under Configuration is loaded properly
When I click on "Clario Commands" menu option
Then "Clario Commands" page under Configuration is loaded properly
When I click on "Communication Type" menu option
Then "Communication Type" page under Configuration is loaded properly
When I click on "Credentialing" menu option
Then "Credentialing" page under Configuration is loaded properly
When I click on "Custom Reports" menu option
Then "Custom Reports" page under Configuration is loaded properly
When I click on "Database" menu option
Then "Database" page under Configuration is loaded properly
When I click on "Edit Field" menu option
Then "Edit Field" page under Configuration is loaded properly
When I click on "Images/Icons" menu option
Then "Images/Icons" page under Configuration is loaded properly
When I click on "Integration Credentials" menu option
Then "Integration Credentials" page under Configuration is loaded properly
When I click on "Integration Modality" menu option
Then "Integration Modality" page under Configuration is loaded properly
When I click on "Integration Priority" menu option
Then "Integration Priority" page under Configuration is loaded properly
When I click on "Integration Site Map" menu option
Then "Integration Site Map" page under Configuration is loaded properly
When I click on "LDAP Groups" menu option
Then "LDAP Groups" page under Configuration is loaded properly
When I click on "LDAP Server" menu option
Then "LDAP Server" page under Configuration is loaded properly
When I click on "Location" menu option
Then "Location" page under Configuration is loaded properly
When I click on "Mail Server" menu option
Then "Mail Server" page under Configuration is loaded properly
When I click on "My Reading Queue" menu option
Then "My Reading Queue" page under Configuration is loaded properly
When I click on "Note" menu option
Then "Note" page under Configuration is loaded properly
When I click on "Note Message" menu option
Then "Note Message" page under Configuration is loaded properly
When I click on "Portal Order Generation" menu option
Then "Portal Order Generation" page under Configuration is loaded properly
When I click on "Practice" menu option
Then "Practice" page under Configuration is loaded properly
When I click on "Reading Location" menu option
Then "Reading Location" page under Configuration is loaded properly
When I click on "RVU Task" menu option
Then "RVU Task" page under Configuration is loaded properly
When I click on "Reading Location" menu option
Then "Reading Location" page under Configuration is loaded properly
When I click on "Search Result" menu option
Then "Search Result" page under Configuration is loaded properly
When I click on "Security" menu option
Then "Security" page under Configuration is loaded properly
When I click on "Site" menu option
Then "Site" page under Configuration is loaded properly
When I click on "Site Report Template" menu option
Then "Site Report Template" page under Configuration is loaded properly
When I click on "Site System" menu option
Then "Site System" page under Configuration is loaded properly
When I click on "Status" menu option
Then "Status" page under Configuration is loaded properly
When I click on "Status" menu option
Then "Status" page under Configuration is loaded properly
When I click on "Status Change Icons" menu option
Then "Status Change Icons" page under Configuration is loaded properly
When I click on "Styling" menu option
Then "Styling" page under Configuration is loaded properly
When I click on "Tech QA Rating" menu option
Then "Tech QA Rating" page under Configuration is loaded properly
When I click on "WebSocket Certificate" menu option
Then "WebSocket Certificate" page under Configuration is loaded properly
When I click on "Worklist Groups" menu option
Then "Worklist Groups" page under Configuration is loaded properly
When I click on "Worklist Tuning" menu option
Then "Worklist Tuning" page under Configuration is loaded properly
When I click on "Workload Gauge" menu option
Then "Workload Gauge" page under Configuration is loaded properly










	