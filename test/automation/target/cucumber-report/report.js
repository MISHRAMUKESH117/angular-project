$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/features/Worklist.feature");
formatter.feature({
  "name": "Worklist",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@SmartWorklist"
    }
  ]
});
formatter.scenario({
  "name": "create worklists - single,combine, combine worklist with exclusions",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SmartWorklist"
    },
    {
      "name": "@automated"
    },
    {
      "name": "@SmartWorklist"
    },
    {
      "name": "@TC_WT-007"
    }
  ]
});
formatter.step({
  "name": "Clario application is launched",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.Launch.clario_application_is_launched()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "log into the application as \"admin\" user",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Login.loginAsUser(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open \"Smart Worklist\" application",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Home.i_open_application(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "worklist is launched",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Worklist.worklistIsLaunched()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I close the Advanced Search if it is loaded",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Worklist.isAdvancedSearchLoaded_Close()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click \"All Intelerad\" from \"My Worklist\"",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_click_from(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click Advanced Search",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Worklist.iClickAdvancedSearch()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Advanced Search is launched",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.AdvancedSearch.isAdvancedSearchLaunched()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select Advanced Search module \"Exam\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.AdvancedSearch.setAdvanceSearchType(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I perform Advanced Search with following details",
  "rows": [
    {},
    {},
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "steps.AdvancedSearch.iPerformAdvancedSearch(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element to be clickable: By.xpath: //div[text()\u003d\u0027Stat\u0027] (tried for 30 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027CITIUS-VM5\u0027, ip: \u002710.90.192.15\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_251\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 83.0.4103.116, chrome: {chromedriverVersion: 83.0.4103.39 (ccbf011cb2d2b..., userDataDir: C:\\Users\\C-SKUK~1\\AppData\\L...}, goog:chromeOptions: {debuggerAddress: localhost:59905}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 13e54a107efee031cafcfb0d0dcab7ad\r\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:95)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:272)\r\n\tat Actions.Web.waitForElementClickability(Web.java:119)\r\n\tat Actions.Web.selectDropDownElement(Web.java:350)\r\n\tat pages.AdvancedSearch.advanceSearchDropDownSelect(AdvancedSearch.java:64)\r\n\tat pages.AdvancedSearch.setAdvancedSearchValues(AdvancedSearch.java:55)\r\n\tat steps.AdvancedSearch.iPerformAdvancedSearch(AdvancedSearch.java:34)\r\n\tat ✽.I perform Advanced Search with following details(file:///C:/SVN%20Repository/Worklist/test/automation/src/test/java/features/Worklist.feature:275)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I click on \"Search\" action",
  "keyword": "When "
});
formatter.match({
  "location": "steps.AdvancedSearch.iClickOnAction(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "exam is displayed \"354697231\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Worklist.examisLoaded(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click on Add to Worklist to create new worklist",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Notes.i_click_on_to_add_note_in_worklist()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select \"InteleViewer Worklists\" from worklist group to add new worklist",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_select_to_add_note_in_worklist(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I set \"Demo Test Worklist 1\" as worklist name",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_set_as_worklist(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click on \"Save\" button of \"InteleViewer Worklists\" dialogue",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Worklist.addNewWorklist(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Demo Test Worklist 1\" worklist is added under \"InteleViewer Worklists\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Notes.worklist_is_added_in(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select Advanced Search module \"Exam\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.AdvancedSearch.setAdvanceSearchType(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I perform Advanced Search with following details",
  "rows": [
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "steps.AdvancedSearch.iPerformAdvancedSearch(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click on \"Search\" action",
  "keyword": "When "
});
formatter.match({
  "location": "steps.AdvancedSearch.iClickOnAction(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "exam is displayed \"10307\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Worklist.examisLoaded(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click on Add to Worklist to create new worklist",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Notes.i_click_on_to_add_note_in_worklist()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select \"InteleViewer Worklists\" from worklist group to add new worklist",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_select_to_add_note_in_worklist(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I set \"Demo Test Worklist 2\" as worklist name",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_set_as_worklist(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click on \"Save\" button of \"InteleViewer Worklists\" dialogue",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Worklist.addNewWorklist(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Demo Test Worklist 2\" worklist is added under \"InteleViewer Worklists\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Notes.worklist_is_added_in(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click \"Demo Test Worklist 2\" from \"InteleViewer Worklists\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Notes.i_click_from(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "exam is displayed \"10307\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Worklist.examisLoaded(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I drag drop \"Demo Test Worklist 1\" on \"Demo Test Worklist 2\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Worklist.worklistDragDrop(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I set \"Combined Demo Worklists\" as worklist name",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_set_as_worklist(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click on \"Combine\" button of \"Combine Worklist\" dialogue",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Worklist.addNewWorklist(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Combined Demo Worklists\" worklist is added under \"InteleViewer Worklists\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Notes.worklist_is_added_in(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click \"Combined Demo Worklists\" from \"InteleViewer Worklists\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Notes.i_click_from(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "exam is displayed \"354697231\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Worklist.examisLoaded(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "exam is displayed \"10307\"",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Worklist.examisLoaded(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I expand \"Combined Demo Worklists\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Worklist.expandWorklist(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click \"Demo Test Worklist 2\" from \"Combined Demo Worklists\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Notes.i_click_from(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I right click on \"Demo Test Worklist 2\" from \"Combined Demo Worklists\"",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_right_click_on_from(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select \"Exclude\" option from worklist menu",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Notes.i_select_option_to_delete_worklist(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click \"Combined Demo Worklists\" from \"InteleViewer Worklists\"",
  "keyword": "When "
});
formatter.match({
  "location": "steps.Notes.i_click_from(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "exam is displayed \"354697231\"",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.Worklist.examisLoaded(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "exam is not displayed \"10307\"",
  "keyword": "And "
});
formatter.match({
  "location": "steps.Worklist.examisNotLoaded(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png", null);
formatter.after({
  "status": "passed"
});
});